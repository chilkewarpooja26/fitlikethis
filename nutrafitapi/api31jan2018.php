<?php
error_reporting(0);
require_once("Rest.inc.php");
require_once("../wp-config.php");
require_once('../wp-includes/pluggable.php');
require_once('../wp-includes/media.php');
class API extends REST {
public $data = "";
const DB_SERVER = "localhost";
const DB_USER = "etpl2012_nutrafi";
const DB_PASSWORD = "nutrafit123";
const DB = "etpl2012_nutrafitdivi";
const HOST = "http://exceptionaire.co/nutrafitDivi/";
const FROM_EMAIL ='kiran.k@exceptionaire.co';
const BACKGROUND = "background_img";
const SK_FILES = "sk_files";
private $db = NULL;
public function __construct(){
parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
}
		
/*
*  Database connection 
*/
private function dbConnect(){
	$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
	if($this->db)
	mysql_select_db(self::DB,$this->db);
	mysql_query("set names utf8");	 
}
		
/*
* Public method for access api.
* This method dynmically call the method based on the query string
*
*/
public function processApi(){
	$func = strtolower(trim(str_replace("api/","",$_REQUEST['rquest'])));
	if((int)method_exists($this,$func) > 0)
	$this->$func();
	else
	$this->response('',404);				
}
		
/* 
********Login API***********
*  Login must be POST method
*  email : <USER EMAIL>
*  pwd : <USER PASSWORD>
*/
		
private function login()
{
	if($this->get_request_method() != "POST"){
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}
        
	if(isset($this->_request['user_email']))
		$email = $this->_request['user_email'];		
	if(isset($this->_request['user_pass']))	
		$password = $this->_request['user_pass'];
        if(isset($this->_request['device_token']))	
		$device_token = $this->_request['device_token'];
	if( empty($email) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = 'Please enter your email';
		$this->response($this->json($error), 404);
	}
			
	if( empty($password) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = 'Please enter your password';
		$this->response($this->json($error), 404);
	}

	if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

	$login_success = "";
	$login_pass = "";

	if('es' == $applang){
		$login_success = "Has iniciado sesión correctamente.";
		$login_pass = "La dirección de correo electrónico o la contraseña son incorrectas.";
	}
	else{
		$login_success = "You have successfully logged in.";
		$login_pass = "Email address or password is incorrect.";
	}

	if(!empty($email) and !empty($password)){
		if(filter_var($email, FILTER_VALIDATE_EMAIL)){
		$sql = mysql_query("SELECT nu_users.ID, nu_users.user_email,nu_users.user_pass, nu_users.display_name,nu_usermeta.meta_value AS user_type
	    FROM nu_users, nu_usermeta
	    WHERE nu_users.user_email = '".$email."'
	    AND nu_users.ID = nu_usermeta.user_id
	    AND nu_usermeta.meta_key = 'user_type'", $this->db);

		if(mysql_num_rows($sql) > 0){
			$result['Status'] = 'Success';
			$result['msg'] = $login_success;
			while($row = mysql_fetch_array($sql,MYSQL_ASSOC))
			{
				$payment_status = (get_user_meta($row["ID"],"payment_status", true));
				if($payment_status=='1'){$payment_status='1';}else{$payment_status='0';}
                  
                  	 if(wp_check_password( trim( $password ), $row["user_pass"]))
                  	 {
                                       $age = (get_user_meta($row["ID"],"age", true));
                  	               $weight = (get_user_meta($row["ID"],"weight", true));
				       $device_token_present = (get_user_meta($row["ID"],"device_token", true));
                  	  		if($device_token_present == "")
                  	  		{
                  	  			$device_token = $device_token;
                  	  			update_user_meta( $row["ID"], 'device_token', $device_token);
                  	  	
                  	  		}
                  	  		$device_token = (get_user_meta($row["ID"],"device_token", true));
                                       global $wpdb;
                  	  $calories_query = "SELECT calories FROM nu_payments WHERE user_id = ".$row["ID"]."  order by payment_id";
	                  $calories_result = $wpdb->get_row($calories_query);
	                  $calories=$calories_result->calories; 
					 $result["Login"] = $row;
					 $result["device_token"] = $device_token;
                                         $result["age"] = $age;
					 $result["weight"] = $weight; 
                                         $result["payment_status"] = $payment_status;
                                         $result["calories"] = $calories;
                                         $query_device_token = "UPDATE nu_usermeta SET  
                                         meta_value = '".$device_token."' WHERE user_id ='".$row["ID"]."' and meta_key='device_token'";
		                         $result_query_device_token = mysql_query($query_device_token);
                                         $this->response($this->json($result), 200);   
				     }
				     else
				     {
					  $result['Status'] = 'Failed';
					  $result['msg'] = $login_pass;
				     }
                  

				
			}				
		}
		
                           
			//$this->response($this->json($result), 200);
		}
	}
	$error = array('Status' => "Failed", "msg" => $login_pass);
	$this->response($this->json($error), 400);
} 
		
/* 
********Register API***********
*/
private function register()	
{ 
	if($this->get_request_method() != "POST"){		
		$error['Status'] = 'Failed';	
		$error['msg'] = 'Not Acceptable';
		$this->response($this->json($error), 400);
	}
	$name	 				= $this->_request['name'];
	$email 					= $this->_request['email'];
	$password 				= $this->_request['password'];
	$gender 				= $this->_request['gender'];
        $allergies 				= $this->_request['allergies'];
	$weight 				= $this->_request['weight'];
	$age 				        = $this->_request['age'];
	$goal 				        = $this->_request['goal'];
        $descriptiona_about_me                  = $this->_request['description-about-me'];
	$user_type 				= $this->_request['user_type'];
        $device_token 		                = $this->_request['device_token'];
	$date 					= date("Y-m-d H:i:s");
        $user_pass = wp_hash_password( $password );
        $allergiesone 		    = $this->_request['allergiesone'];
        $myString = '"'.$allergiesone.'"';
        $strex = explode(",",$myString);
        $allergiesone_data = serialize($strex);  

    if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$regis_success = '';
	$regis_faild = '';
	$emailexists = '';

	if('es' == $applang){
		$regis_success = "Has sido registrado con éxito.";
		$regis_faild = "Registro fallido. Por favor, inténtelo de nuevo.";
		$emailexists = "¡Lo siento! el Email ya existe.";
		$input_name = "Por favor, escriba su nombre.";
		$input_email = "Por favor introduzca su correo electrónico.";
		$input_pass = "Por favor, introduzca su contraseña.";
	}
	else{
		$regis_success = "You have been registered successfully.";
		$regis_faild = "Registration failed. Please try again.";
		$emailexists = "Sorry! email already exists.";
		$input_name = "Please enter your name.";
		$input_email = "Please enter your email.";
		$input_pass = "Please enter your password.";
		}

		
	if( empty($name) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_name;
		$this->response($this->json($error), 404);
	}
	if( empty($email) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_email;
		$this->response($this->json($error), 404);
	}
	if( empty($password) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_pass;
		$this->response($this->json($error), 404);
	}
	// Input validations
	if(!empty($name) && !empty($email) && !empty($password))
	{ 
		$e_exists   	=  mysql_query(" SELECT user_email FROM `nu_users` WHERE `user_email` LIKE  '%$email%'") or die(mysql_error());
		$email_check 	=  mysql_num_rows($e_exists);
		if( $email_check > 0)
		{
			$error['Status'] = 'Failed';	
			$error['msg'] 	 = $emailexists;
			$this->response($this->json($error), 404);
		}
		$rawName = $name; 
        $nameChunks = explode(" ", $rawName);
        $fullname = $nameChunks[0];
		$query= "INSERT INTO nu_users (user_login,display_name, user_email, user_pass,user_registered)
		VALUES('$fullname','$name', '$email','".$user_pass."','$date' )"; 
		$abc	= mysql_query($query) or die(mysql_error());  
		$result['user_id'] = mysql_insert_id();

		// Checked user type and register according to it START ABL

		if("Nutritionist" == $user_type){
			
		$nutri_user_type = "Nutritionist";
		$account_status = "awaiting_admin_review";
		$nu_capabilities = 'a:1:{s:10:"subscriber";b:1;}';
		$role = "nutritionist";
		$nu_user_level = 0;

		$data = Array ('first_name'=>$name,'gender' => $gender,'age' => $age,'user_type' => $nutri_user_type,'description-about-me'=>$descriptiona_about_me,'device_token'=>$device_token,'account_status'=>$account_status,'nu_capabilities'=>$nu_capabilities,'role'=>$role,'nu_user_level'=>$nu_user_level);
		$items = array();
		foreach($data as $key => $val){
		  $items[]="('".$result['user_id']."','$key', '$val')";
		}

	       $sql = "insert into nu_usermeta (`user_id`,`meta_key`,`meta_value`) values".implode(',',$items);
		$mapping	= mysql_query($sql) or die(mysql_error());



		}
		else{

			$nu_capabilities = 'a:1:{s:10:"subscriber";b:1;}';
			$account_status = "approved";
			$role = "member";

		$data = Array ('first_name'=>$name,'gender' => $gender,'allergies'=>$allergies, 'allergiesone'=>$allergiesone_data,'weight' => $weight, 'age' => $age,'goal' => $goal,'user_type' => $user_type,'description-about-me'=>$descriptiona_about_me,'device_token'=>$device_token,'nu_capabilities'=>$nu_capabilities,'account_status'=>$account_status,'role'=>$role);
		$items = array();
		foreach($data as $key => $val){
		  $items[]="('".$result['user_id']."','$key', '$val')";
		}

	       $sql = "insert into nu_usermeta (`user_id`,`meta_key`,`meta_value`) values".implode(',',$items);
		$mapping	= mysql_query($sql) or die(mysql_error());
		}

		// Checked user type and register according to it END ABL

		$result['user_type'] = $user_type;

		if($abc)
		{
			$result['Status'] = 'Success';	
			$result['msg'] = $regis_success;	
			// If success everythig is good send header as "OK" and user details
			$this->response($this->json($result),200);
		}else
		{
			$result['Status'] = 'Unsuccess';	
			$result['msg'] = $regis_faild;	
			// If success everythig is good send header as "OK" and user details
			$this->response($this->json($result),400);
		}	
	} 
}



/* * Allergies List API * */

private function allergies_list()
	{ 
	   // Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$error['Status'] = 'Failed';	
				$error['msg'] = 'Not Acceptable';
				$this->response($this->json($error), 400);
			}			
		   
            $mArray =array(
             	            array("allergies" => "Milk"),
                            array("allergies" => "Eggs"),
                            array("allergies" => "Tree nuts"),
                            array("allergies" => "Soy"),
                            array("allergies" => "Wheat"),
                            array("allergies" => "Fish"),
                            array("allergies" => "Shellfish")
                          );
                    $result['Status'] = 'Success';
		    $result["allergies_list"] =$mArray ;
		    $this->response($this->json($result), 200);

	    
    }


/* 
********Allergies List API from .co***********
*  
*/

private function allergiesone_list(){

	//$result['Status'] = 'In, allergies_list function';
	global $wpdb;
	$allergies_list_query = "SELECT `meta_value` FROM `nu_postmeta` WHERE `meta_key`= '_um_custom_fields'";
	$allergies_list_query_result = $wpdb->get_results($allergies_list_query, 'ARRAY_A');
	$a = unserialize($allergies_list_query_result[0]['meta_value']);
	//$mArray = $a['allergies']['options'];  
    $id=0;
	foreach ($a['allergiesone']['options'] as $res) {
		$arrtmp[$id]=$arrayName = array('allergiesone' => $res);
		$id++;	
	}
	//print_r($arrtmp); die;    
   $result['Status'] = 'Success';
   $result["allergies_list"] = $arrtmp ;
   $this->response($this->json($result), 200);

}	

private function sendNotificationEmail( $name, $email, $password )
	{
		if($this->get_request_method() != "POST"){
			$error['Status'] 	= 'Failed';	
			$error['msg'] 		= 'Not Acceptable';
			$this->response($this->json($error), 400);
		}
	   $to 	     = $email;
	   $subject  = "User Passwod";
	   $message  = "Hello ".$name.", <br /><br />\r\n";
	   $message .= "Thank you for registering at Nutrafit.<br />\r\n";
	   $message .= "Your password is ".$password." thank you:<br /><br /><br />\r\n";
	   $headers  = 'MIME-Version: 1.0' . PHP_EOL; 
	   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
           //echo $message;exit;
	   // More headers
	   $headers .= "From:nutrafit@exceptionaire.com \r\n";
	   $retval 	 = mail($to,$subject,$message,$headers);
			   //echo $retval;

	   	if(isset($this->_request['applang']))
	   	$applang = $this->_request['applang'];
	   	$emailmsg_succ = "";
	   	$emailmsg_fail = "";

		if('es' == $applang){
		$emailmsg_succ = "Correo electrónico enviado con éxito ..";
		$emailmsg_fail = "El correo electrónico no pudo ser enviado..";
	}
	else{
		$emailmsg_succ = "Email successfully sent..";
		$emailmsg_fail = "Email could not be sent..";

	}

	   if( $retval == true )  
	   {
			$result['Status'] = 'Success';	
			$result['msg'] 	  = $emailmsg_succ;						
	   }
	   else
	   {
			$error['Status'] = 'Failed';	
			$error['msg'] 	 = $emailmsg_fail;
			$this->response($this->json($error), 509);
	   }
		
	}


private function fblogin()
	{ 
		if($this->get_request_method() != "POST")
			{
				$error['Status'] = 'Failed';	
				$error['msg'] = 'Not Acceptable';
				$this->response($this->json($error), 400);
			}
		$fid = $this->_request['fid'];	

		if(isset($this->_request['fb_profile_picture']))
		$fbpicture = $this->_request['fb_profile_picture'];

		$fbpicture = str_replace("#$$$#","&",$fbpicture);

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$facebook_id = "";
		$log_succ = "";
		$userexists = "";
		$input_name = "";
		$input_email ="";
		$alradyexists = "";
		$register_succ = "";
		$register_fail = "";

		if('es' == $applang){
		$facebook_id = "ID de facebook no encontrado.";
		$log_succ = "Has iniciado sesión correctamente.";
		$userexists = "¡Lo siento! El usuario no existe.";
		$input_name = "Por favor, escriba su nombre.";
		$input_email = "El ID de correo electrónico no existe.";
		$alradyexists = "¡Lo siento! ¡El usuario ya existe!";
		$register_succ = "Has sido registrado con éxito.";
		$register_fail = "Registro fallido. Por favor, inténtelo de nuevo.";
		
		}
		else{
			$facebook_id = "facebook id not found.";
			$log_succ = "You have logged in successfully.";
			$userexists = "Sorry! User does not exist.";
			$input_name = "Please enter your name.";
			$input_email = "Email id not exist.";
			$alradyexists = "Sorry! User already exists!";
			$register_succ = "You have been registered successfully.";
			$register_fail = "Registration failed. Please try again.";
			}

		if( empty($fid) )
			{
				$error['Status'] = 'Failed';	
				$error['msg'] = $facebook_id;
				$this->response($this->json($error), 404);
			}
		else
		{
			$credentials['fid'] = $fid;
	        $q="SELECT * FROM nu_social_users  WHERE identifier LIKE  '$fid'";        
	        $sql=mysql_query($q) or die(mysql_error());	
	                        	
	        if(mysql_num_rows($sql) > 0)
		        { 
				while($row = mysql_fetch_array($sql,MYSQL_ASSOC))
					{                               
			        $userid=$row['ID'] ;
					$q="SELECT * FROM nu_users  WHERE ID =".$row['ID'] ; 
					$sql=mysql_query($q) or die(mysql_error());
			        if(mysql_num_rows($sql) > 0)	
						{
                                           
                                                   $payment_status = (get_user_meta($userid,"payment_status", true));
                                                   $user_type = (get_user_meta($userid,"user_type", true));  
				                   if($payment_status=='1'){$payment_status='1';}else{$payment_status='0';}

						$result['Status'] = 'Success';	
						$result['msg'] = $log_succ;	
						$result['user_id']=$userid;
                                                $result['user_type']=$user_type;
                                                $result["payment_status"] = $payment_status;
                                         
                                                 while($email_row = mysql_fetch_array($sql,MYSQL_ASSOC))
                                                 {
                        	                   $email_exist=$email_row['user_email'];
                        	                   if($email_exist)
                        	                     {
                                                      $result['email_status']='1';
                        	                     }else
                        	                     {
                                                      $result['email_status']='0';
                        	                     }
                                                 } 					
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($result),200);
						}
			        else
						{
				        $error['Status'] = 'Failed';	
						$error['msg']= $userexists;
						$this->response($this->json($error), 517);
					    }
		        } 
	        }
	        else
		        {
		     //       $error['Status'] = 'Failed';	
				   // $error['msg'] = 'Sorry! User does not exist!!';
				   // $this->response($this->json($error), 517);


		        	  function rand_string( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars),0,$length);
        }
		$name	 				= $this->_request['name'];
		$email 					= $this->_request['email'];
		$fid 					= $this->_request['fid'];
		$password               = rand_string(8);  
		$fb_pass = wp_hash_password($password);
                $user_type 				= $this->_request['user_type']; 
                 $device_token 		    = $this->_request['device_token'];          
		$date 					= date("Y-m-d H:i:s");

		if( empty($name) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_name;
			$this->response($this->json($error), 404);
		}
		
				
		if( empty($email) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_email;
			$this->response($this->json($error), 404);
		}
		

		  if(!empty($name) && !empty($email) && !empty($fid ))
		{ 
		$e_exists=mysql_query("SELECT * FROM nu_users WHERE user_email ='".$email."'") or die(mysql_error());
		$email_check 	=  mysql_num_rows($e_exists);

        $e_exists=mysql_query("SELECT * FROM nu_social_users WHERE identifier =  '".$fid."'") or die(mysql_error());
				$email_check1 	=  mysql_num_rows($e_exists);

               if($email_check > 0)
				{
					$error['Status'] = 'Failed';	
					$error['msg'] 	 = $alradyexists;
					$this->response($this->json($error), 404);
				}
	
				if( $email_check1 > 0)
				{
					$error['Status'] = 'Failed';	
					$error['msg'] 	 = $alradyexists;
					$this->response($this->json($error), 404);
				}
	    $rawName = $name; 
        $nameChunks = explode(" ", $rawName);
        $fullname = $nameChunks[0];					
		$query="INSERT INTO nu_users (user_login,display_name, user_email, user_pass,user_registered)
		VALUES('$fullname','$name', '$email','".$fb_pass."','$date' )"; 

        $abc=mysql_query($query) or die(mysql_error());  
		$result['user_id'] = mysql_insert_id();
                $result['payment_status'] = '0';

        $data = Array ('first_name'=>$name,'gender' => $gender,'allergies'=>$allergies, 'allergiesone'=>$allergiesone,'weight' => $weight, 'age' => $age,'goal' => $goal,'user_type' =>$user_type,'description-about-me'=>$descriptiona_about_me,'device_token'=>$device_token,'fb_profile_picture'=>$fbpicture);
		$items = array();
		foreach($data as $key => $val){
		  $items[]="('".$result['user_id']."','$key', '$val')";
		}
		$u_map= "insert into nu_usermeta (`user_id`,`meta_key`,`meta_value`) values".implode(',',$items);
		$mapping= mysql_query($u_map) or die(mysql_error());
	
        $u_map = "INSERT INTO nu_social_users (ID, type ,identifier) 
                 VALUES('".$result['user_id']."', 'fb' , '".$fid."')";

	    $mapping= mysql_query($u_map) or die(mysql_error());
	
	if($abc)
    {
        //self::sendNotificationEmail( $name, $email, $password );
		$result['Status'] = 'Success';	
		$result['msg'] = $register_succ;	
		$result['user_type'] = $user_type;									
		// If success everythig is good send header as "OK" and user details
		$this->response($this->json($result),200);
        }
        else
		{
		$result['Status'] = 'Unsuccess';	
		$result['msg'] = $register_fail;	
		// If success everythig is good send header as "OK" and user details
		$this->response($this->json($result),400);
        }		
		} 

		       }
	    }
    }



/*forgot password API
		 * 
		 */
		 
private function forgotPassword()
{
	if($this->get_request_method() != "POST")
	{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_email = "";
	$pass_mail = "";
	$invalidemail = "";

	if('es' == $applang){
		$input_email = "Por favor introduzca su correo electrónico.";
		$pass_mail ="Se ha enviado una nueva contraseña a tu dirección de correo electrónico. Consulta tu correo electrónico.";
		$invalidemail = "Dirección de correo electrónico no válida.";
	}
	else{
		$input_email = "Please enter your email.";
		$pass_mail = "A new password has been sent to your e-mail address.Please check your email.";
		$invalidemail = "Invalid Email address.";
	}

	if(isset($this->_request['email']))
	$email = $this->_request['email'];	
	if( empty($email) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_email;
		$this->response($this->json($error), 522);
	}
	// Input validations
	if(!empty($email))
	{
		if(filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			
			$sql = mysql_query("SELECT ID,user_email,display_name FROM nu_users WHERE user_email = '$email' LIMIT 1", $this->db);
			
			if(mysql_num_rows($sql) > 0){
			
			while($row = mysql_fetch_array($sql,MYSQL_ASSOC))
			{

				 function rand_string( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars),0,$length);
        }

        $password               = rand_string(8);  
		$fb_pass = wp_hash_password($password);

             
		//echo $password;
				mysql_query("update nu_users set user_pass='".$fb_pass."' where user_email='".$email."'");
				$uname = $row['display_name'];

			
        self::sendNotificationEmail1( $name, $email, $password );
		$result['Status'] = 'Success';	
		$result['msg'] = $pass_mail;	
									
		$this->response($this->json($result),200);

			}
			$this->response($this->json($result), 200);
			}
			else{
			$error = array('Status' => "Failed", "msg" => $invalidemail);
	$this->response($this->json($error), 400);
			}
		}
	}
	// If invalid inputs "Bad Request" status message and reason
	$error = array('Status' => "Failed", "msg" => $invalidemail);
	$this->response($this->json($error), 400);
} 
	
		
private function sendNotificationEmail1($name,$email, $password )
	{
		if($this->get_request_method() != "POST"){
			$error['Status'] 	= 'Failed';	
			$error['msg'] 		= 'Not Acceptable';
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$email_succ = "";
		$email_fail = "";

		if('es' == $applang){
			$email_succ = "Correo electrónico enviado con éxito ..";
			$email_fail = "El correo electrónico no pudo ser enviado..";
		}
		else{
			$email_succ = "Email successfully sent..";
			$email_fail = "Email could not be sent..";

		}

	   $to 	     = $email;
	   $subject  = "Your New Passwod";
	   $message  = "Hello ".$name.", <br /><br />\r\n";
	   $message .= "You have change your password.<br />\r\n";
	   $message .= "Your new password is ".$password."<br /><br /> thank you.<br /><br /><br />\r\n";
	   $headers  = 'MIME-Version: 1.0' . PHP_EOL; 
	   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//echo $message;exit;
	  // More headers
	   $headers .= "From:nutrafit@exceptionaire.com \r\n";
	   $retval 	 = mail($to,$subject,$message,$headers);
			   //echo $retval;
	   if( $retval == true )  
	   {
			$result['Status'] = 'Success';	
			$result['msg'] 	  = $email_succ;						
	   }
	   else
	   {
			$error['Status'] = 'Failed';	
			$error['msg'] 	 = $email_fail;
			$this->response($this->json($error), 509);
	   }
		
	}



/*profile API
		 * 
		 */
private function profile()
{
	if($this->get_request_method() != "POST"){
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}
	// if(isset($this->_request['user_email']))
	// 	$email = $this->_request['user_email'];	

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_userid = "";
	$ivalid_userid = "";
	if('es' == $applang){
			$input_userid = "Por favor, introduzca el ID de usuario.";
			$ivalid_userid = "ID de usuario invalido.";
		}
		else{
			$input_userid = "Please enter user id.";
			$ivalid_userid = "Invalid User Id.";
		}

	if(isset($this->_request['user_id']))	
		$user_id = $this->_request['user_id'];

	if( empty($user_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
	}
			
	// if( empty($email) )
	// {
	// 	$error['Status'] = 'Failed';	
	// 	$error['msg'] = 'Please enter your email id';
	// 	$this->response($this->json($error), 404);
	// }

	if(!empty($user_id))
         {
               global $wpdb;
               $profile_query = "select * from nu_users as u where u.ID='".$user_id."'"; 
               $profile_result = $wpdb->get_row($profile_query);
	       //$image_attributes = wp_get_attachment_image_src(get_user_meta($user_id,"profile_image", true),"thumbnail");
               //$result["profile_image"] =$image_attributes[0];
		$image_attributeskk = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
                if($image_attributeskk)
     	        {
                $image_attributes = $image_attributeskk;
     	        }else
     	        {
                $image_attributes="";
        	}
                  	
		$result['Status'] = 'Success';
		$result["name"] = $profile_result->display_name;
		$result["email"] = $profile_result->user_email;
		$result["gender"] = get_user_meta($user_id,"gender", true);
                //$result["allergies"] = get_user_meta($user_id,"allergies", true); 
		$result["weight"] = get_user_meta($user_id,"weight", true);
		$result["age"] = get_user_meta($user_id,"age", true);
		$result["goal"] = get_user_meta($user_id,"goal", true);
                $result["description-about-me"] = get_user_meta($user_id,"description-about-me", true);
                $result["profile_image"] =$image_attributes;
                $allergiesone = get_user_meta($user_id,"allergiesone", true);
                if($allergiesone!=''){
                $resultstr = array();
                foreach ($allergiesone as $result_aller) { $resultstr[] = $result_aller;}
                $allergie = implode(",",$resultstr);
                $allergie_user=str_replace('"', '',  $allergie);
                 //echo $allergie;exit;
                $result["allergiesone"] =$allergie_user;
                }
                  $plan_query = "select post_id,sub_plan_duration,payment_date from nu_payments where user_id='".$user_id."' order by payment_id ASC"; 
               $paln_result = $wpdb->get_row($plan_query);
               $plan_id= $paln_result->post_id;

               /* get paln expire date*/
               $spd = $paln_result->sub_plan_duration;
               $spdvar = $spd; 
               if($spdvar!='')
               {
               $spdChunks = explode(" ", $spdvar);
               $spdvar= $spdChunks[0]; 
               $delidate = $paln_result->payment_date; 
               $monthlyDate = strtotime("+$spdvar week".$delidate);
               $monthly = date("Y/m/d", $monthlyDate);
               $ExpireDate = $monthly;
               }
	           $current_date = date("Y/m/d");
	             
	          $custom_args = array('post_type' => 'plan');
	          $custom_query = new WP_Query( $custom_args ); 
	          if ($custom_query->have_posts())
	          {
		      while ($custom_query->have_posts()) 
		      {
			  $custom_query->the_post();
	          if(have_rows('sub_plan') ):
		      while ( have_rows('sub_plan') ) : the_row();            
			  $award = get_sub_field('award');            
			  $myplan_id=get_the_ID();
			  if($myplan_id ==$plan_id && $award!='') 
			  {
			  $result['Status'] = 'Success';	
			  if($ExpireDate<$current_date)
              {
              $result['award_image']=$award;				                     
              }else
              {
              echo "";
              }	
			  }
			  else 
			  { 
			  echo "";
			  }
		      endwhile;//sub plan
	          endif;//sub plan
		      }//while custom_query
		      $this->response($this->json($result), 200);
	          }//if custom_query

		
	
		$this->response($this->json($result), 200);
	}
	$error = array('Status' => "Failed", "msg" => $ivalid_userid);
	$this->response($this->json($error), 400);
}



/* * Update Profile API * */

private function updateprofile()
	{ 
	       // Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$error['Status'] = 'Failed';	
				$error['msg'] = 'Not Acceptable';
				$this->response($this->json($error), 400);
			}
		
			$user_id	 			= $this->_request['user_id'];
			$name	 				= $this->_request['name'];
			
			$phone 					= $this->_request['phone'];
			$weight 				= $this->_request['weight'];
			$age 				    = $this->_request['age'];
                        $allergies 				= $this->_request['allergies'];
                        $allergiesone 				= $this->_request['allergiesone'];
                        $description_about_me 				= $this->_request['description-about-me'];

			if(isset($this->_request['applang']))
			$applang = $this->_request['applang'];

			$present_user = "";
			$profileupdt = "";

			if('es' == $applang){
				$present_user = "¡Lo siento! Usuario no encontrado";
				$profileupdt = "Perfil actualizado con éxito.";
			}
			else{
				$present_user = "Sorry! User not found.";
				$profileupdt = "Profile updated successfully.";
			}

	       if ( empty ($user_id )) 
			{
				$error['Status'] = 'Failed';	
				$error['msg']	 = $present_user;
				$this->response($this->json($error), 517);
			}

		    $query  = " SELECT * FROM nu_users WHERE ID = ".$user_id." ";	  
			$sql   	= mysql_query($query) or die(mysql_error());
			$chk 	=  mysql_num_rows($sql);

	      if($chk >0)
	      {
		    if(!empty($_POST["name"]) )
		    {
		     $query = "UPDATE  nu_users SET display_name = '".$_POST['name']."' WHERE ID =".$user_id;
		    $sql   = mysql_query($query) or die(mysql_error());

		    $metaquery = "UPDATE  nu_usermeta SET meta_value = '".$_POST['name']."' WHERE user_id ='".$user_id."' and meta_key='first_name'";
		    $metasql = mysql_query($metaquery) or die(mysql_error());
		    }

		    if(!empty($_POST["address"]) )
		    {
		    $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['address']."' WHERE user_id ='".$user_id."' and meta_key='address'";
		    $sql   = mysql_query($query) or die(mysql_error());
		    }

		    if(!empty($_POST["gender"]) )
		    {
		    $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['gender']."' WHERE user_id ='".$user_id."' and meta_key='gender'";
		    $sql   = mysql_query($query) or die(mysql_error());
		    }
                    if(!empty($_POST["allergies"]) )
		    {
		    $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['allergies']."' WHERE user_id ='".$user_id."' and meta_key='allergies'";
		    $sql   = mysql_query($query) or die(mysql_error());
		    }
                    if(!empty($_POST["allergiesone"]) )
            {
		      $allergiesone = $_POST["allergiesone"];
              $myString = $allergiesone;
              $strex = explode(",",$myString);
              $allergiesone_data = serialize($strex);

            $query = "UPDATE nu_usermeta SET meta_value = '".$allergiesone_data."' WHERE user_id ='".$user_id."' and meta_key='allergiesone'";
		    $sql   = mysql_query($query) or die(mysql_error());
		    }

		    if(!empty($_POST["weight"]) )
		    {
		    $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['weight']."' WHERE user_id ='".$user_id."' and meta_key='weight'";
		    $sql   = mysql_query($query) or die(mysql_error());
		    }

		    if(!empty($_POST["age"]) )
		    {
		    $query = "UPDATE nu_usermeta SET  meta_value = '".$_POST['age']."' WHERE user_id ='".$user_id."' and meta_key='age'";
		    $sql = mysql_query($query) or die(mysql_error());
		    }

		    if(!empty($_POST["goal"]) )
		    {
		    $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['goal']."' WHERE user_id ='".$user_id."' and meta_key='goal'";
		    $sql 	 = mysql_query($query) or die(mysql_error());
		    }
                    
                    if(!empty($_POST["description-about-me"]) )
		    {
		   $query = "UPDATE nu_usermeta SET meta_value = '".$_POST['description-about-me']."' WHERE user_id ='".$user_id."' and meta_key='description-about-me'";
		    $sql 	 = mysql_query($query) or die(mysql_error());
		    }

			$result['Status'] 	= 'Success';	
			$result['msg'] 		= $profileupdt;		
			// If success everythig is good send header as "OK" and user details
			$this->response($this->json($result),200);

	   }

	    else
	    {
	     $error['Status'] = 'Failed';	
		 $error['msg'] 	 = $present_user;
		 $this->response($this->json($error), 512);
	    }	
  }




/* 
********Upload Profile Pic API***********
*/

  private function uploadprofilepic()
	{ 
		// Cross validation if the request method is POST else it will return "Not Acceptable" status
		if($this->get_request_method() != "POST"){
			$error['Status'] = 'Failed';	
			$error['msg'] = 'Not Acceptable';
			$this->response($this->json($error), 400);
		}
		$user_id	 			= $this->_request['user_id'];
		$profile_image	 	    = $this->_request['profile_image'];

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$user_notfind = "";
		$img_updtsucc = "";

		if('es' == $applang){
				$user_notfind = "¡Lo siento! Usuario no encontrado.";
				$img_updtsucc = "La imagen del perfil se actualizó correctamente.";		
		}
		else{
				$user_notfind = "Sorry! User not found.";
				$img_updtsucc = "Profile image updated successfully.";	
		}
				
		if ( empty ($user_id )) 
		{
			$error['Status'] = 'Failed';	
			$error['msg']	 = $user_notfind;
			$this->response($this->json($error), 517);
		}
		$data = array();	
		extract($_POST);

	    $userMeta = get_user_meta($user_id);
	    $profile_image = $userMeta['profile_image'][0];
	    require_once( '../wp-admin/includes/image.php' );
	    require_once('../wp-admin/includes/file.php' );

	    $upload_overrides = array('test_form' => FALSE);
	    $movefile = wp_handle_upload($_FILES['profile_image'], $upload_overrides);
	    $attachment = array(
	            'guid' => $movefile['url'],
	            'post_mime_type' => $_FILES['profile_image']['type'],
	            'post_title' => preg_replace('/\.[^.]+$/', '', basename($movefile['url'])),
	            'post_content' => '',
	            'post_status' => 'inherit'
	        );
//print_r( $attachment);
	    $attachment_id = wp_insert_attachment($attachment, $movefile['file']);
	    $attach_data = wp_generate_attachment_metadata($attachment_id, $movefile['file']);
	    wp_update_attachment_metadata($attachment_id, $attach_data);
	    $data['profile_image'] = $attachment_id;
	    update_user_meta($user_id, 'profile_image',$attachment_id);
            //print_r($_FILES);exit;
            $error = array('Status' => "Success", "msg" => $img_updtsucc);
	    $this->response($this->json($error), 400);	
  }



/* 
********Delete Profile Pic API***********
*/

  private function deleteprofilepic()
	{ 
		// Cross validation if the request method is POST else it will return "Not Acceptable" status
		if($this->get_request_method() != "POST"){
			$error['Status'] = 'Failed';	
			$error['msg'] = 'Not Acceptable';
			$this->response($this->json($error), 400);
		}
		$user_id	 			= $this->_request['user_id'];
		$profile_image	 	    = $this->_request['profile_image'];

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$del_succ = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca el ID de usuario.";
			$del_succ = "Imagen de perfil eliminada correctamente.";
		}
		else{
			$input_userid = "Please enter user id.";
			$del_succ = "Profile image deleted successfully.";					
		}
				
		if ( empty ($user_id )) 
		{
			$error['Status'] = 'Failed';	
			$error['msg']	 = $input_userid;
			$this->response($this->json($error), 517);
		}
		$data = array();	
		extract($_POST);

	    $userMeta = get_user_meta($user_id);
	    //print_r($userMeta);exit;
    wp_delete_attachment($attachment_id, $force_delete);
    delete_user_meta($user_id, 'profile_image');
    $results['attachment_id'] = $attachment_id;
    $results['featured_image'] = '';
    $results['profile_image_gallery'] = 0;

            $error = array('Status' => "Success", "msg" => $del_succ);
	    $this->response($this->json($error), 400);	
  }

   	/* 
********Plan API***********
*/		
		
	private function plan()
     {
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$plannotfound = "";

	if('es' == $applang){
		$plannotfound = "Plan no encontrado.";
	}
	else{
		$plannotfound = "Plan not found.";				
	}

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$custom_args = array(
	'post_type' => 'plan', 
	'order' =>'ASC',
        'post_status' => 'publish',
	'paged' => $paged
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		$result['Status'] = 'Success';
		while ($custom_query->have_posts()) 
		{
		$custom_query->the_post(); 
		$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$featimage = $imageid['0'];
		$image_url = $featimage;
                $youtube_video_link = get_field( "youtube_video_link" );
                $ytarray=explode("/", $youtube_video_link);
                $ytendstring=end($ytarray);
                $ytendarray=explode("?v=", $ytendstring);
                $ytendstring=end($ytendarray);
                $ytendarray=explode("&", $ytendstring);
                $ytcode=$ytendarray[0];            
		// $result["plan_info"][]= get_the_title();
		// $result["plan_info"][]= $image_url;
		// $result["plan_info"][]= get_the_content();
		// $result["plan_info"][]= get_the_ID();     

		$result['plan_info'][]=array('plan_title'=>get_the_title(),
			                     'plan_image'=>$image_url,
			                     'plan_content'=>get_the_content(),
                                             'youtube_video_id'=>$ytcode, 
			                     'plan_id'=>get_the_ID());

		}//while
		$this->response($this->json($result), 200);
	}//if
	else{
	    $this->response('no records', 204);	// If no records "No Content" status
	} 
	// If invalid inputs "Bad Request" status message and reason
	$error = array('status' => "Failed", "msg" => $plannotfound);
	$this->response($this->json($error), 400);
    }
		
  

 	/* 
********PlanDetails API***********
*/

private function plandetails()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_planid = "";
	$invalid_planid = "";
	if('es' == $applang){
		$input_planid = "Ingrese el ID del plan.";
		$invalid_planid = "ID de plan no válido.";
	}
	else{
		$input_planid = "Please enter plan id.";
		$invalid_planid = "Invalid Plan id.";				
	}

if(isset($this->_request['plan_id']))
	$plan_id = $this->_request['plan_id'];	
	if( empty($plan_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_planid;
		$this->response($this->json($error), 522);
	}
	// Input validations
	if(!empty($plan_id))
	{
	$custom_args = array(
	'post_type' => 'plan'
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		//$result['Status'] = 'Success';
		while ($custom_query->have_posts()) 
		{
		$custom_query->the_post();
		$plan_banner_title = get_field( "plan_banner_title" ); 
		$image_url = get_field('plan_banner_image');
                $plan_id;
                $myplan_id=get_the_ID();
	        if ($myplan_id ==$plan_id) 
	        {
	        $result['Status'] = 'Success';
			$result['plan_details_info']=array('plan_banner_title'=>$plan_banner_title,
			                                     'plan_banner_image'=>$image_url,
			                                     'plan_content'=>get_the_content(),
			                                     'plan_id'=>get_the_ID());
		    }//if myplan_id 
		    
		}//while
		$this->response($this->json($result), 200);
	}//if custom_query
	else{
	    $this->response('no records', 204);	// If no records "No Content" status
	} 
}
   	// If invalid inputs "Bad Request" status message and reason
	$error = array('status' => "Failed", "msg" => $invalid_planid);
	$this->response($this->json($error), 400);
}		
	
		
 	/* 
********Sub Plan API***********
*/

private function subplan()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$check_planid = "";
	$check_subplaid ="";

	if('es' == $applang){
		$check_planid = "Ingrese el ID del plan.";
		$check_subplaid ="Invalid subplan id.";
	}
	else{
		$check_planid = "Please enter plan id.";
		$check_subplaid ="Invalid subplan id.";				
	}

if(isset($this->_request['plan_id']))
	$plan_id = $this->_request['plan_id'];	
	if( empty($plan_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $check_planid;
		$this->response($this->json($error), 522);
	}
	// Input validations
	if(!empty($plan_id))
	{
	$custom_args = array(
	'post_type' => 'plan'
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		while ($custom_query->have_posts()) 
		{
			$custom_query->the_post();
	        if(have_rows('sub_plan') ):
		        while ( have_rows('sub_plan') ) : the_row(); 
                                $plan_title=get_the_title();
			        $plan_banner_title = get_field( "plan_banner_title" );
			        $sub_plan_title = get_sub_field('sub_plan_title');
			        $sub_plan_duration = get_sub_field('sub_plan_duration');
			        $sub_plan_price = get_sub_field('sub_plan_price');
			        $sub_plan_speciality = get_sub_field('sub_plan_speciality');
                                $payment_type_msg = get_sub_field('payment_type'); 
                                if($payment_type_msg!='' && !empty($payment_type_msg))
                                {$payment_msg= " a month";} else{$payment_msg= " one time purchase";}
			        $myplan_id=get_the_ID();
			        if($myplan_id ==$plan_id) 
				    {
					 $result['Status'] = 'Success';
			                 $result['plan_title']=$plan_title;
			                 $result['plan_banner_title']=$plan_banner_title;     	
				         $result['sub_plan'][]=array('sub_plan_title'=>$sub_plan_title,
				       	                           'sub_plan_duration'=>$sub_plan_duration,
				       	                           'sub_plan_price'=>$sub_plan_price,
                                                                   'payment_type'=>$payment_msg,   
				       	                           'sub_plan_speciality'=>$sub_plan_speciality
							                      );
			        }
		        endwhile;//sub plan
	        endif;//sub plan
		}//while custom_query
		$this->response($this->json($result), 200);
	}//if custom_query
	else{
	    $this->response('no records', 204);	// If no records "No Content" status
	} 
}
   	// If invalid inputs "Bad Request" status message and reason
	$error = array('status' => "Failed", "msg" => $check_subplaid);
	$this->response($this->json($error), 400);
}


 	/* 
********Piechart API***********
*/

private function piechart()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_planid = "";
	$input_gender = "";
	$input_weight = "";
	$input_subplan = "";
	$nodata_found = "";
	$invalid_planid = "";
	
	if('es' == $applang){
		$input_planid = "Ingrese el ID del plan.";
		$input_gender = "Por favor seleccione el género.";
		$input_weight = "Por favor ingrese peso.";
		$input_subplan = "Por favor ingrese el título del sub-plan.";
		$nodata_found = "Datos no encontrados.";
		$invalid_planid = "ID de plan no válido.";
	}
	else{
		$input_planid = "Please enter plan id.";
		$input_gender = "Please select gender.";
		$input_weight = "Please enter weight.";
		$input_subplan = "Please enter sub plan title.";
		$nodata_found = "No data found.";
		$invalid_planid = "Invalid Plan id.";	
	}

    if(isset($this->_request['plan_id']))
	$plan_id = $this->_request['plan_id'];

	 if(isset($this->_request['gender']))
	 $gender = $this->_request['gender'];

	 if(isset($this->_request['weight']))
	 $weight = $this->_request['weight'];

	 if(isset($this->_request['sub_plan_title']))
	 $sub_plan_title = $this->_request['sub_plan_title'];	

	if( empty($plan_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_planid;
		$this->response($this->json($error), 522);
	}
	if( empty($gender) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_gender;
		$this->response($this->json($error), 522);
	}
	if( empty($weight) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_weight;
		$this->response($this->json($error), 522);
	}
	if( empty($sub_plan_title) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_subplan;
		$this->response($this->json($error), 522);
	}
    
        $query  = " SELECT ID,post_type FROM nu_posts WHERE ID = ".$plan_id." and post_type= 'plan'";	  
		$sql   	= mysql_query($query) or die(mysql_error());
		$chk 	=  mysql_num_rows($sql);
        if($chk ==0)
        {
         $error = array('status' => "Failed", "msg" => $nodata_found);
	     $this->response($this->json($error), 400);
        }

	// Input validations
	if( !empty($plan_id) and !empty($gender) and !empty($weight) )
	{
	$custom_args = array(
	'post_type' => 'plan'
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		while ($custom_query->have_posts()) 
		{
			$custom_query->the_post();
	        if(have_rows('sub_plan') ):
		        while ( have_rows('sub_plan') ) : the_row(); 
                     $myplan_id=get_the_ID();
			        if($myplan_id ==$plan_id && $gender=='Male') 
				    {
				    $my_sub_plan_title = get_sub_field('sub_plan_title');
                                    $sub_plan_duration = get_sub_field('sub_plan_duration');
				    if( have_rows('men') ):
                    while ( have_rows('men') ) : the_row();  
                     $men_formula_for_calories = get_sub_field('formula_for_calories');  
                     $men_formula_for_proteins = get_sub_field('formula_for_proteins'); 
                     $men_formula_for_carbs = get_sub_field('formula_for_carbs');
                     
                     $cal=$weight*$men_formula_for_calories;
                     $pro=$men_formula_for_proteins*$weight;
                     $carb=$men_formula_for_carbs*$weight;
                     $fat= ($cal-(($pro*4)+($carb*4)))/9;
                          
                    $proper=round(($pro*400)/$cal)."%";
                    $carbper=round(($carb*400)/$cal)."%"; 
                    $fatper=round(($fat*900)/$cal)."%";   

                     if($my_sub_plan_title==$sub_plan_title)
                     {
                     	//$men_formula_for_calories = get_sub_field('formula_for_calories');  
			          $result['Status'] = 'Success'; 
                                  $result['title']="Macronutrient Distribution for ".$my_sub_plan_title. " in " .$sub_plan_duration." -MEN";   
				  $result['Protein']=$proper;
                                  $result['Carb']=$carbper;
                                  $result['Fat']=$fatper;
                                  $result['plan_content']=get_the_content();
                        } 
				    endwhile;//men
                    endif;//men   
			        }else if($myplan_id ==$plan_id && $gender=='Female') 
				    {
				     $my_sub_plan_title = get_sub_field('sub_plan_title');
                                     $sub_plan_duration = get_sub_field('sub_plan_duration');
                     if( have_rows('women') ):
                     while ( have_rows('women') ) : the_row(); 
                     $women_formula_for_calories = get_sub_field('women_formula_for_calories');  
                     $women_formula_for_proteins = get_sub_field('women_formula_for_proteins'); 
                     $women_formula_for_carbs = get_sub_field('women_formula_for_carbs');
                     
                     $cal=$weight*$women_formula_for_calories;
                     $pro=$women_formula_for_proteins*$weight;
                     $carb=$women_formula_for_carbs*$weight;
                     $fat= ($cal-(($pro*4)+($carb*4)))/9;
                          
                    $proper=round(($pro*400)/$cal)."%";
                    $carbper=round(($carb*400)/$cal)."%"; 
                    $fatper=round(($fat*900)/$cal)."%";    

                     if($my_sub_plan_title==$sub_plan_title)
                     {
		               $result['Status'] = 'Success'; 
                               $result['title']="Macronutrient Distribution for ".$my_sub_plan_title. " in " .$sub_plan_duration." -WOMEN";    
			       $result['Protein']=$proper;
                               $result['Carb']=$carbper;
                               $result['Fat']=$fatper;
                               $result['plan_content']=get_the_content();
                     } 
				    endwhile;//women
                    endif;//women   
			        }
		        endwhile;//sub plan
	        endif;//sub plan
		}//while custom_query
		$this->response($this->json($result), 200);
	}//if custom_query
	else{
	    $this->response('no records', 204);	// If no records "No Content" status
	} 
}
   	// If invalid inputs "Bad Request" status message and reason
	$error = array('status' => "Failed", "msg" => $invalid_planid);
	$this->response($this->json($error), 400);
}		
		


private function changePassword()
{ 
		// Cross validation if the request method is POST else it will return "Not Acceptable" status
		if($this->get_request_method() != "POST")
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = 'Not Acceptable';
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$input_oldpwd = "";
		$input_newpwd = "";
		$input_confipwd = "";
		$notmatch_nncpwd = "";
		$updt_succ = "";
		$old_pwdnocorrt = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca el ID de usuario.";
			$input_oldpwd = "Por favor ingrese la contraseña antigua.";
			$input_newpwd = "Por favor, ingrese una nueva contraseña.";
			$input_confipwd = "Por favor, introduzca la contraseña de confirmación.";
			$notmatch_nncpwd = "La nueva contraseña y la contraseña de confirmación no coinciden.";
			$updt_succ = "Contraseña actualizada exitosamente.";
			$old_pwdnocorrt = "Antigua contraseña es incorrecta.";
		}
		else{
			$input_userid = "Please enter user id.";
			$input_oldpwd = "Please enter old password.";
			$input_newpwd = "Please enter new password.";
			$input_confipwd = "Please enter confirm password.";
			$notmatch_nncpwd = "New password and confirm password did not match.";
			$updt_succ = "Password updated successfully.";
			$old_pwdnocorrt = "Old password is incorrect.";
		}
			
		$user_id				= $this->_request['user_id'];
		$old_password			        = $this->_request['old_password'];
		$new_password 			        = $this->_request['new_password'];
		$confirm_password 		        = $this->_request['confirm_password'];
				
		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}
		if( empty($old_password) )
		{
			$error['Status'] = 'Failed';	
			$error['msg']	 = $input_oldpwd;
			$this->response($this->json($error), 517);
		}
		if( empty($new_password) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_newpwd;
			$this->response($this->json($error), 404);
		}
		if( empty($confirm_password) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_confipwd;
			$this->response($this->json($error), 404);
		}
				
		if(!empty($old_password))
		{
			//$oldpassword = wp_hash_password($old_password);
			$newpassword = wp_hash_password($new_password);
			//$repeatnewpassword = wp_hash_password($confirm_password);
			$sql = mysql_query("SELECT ID,user_pass FROM nu_users WHERE ID='".$user_id."'");
			if(@mysql_num_rows($sql) > 0)
			{
				while($row = @mysql_fetch_array($sql,@MYSQL_ASSOC))
				{
					//echo $old_password;
					//echo $row["user_pass"];exit;
					if(wp_check_password( trim( $old_password), $row["user_pass"]))
					{
						//echo "1";
						$result["Login"] = $row;
						if($new_password!=$confirm_password)
						{
							//echo "3";
						$error = array('Status' => "Failed", "msg" => $notmatch_nncpwd);
						$this->response($this->json($error), 400);
						}else
						{
							//echo "4";
							$query = "UPDATE nu_users SET user_pass = '".$newpassword."' WHERE  ID='".$user_id."'";
							$sql = @mysql_query($query) or die(mysql_error());
							$error = array('Status' => "Success", "msg" => $updt_succ);
							$this->response($this->json($error), 400);
						}		  
					}
					else
					{
						//echo "2";
						$error = array('Status' => "Failed", "msg" => $old_pwdnocorrt);
						$this->response($this->json($error), 400);
					}
				}	
			}
        }

   
}	


	/* 
********User List API***********
*/

private function user_list()
{
	if($this->get_request_method() != "POST"){
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_nutriid = "";
	$nofound_user = "";

	if('es' == $applang){
		$input_nutriid = "Por favor ingrese el ID del nutricionista.";
		$nofound_user = "Usuario no encontrado.";
	}
	else{
		$input_nutriid = "Please enter nutritionist id.";
		$nofound_user = "User Not Found.";					
	}

	$nutritionist_id= $this->_request['nutritionist_id'];
	if( empty($nutritionist_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_nutriid;
		$this->response($this->json($error), 404);
	}
	$sql = mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_email,nu_usermeta.meta_value 
	                        AS user_type
		                    FROM nu_users, nu_usermeta
		                    WHERE nu_users.ID = nu_usermeta.user_id
		                    AND nu_usermeta.meta_key = 'payment_status'
		                    AND nu_usermeta.meta_value = '1' 
		                    order by ID
		                 ");
	while($row = mysql_fetch_array($sql,MYSQL_ASSOC))
	{
		$user_id=$row["ID"];
		$image_attributeskk = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
		if($image_attributeskk)
		{
		$image_attributes = $image_attributeskk;
		}else
		{
		$image_attributes="";
		}
		global $wpdb;
		$my_query = "SELECT * FROM  nu_invite_user WHERE user_id = '".$user_id."' and nutritionist_id = '".$nutritionist_id."'";
		$my_result = $wpdb->get_row($my_query);
		if($my_result)
		{
		$invite_status='1';
		}else
		{
		$invite_status='0';
		}
		$result['User List'][]=array(
		                            'image'=>$image_attributes, 
			                        'user_id'=>$row["ID"],
			                        'name'=>$row["display_name"],
						            'user_email'=>$row["user_email"],
		                            'invite_status'=>$invite_status,
					                );
	}
	$result['Status'] 	= 'Success';
	$this->response($this->json($result),200);
	            
	$error = array('Status' => "Failed", "msg" => $nofound_user);
	$this->response($this->json($error), 400);
}



	/* 
********Invite User API***********
*/

private function invite_user()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_userid = "";
	$invitation_sent = "";
	$invitation_faild = "";

	if('es' == $applang){
		$input_userid = "Por favor, introduzca su ID de usuario.";
		$invitation_sent = "Invitación enviada correctamente.";
		$invitation_faild = "La invitación ha fallado.";
	}
	else{
		$input_userid = "Please enter your user id.";
		$invitation_sent = "Invitation sent successfully.";	
		$invitation_faild = "Invitation failed.";		
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['invite_id']))
	$invite_id = $this->_request['invite_id'];

	if(!empty($user_id) && !empty($invite_id))
	{
		global $wpdb;
		$invitation_date = date('Y/m/d');
		$sql_invite ="INSERT INTO `nu_invite_user`(`nutritionist_id`,`user_id`,`invite_status`,`invitation_date`,`block_status`) VALUES ('$user_id','$invite_id','0','$invitation_date','Unblock')";
		$row=$wpdb->get_row($sql_invite);
		 
		$profile_query = "select display_name,user_email from nu_users where ID='".$user_id."'"; 
		$profile_result = $wpdb->get_row($profile_query);
		$name=$profile_result->display_name;
		$email=$profile_result->user_email;

		$to 	  = $email;
		$subject  = "FitLikethis";
		$message  = "Hello ".$name.", <br /><br />\r\n";
		$message .= "You are inviting at Nutrafit.<br />\r\n";
		$message .= "Website Link:<br /><br /><br />\r\n";
		$message .= '<a href="http://exceptionaire.co/nutrafitDivi/">Click Here</a>';
		$headers  = 'MIME-Version: 1.0' . PHP_EOL; 
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= "From:nutrafit@exceptionaire.com \r\n";
		$retval 	 = mail($to,$subject,$message,$headers);

		if( $retval == true )  
		{
			$error['Status'] = 'Success';	
			$error['msg'] = $invitation_sent;
			$this->response($this->json($error), 404);						
		}
		else
		{  
			$error['Status'] = 'Failed';	
			$error['msg'] 	 = $invitation_faild;
			$this->response($this->json($error), 404);
		}
	}	
}


	/* 
********Check Invite Status API***********
*/

private function check_invite_status()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$input_userid = "";
	$invitation_nofound = "";
	$invitation_succ = "";
	$invitation_reject = "";

	if('es' == $applang){
		$input_userid = "Por favor, introduzca su ID de usuario.";
		$invitation_nofound = "No se encontraron invitaciones.";
		$invitation_succ = "La invitación se aceptó correctamente.";
		$invitation_reject = "La invitación se rechazó correctamente.";
	}
	else{
		$input_userid = "Please enter your user id.";
		$invitation_nofound = "No invitations found.";
		$invitation_succ = "Invitation accepted successfully.";
		$invitation_reject = "Invitation rejected successfully.";	
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}
	global $wpdb;
	$nutritionist_list_query = "select * from nu_invite_user where user_id='".$user_id."'";
	$nutritionist_result = $wpdb->get_results($nutritionist_list_query);
       if($nutritionist_result)
	{
	  foreach($nutritionist_result as $nutritionist_row)
	  {
		$name_query = "select display_name from nu_users where ID='".$nutritionist_row->nutritionist_id."'";//get name from nutritionist_id
		$name_result = $wpdb->get_results($name_query);
	    foreach($name_result as $name_row)
		{
                      $image_attributes_nut = wp_get_attachment_url( get_user_meta($nutritionist_row->nutritionist_id,"profile_image", true) );
                      if($image_attributes_nut)
     	              {
                       $image_attributes = $image_attributes_nut;
     	              }else
     	              {
                      $image_attributes="";
     	              }
			$result['Status'] = 'Success';
			$result['nutritionist_invite_list'][]=array(
			'nutritionist_id'=>$nutritionist_row->nutritionist_id,
			'nutritionist_name'=>$name_row->display_name,
			'invite_status'=>$nutritionist_row->invite_status,
                        'profile_image'=>$image_attributes 
			
			);          			
		}
	 }
       }
    else
 	{ 
 	 //$result['msg'] = 'No invites.';
          $error = array('Status' => "Failed", "msg" => $invitation_nofound);
          $this->response($this->json($error), 400);
 	}	
	    if(isset($this->_request['nutritionist_id']))
		$nutritionist_id = $this->_request['nutritionist_id'];
         
        if(isset($this->_request['invite_status']))
        $invite_status = $this->_request['invite_status']; 

	    if(!empty($nutritionist_id) && !empty($invite_status))
	    {
		    $sql_accept ="UPDATE `nu_invite_user` set `invite_status`=($invite_status) WHERE `nutritionist_id` LIKE  '".$nutritionist_id."' and `user_id` LIKE  '".$user_id."'";
			$row=$wpdb->get_row($sql_accept);
			$result1['Status'] = 'Success';
                         if($invite_status==1)
			{
			$result1['msg'] = $invitation_succ;
	 	        }elseif ($invite_status==2) {
	 	    	$result1['msg'] = $invitation_reject;
	 	        }else
	 	        {

	 	        } 
			$this->response($this->json($result1),200); 	
	    }
    	
	$this->response($this->json($result),200);     
}


	/* 
********User Signup Graph API***********
*/


private function signup_user_graph()
{

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];

	$invalid_format = "";
	$nofound_user = "";

	if('es' == $applang){
		$invalid_format = "Formato inválido";
		$nofound_user = "Usuario no encontrado.";
	}
	else{
		$invalid_format = "Invalid Format.";
		$nofound_user = "User not found.";				
	}

   if($this->get_request_method() != "POST")
   {
    $error = array('Status' => "Not Acceptable", "msg" => $invalid_format);
    $this->response($this->json($error), 400);
   }
	$current_date=date('Y-m-d');


	$current_date_users_sql = mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_registered,nu_usermeta.meta_value 
                        AS user_type
	                    FROM nu_users, nu_usermeta
	                    WHERE nu_users.ID = nu_usermeta.user_id
	                    AND nu_usermeta.meta_key = 'user_type'
	                    AND nu_usermeta.meta_value = 'User' 
	                    AND date(user_registered) LIKE '$current_date'
	                    order by ID
	                 ");
$current_date_user_cnt = mysql_num_rows($current_date_users_sql);
$result['Status'] 	= 'Success';
$result['current_date_user_cnt'] 	=  $current_date_user_cnt;

while($users_row = mysql_fetch_array($current_date_users_sql,MYSQL_ASSOC))
{
$rawDate = $users_row["user_registered"]; 
$dateChunks = explode(" ", $rawDate);
$user_registered_date = $dateChunks[0];

$result['Current Date Signup'][]=array(                            
	                         'user_id'=>$users_row["ID"],
	                         'name'=>$users_row["display_name"],
				 'user_registered'=>$user_registered_date,
				 'user_type'=>$users_row["user_type"],                  
			              );

}

$cdate = date("Y-m-d h:i:s");
$date = strtotime($cdate);
$date = strtotime("-8 day", $date);
$before_one_week=  date('Y-m-d h:i:s', $date);

$week_user_sql= mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_registered,nu_usermeta.meta_value 
                            AS user_type
	                    FROM nu_users, nu_usermeta 
	                    WHERE nu_users.ID = nu_usermeta.user_id
	                    AND nu_usermeta.meta_key = 'user_type' 
                            AND nu_usermeta.meta_value = 'User'
	                    AND (date(nu_users.user_registered) BETWEEN '".$before_one_week."' AND '".$cdate."')
	                    order by ID");

$week_user_cnt = mysql_num_rows($week_user_sql);

$result['current_week_user_cnt'] 	=  $week_user_cnt;
while($week_row = mysql_fetch_array($week_user_sql,MYSQL_ASSOC))
{

$rawDate = $week_row["user_registered"]; 
$dateChunks = explode(" ", $rawDate);
$user_registered_date = $dateChunks[0];

    $result['Current Week Signup'][]=array(                            
	                         'user_id'=>$week_row["ID"],
	                         'name'=>$week_row["display_name"],
	                         'user_registered'=>$user_registered_date,
				             'user_type'=>$week_row["user_type"],                  
			                );

}


$month_user_sql= mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_registered,nu_usermeta.meta_value 
                            AS user_type
	                    FROM nu_users, nu_usermeta 
	                    WHERE nu_users.ID = nu_usermeta.user_id
	                    AND nu_usermeta.meta_key = 'user_type' 
                            AND nu_usermeta.meta_value = 'User'
	                    AND MONTH( CAST( `user_registered` AS date ) ) = MONTH( NOW( ) )
                            AND YEAR( CAST( `user_registered` AS date ) ) = YEAR( NOW( ) )
	                    order by ID");

$month_user_cnt = mysql_num_rows($month_user_sql);

$result['current_month_user_cnt'] 	=  $month_user_cnt;
while($month_row = mysql_fetch_array($month_user_sql,MYSQL_ASSOC))
{

$rawDate = $month_row["user_registered"]; 
$dateChunks = explode(" ", $rawDate);
$user_registered_date = $dateChunks[0];

    $result['Current Month Signup'][]=array(                            
	                         'user_id'=>$month_row["ID"],
	                         'name'=>$month_row["display_name"],
	                         'user_registered'=>$user_registered_date,
				 'user_type'=>$month_row["user_type"],                  
			                );

}
$this->response($this->json($result),200);
         
$error = array('Status' => "Failed", "msg" => $nofound_user);
$this->response($this->json($error), 400);
}





/* 
********User Payment Status API***********
*/

private function user_payment_status()
{
	if($this->get_request_method() != "POST")
	{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$nofound_user = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca el ID de usuario.";
			$nofound_user = "Usuario no encontrado.";
		}
		else{
			$input_userid = "Please enter user id.";
			$nofound_user = "User not found.";		
		}

	if(isset($this->_request['user_id']))	
	$user_id = $this->_request['user_id'];

	if( empty($user_id) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
	}	
	global $wpdb;
	$payment_status_query = "SELECT meta_value FROM nu_usermeta WHERE user_id = ".$user_id." and meta_key = 'payment_status'";
	$payment_status_result = $wpdb->get_row($payment_status_query);
	$payment_status=$payment_status_result->meta_value;
		if($payment_status=='1')
		{
		 $payment_status='1';
		}else
		{
		 $payment_status='0';
		}
        $calories_query = "SELECT calories FROM nu_payments WHERE user_id = ".$user_id."  order by payment_id";
	$calories_result = $wpdb->get_row($calories_query);
	$calories=$calories_result->calories;
 
	$result['User Payment Status']=array('payment_status'=>$payment_status,'calories'=>$calories);  
	$result['Status'] 	= 'Success';
	$this->response($this->json($result),200);
	$error = array('Status' => "Failed", "msg" => $nofound_user);
	$this->response($this->json($error), 400);
}



/* 
********Quick Tips API***********
*/		
				
private function quick_tips()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$nofound_plan = "";
	$nofound_record = "";

	if('es' == $applang){
			$nofound_plan = "Plan no encontrado.";
			$nofound_record = "no hay registros.";
		}
		else{
				$nofound_plan = "Plan not found.";
				$nofound_record = "no records.";
		}

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$custom_args = array(
	'post_type' => 'tip', 
	'post_status' => 'publish',
	'paged' => $paged
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		$result['Status'] = 'Success';
		while ($custom_query->have_posts()) 
		{
		$custom_query->the_post(); 
		
		$diet_tips = get_field('diet_tips');
        $workout_tips = get_field('workout_tips');
        $planning_tips = get_field('planning_tips');
        
		//$result['fitness_tips'][]=array('diet_tips'=>$diet_tips,
		//	                         'workout_tips'=>$workout_tips,
		//	                         'planning_tips'=>$planning_tips
		//	                         );
                 
                 $result['fitness_tips'][0]=array('tips'=>$diet_tips);
		 $result['fitness_tips'][1]=array('tips'=>$workout_tips);
		 $result['fitness_tips'][2]=array('tips'=>$planning_tips);

		}//while
		$this->response($this->json($result), 200);
	}//if
	else{
	    $this->response($nofound_record, 204);	// If no records "No Content" status
	} 
	// If invalid inputs "Bad Request" status message and reason
	$error = array('status' => "Failed", "msg" => $nofound_plan);
	$this->response($this->json($error), 400);
}



/* 
********Add Workout API***********
*/
private function add_workout()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
	//$this->response('',406);
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_workout = "";
	$input_wkout_count = "";
	$input_ytbid = "";
	$wkotadded_succ = "";
	$wkotupdate_succ = "";
	$ivalid_userid = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_workout = "Por favor, introduzca el nombre del entrenamiento.";
			$input_wkout_count = "Por favor ingrese el número de entrenamiento.";
			$input_ytbid = "Ingresa la ID del video de youtube.";
			$wkotadded_succ = "Entrenamiento agregado correctamente.";
			$wkotupdate_succ = "Entrenamiento actualizado con éxito.";
			$ivalid_userid = "ID de usuario invalido.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_workout = "Please enter workout name.";
			$input_wkout_count = "Please enter workout count.";
			$input_ytbid = "Please enter youtube video id.";
			$wkotadded_succ = "Workout added successfully.";
			$wkotupdate_succ = "Workout updated successfully.";
			$ivalid_userid = "Invalid User Id.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['workout_name']))
	$workout_name = $this->_request['workout_name'];
	if( empty($workout_name) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_workout;
	$this->response($this->json($error), 404);
	}

	$workout_name = str_replace("#*$*#","+",$workout_name);

	if(isset($this->_request['workout_count']))
	$workout_count = $this->_request['workout_count'];
	if( empty($workout_count) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_wkout_count;
	$this->response($this->json($error), 404);
	}
    
    if(isset($this->_request['youtube_video_id']))
	$youtube_video_id = $this->_request['youtube_video_id'];
	if( empty($youtube_video_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_ytbid;
	$this->response($this->json($error), 404);
	}

	if(!empty($user_id) && !empty($workout_name) && !empty($workout_count) && !empty($youtube_video_id))
	{
		global $wpdb;
		$todays_date=date("Y/m/d");
		$add_workout_query = "SELECT * from " . $wpdb->prefix . "add_workout WHERE user_id = '".$user_id."' and workout_name = '".$workout_name."' and youtube_video_id='".$youtube_video_id."' order by id DESC";
		$add_workout_result = $wpdb->get_row($add_workout_query);
		$workout_date=$add_workout_result->workout_date;
		if($todays_date!=$workout_date)
		{

          $insert_workout ="INSERT INTO " . $wpdb->prefix . "add_workout(user_id,workout_name,workout_count,youtube_video_id,workout_date) VALUES ('$user_id','$workout_name','$workout_count','$youtube_video_id','$todays_date')";
		  $row_insert_workout=$wpdb->get_row($insert_workout);

		$result['Status'] = 'Success';
	    $result['msg'] = $wkotadded_succ;		
		}else
		{
          $previous_count=$add_workout_result->workout_count;
          $new_workout_count=$previous_count+$workout_count;
  	sendPushnotificationAndroid();
          $update_workout="UPDATE " . $wpdb->prefix . "add_workout set workout_count='".$new_workout_count."' where user_id='".$user_id."' and workout_name='".$workout_name."' and youtube_video_id='".$youtube_video_id."' and workout_date='".$todays_date."'";
          $up_row_workcount=$wpdb->get_row($update_workout);
          $result['Status'] = 'Success';
	      $result['msg'] = $wkotupdate_succ;	     
		}
	$this->response($this->json($result), 200);		
	}
	// If invalid inputs "Bad Request" status message and reason
	$error = array('Status' => "Failed", "msg" => $ivalid_userid);
	$this->response($this->json($error), 400);
}	
	


	/* 
********Workout API***********
*/

private function workout()
{
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		//$this->response('',406);
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$invalid_planid = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$invalid_planid = "ID de plan no válido.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$invalid_planid = "Invalid Plan id.";
	}

if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

	global $wpdb;
	$plan_id_query = "SELECT post_id FROM nu_payments WHERE user_id = ".$user_id." order by payment_id DESC";
	$plan_id_result = $wpdb->get_row($plan_id_query);
	$plan_id=$plan_id_result->post_id;

	// Input validations
	if(!empty($plan_id))
	{
	$custom_args = array(
	'post_type' => 'plan'
	);
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		while ($custom_query->have_posts()) 
		{
			$custom_query->the_post();
	        if(have_rows('workout_video') ):
		        while ( have_rows('workout_video') ) : the_row(); 
		            $plan_title=get_the_title();
			        $video_title = get_sub_field('video_title');
			        $youtube_video_link = get_sub_field("youtube_link");
                    $ytarray=explode("/", $youtube_video_link);
                    $ytendstring=end($ytarray);
                    $ytendarray=explode("?v=", $ytendstring);
                    $ytendstring=end($ytendarray);
                    $ytendarray=explode("&", $ytendstring);
                    $ytcode=$ytendarray[0]; 
			        $myplan_id=get_the_ID();
			        if($myplan_id ==$plan_id) 
				    {     
                                       $add_workout_query = "SELECT * from " . $wpdb->prefix . "add_workout WHERE 
                                       user_id = '".$user_id."' and youtube_video_id='".$ytcode."'  and workout_name='".$video_title."' ";
		                       $add_workout_result = $wpdb->get_results($add_workout_query);
		                       if($add_workout_result)
		                         {
		                         	$workout_count = 0;
		                           foreach ($add_workout_result as $workout_count_res) 
	                                   {			
	                                    $workout_count= $workout_count + $workout_count_res->workout_count;	
	                                   }
				        }
				      else
				       {
				      	 $workout_count='0';
				       }
				    	$result['Status'] = 'Success';
			                $result['plan_title;']=$plan_title;
			           
				        $result['workout_video'][]=array('video_title'=>$video_title,
				        	                         'youtube_link'=>$ytcode,
                                                                         'workout_count'=>$workout_count                       
							                );
			        }
		        endwhile;//sub plan
	        endif;//sub plan
		}//while custom_query
		$this->response($this->json($result), 200);
	}//if custom_query
	else{
	    $this->response('no records', 204);	// If no records "No Content" status
	} 
}
   	// If invalid inputs "Bad Request" status message and reason
	$error = array('Status' => "Failed", "msg" => $invalid_planid);
	$this->response($this->json($error), 400);
}


/* 
********Weight Tracker API***********
*/

private function weight_tracker()
{
		// Cross validation if the request method is POST else it will return "Not Acceptable" status
		if($this->get_request_method() != "POST")
		{
			//$this->response('',406);
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$invalid_userid = "Invalid user id.";					
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['current_weight']))
		$current_weight = $this->_request['current_weight'];

	   if(isset($this->_request['current_calories']))
		$current_calories_req = $this->_request['current_calories'];

		global $wpdb;
         $query = "select calories,weight from " . $wpdb->prefix . "payments where user_id=".$user_id; 
         $row=$wpdb->get_row($query . " order by payment_id DESC");

         $query_rr = "select current_weight as ww_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."' order by id DESC";
         $row_weight=$wpdb->get_row($query_rr);

         $sql_check_weight = "select count(0) as row_count_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."'";
         $row_weight_kk=$wpdb->get_row($sql_check_weight);

		//foreach($result_weight as $row)
		//{

			 $calories_adhicha=$row->calories;

		       if($row_weight_kk->row_count_weight>0)
                         {
                           $before_weight=$row_weight->ww_weight;
                           $before_lbs_weight=$before_weight;
                         }else
                         {
                          $before_weight=$row->weight;
                          $before_lbs_weight=$before_weight;
                         }
                        $reg_user_weight=$row->weight;
			$current_lbs_weight=$current_weight;
			$weight_difference=$current_lbs_weight-$reg_user_weight;
			$current_date = date("Y/m/d");
			$result['Status'] = 'Success';
			$result['starting_weight'] = $row->weight;
			$result['current_weight']=$before_lbs_weight." lbs";
            
			$sql_check ="SELECT * FROM  `nu_weight_tracker` WHERE  `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
			$row=$wpdb->get_row($sql_check);

			$sql_check ="SELECT user_id FROM  `nu_weight_tracker` WHERE  `user_id` LIKE  '".$user_id."'";
			$row11=$wpdb->get_row($sql_check);

			if($row==0 && $current_weight!='')
			{
				//echo "insert";
				$sql_check1 ="INSERT INTO `nu_weight_tracker`(`user_id`,`before_weight`,`current_weight`, `current_lbs_weight`, `weight_difference`,`current_date`,`date`) VALUES ('$user_id','$before_weight','$current_weight','$current_lbs_weight','$weight_difference','$current_date','$current_date')";
				$row=$wpdb->get_row($sql_check1);
				$result['current_weight']=$current_weight."lbs";
				
				if($reg_user_weight>=$current_lbs_weight)
				{          
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Lost";
				}else
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Gained";
				}
			}
			if($current_weight!='')
			{
				//echo "update";
				$sql_check2 ="UPDATE `nu_weight_tracker` set `current_weight`=('$current_weight'),`current_lbs_weight`=('$current_lbs_weight'),`weight_difference`=('$weight_difference') WHERE `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
				$row=$wpdb->get_row($sql_check2);
                                  $result['current_weight']=$current_lbs_weight."lbs";

				if($reg_user_weight>=$current_lbs_weight)
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Lost";
				}else
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Gained";
				}

                     
                    $weight=$current_weight;
$query_ma  = "SELECT post_id,gender,sub_plan_title FROM ".$wpdb->prefix."payments where user_id = '".$user_id."' order by payment_id DESC";
	 $plan_result_kk = $wpdb->get_row($query_ma);
	$plan_id=$plan_result_kk->post_id;
	$gender=$plan_result_kk->gender;
	$sub_plan_title=$plan_result_kk->sub_plan_title;

	$query  = "SELECT ID,post_type FROM nu_posts WHERE ID = '".$plan_id."' and post_type= 'plan'";
	$plan_result = $wpdb->get_row($query);

	$custom_args = array('post_type' => 'plan');
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		while ($custom_query->have_posts()) 
		{
			$custom_query->the_post();
			if(have_rows('sub_plan') ):
			while ( have_rows('sub_plan') ) : the_row(); 
			$myplan_id=get_the_ID();
			if($myplan_id == $plan_id && $gender=='Male') 
			{
			    $my_sub_plan_title = get_sub_field('sub_plan_title');

				if( have_rows('men') ):
				while ( have_rows('men') ) : the_row();

				$men_formula_for_calories = get_sub_field('formula_for_calories');  
				$men_formula_for_proteins = get_sub_field('formula_for_proteins'); 
				$men_formula_for_carbs = get_sub_field('formula_for_carbs');
				                     
				$cal=$weight*$men_formula_for_calories;
				$pro=$men_formula_for_proteins*$weight;
				$carb=$men_formula_for_carbs*$weight;
				$fat= ($cal-(($pro*4)+($carb*4)))/9;

				$men_calories=round($cal,2);
				$men_pro=round($pro,2);  
				$men_carb=round($carb,2);  
				$men_fat=round($fat,2);    
                
                if($my_sub_plan_title==$sub_plan_title)
                {
				    $sql_accept ="UPDATE  ".$wpdb->prefix."payments set calories='". $men_calories."',protein_gram='".$men_pro."',carb_gram='".$men_carb."',fat_gram='".$men_fat."' WHERE user_id =  '".$user_id."' and post_id='".$myplan_id."' and sub_plan_title='".$my_sub_plan_title."'";
					$row=$wpdb->get_row($sql_accept);

					$plan_history_men ="UPDATE ".$wpdb->prefix."plan_history set old_calories='".$men_calories."',old_protien='".$men_pro."',old_carbs='".$men_carb."',old_fats='".$men_fat."'  WHERE user_id =  '".$user_id."' and old_date='".date('Y/m/d')."' ";
					$row_plan_history=$wpdb->get_row($plan_history_men);
                                        $result['latest_calories']=$men_calories;
                                        
                    
                }

				endwhile;//men
				endif;//men   
			}else if($myplan_id ==$plan_id && $gender=='Female') 
			{
				$my_sub_plan_title = get_sub_field('sub_plan_title');

				if( have_rows('women') ):
				while ( have_rows('women') ) : the_row(); 

				$women_formula_for_calories = get_sub_field('women_formula_for_calories');  
                $women_formula_for_proteins = get_sub_field('women_formula_for_proteins'); 
                $women_formula_for_carbs = get_sub_field('women_formula_for_carbs');

				$cal=$weight*$women_formula_for_calories;
				$pro=$women_formula_for_proteins*$weight;
				$carb=$women_formula_for_carbs*$weight;
				$fat= ($cal-(($pro*4)+($carb*4)))/9;

				$women_calories=round($cal,2);
				$women_pro=round($pro,2);  
				$women_carb=round($carb,2);  
				$women_fat=round($fat,2); 
				if($my_sub_plan_title==$sub_plan_title)
                {                           
					$sql_accept ="UPDATE  ".$wpdb->prefix."payments set calories='". $women_calories."',protein_gram='".$women_pro."',carb_gram='".$women_carb."',fat_gram='".$women_fat."'  WHERE user_id =  '".$user_id."' and post_id='".$myplan_id."' and sub_plan_title='".$my_sub_plan_title."'";
					$row=$wpdb->get_row($sql_accept);

					$plan_history_men ="UPDATE ".$wpdb->prefix."plan_history set old_calories='".$women_calories."',old_protien='".$women_pro."',old_carbs='".$women_carb."',old_fats='".$women_fat."'  WHERE user_id =  '".$user_id."' and old_date='".date('Y/m/d')."' ";
					$row_plan_history=$wpdb->get_row($plan_history_men);
                                        $result['latest_calories']=$women_calories;

                     
			    } 
				endwhile;//women
				endif;//women   
			}
			endwhile;//sub plan
			endif;//sub plan
		}//while custom_query
	}//if custom query

			}
			elseif($row11!=0)
			{
				//echo "show";
				$sql_check_login ="SELECT before_weight,current_lbs_weight,weight_difference FROM `nu_weight_tracker` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
				$row=$wpdb->get_row($sql_check_login);
				$weight_difference_adhicha=$row->weight_difference;
                                $current_lbs_weight1=$row->current_lbs_weight;
                                $before_weight1=$row->before_weight; 

				if($reg_user_weight>=$current_lbs_weight1)
				{
					
					$result['weight_difference']=$weight_difference_adhicha;
					$result['weight_text']="lbs Lost";
				}
				else
				{
					
					$result['weight_difference']=$weight_difference_adhicha;
					$result['weight_text']="lbs Gained";
				}
			} 
			else
			{
				
				$result['weight_difference']='0';
                                $result['weight_text']="";
			} 

            /******************Calories Consumed ***********************/
			if(isset($this->_request['calories_burned']))
		     $calories_burned = $this->_request['calories_burned'];
            
             if(isset($this->_request['steps_count']))
		     $steps_count = $this->_request['steps_count']; 

			$result['calories_adhicha']=$calories_adhicha;

			$sql_check ="SELECT user_id FROM  `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."'";
            $calories_row_blank=$wpdb->get_row($sql_check);          

            $sql_check ="SELECT * FROM  `nu_customcalories` WHERE  `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
            $row=$wpdb->get_row($sql_check);


            if($row==0 && $calories_burned!='' && $steps_count!='')
            {  
	           $sql_check1 ="INSERT INTO `nu_customcalories`(`user_id`, `calories`,`calories_burned`,`steps_count`,  `current_date`) VALUES ('$user_id','$current_calories_req','$calories_burned','$steps_count','$current_date')";
	          $row=$wpdb->get_row($sql_check1);
	          $result['current_calories']=$current_calories_req;
	          if($current_calories_req){$result['current_calories']=$current_calories_req;}
	                               else{$result['current_calories']='0';}
              if($calories_burned){$result['calories_burned']=$calories_burned;;}
	                               else{$result['calories_burned']='0';}
	          if($steps_count){$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}    
            }
            else if($current_calories_req!='' || $calories_burned!='' || $steps_count!='' )
            {
            	if($current_calories_req!='')
            	{
	              $sql_check2 ="UPDATE `nu_customcalories` set `calories`=('$current_calories_req') WHERE `current_date` LIKE  '".$current_date."'";
	             $row=$wpdb->get_row($sql_check2);
	            }

	            if($calories_burned!='' && $steps_count!='')
               {
                 $sql_check2 ="UPDATE `nu_customcalories` set `calories_burned`=('$calories_burned'),`steps_count`=('$steps_count') WHERE `current_date` LIKE  '".$current_date."'";
	             $row=$wpdb->get_row($sql_check2);
               }

	            $sql_check_login ="SELECT calories,calories_burned,steps_count FROM `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
				$row=$wpdb->get_row($sql_check_login);

				$calories_adhicha=$row->calories;
				$calories_burned=$row->calories_burned;
				$steps_count=$row->steps_count;

				// added calories consumed current start 13-04-2017
				$calories_consumed = mobile_macro_nutrients($this->_request['user_id']);
				if($calories_consumed){
					$result['calories_consumed']=$calories_consumed[0]['recipe_calories'];
				}else{
					$result['calories_consumed']='0';
				}
				// calories consumed current end                


               if($calories_adhicha){$result['current_calories']=$calories_adhicha;}
	                               else{$result['current_calories']='0';}
               if($calories_burned){$result['calories_burned']=$calories_burned;}
	                               else{$result['calories_burned']='0';}
	           if($steps_count){$result['steps_count']=$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}
            }
           
            elseif($calories_row_blank!=0)
			{
				$sql_check_login ="SELECT calories,calories_burned,steps_count FROM `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
				$row=$wpdb->get_row($sql_check_login);
				$calories_adhicha=$row->calories;
				$calories_burned=$row->calories_burned;
				$steps_count=$row->steps_count;
				
				// added calories consumed current start 06-04-2017
				$calories_consumed = mobile_macro_nutrients($this->_request['user_id']);
				if($calories_consumed){
					$result['calories_consumed']=$calories_consumed[0]['recipe_calories'];
				}else{
					$result['calories_consumed']='0';
				}
				// calories consumed current end

                 if($calories_adhicha){$result['current_calories']=$calories_adhicha;}
	                               else{$result['current_calories']='0';}
               if($calories_burned){$result['calories_burned']=$calories_burned;}
	                               else{$result['calories_burned']='0';}
	           if($steps_count){$result['steps_count']=$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}		
			} 
           else
           {
          	$result['current_calories']='0';
          	$result['calories_burned']='0';
          	$result['calories_consumed']='0';
          	$result['steps_count']='0';
           }
                $sql_user="select payment_date,sub_plan_duration,calories from " . $wpdb->prefix . "payments where user_id='".$user_id."' order by payment_id DESC";

	          $row_user=$wpdb->get_row($sql_user);
	          if($row_user)
	          {           
		     $plan_start_date=$row_user->payment_date;
                     $spd = $row_user->sub_plan_duration;
                     $spdvar = $spd; 
                      if($spdvar!='')
                       {
                        $spdChunks = explode(" ", $spdvar);
                        $spdvar= $spdChunks[0]; 
                        $delidate = $row_user->payment_date; 
                        $monthlyDate = strtotime("+$spdvar week".$delidate);
                        $monthly = date("Y/m/d", $monthlyDate);
                        $plan_expire_date = $monthly;
                       }
                     $result["plan_start_date"] = $plan_start_date;
                     // ABL edited 20170821
					 //$result["plan_end_date"] = $plan_expire_date;
					 $result["plan_end_date"] = date('Y/m/d', strtotime('-1 day', strtotime($plan_expire_date)));       
	          }
                  
                  $allergiesone = get_user_meta($user_id,"allergiesone", true);
	          $result['allergiesone']=$allergiesone;
		// added new 13-04-2017 start	
		  $result['latest_calories_again']=$row_user->calories;
		// added new 13-04-2017 end
			$this->response($this->json($result), 200);
		//}
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}



	/* 
********Weight Tracker API***********
*/

private function weight_graph()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$notontrack = "";
		$enter_weight = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$notontrack = "No en el camino.";
				$enter_weight = "Introduzca el peso para ver el gráfico del peso.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$notontrack = "Not on track.";
				$enter_weight = "Enter weight to see weight graph track.";
				$invalid_userid = "Invalid User Id.";				
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}
		global $wpdb;
         $sql_graph = "select count(0) as row_count_user from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."'";
         $row_graph=$wpdb->get_row($sql_graph);

         if($row_graph->row_count_user>0)
         {	
	          global $wpdb;
	        $query_sub_plan_duration = "select payment_date from " . $wpdb->prefix . "payments where user_id='".$user_id."' ";
	        $row_sub_plan_duration=$wpdb->get_row($query_sub_plan_duration . " order by payment_id DESC");
	        $plan_start_date=$row_sub_plan_duration->payment_date;

	        $result['Status'] = 'Success';
	        $cdate = date("Y-m-d h:i:s");
	          
	        $sql_user="select * from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."' AND (date(nu_weight_tracker.current_date) BETWEEN '".$plan_start_date."' AND '".$cdate."')";
	          $row_user=$wpdb->get_results($sql_user);
	          if($row_user)
	          {
		          foreach ($row_user as $user_weight) 
		          {
			          $result['Weekly Weight Tracker Graph'][]=array(
			          'user_weight'=>$user_weight->current_weight,
			          'user_date'=>$user_weight->current_date
			          );          		       
		          }
	          }
	          else
	          {
		          $result['Status'] = 'Failed';
			      $result['msg'] = $notontrack;
	          }
          }
          else
          {
	          $result['Status'] = 'Failed';
		      $result['msg'] = $enter_weight;
          }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


private function message()
{
		if($this->get_request_method() != "POST")
		{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$nodata_found_msg = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$nodata_found_msg = "No hay mensaje para mostrar.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$nodata_found_msg = "No message to display.";
				$invalid_userid = "Invalid User Id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];
                $from_user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
		}

		if(isset($this->_request['nutritionist_id']))
		$nutritionist_id = $this->_request['nutritionist_id'];

	    if(isset($this->_request['message_contents']))
		$message_contents = $this->_request['message_contents'];


		if(!empty($user_id))
		{
				$sql = mysql_query("SELECT nu_users.ID,nu_usermeta.meta_value AS user_type
					                FROM nu_users, nu_usermeta
					                WHERE nu_users.ID = '".$user_id."'
					                AND nu_users.ID = nu_usermeta.user_id
					                AND nu_usermeta.meta_key = 'user_type'", $this->db);

				if(mysql_num_rows($sql) > 0)
				{
					$result['Status'] = 'Success';
					//$result['msg'] = 'You have successfully logged in';
					while($row = mysql_fetch_array($sql,MYSQL_ASSOC))
					{
						if($row["user_type"]=='User')
						{
							//echo "Login as user";
							global $wpdb;
	                        $name_query = "select * from " . $wpdb->prefix . "invite_user where user_id='".$user_id."' and invite_status='1'";
	                        $name_result = $wpdb->get_results($name_query);
	                        if($name_result)
	                        {
		                     foreach($name_result as $name_row)
		                      {
			                   $user_id=$name_row->nutritionist_id;
                                           $fun_user_email = get_user_by('id',$user_id);
                                           $nutritionist_email = stripslashes($fun_user_email->user_email);
			                   $name = (get_user_meta($user_id,"first_name", true));
			                   $image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
		                       if($image_url)
		                       {
		                        $profile_image = $image_url;
		                       }else
		                       {
		                        $profile_image="";
		                       }
			                   $result['Status'] = 'Success';
			                   $result['Nutritionist List'][]=array(
			                   'user_id'=>$name_row->nutritionist_id,	
			                   'name'=>$name,
			                   'profile_image'=>$profile_image,
                                           'emailid'=>$nutritionist_email    
			                   );          			
		                      }
                            }
						}//user_tye if
						else
						{
						  //echo "Login as Nutritionist";
						  global $wpdb;
	                      $name_query = "select * from " . $wpdb->prefix . "invite_user where nutritionist_id='".$user_id."' and invite_status='1'";
	                      $name_result = $wpdb->get_results($name_query);
	                      if($name_result)
	                       {
		                    foreach($name_result as $name_row)
		                    {
			                 $user_id=$name_row->user_id;
                                        $fun_user_email = get_user_by('id',$user_id);
                                         $user_email = stripslashes($fun_user_email->user_email);
			                 $name = (get_user_meta($user_id,"first_name", true));
			                 $image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
		                     if($image_url)
		                     {
		                      $profile_image = $image_url;
		                     }else
		                     {
		                     $profile_image="";
		                     }
			                 $result['Status'] = 'Success';
			                 $result['Nutritionist List'][]=array(
		                         'user_id'=>$name_row->user_id,		
			                 'name'=>$name,
			                 'profile_image'=>$profile_image,
                                          'emailid'=>$user_email		
			                  );          			
	                    	}
	                      }
						}
					}				
				}
              if(!empty($nutritionist_id) && !empty($this->_request['user_id']))
		        {
		        global $wpdb;
			$query = "select id,message_contents from nu_fep_messages where from_user=".$nutritionist_id." and to_user=".$this->_request['user_id']; 
                $result = $wpdb->get_results($query);

                $row_pp=$wpdb->get_row($query);
                $message_contentsaa=$row_pp->message_contents;
                if($row_pp==0)
                	{	$result['Status'] = 'Failed';
                		$result['msg'] = $nodata_found_msg;
                	}
                	else
                	{
                   foreach($result as $row)
                   {
                   	$result1['Status'] = 'Success';
                   // $result['Message List'][]=array($row->message_contents); 
                    $result1['Message List'][]=array('messassge_id'=>$row->id,'messassge_contents'=>$row->message_contents);          			
                   }
                   $this->response($this->json($result1), 200);
               }
                }

				$this->response($this->json($result), 200);	
		}
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
} 


/* 
********New Message API***********
*  
*/
		
private function new_message()
{
		if($this->get_request_method() != "POST")
		{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$input_email = "";
		$input_msgcontent = "";
		$user_notfind = "";
		$msgsent_succ = "";
		$invalid_userid = "";
		$userblock = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$input_email = "Por favor, introduzca el correo electrónico del destinatario.";
				$input_msgcontent = "Introduzca el contenido del mensaje.";
				$user_notfind = "El usuario no existe.";
				$msgsent_succ = "Mensaje enviado con éxito.";
				$invalid_userid = "ID de usuario invalido.";
				$userblock = "El usuario está bloqueado.";
				$usertouser = "No tiene permiso para enviar mensajes a otros usuarios.";
				$nutritonutri = "No tienes permiso para enviar mensajes a otros nutricionistas.";
				$user_unaccept_nutri = "No ha aceptado / recibido la invitación de este nutricionista.";
				$nutri_unaccept_user = "Este usuario no ha aceptado tu invitación.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$input_email = "Please enter recipient email.";
				$input_msgcontent = "Please enter message contents.";
				$user_notfind = "User does not exist.";
				$msgsent_succ = "Message sent successfully.";
				$invalid_userid = "Invalid User Id.";
				$userblock = "User is blocked.";
				$usertouser = "You dont have permission to send messages to other users.";
				$nutritonutri = "You dont have permission to send messages to other nutritionists.";
				$user_unaccept_nutri = "You have not accepted/recieved inviatation from this nutritionist.";
				$nutri_unaccept_user = "This user has not accepted your invitation.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];
	    $from_user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
		}

		if(isset($this->_request['recipient_email']))
		$recipient_email = $this->_request['recipient_email'];

                 if( empty($recipient_email) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_email;
		$this->response($this->json($error), 404);
		}

	        if(isset($this->_request['message_contents']))
		$message_contents = $this->_request['message_contents'];
                
                 if( empty($message_contents) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_msgcontent;
		$this->response($this->json($error), 404);
		}  

		global $wpdb;
		$getuserid = "SELECT ID FROM ".$wpdb->prefix."users WHERE user_email='".$recipient_email."'";
      	$getuserid_result = $wpdb->get_results($getuserid,ARRAY_A);
      	$emailuserid = $getuserid_result[0]['ID'];

      	$presentuserid = array_filter($getuserid_result);

      	if(empty($presentuserid)){

      		$error = array('Status' => "Failed", "msg" => $user_notfind);
			$this->response($this->json($error), 400);
      	}
      	$chekid = get_user_meta($emailuserid,'user_type');
      	$nutri_id = "";
      	$uid = "";
      	if('User' == $chekid[0]){
 									$uid = $emailuserid;
 									$nutri_id = $from_user_id;
								}
						      else{

						        $nutri_id = $emailuserid;
						        $uid = $from_user_id;
						      }

		// Check same user type not allow to send message START

						
						$userid_usertype = get_user_meta($uid,'user_type');
						$nutriid_usertype = get_user_meta($nutri_id,'user_type');

						$userid_usertype = $userid_usertype[0];
						$nutriid_usertype = $nutriid_usertype[0];

						
						if($userid_usertype == $nutriid_usertype){
	
							if('User' == $userid_usertype){
								$msg = $usertouser;
							}
							else{
								$msg = $nutritonutri;
								}
							
							$error = array('Status' => "Failed", "msg" => $msg);
							$this->response($this->json($error), 400);								
						}
						else{

							$statusquery= "select invite_status from ".$wpdb->prefix."invite_user where user_id=".$uid." and nutritionist_id= ".$nutri_id."";
							$statusquery_result = $wpdb->get_row($statusquery);
							//echo "Hi - ".$statusquery_result->invite_status;

							if(0 == $statusquery_result->invite_status){
								
								$useridusertype = get_user_meta($user_id,'user_type');
								if('User' == $useridusertype[0]){
									$msg = $user_unaccept_nutri;
								}
								else{
									$msg = $nutri_unaccept_user;
									
								}

								$error = array('Status' => "Failed", "msg" => $msg);
								$this->response($this->json($error), 400);
							}
						}

						
		// Check same user type not allow to send message END						      

      	$check_userblockornot = "select block_status from " . $wpdb->prefix . "invite_user where nutritionist_id='".$nutri_id."' and user_id='".$uid."'";
    	$check_userblockornot_result = $wpdb->get_results($check_userblockornot,ARRAY_A);

      	if('Unblock' == $check_userblockornot_result[0]['block_status']){

      		if(!empty($user_id))
		{
				
                if(!empty($recipient_email) && !empty($message_contents))
		        {
		         //echo "11";exit; 	
		        global $wpdb;
		        $user = get_user_by( 'email', $recipient_email);
                $recipient_id = $user->ID;
		        $msg_date = date('Y-m-d H:i:s');
                $message_title= substr($message_contents, 0, 20);
                $msg_sql ="INSERT INTO `nu_fep_messages`(`from_user`, `to_user`, `last_sender`,`send_date`,`last_date`,`message_title`,`message_contents`,`status`) VALUES ('$from_user_id','$recipient_id','$from_user_id','$msg_date','$msg_date','$message_title','$message_contents','0')";
                 $row=$wpdb->get_row($msg_sql);
                	$result['Status'] = 'Success';
                   $result['msg'] = $msgsent_succ;
               }

				$this->response($this->json($result), 200);	
		}
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
      	}
      	else{

    		$error = array('Status' => "Failed", "msg" => $userblock);
			$this->response($this->json($error), 400);
    	}

		
} 



/* 
********Reply Message API***********
*  
*/
		
private function reply_message()
{
		if($this->get_request_method() != "POST")
		{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_formid = "";
		$input_msgid = "";
		$input_toid = "";
		$input_msgcontent = "";
		$msgsent_succ = "";
		$invalid_userid = "";
		$user_blocked = "";

		if('es' == $applang){
			$input_formid = "Por favor, ingrese desde id.";
			$input_msgid = "Por favor, ingrese la ID del mensaje";
			$input_toid = "Por favor, ingrese al id.";
			$input_msgcontent = "Introduzca el contenido del mensaje.";
			$msgsent_succ = "Mensaje enviado con éxito.";
			$invalid_userid = "ID de usuario invalido.";
			$user_blocked = "El usuario está bloqueado.";
				
		}
		else{
			$input_formid = "Please enter from id.";
			$input_msgid = "Please enter message id.";
			$input_toid = "Please enter to id.";
			$input_msgcontent = "Please enter message contents.";
			$msgsent_succ = "Message sent successfully.";
			$invalid_userid = "Invalid User Id.";
			$user_blocked = "User is blocked.";	
		}

		if(isset($this->_request['from_id']))
		$from_id = $this->_request['from_id'];

		if( empty($from_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_formid;
		$this->response($this->json($error), 404);
		}

		if(isset($this->_request['message_id']))
		$message_id = $this->_request['message_id'];

	    if( empty($message_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_msgid;
		$this->response($this->json($error), 404);
		}

        if(isset($this->_request['to_id']))
		$to_id = $this->_request['to_id'];

		if( empty($to_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_toid;
		$this->response($this->json($error), 404);
		}

	    if(isset($this->_request['message_contents']))
		$message_contents = $this->_request['message_contents'];
        if( empty($message_contents) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_msgcontent;
		$this->response($this->json($error), 404);
		}

		$chekid = get_user_meta($to_id,'user_type');

		if('User' == $chekid[0]){
 									$uid = $to_id;
 									$nutri_id = $from_id;
								}
						      else{

						        $nutri_id = $to_id;
						        $uid = $from_id;
						      }
        global $wpdb;						      
		$check_userblockornot = "select block_status from " . $wpdb->prefix . "invite_user where nutritionist_id='".$nutri_id."' and user_id='".$uid."'";
    	$check_userblockornot_result = $wpdb->get_results($check_userblockornot,ARRAY_A);	

    	if('Unblock' == $check_userblockornot_result[0]['block_status']){

    		if(!empty($from_id))
		{
				
                if(!empty($from_id) && !empty($to_id) && !empty($message_id) && !empty($message_contents))
		        {
		         //echo "11";exit; 	
		        
		        
		        $msg_date = date('Y-m-d H:i:s');
                $message_title= substr('Re:'.$message_contents, 0, 20);
                $msg_sql ="INSERT INTO `nu_fep_messages`(`parent_id`,`from_user`, `to_user`, `last_sender`,`send_date`,`last_date`,`message_title`,`message_contents`,`status`) VALUES ('$message_id','$from_id','$to_id','$from_id','$msg_date','$msg_date','$message_title','$message_contents','0')";
                 $row=$wpdb->get_row($msg_sql);
                 $result['Status'] = 'Success';
                 $result['msg'] = $msgsent_succ;
               }

				$this->response($this->json($result), 200);	
		}
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);

    	}
    	else{

    		$error = array('Status' => "Failed", "msg" => $user_blocked);
			$this->response($this->json($error), 400);
    	}					      

		
} 



/* 
********Nutritionist Dashbboard API***********
*  
*/
		
private function nutritionist_dashboard()
{
		if($this->get_request_method() != "POST")
		{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$nofound_plan = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$nofound_plan = "No se ha encontrado ningún plan.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$nofound_plan = "No plan found.";	
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];
	    $from_user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
		}

		$month_user_sql= mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_registered,nu_usermeta.meta_value 
                            AS user_type
	                    FROM nu_users, nu_usermeta 
	                    WHERE nu_users.ID = nu_usermeta.user_id
	                    AND nu_usermeta.meta_key = 'user_type' 
                            AND nu_usermeta.meta_value = 'User'
	                    AND MONTH( CAST( `user_registered` AS date ) ) = MONTH( NOW( ) )
                            AND YEAR( CAST( `user_registered` AS date ) ) = YEAR( NOW( ) )
	                    order by ID");
        $month_user_cnt = mysql_num_rows($month_user_sql);
        $today = date("F-Y");      
        $result['current_month']=$today;
        $result['total_signup_user']= $month_user_cnt;
        while($month_row = mysql_fetch_array($month_user_sql,MYSQL_ASSOC))
        {
         $rawDate = $month_row["user_registered"];        
		 global $wpdb;
		 $plan_query = "SELECT DISTINCT(sub_plan_title) as title,(plan_title) as plan_title, 
		                count(sub_plan_title) AS count 
		                FROM nu_payments
		                WHERE MONTH( CAST( `payment_date` AS date ) ) = MONTH( NOW( ) ) 
		                AND YEAR( CAST( `payment_date` AS date ) ) = YEAR( NOW( ) ) 
		                GROUP BY sub_plan_title 
		                order by count DESC limit 3";
         $plan_result = $wpdb->get_results($plan_query);
          $row_pk=$wpdb->get_row( $plan_query);      
                if($row_pk==0)
                	{	
                		$result['msg'] = $nofound_plan;
				//added 20170426				
				$result['Status'] = 'Success';
                	}
                	else
                	{
                          foreach($plan_result as $plan_row)
                          {
                   	   $result['Status'] = 'Success';
                           $result['top_3_plan'][]=array('plan_title'=>$plan_row->plan_title,'sub_plan_title'=>$plan_row->title,'count'=>$plan_row->count);          	
                         }
                       }
         $this->response($this->json($result),200);
        }
		$error = array('Status' => "Failed", "msg" => $nofound_plan);
		$this->response($this->json($error), 400);
} 


/* Contact Us API **/
		 
private function contactus()
{
	if($this->get_request_method() != "POST")
	{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_username = "";
	$input_emailid = "";
	$input_msgcontent = "";
	$thk_msg = "";

	if('es' == $applang){
			$input_username = "Por favor, escriba su nombre.";
			$input_emailid = "Por favor introduzca su correo electrónico.";
			$input_msgcontent = "Por favor ingrese su mensaje.";
			$thk_msg = "Gracias por su mensaje. Nos pondremos en contacto con usted pronto.";
	}
	else{
			$input_username = "Please enter your name.";
			$input_emailid = "Please enter your email.";
			$input_msgcontent = "Please enter your message.";
			$thk_msg = "Thank you for your message. We will contact you soon.";			
	}

	if(isset($this->_request['name']))
	$name = $this->_request['name'];	
	if( empty($name) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_username;
		$this->response($this->json($error), 522);
	}
	if(isset($this->_request['email']))
	$email = $this->_request['email'];	
	if( empty($email) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_emailid;
		$this->response($this->json($error), 522);
	}
	if(isset($this->_request['user_messages']))
	$user_messages = $this->_request['user_messages'];	
	if( empty($user_messages) )
	{
		$error['Status'] = 'Failed';	
		$error['msg'] 	 = $input_msgcontent;
		$this->response($this->json($error), 522);
	}
        
	// Input validations
	if(!empty($name) && !empty($email) && !empty($user_messages))
	{
        self::contactusNotificationEmail( $name, $email, $user_messages);
		$result['Status'] = 'Success';	
		$result['msg'] = $thk_msg;		
	}
	// If invalid inputs "Bad Request" status message and reason
	$error = array('Status' => "Success", "msg" => $thk_msg);
	$this->response($this->json($error), 400);
} 
	
		
private function contactusNotificationEmail($name,$email, $user_messages)
	{
		if($this->get_request_method() != "POST"){
			$error['Status'] 	= 'Failed';	
			$error['msg'] 		= 'Not Acceptable';
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$msg_emailsent = "";
		$msg_emailfail = "";

		if('es' == $applang){
				$msg_emailsent = "Correo enviado correctamente.";
				$msg_emailfail = "El correo electrónico no pudo ser enviado.";
		}
		else{
				$msg_emailsent = "Email sent successfully.";
				$msg_emailfail = "Email could not be sent.";	
		}

	   $to 	     = 'vijay.k@exceptionaire.co';
	   $subject  = "Contact From ".$name." ";
	   $message .= "Name: ".$name." <br /><br />\r\n";
	   $message .= "Email: ".$email." <br /><br />\r\n";
	   $message .= "message: ".$user_messages." <br /><br />\r\n";

	   $headers  = 'MIME-Version: 1.0' . PHP_EOL; 
	   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          //echo $message;exit;
	  // More headers
	   //$headers .= $email;
	   $retval 	 = mail($to,$subject,$message,$headers);
			   //echo $retval;
	   if( $retval == true )  
	   {
			$result['Status'] = 'Success';	
			$result['msg'] 	  = $msg_emailsent;						
	   }
	   else
	   {
			$error['Status'] = 'Failed';	
			$error['msg'] 	 = $msg_emailfail;
			$this->response($this->json($error), 509);
	   }
		
	}
	


/*** This API for Nutritionist login to see basic info of un subscribe user if weight graph fails***/

private function un_subscribe_userdetails()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$invalid_userid = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$invalid_userid = "ID de usuario invalido.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$invalid_userid = "Invalid User Id.";	
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

            global $wpdb;
            $profile_query = "select * from " . $wpdb->prefix . "users as u where u.ID='".$user_id."'"; 
            $profile_row=$wpdb->get_row($profile_query);
            $name= $profile_row->display_name;
            $email=$profile_row->user_email;
            $gender= get_user_meta($user_id,"gender", true);
            $allergies= get_user_meta($user_id,"allergiesone", true);
            $gender= get_user_meta($user_id,"gender", true);
            $age= get_user_meta($user_id,"age", true);
            $weight= get_user_meta($user_id,"weight", true);
            $goal= get_user_meta($user_id,"goal", true);
            if($profile_row!='')
            {
            $image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
                if($image_url)
     	        {
                $profile_image = $image_url;
     	        }else
     	        {
                $profile_image="";
        	}	
			$result['Status'] = 'Success';	
			$result['name']=$name;
			$result['email']=$email;
			$result['gender']=$gender;
			$result['allergies']=$allergies;
			$result['age']=$age;
			$result['weight']=$weight;
			$result['goal']=$goal;
			$result['profile_image']=$profile_image;

			$this->response($this->json($result), 200);	
            }
			$error = array('Status' => "Failed", "msg" => $invalid_userid);
	        $this->response($this->json($error), 400);
}





/*** This API for Nutritionist login to see user accepted invitation list***/

private function my_users()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_nutriid = "";
	$userlist_empty = "";

	if('es' == $applang){
			$input_nutriid = "Por favor ingrese su ID de nutricionista.";
			$userlist_empty = "La lista de usuarios está vacía.";
	}
	else{
			$input_nutriid = "Please enter your nutritionist id.";
			$userlist_empty = "User list is empty.";
	}

	if(isset($this->_request['nutritionist_id']))
	$nutritionist_id = $this->_request['nutritionist_id'];
	if( empty($nutritionist_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_nutriid;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$name_query = "select * from " . $wpdb->prefix . "invite_user where nutritionist_id='".$nutritionist_id."' and invite_status='1'";
	$name_result = $wpdb->get_results($name_query);
	if($name_result)
	{
		foreach($name_result as $name_row)
		{
			$user_id=$name_row->user_id;
                        $block_status=$name_row->block_status; 
			$name = (get_user_meta($user_id,"first_name", true));
			$image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
		    if($image_url)
		    {
		    $profile_image = $image_url;
		    }else
		    {
		    $profile_image="";
		    }
			$result['Status'] = 'Success';
			$result['user_list'][]=array(
			'name'=>$name,
			'image_url'=>$profile_image,
			'user_id'=>$name_row->user_id,
                        'block_status'=>$block_status,
			);          			
		}
	    $this->response($this->json($result), 200);	
    }else
    {
      $error = array('Status' => "Success", "msg" => $userlist_empty);
      $this->response($this->json($error), 400);
    }

}


/*** This API for User login to see user accepted Nutritionist list***/

private function my_nutritionist()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$nutrilist_empt = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$nutrilist_empt = "La lista de nutricionistas está vacía.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$nutrilist_empt = "Nutritionist list is empty";	
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$name_query = "select * from " . $wpdb->prefix . "invite_user where user_id='".$user_id."' and invite_status='1'";
	$name_result = $wpdb->get_results($name_query);
	if($name_result)
	{
		foreach($name_result as $name_row)
		{
			$user_id=$name_row->nutritionist_id;
                        $block_status=$name_row->block_status;
			$name = (get_user_meta($user_id,"first_name", true));
			$image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
		    if($image_url)
		    {
		    $profile_image = $image_url;
		    }else
		    {
		    $profile_image="";
		    }
			$result['Status'] = 'Success';
			$result['nutritionist_list'][]=array(
			'name'=>$name,
			'image_url'=>$profile_image,
			'nutritionist_id'=>$name_row->nutritionist_id,
                        'block_status'=>$name_row->block_status,
			);          			
		}
	    $this->response($this->json($result), 200);	
    }else
    {
      $error = array('Status' => "Success", "msg" => $nutrilist_empt);
      $this->response($this->json($error), 400);
    }

}	




/*** This API for Nutritionist login to block user***/

private function block_users()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_nutriid = "";
	$input_userid = "";
	$blockstatus = "";
	$block_succ = "";
	$unblock_succ = "";
	$listempt = "";

	if('es' == $applang){
			$input_nutriid = "Por favor ingrese su ID de nutricionista.";
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$blockstatus = "Por favor, seleccione el estado del bloque.";
			$block_succ = "Usuario bloqueado correctamente.";
			$unblock_succ = "Usuario desbloqueado correctamente.";
			$listempt = "La lista de usuarios está vacía.";
	}
	else{
			$input_nutriid = "Please enter your nutritionist id.";
			$input_userid = "Please enter your user id.";
			$blockstatus = "Please select block status.";
			$block_succ = "User blocked successfully.";
			$unblock_succ = "User unblocked successfully.";
			$listempt = "User list is empty.";	
	}

	if(isset($this->_request['nutritionist_id']))
	$nutritionist_id = $this->_request['nutritionist_id'];
	if( empty($nutritionist_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_nutriid;
	$this->response($this->json($error), 404);
	}
    
	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

    if(isset($this->_request['block_status']))
	$block_status = $this->_request['block_status'];
	if( empty($block_status) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $blockstatus;
	$this->response($this->json($error), 404);
	}
	if(!empty($nutritionist_id) && !empty($user_id) && !empty($block_status))
	{

		if($block_status=='Block')
		{
	
        global $wpdb;
	 $sql_block_user ="UPDATE `nu_invite_user` set `block_status`='Block' WHERE `user_id` LIKE  '".$user_id."' and `nutritionist_id` LIKE  '".$nutritionist_id."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
			$result['Status'] = 'Success';
		$result['msg'] = $block_succ;
		}else
		{
			global $wpdb;
	 $sql_unblock_user ="UPDATE `nu_invite_user` set `block_status`='Unblock' WHERE `user_id` LIKE  '".$user_id."' and `nutritionist_id` LIKE  '".$nutritionist_id."'";
			$row_unblock_user=$wpdb->get_row($sql_unblock_user);
			$result['Status'] = 'Success';
		$result['msg'] = $unblock_succ;
		}	
		
        $this->response($this->json($result), 200);	
	   
    }else
    {
      $error = array('Status' => "Success", "msg" => $listempt);
      $this->response($this->json($error), 400);
    }

}	



/*** This API for User login to block nutritionist***/

private function block_nutritionist()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_nutriid = "";
	$blockstatus = "";
	$nutriblock_succ = "";
	$nutriunblck = "";
	$nutrilist_empt = "";
	
	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_nutriid = "Por favor ingrese su ID de nutricionista.";
			$blockstatus = "Por favor, seleccione el estado del bloque.";
			$nutriblock_succ = "Nutricionista bloqueado con éxito.";
			$nutriunblck = "Nutricionista desbloqueado con éxito.";
			$nutrilist_empt = "La lista de nutricionistas está vacía.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_nutriid = "Please enter your nutritionist id.";
			$blockstatus = "Please select block status.";
			$nutriblock_succ = "Nutritionist blocked successfully.";
			$nutriunblck = "Nutritionist unblocked successfully.";
			$nutrilist_empt = "Nutritionist list is empty.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['nutritionist_id']))
	$nutritionist_id = $this->_request['nutritionist_id'];
	if( empty($nutritionist_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_nutriid;
	$this->response($this->json($error), 404);
	}

    if(isset($this->_request['block_status']))
	$block_status = $this->_request['block_status'];
	if( empty($block_status) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $blockstatus;
	$this->response($this->json($error), 404);
	}
	if(!empty($user_id) && !empty($nutritionist_id) && !empty($block_status))
	{

		if($block_status=='Block')
		{
	
        global $wpdb;
	 $sql_block_user ="UPDATE `nu_invite_user` set `block_status`='Block' WHERE `nutritionist_id` LIKE  '".$nutritionist_id."' and `user_id` LIKE  '".$user_id."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
			$result['Status'] = 'Success';
		$result['msg'] = $nutriblock_succ;
		}else
		{
			global $wpdb;
	 $sql_unblock_user ="UPDATE `nu_invite_user` set `block_status`='Unblock' WHERE `nutritionist_id` LIKE  '".$nutritionist_id."' and `user_id` LIKE  '".$user_id."'";
			$row_unblock_user=$wpdb->get_row($sql_unblock_user);
			$result['Status'] = 'Success';
		$result['msg'] = $nutriunblck;
		}	
		
        $this->response($this->json($result), 200);	
	   
    }else
    {
      $error = array('Status' => "Success", "msg" => $nutrilist_empt);
      $this->response($this->json($error), 400);
    }

}


/*** This API for Nutritionist login to see basic info of un subscribe user ***/

private function perticular_user_screen()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$invalid_userid = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$invalid_userid = "ID de usuario invalido.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$invalid_userid = "Invalid User Id.";	
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];
	if( empty($user_id) )
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}
           

            global $wpdb;
            $profile_query = "select * from " . $wpdb->prefix . "users as u where u.ID='".$user_id."'"; 
            $profile_row=$wpdb->get_row($profile_query);
            $name= $profile_row->display_name;
            $email=$profile_row->user_email;
            $gender= get_user_meta($user_id,"gender", true);
            $allergies= get_user_meta($user_id,"allergiesone", true);
            $gender= get_user_meta($user_id,"gender", true);
            $age= get_user_meta($user_id,"age", true);
            $weight= get_user_meta($user_id,"weight", true);
            $goal= get_user_meta($user_id,"goal", true);
            if($profile_row!='')
            {
            $image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
                if($image_url)
     	        {
                $profile_image = $image_url;
     	        }else
     	        {
                $profile_image="";
        	}
                
                if($allergies)
                {
                $allergiesone= $allergies;
                }else
                {
                $blank_arr=array();
                $allergiesone=$blank_arr;
                }

			$result['Status'] = 'Success';	
			$result['name']=$name;
			$result['email']=$email;
			$result['gender']=$gender;
			$result['allergies']=$allergiesone;
			$result['age']=$age;
			$result['weight']=$weight;
			$result['goal']=$goal;
			$result['profile_image']=$profile_image;

            $plan_query = "select post_id,sub_plan_duration,payment_date from nu_payments where user_id='".$user_id."' order by payment_id ASC"; 
               $paln_result = $wpdb->get_row($plan_query);
               $plan_id= $paln_result->post_id;

               /* show award image when paln expire*/
               $spd = $paln_result->sub_plan_duration;
               $spdvar = $spd; 
               if($spdvar!='')
               {
               $spdChunks = explode(" ", $spdvar);
               $spdvar= $spdChunks[0]; 
               $delidate = $paln_result->payment_date; 
               $monthlyDate = strtotime("+$spdvar week".$delidate);
               $monthly = date("Y/m/d", $monthlyDate);
               $ExpireDate = $monthly;
               }
	           $current_date = date("Y/m/d");
	             
	          $custom_args = array('post_type' => 'plan');
	          $custom_query = new WP_Query( $custom_args ); 
	          if ($custom_query->have_posts())
	          {
		      while ($custom_query->have_posts()) 
		      {
			  $custom_query->the_post();
	          if(have_rows('sub_plan') ):
		      while ( have_rows('sub_plan') ) : the_row();            
			  $award = get_sub_field('award');            
			  $myplan_id=get_the_ID();
			  if($myplan_id ==$plan_id && $award!='') 
			  {
			  $result['Status'] = 'Success';	
			  if($ExpireDate<$current_date)
              {
              $result['award_image']=$award;				                     
              }else
              {
              echo "";
              }	
			  }
			  else 
			  { 
			  echo "";
			  }
		      endwhile;//sub plan
	          endif;//sub plan
		      }//while custom_query
	          }//if custom_query

          
         $query = "select calories,weight from " . $wpdb->prefix . "payments where user_id=".$user_id; 
         $row=$wpdb->get_row($query . " order by payment_id DESC");

         $query_rr = "select current_weight as ww_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."' order by id DESC";
         $row_weight=$wpdb->get_row($query_rr);

         $sql_check_weight = "select count(0) as row_count_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."'";
         $row_weight_kk=$wpdb->get_row($sql_check_weight);

			 $calories_adhicha=$row->calories;

		if($row_weight_kk->row_count_weight>0)
         {
          $before_weight=$row_weight->ww_weight;
          $before_lbs_weight=$before_weight;
         }else
         {
          $before_weight=$row->weight;
          $before_lbs_weight=$before_weight;
         }
                        $reg_user_weight=$row->weight;

			$current_lbs_weight=$current_weight;
			$weight_difference=$current_lbs_weight-$reg_user_weight;
			$current_date = date("Y/m/d");
			         
			$result['current_weight']=$before_lbs_weight."lbs";
            
			$sql_check ="SELECT * FROM  `nu_weight_tracker` WHERE  `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
			$row=$wpdb->get_row($sql_check);

			$sql_check ="SELECT user_id FROM  `nu_weight_tracker` WHERE  `user_id` LIKE  '".$user_id."'";
			$row11=$wpdb->get_row($sql_check);

			if($row==0 && $current_weight!='')
			{
				//echo "insert";
				$sql_check1 ="INSERT INTO `nu_weight_tracker`(`user_id`,`before_weight`,`current_weight`, `current_lbs_weight`, `weight_difference`,`current_date`,`date`) VALUES ('$user_id','$before_weight','$current_weight','$current_lbs_weight','$weight_difference','$current_date','$current_date')";
				$row=$wpdb->get_row($sql_check1);
				$result['current_weight']=$current_weight."lbs";
				
				if($reg_user_weight>=$current_lbs_weight)
				{          
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Lost";
				}else
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Gain";
				}
			}
			else if($current_weight!='')
			{
				//echo "update";
				$sql_check2 ="UPDATE `nu_weight_tracker` set `current_weight`=('$current_weight'),`current_lbs_weight`=('$current_lbs_weight'),`weight_difference`=('$weight_difference') WHERE `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
				$row=$wpdb->get_row($sql_check2);

				if($reg_user_weight>=$current_lbs_weight)
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Lost";
				}else
				{
					
					$result['weight_difference']=$weight_difference;
					$result['weight_text']="lbs Gain";
				}
			}
			elseif($row11!=0)
			{
				//echo "show";
				$sql_check_login ="SELECT before_weight,current_lbs_weight,weight_difference FROM `nu_weight_tracker` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
                $row=$wpdb->get_row($sql_check_login);
                $weight_difference_adhicha=$row->weight_difference;
                 $current_lbs_weight1=$row->current_lbs_weight;
                $before_weight1=$row->before_weight;

                if($before_weight1>=$current_lbs_weight1)
				{
					
					$result['weight_difference']=$weight_difference_adhicha;
					$result['weight_text']="lbs Lost";
				}
				else
				{
					
					$result['weight_difference']=$weight_difference_adhicha;
					$result['weight_text']="lbs Gain";
				}
			} 
			else
			{
				
				$result['weight_difference']='0';
                                $result['weight_text']="";
			} 

            /******************Calories Consumed ***********************/
  
            if(isset($this->_request['calories_burned']))
		     $calories_burned = $this->_request['calories_burned'];
            
             if(isset($this->_request['steps_count']))
		     $steps_count = $this->_request['steps_count'];
             
	    if($calories_adhicha)
            {
            $calories_adhicha=$calories_adhicha;
            }else
            {
            $calories_adhicha="";
            }
	     $result['calories_adhicha']=$calories_adhicha;

	    $sql_check ="SELECT user_id FROM  `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."'";
            $calories_row_blank=$wpdb->get_row($sql_check);          

            $sql_check ="SELECT * FROM  `nu_customcalories` WHERE  `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
            $row=$wpdb->get_row($sql_check);


            if($row==0 && $calories_burned!='' && $steps_count!='')
            {  
	           $sql_check1 ="INSERT INTO `nu_customcalories`(`user_id`, `calories`,`calories_burned`,`steps_count`,  `current_date`) VALUES ('$user_id','$current_calories_req','$calories_burned','$steps_count','$current_date')";
	          $row=$wpdb->get_row($sql_check1);
	          $result['current_calories']=$current_calories_req;
	          if($current_calories_req){$result['current_calories']=$current_calories_req;}
	                               else{$result['current_calories']='0';}
              if($calories_burned){$result['calories_burned']=$calories_burned;;}
	                               else{$result['calories_burned']='0';}
	          if($steps_count){$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}    
            }
            else if($current_calories_req!='' || $calories_burned!='' || $steps_count!='' )
            {
            	if($current_calories_req!='')
            	{
	              $sql_check2 ="UPDATE `nu_customcalories` set `calories`=('$current_calories_req') WHERE `current_date` LIKE  '".$current_date."'";
	             $row=$wpdb->get_row($sql_check2);
	            }

	            if($calories_burned!='' && $steps_count!='')
               {
                 $sql_check2 ="UPDATE `nu_customcalories` set `calories_burned`=('$calories_burned'),`steps_count`=('$steps_count') WHERE `current_date` LIKE  '".$current_date."'";
	             $row=$wpdb->get_row($sql_check2);
               }

	            $sql_check_login ="SELECT calories,calories_burned,steps_count FROM `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
				$row=$wpdb->get_row($sql_check_login);

				$calories_adhicha=$row->calories;
				$calories_burned=$row->calories_burned;
				$steps_count=$row->steps_count;
                
               if($calories_adhicha){$result['current_calories']=$calories_adhicha;}
	                               else{$result['current_calories']='0';}
               if($calories_burned){$result['calories_burned']=$calories_burned;}
	                               else{$result['calories_burned']='0';}
	           if($steps_count){$result['steps_count']=$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}
            }
            elseif($calories_row_blank!=0)
			{
				$sql_check_login ="SELECT calories,calories_burned,steps_count FROM `nu_customcalories` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC LIMIT 0, 1";
				$row=$wpdb->get_row($sql_check_login);
				$calories_adhicha=$row->calories;
				$calories_burned=$row->calories_burned;
				$steps_count=$row->steps_count;
				
                if($calories_adhicha){$result['current_calories']=$calories_adhicha;}
	                               else{$result['current_calories']='0';}
               if($calories_burned){$result['calories_burned']=$calories_burned;}
	                               else{$result['calories_burned']='0';}
	           if($steps_count){$result['steps_count']=$result['steps_count']=$steps_count;}
	                               else{$result['steps_count']='0';}		
			} 
           else
           {
          	$result['current_calories']='0';
          	$result['calories_burned']='0';
          	$result['steps_count']='0';
           }
                          
     
			$this->response($this->json($result), 200);	
            }
			$error = array('Status' => "Failed", "msg" => $invalid_userid);
	        $this->response($this->json($error), 400);
}




	/* 
********Memberships Page API***********
*/

private function memberships_page()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$no_member = "";
		$not_founduser = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$no_member = "Usted no es miembro.";
				$not_founduser = "Usuario no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_userid = "Please enter your user id.";
				$no_member = "You are not member.";
				$not_founduser = "User not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}
		global $wpdb;
         $sql_graph = "select count(0) as row_count_user from ".$wpdb->prefix."payments where user_id='".$user_id."'";
         $row_graph=$wpdb->get_row($sql_graph);

         if($row_graph->row_count_user>0)
         {	
	          $result['Status'] = 'Success';
	          
	          $sql_user="select plan_title,sub_plan_title,payment_gross,recuring,sub_plan_duration,payment_date from " . $wpdb->prefix . "payments where user_id='".$user_id."' order by payment_id DESC";
	          $row_user=$wpdb->get_results($sql_user);
	          if($row_user)
	          {

		          foreach ($row_user as $user_weight) 
		          {
		          	    /* show award image when paln expire*/
                        $spd = $user_weight->sub_plan_duration;
                        $spdvar = $spd; 
                        if($spdvar!='')
                        {
                         $spdChunks = explode(" ", $spdvar);
                         $spdvar= $spdChunks[0]; 
                         $delidate = $user_weight->payment_date; 
                         $monthlyDate = strtotime("+$spdvar week".$delidate);
                         $monthly = date("Y/m/d", $monthlyDate);
                         $ExpireDate = $monthly;
                        }

                        $recuring = $user_weight->recuring;
                        if($recuring=='No')
                        {
                          $member_type='One Time Purchase';
                        }else
                        {
                          $member_type='A Month';
                        }

			          $result['Memberships Details'][]=array(
			          'plan_title'=>$user_weight->plan_title,
			          'sub_plan_title'=>$user_weight->sub_plan_title,
			          'plan_price'=>$user_weight->payment_gross,
			          'sub_plan_duration'=>$user_weight->sub_plan_duration,
			          'member_type'=>$member_type,
			          'start_date'=>$user_weight->payment_date,
			          'expire_date'=>$ExpireDate
			          );          		       
		          }
	          }
	          else
	          {
		          $result['Status'] = 'Failed';
			      $result['msg'] = $no_member;
	          }
          }
          else
          {
	          $result['Status'] = 'Failed';
		      $result['msg'] = $not_founduser;
          }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}

	/* 
********Protien Carb Fat API***********
*/

private function protien_carb_fat()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$no_member = "";
		$nofound_user ="";
		$invalid_userid = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$no_member = "Usted no es miembro.";
			$nofound_user ="Usuario no encontrado.";
			$invalid_userid = "ID de usuario invalido.";	
		}
		else{
			$input_userid = "Please enter your user id.";
			$no_member = "You are not member.";
			$nofound_user ="User not found.";
			$invalid_userid = "Invalid user id.";	
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}
		global $wpdb;
         $sql_graph = "select count(0) as row_count_user from ".$wpdb->prefix."payments where user_id='".$user_id."'";
         $row_graph=$wpdb->get_row($sql_graph);

         if($row_graph->row_count_user>0)
         {	
	          $result['Status'] = 'Success';
	          
	          $sql_user="select protein,carb,fat,protein_gram,carb_gram,fat_gram,calories,payment_date,sub_plan_duration from " . $wpdb->prefix . "payments where user_id='".$user_id."' order by payment_id DESC";
	          $row_user=$wpdb->get_row($sql_user);


	          if($row_user)
	          {           
		     $plan_start_date=$row_user->payment_date;
                     $spd = $row_user->sub_plan_duration;
                     $spdvar = $spd; 
                      if($spdvar!='')
                       {
                        $spdChunks = explode(" ", $spdvar);
                        $spdvar= $spdChunks[0]; 
                        $delidate = $row_user->payment_date; 
                        $monthlyDate = strtotime("+$spdvar week".$delidate);
                        $monthly = date("Y/m/d", $monthlyDate);
                        // ABL edited 20170822
                        //$plan_expire_date = $monthly;
                        $plan_expire_date = date('Y/m/d', strtotime('-1 day', strtotime($monthly)));
                       }

					 $result["protein in gram"] = $row_user->protein_gram;
					 $result["carb in gram"] = $row_user->carb_gram;
					 $result["fat in gram"] = $row_user->fat_gram;
                                         $result["fat in gram"] = $row_user->fat_gram;
                                         $result["calories"] = $row_user->calories;
                                         $result["plan_start_date"] = $plan_start_date;
					 $result["plan_end_date"] = $plan_expire_date;
                                         $allergies = get_user_meta($user_id,"allergiesone", true);
                                         if($allergies)
                                         {
                                           $allergiesone= $allergies;
                                         }else
                                         {
                                         $blank_arr=array();
                                         $allergiesone=$blank_arr;
                                         }
	                                 $result['allergiesone']=$allergiesone;
                     
                     
	          }
	          else
	          {
		          $result['Status'] = 'Failed';
			      $result['msg'] = $no_member;
	          }
          }
          else
          {
	          $result['Status'] = 'Failed';
		      $result['msg'] = $nofound_user;
          }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}



private function get_block_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_userid = "";
		$input_mealtype = "";
		$input_recipedate = "";
		$recipe_fav = "";
		$recipe_blck_succ = "";
		$removeblock_succ = "";
		$nofound_recipeuserid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$input_mealtype = "Ingrese su tipo de comida.";
				$input_recipedate = "Por favor envíe la fecha de la receta.";
				$recipe_fav = "La receta ya se ha añadido como favoritos.";
				$recipe_blck_succ = "Se agregó receta a elementos bloqueados correctamente.";
				$removeblock_succ = "La receta se eliminó de los elementos bloqueados correctamente.";
				$nofound_recipeuserid = "Identificador de la receta o del usuario no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_userid = "Please enter your user id.";
				$input_mealtype = "Please enter your meal type.";
				$input_recipedate = "Please send recipe date.";
				$recipe_fav = "Recipe already added as favorites.";
				$recipe_blck_succ = "Recipe added to blocked items successfully.";
				$removeblock_succ = "Recipe removed from blocked items successfully.";
				$nofound_recipeuserid = "Recipe id or User id not found.";
				$invalid_userid = "Invalid User Id.";
		}

		if(isset($this->_request['blk_recipe_id']))
		$blk_recipe_id = $this->_request['blk_recipe_id'];

		if( empty($blk_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['blk_user_id']))
		$blk_user_id = $this->_request['blk_user_id'];

		if( empty($blk_user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['blk_recipe_name']))
		$blk_recipe_name = $this->_request['blk_recipe_name'];

		if(isset($this->_request['blk_recipe_image']))
		$blk_recipe_image = $this->_request['blk_recipe_image'];
                
                if(isset($this->_request['blk_recipe_calories']))
		$blk_recipe_calories = $this->_request['blk_recipe_calories'];
        
		if(isset($this->_request['blk_recipe_protien']))
		$blk_recipe_protien = $this->_request['blk_recipe_protien'];

	        if(isset($this->_request['blk_recipe_fats']))
		$blk_recipe_fats = $this->_request['blk_recipe_fats'];

	        if(isset($this->_request['blk_recipe_carbs']))
		$blk_recipe_carbs = $this->_request['blk_recipe_carbs'];

                if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

		if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		}
 		
 		// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$blk_user_id."'";
			$getdate = $wpdb->get_row($getplans_date);
		// Get plan date end

		if(!empty($blk_recipe_id) and !empty($blk_user_id) and !empty($meal_type))
		{
			global $wpdb;
			$alredy_exist_item= "select * from " . $wpdb->prefix . "block_items where recipe_id='".$blk_recipe_id."' and user_id='".$blk_user_id."' ";
            $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);

          $alredy_exist_item= "select * from " . $wpdb->prefix . "block_items where recipe_id='".$blk_recipe_id."' and user_id='".$blk_user_id."' ";
            $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);


             $alredy_favorites_item= "select recipe_id from " . $wpdb->prefix . "favorites_items where recipe_id='".$blk_recipe_id."' and user_id='".$blk_user_id."' ";
            $alredy_favorites_item_row = $wpdb->get_row($alredy_favorites_item);
           
           if($alredy_favorites_item_row->recipe_id!='')
            {

		    $result['Status'] = 'Failed';
	        $result['msg'] = $recipe_fav;
	       }
            
            else if($alredy_exist_item_row->recipe_id=='')
            {
		    $sql_block_items = "INSERT INTO nu_block_items (recipe_id,user_id,recipe_name,recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs) VALUES ('".$blk_recipe_id."','".$blk_user_id."','".$blk_recipe_name."','".$blk_recipe_image."','".$blk_recipe_calories."','".$blk_recipe_protien."','".$blk_recipe_fats."','".$blk_recipe_carbs."')";
		    $res_block_items = $wpdb->get_row($sql_block_items);


                    
            $recipe_current_date=$recipe_date; 

		    // $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='1' WHERE user_id='".$blk_user_id."' and meal_type='".$meal_type."' and recipe_id='".$blk_recipe_id."' and recipe_current_date='".$recipe_current_date."'";

		    $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='1' WHERE user_id='".$blk_user_id."' and recipe_id='".$blk_recipe_id."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";

				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

		    $result['Status'] = 'Success';
	        $result['msg'] = $recipe_blck_succ;
	       }else
	       {
                $sql = "Delete from nu_block_items where recipe_id='".$blk_recipe_id."' and user_id='".$blk_user_id."'";
	        $res = $wpdb->get_row($sql);
      
                $recipe_current_date=$recipe_date;

	        // $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='0' WHERE user_id='".$blk_user_id."' and meal_type='".$meal_type."' and recipe_id='".$blk_recipe_id."' and recipe_current_date='".$recipe_current_date."'";

                $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='0' WHERE user_id='".$blk_user_id."' and recipe_id='".$blk_recipe_id."' and recipe_current_date >= '".$getdate->payment_date."' AND  recipe_current_date <= '".$getdate->plan_expire_date."'";

				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

	       	$result['Status'] = 'Success';
                $result['recipe_status'] = 'Block';
	        $result['msg'] = $removeblock_succ;
	       }

        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeuserid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


	/* 
********Show Block Items API***********
*/

private function show_block_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$blklist_empt ="";
		$notfound_userid = "";
		$invalid_userid = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$blklist_empt ="La lista de elementos de bloque está vacía.";
			$notfound_userid = "No se ha encontrado la identificación de usuario.";
			$invalid_userid = "ID de usuario invalido.";
		}
		else{
			$input_userid = "Please enter your user id.";
			$blklist_empt ="Block item list is empty.";
			$notfound_userid = "User id not found.";
			$invalid_userid = "Invalid user id.";	
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(!empty($user_id))
		{
			 global $wpdb;
	         $block_items_query = "select * from " . $wpdb->prefix . "block_items where user_id='".$user_id."'";
	         $block_items_result = $wpdb->get_results($block_items_query. " order by id DESC");
	         if($block_items_result)
	         {
		         foreach($block_items_result as $block_items_row)
		         {
		         $result['Status'] = 'Success';	
		         $result['Block Items Details'][]=array('recipe_id'=>$block_items_row->recipe_id,'recipe_name'=>$block_items_row->recipe_name,'recipe_image'=>$block_items_row->recipe_image,'recipe_calories'=>$block_items_row->recipe_calories,'recipe_protien'=>$block_items_row->recipe_protien,'recipe_fats'=>$block_items_row->recipe_fats,'recipe_carbs'=>$block_items_row->recipe_carbs);
		       }
	         }else
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$blklist_empt;

	         }
       }
       else
       {
	    $result['Status'] = 'Failed';
	    $result['msg'] = $notfound_userid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


	/* 
********Delete Block Items API***********
*/

private function delete_block_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_userid = "";
		$recipeblk_removesucc = "";
		$nofound_recipeuserid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$recipeblk_removesucc = "La receta se eliminó de los elementos bloqueados correctamente.";
				$nofound_recipeuserid = "Identificador de la receta o del usuario no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_userid = "Please enter your user id.";
				$recipeblk_removesucc = "Recipe removed from blocked items successfully.";
				$nofound_recipeuserid = "Recipe id or User id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['delblock_recipe_id']))
		$delblock_recipe_id = $this->_request['delblock_recipe_id'];

		if( empty($delblock_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['delblock_user_id']))
		$delblock_user_id = $this->_request['delblock_user_id'];

		if( empty($delblock_user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$delblock_user_id."'";
			$getdate = $wpdb->get_row($getplans_date);
		// Get plan date end

		if(!empty($delblock_recipe_id) and !empty($delblock_user_id))
		{
			global $wpdb;
			$sql = "Delete from nu_block_items  where recipe_id='".$delblock_recipe_id."' and user_id='".$delblock_user_id."'";
	        $res = $wpdb->get_row($sql);

$recipe_current_date=date("Y/m/d"); 
		    $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='0' WHERE user_id='".$delblock_user_id."' and recipe_id='".$delblock_recipe_id."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

		    $result['Status'] = 'Success';
	        $result['msg'] = $recipeblk_removesucc;
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeuserid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}





/* 
********Get Favorites Items API***********
*/

private function get_favorites_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_userid = "";
		$input_mealtype = "";
		$send_recipedate = "";
		$already_blocked = "";
		$add_recipefav = "";
		$remove_favsucc = "";
		$nofound_recipeuserid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$input_mealtype = "Ingrese su tipo de comida.";
				$send_recipedate = "Por favor envíe la fecha de la receta.";
				$already_blocked = "Receta ya agregada como bloqueada.";
				$add_recipefav = "La receta se agregó a los favoritos correctamente.";
				$remove_favsucc = "Se ha eliminado la receta de los favoritos correctamente.";
				$nofound_recipeuserid = "Identificador de la receta o del usuario no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_userid = "Please enter your user id.";
				$input_mealtype = "Please enter your meal type.";
				$send_recipedate = "Please send recipe date.";
				$already_blocked = "Recipe already added as blocked.";
				$add_recipefav = "Recipe added to favorites successfully.";
				$remove_favsucc = "Recipe removed from favorites successfully.";
				$nofound_recipeuserid = "Recipe id or User id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['fav_recipe_id']))
		$fav_recipe_id = $this->_request['fav_recipe_id'];

		if( empty($fav_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['fav_user_id']))
		$fav_user_id = $this->_request['fav_user_id'];

		if( empty($fav_user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}
        
        if(isset($this->_request['fav_recipe_name']))
		$fav_recipe_name = $this->_request['fav_recipe_name'];

		if(isset($this->_request['fav_recipe_image']))
		$fav_recipe_image = $this->_request['fav_recipe_image'];
                
                if(isset($this->_request['fav_recipe_calories']))
		$fav_recipe_calories = $this->_request['fav_recipe_calories'];
        
		if(isset($this->_request['fav_recipe_protien']))
		$fav_recipe_protien = $this->_request['fav_recipe_protien'];

	        if(isset($this->_request['fav_recipe_fats']))
		$fav_recipe_fats = $this->_request['fav_recipe_fats'];

	        if(isset($this->_request['fav_recipe_carbs']))
		$fav_recipe_carbs = $this->_request['fav_recipe_carbs'];

                if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

		if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $send_recipedate;
			$this->response($this->json($error), 404);
		}

		// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$fav_user_id."'";
			$getdate = $wpdb->get_row($getplans_date);
		// Get plan date end

		if(!empty($fav_recipe_id) and !empty($fav_user_id) && !empty($meal_type))
		{
			global $wpdb;
			$alredy_exist_item= "select * from " . $wpdb->prefix . "favorites_items where recipe_id='".$fav_recipe_id."' and user_id='".$fav_user_id."' ";
            $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);
      
             $alredy_block_item= "select recipe_id from " . $wpdb->prefix . "block_items where recipe_id='".$fav_recipe_id."' and user_id='".$fav_user_id."' ";
            $alredy_block_item_row = $wpdb->get_row($alredy_block_item);
           
           if($alredy_block_item_row->recipe_id!='')
            {

		    $result['Status'] = 'Failed';
	        $result['msg'] = $already_blocked;
	       }
           else if($alredy_exist_item_row->recipe_id=='')
            {
			$sql_block_items = "INSERT INTO nu_favorites_items (recipe_id,user_id,recipe_name,recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs) VALUES ('".$fav_recipe_id."','".$fav_user_id."','".$fav_recipe_name."','".$fav_recipe_image."','".$fav_recipe_calories."','".$fav_recipe_protien."','".$fav_recipe_fats."','".$fav_recipe_carbs."')";
		    $res_block_items = $wpdb->get_row($sql_block_items);

                    $recipe_current_date=$recipe_date;
	        $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='1' WHERE user_id='".$fav_user_id."' and recipe_id='".$fav_recipe_id."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."' ";
		  $row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

		$result['Status'] = 'Success';
	        $result['msg'] = $add_recipefav;
	        }else
	        {
                $sql = "Delete from nu_favorites_items where recipe_id='".$fav_recipe_id."' and user_id='".$fav_user_id."'";
	        $res = $wpdb->get_row($sql);
 
                $recipe_current_date=$recipe_date;
	         $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='0' WHERE user_id='".$fav_user_id."' and recipe_id='".$fav_recipe_id."' and  recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
		$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

	       	$result['Status'] = 'Success';
                $result['recipe_status'] = 'Unblock';
	        $result['msg'] = $remove_favsucc;
	        }
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeuserid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}



/* 
********Show Favorites Items API***********
*/

private function show_favorites_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$favitemlist_empt = "";
		$nofound_userid ="";
		$invalid_userid = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$favitemlist_empt = "La lista de elementos favoritos está vacía.";
			$nofound_userid ="No se ha encontrado la identificación de usuario.";
			$invalid_userid = "ID de usuario invalido.";	
		}
		else{
			$input_userid = "Please enter your user id.";
			$favitemlist_empt = "Favorites item list is empty.";
			$nofound_userid ="User id not found.";
			$invalid_userid = "Invalid user id.";	
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(!empty($user_id))
		{
			 global $wpdb;
	         $favorites_items_query = "select * from " . $wpdb->prefix . "favorites_items where user_id='".$user_id."'";
             $favorites_items_result = $wpdb->get_results($favorites_items_query. " order by id DESC");
	        if($favorites_items_result)
                {
                 foreach($favorites_items_result as $favorites_items_row)
                 {
		         $result['Status'] = 'Success';	
		         $result['Favorites Items Details'][]=array('recipe_id'=>$favorites_items_row->recipe_id,'recipe_name'=>$favorites_items_row->recipe_name,'recipe_image'=>$favorites_items_row->recipe_image,'recipe_calories'=>$favorites_items_row->recipe_calories,'recipe_protien'=>$favorites_items_row->recipe_protien,'recipe_fats'=>$favorites_items_row->recipe_fats,'recipe_carbs'=>$favorites_items_row->recipe_carbs);
		 }
	       }else
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$favitemlist_empt;

	         }
       }
       else
       {
	    $result['Status'] = 'Failed';
	    $result['msg'] = $nofound_userid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


	/* 
********Delete Favorites Items API***********
*/

private function delete_favorites_items()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_userid = "";
		$remove_recipefav = "";
		$nofound_recipeuserid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$remove_recipefav = "Se ha eliminado la receta de los favoritos correctamente.";
				$nofound_recipeuserid = "Identificador de la receta o del usuario no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_userid = "Please enter your user id.";
				$remove_recipefav = "Recipe removed from favourites successfully.";
				$nofound_recipeuserid = "Recipe id or User id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['delfav_recipe_id']))
		$delfav_recipe_id = $this->_request['delfav_recipe_id'];

		if( empty($delfav_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['delfav_user_id']))
		$delfav_user_id = $this->_request['delfav_user_id'];

		if( empty($delfav_user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$delfav_user_id."'";
			$getdate = $wpdb->get_row($getplans_date);
		// Get plan date end

		if(!empty($delfav_recipe_id) and !empty($delfav_user_id))
		{
			global $wpdb;
			$sql = "Delete from nu_favorites_items  where recipe_id='".$delfav_recipe_id."' and user_id='".$delfav_user_id."'";
	        $res = $wpdb->get_row($sql);

$recipe_current_date=date("Y/m/d"); 
		    $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='0' WHERE user_id='".$delfav_user_id."' and recipe_id='".$delfav_recipe_id."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

		    $result['Status'] = 'Success';
	        $result['msg'] = $remove_recipefav;
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeuserid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


/* 
********Get Yumlly API Data first time***********
*/

private function save_yummly_response()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$input_mealplan = "";
		$add_mealplansucc = "";
		$faildto_addmealplan = "";
		$invalid_userid = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_mealplan = "Ingrese su plan de comidas.";
			$add_mealplansucc = "Plan de comidas añadido con éxito.";
			$faildto_addmealplan = "No se pudo agregar el plan de comidas.";
			$invalid_userid = "ID de usuario invalido.";
		}
		else{
			$input_userid = "Please enter your user id.";
			$input_mealplan = "Please enter your meal plan.";
			$add_mealplansucc = "Meal plan added sucessfully.";
			$faildto_addmealplan = "Failed to add meal plan.";
			$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['meal_plan_json']))
		$meal_plan_json = $this->_request['meal_plan_json'];

		if( empty($meal_plan_json) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealplan;
			$this->response($this->json($error), 404);
		}

       $array = json_decode(stripslashes($meal_plan_json), true);

                //$onlyconsonants = stripslashes($meal_plan_json);
		//$array = json_decode(stripslashes($onlyconsonants), true);

       if(!empty($user_id) and !empty($meal_plan_json))
	   {
	       $meal_types = array("Breakfast","Lunch","Dinner","Snacks");
	       $count_meal_type = count($meal_types);
	       for ($j=0; $j <$count_meal_type ; $j++) 
	       { 
			  //echo "<pre>";print_r($meal_types[$j]);
			   for ($i=0; $i <count($array[$meal_types[$j]]); $i++) 
			   { 
			       $meal_type=$meal_types[$j];
			       $recipe_name=$array[$meal_types[$j]][$i]['recipe_name'];
			       $recipe_image=$array[$meal_types[$j]][$i]['recipe_image'];
			       $recipe_calories=$array[$meal_types[$j]][$i]['recipe_calories'];
			       $recipe_protien=$array[$meal_types[$j]][$i]['recipe_protien'];
			       $recipe_fats=$array[$meal_types[$j]][$i]['recipe_fats'];
			       $recipe_carbs=$array[$meal_types[$j]][$i]['recipe_carbs'];
			       $recipe_ingredients=$array[$meal_types[$j]][$i]['recipe_ingredients'];
			       $ingrdientsLines=$array[$meal_types[$j]][$i]['recipe_ingredients_value'];
			       $recipe_serving = $array[$meal_types[$j]][$i]['recipe_serving']; 		
			       $recipe_id=$array[$meal_types[$j]][$i]['recipe_id'];
                               $source_recipe_url=$array[$meal_types[$j]][$i]['source_recipe_url'];
                               $direction=$array[$meal_types[$j]][$i]['direction'];
			       $recipe_current_date = date("Y/m/d");

			       global $wpdb;
			       $sql_query_meal ="INSERT INTO ".$wpdb->prefix."meal_plan(user_id,meal_type,recipe_name, recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_ingredients,recipe_ingredients_value,recipe_serving,recipe_id,source_recipe_url,direction,favorite_status,blocked_status,recipe_current_date) 
						VALUES ('$user_id','$meal_type','$recipe_name','$recipe_image','$recipe_calories','$recipe_protien','$recipe_fats','$recipe_carbs','$recipe_ingredients','$ingrdientsLines','$recipe_serving','$recipe_id','$source_recipe_url','$direction','0','0','$recipe_current_date')";
				  $row_meal=$wpdb->query($sql_query_meal);
		       }//for i 
	       }//for j
	       if($row_meal)
	       {
                 //insert data in plan_history table first time start
                  global $wpdb; 
		   $plan_payment  = "SELECT calories,carb_gram,protein_gram,fat_gram FROM ".$wpdb->prefix."payments where user_id = '".$user_id."' order by payment_id DESC";
	            $row_plan_payment = $wpdb->get_row($plan_payment);

	            $plan_history ="insert into ".$wpdb->prefix."plan_history(user_id,old_calories,old_protien, old_fats,old_carbs,old_date) VALUES ('".$user_id."','".$row_plan_payment->calories."','".$row_plan_payment->protein_gram."','".$row_plan_payment->fat_gram."','".$row_plan_payment->carb_gram."','".$recipe_current_date."')";
		        $row_plan_history=$wpdb->query($plan_history);
		 //insert data in plan_history table first time end     
  
	       $result['Status'] = 'Success';
	       $result['msg'] = $add_mealplansucc;
               
	      }else
	      {
	      	$result['Status'] = 'Failed';
	       $result['msg'] = $faildto_addmealplan;
	      }
       }//if
       else
       {
		    $result['Status'] = 'Failed';
		    $result['msg'] = $invalid_userid;
       }
       $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


/* 
********Show yummly response API***********
*/

private function get_saved_yummly_response()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$input_recipedate = "";
		$nodata_found = "";
		$nofound_userid = "";
		$invalid_userid = "";

		if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_recipedate = "Por favor envíe la fecha de la receta.";
			$nodata_found = "Datos no encontrados.";
			$nofound_userid = "No se ha encontrado la identificación de usuario.";
			$invalid_userid = "ID de usuario invalido.";
		}
		else{
			$input_userid = "Please enter your user id.";
			$input_recipedate = "Please send recipe date.";
			$nodata_found = "No data found.";
			$nofound_userid = "User id not found.";
			$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];
		
		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		}

                
               //echo "hello".$user_id;   
                
		if(!empty($user_id))
		{
	     global $wpdb;
	     $current_date = $recipe_date;
	   $yummly_response_query = "select * from " . $wpdb->prefix . "meal_plan where user_id='".$user_id."' and recipe_current_date='".$current_date."'";
         $yummly_response_result = $wpdb->get_results($yummly_response_query. " order by id DESC");
         
         
//         echo "hello"; 
//         
//         echo "<pre>";
//         print_r($yummly_response_result);
//         echo "</pre>";
//        // exit;
         
         
	     if($yummly_response_result)
         {
          $meal_types = array("Breakfast","Lunch","Dinner","Snacks");
	      $count_meal_type = count($meal_types);
	      for ($j=0; $j <$count_meal_type; $j++) 
	      { 
	      $yummly_response = "select * from " . $wpdb->prefix . "meal_plan where user_id='".$user_id."' and recipe_current_date='".$current_date."' and meal_type='".$meal_types[$j]."'";
          $yummly_response = $wpdb->get_results($yummly_response. " order by id DESC");
          $array = json_decode(json_encode($yummly_response), True);
        		$finalRescArr = array();

			   for ($i=0; $i <count($array); $i++) 
			   {
			    if($array[$i]['meal_type'] == $meal_types[$j] ){
			    	$finalRescArr[$meal_types[$j]][] = $array[$i];
			    }
			   }
               $result['Status'] = 'Success';
			  $result['Yummly_Response_Details'] [$meal_types[$j]]= $finalRescArr[$meal_types[$j]];
			} 
			

	       }else
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$nodata_found;

	         }
       }
       else
       {
	    $result['Status'] = 'Failed';
	    $result['msg'] = $nofound_userid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


/* 
********Add Meal Items to meal type API***********
*/

private function add_meal()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_mealtype = "";
		$input_recipedate = "";
		$item_addsucc = "";
		$recipe_already = "";
		$nofound_recipeid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_mealtype = "Ingrese su tipo de comida.";
				$input_recipedate = "Por favor envíe la fecha de la receta.";
				$item_addsucc = "Elemento añadido correctamente.";
				$recipe_already = "La receta ya existe.";
				$nofound_recipeid = "No se encontró el ID de receta.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_mealtype = "Please enter your meal type.";
				$input_recipedate = "Please send recipe date.";
				$item_addsucc = "Item added successfully.";
				$recipe_already = "Recipe already exists.";
				$nofound_recipeid = "Recipe id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

	    if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}

        if(isset($this->_request['recipe_name']))
		$recipe_name = $this->_request['recipe_name'];

		if(isset($this->_request['recipe_image']))
		$recipe_image = $this->_request['recipe_image'];

	    if(isset($this->_request['recipe_calories']))
		$recipe_calories = $this->_request['recipe_calories'];

		if(isset($this->_request['recipe_protien']))
		$recipe_protien = $this->_request['recipe_protien'];

	    if(isset($this->_request['recipe_fats']))
		$recipe_fats = $this->_request['recipe_fats'];

		if(isset($this->_request['recipe_carbs']))
		$recipe_carbs = $this->_request['recipe_carbs'];

	    if(isset($this->_request['recipe_ingredients']))
		$recipe_ingredients = $this->_request['recipe_ingredients'];

 	    

		if(isset($this->_request['ingrdientsLines']))
		$ingrdientsLines = $this->_request['ingrdientsLines'];

	    if(isset($this->_request['servings']))
		$servings = $this->_request['servings'];

	    if(isset($this->_request['recipe_id']))
		$recipe_id = $this->_request['recipe_id'];

            if(isset($this->_request['source_recipe_url']))
		$source_recipe_url = $this->_request['source_recipe_url']; 
             
            if(isset($this->_request['direction']))
		$direction = $this->_request['direction'];    
		
            if(isset($this->_request['favorite_status']))
		$favorite_status = $this->_request['favorite_status'];

	    if(isset($this->_request['blocked_status']))
		$blocked_status = $this->_request['blocked_status'];  

		if(isset($this->_request['original_serving']))
		$original_serving = $this->_request['original_serving'];

		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];
		if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		}

		// Conversion grocery START
				  
				  $afterconvert = array();
				  $converted = array();

				  $ingrdientsLines = stripslashes(html_entity_decode($ingrdientsLines));
				  $final_ingredientline = json_decode($ingrdientsLines,TRUE);
				  
				  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {


				  	$convertvalue_unit = "";
				    $convertvalue_amount = "";
				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){

				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
				      }
				      else{
				        $convertvalue_unit = $convert_value['targetunit'];
				        $convertvalue_amount = $convert_value['targetamount'];
				      }
				      unset($convert_value);

				    $afterconvert['id'] = $final_ingredientline[$gg]['id'];
				  	$afterconvert['name'] = str_replace("'", "", $final_ingredientline[$gg]['name']);
				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
				  	$afterconvert['image'] = str_replace("'", "", $final_ingredientline[$gg]['image']);
				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
				  	$afterconvert['amount'] = $convertvalue_amount;
				  	$afterconvert['unit'] = $convertvalue_unit;
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

				  	$converted[$gg] = $afterconvert;
				    unset($afterconvert);
				  }
				  
				  $ingrdientsLines = json_encode($converted);
				  unset($converted);

				  

				  // Conversion grocery END


		if(!empty($user_id) && !empty($meal_type) and !empty($recipe_id))
		{
			global $wpdb;
			$recipe_current_date = $recipe_date;
			$alredy_exist_item= "select * from ".$wpdb->prefix."meal_plan where user_id='".$user_id."' and meal_type='".ucfirst($meal_type)."' and recipe_id='".$recipe_id."' and recipe_current_date = '".$recipe_current_date."' ";
            $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);
            if($alredy_exist_item_row->recipe_id=='')
            {
			$sql_add_meal = "INSERT INTO nu_meal_plan(user_id,meal_type,recipe_name,recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_ingredients,recipe_ingredients_value,recipe_serving,original_serving,recipe_id,source_recipe_url,direction,favorite_status,blocked_status,recipe_current_date) VALUES ('".$user_id."','".ucfirst($meal_type)."','".$recipe_name."','".$recipe_image."','".$recipe_calories."','".$recipe_protien."','".$recipe_fats."','".$recipe_carbs."','".$recipe_ingredients."','".$ingrdientsLines."','".$servings."','".$original_serving."','".$recipe_id."','".$source_recipe_url."','".$direction."','".$favorite_status."','".$blocked_status."','".$recipe_current_date."')";
		    $res_add_meal = $wpdb->get_row($sql_add_meal);
		    $result['Status'] = 'Success';
		    $result['ingrdientsLines'] = $ingrdientsLines;
	        $result['msg'] = $item_addsucc;
	        }else
	        {
            $result['Status'] = 'failed';
	        $result['msg'] = $recipe_already;
	        }
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


	/* 
********Delete Meal Items API***********
*/

private function remove_meal()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipeid = "";
		$input_mealtype = "";
		$input_recipedate = "";
		$nofound_recipeid = "";
		$remove_recipesucc = "";
		$recipeuserdateid_nofound = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipeid = "Ingrese su ID de receta.";
				$input_mealtype = "Ingrese su tipo de comida.";
				$input_recipedate = "Por favor envíe la fecha de la receta.";
				$nofound_recipeid = "No se encontró el ID de receta.";
				$remove_recipesucc = "La receta se eliminó correctamente.";
				$recipeuserdateid_nofound = "ID de usuario, recipe_date o id de receta no encontrado.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipeid = "Please enter your recipe id.";
				$input_mealtype = "Please enter your meal type.";
				$input_recipedate = "Please send recipe date.";
				$nofound_recipeid = "Recipe id not found.";
				$remove_recipesucc = "Recipe removed successfully.";
				$recipeuserdateid_nofound = "User id, recipe_date or recipe id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

	    if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}
        
        if(isset($this->_request['recipe_id']))
		$recipe_id = $this->_request['recipe_id'];

	    if( empty($recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}
      	
		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		}

		if(!empty($user_id) and !empty($meal_type) and !empty($recipe_id) and !empty($recipe_date))
		{
			global $wpdb;
			$current_date = $recipe_date;

			$recipe_current_date = $recipe_date;
			$alredy_exist_item= "select * from ".$wpdb->prefix."meal_plan where user_id='".$user_id."' and meal_type='".$meal_type."' and recipe_id='".$recipe_id."' and recipe_current_date='".$current_date."'";
            $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);
            if($alredy_exist_item_row->recipe_id=='')
            {
            $result['Status'] = 'Failed';
	        $result['msg'] = $nofound_recipeid;
	       }else
	       {
            $sql_remove_meal = "Delete from ".$wpdb->prefix."meal_plan where user_id='".$user_id."' and meal_type='".$meal_type."' and recipe_id='".$recipe_id."' and recipe_current_date='".$current_date."'";
             $row_remove_meal=$wpdb->get_row($sql_remove_meal);

		    $result['Status'] = 'Success';
	        $result['msg'] = $remove_recipesucc;

	      }
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $recipeuserdateid_nofound;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}


	/* 
********Regenerate Meal Items API***********
*/

private function regenerate_meal()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_recipedate = "";
		$input_userid = "";
		$input_mealtype = "";
		$input_newrecipeid = "";
		$input_oldrecipeid = "";
		$regenerat_succ = "";
		$nofound_recipeuserid = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_recipedate = "Por favor envíe la fecha de la receta.";
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$input_mealtype = "Ingrese su tipo de comida.";
				$input_newrecipeid = "Ingrese el nuevo ID de receta.";
				$input_oldrecipeid = "Ingrese su antiguo ID de receta.";
				$regenerat_succ = "La receta se regeneró correctamente.";
				$nofound_recipeuserid = "ID de usuario o ID de receta no encontrados.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_recipedate = "Please send recipe date.";
				$input_userid = "Please enter your user id.";
				$input_mealtype = "Please enter your meal type.";
				$input_newrecipeid = "Please enter new recipe id.";
				$input_oldrecipeid = "Please enter your old recipe id.";
				$regenerat_succ = "Recipe regenerated successfully.";
				$nofound_recipeuserid = "User id or recipe id not found.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

	    if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}
        
        if(isset($this->_request['new_recipe_id']))
		$new_recipe_id = $this->_request['new_recipe_id'];

	    if( empty($new_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_newrecipeid;
			$this->response($this->json($error), 404);
		}
        
        if(isset($this->_request['old_recipe_id']))
		$old_recipe_id = $this->_request['old_recipe_id'];

	    if( empty($old_recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_oldrecipeid;
			$this->response($this->json($error), 404);
		}

        if(isset($this->_request['recipe_name']))
		$recipe_name = $this->_request['recipe_name'];

		if(isset($this->_request['recipe_image']))
		$recipe_image = $this->_request['recipe_image'];

	    if(isset($this->_request['recipe_calories']))
		$recipe_calories = $this->_request['recipe_calories'];

		if(isset($this->_request['recipe_protien']))
		$recipe_protien = $this->_request['recipe_protien'];

	    if(isset($this->_request['recipe_fats']))
		$recipe_fats = $this->_request['recipe_fats'];

		if(isset($this->_request['recipe_carbs']))
		$recipe_carbs = $this->_request['recipe_carbs'];

	    if(isset($this->_request['recipe_ingredients']))
		$recipe_ingredients = $this->_request['recipe_ingredients'];

	    if(isset($this->_request['ingrdientsLines']))
		$ingrdientsLines = $this->_request['ingrdientsLines'];

	    if(isset($this->_request['servings']))
		$servings = $this->_request['servings']; 
             
             if(isset($this->_request['source_recipe_url']))
		$source_recipe_url = $this->_request['source_recipe_url'];
           
             if(isset($this->_request['direction']))
		$direction = $this->_request['direction'];   

		if(isset($this->_request['favorite_status']))
		$favorite_status = $this->_request['favorite_status'];

		if(isset($this->_request['blocked_status']))
		$blocked_status = $this->_request['blocked_status'];  

		if(isset($this->_request['original_serving']))
		$original_serving = $this->_request['original_serving'];

		// Conversion grocery START
				  
				  $afterconvert = array();
				  $converted = array();

				  $ingrdientsLines = stripslashes(html_entity_decode($ingrdientsLines));
				  $final_ingredientline = json_decode($ingrdientsLines,TRUE);
				  
				  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {


				  	$convertvalue_unit = "";
				    $convertvalue_amount = "";
				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){

				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
				      }
				      else{
				        $convertvalue_unit = $convert_value['targetunit'];
				        $convertvalue_amount = $convert_value['targetamount'];
				      }
				      unset($convert_value);

				    $afterconvert['id'] = $final_ingredientline[$gg]['id'];
				  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
				  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
				  	$afterconvert['amount'] = $convertvalue_amount;
				  	$afterconvert['unit'] = $convertvalue_unit;
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

				  	$converted[$gg] = $afterconvert;
				    unset($afterconvert);
				  }

				  $ingrdientsLines = json_encode($converted);
				  unset($converted);

				  

				  // Conversion grocery END

      
		if(!empty($user_id) and !empty($meal_type) and !empty($old_recipe_id) and !empty($new_recipe_id))
		{
			global $wpdb;
                        $favorites_status="select recipe_id from ".$wpdb->prefix."favorites_items where recipe_id='".$new_recipe_id."'";
            $favorites_result=$wpdb->get_row($favorites_status);
            $favorites_recipe_id=$favorites_result->recipe_id;
            if($favorites_recipe_id==$new_recipe_id)
            {
            	$favorite_status='1';
            }else
            {
            	$favorite_status='0';
            }
           
           $block_status="select recipe_id from ".$wpdb->prefix."block_items where recipe_id='".$new_recipe_id."'";
            $block_result=$wpdb->get_row($block_status);
            $block_recipe_id=$block_result->recipe_id;
            if($block_recipe_id==$new_recipe_id)
            {
            	$blocked_status='1';
            }else
            {
            	$blocked_status='0';
            }   
			$recipe_current_date = $recipe_date;
	 	   $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set recipe_name='".$recipe_name."',recipe_image='".$recipe_image."',recipe_calories='".$recipe_calories."',recipe_protien='".$recipe_protien."',recipe_fats='".$recipe_fats."',recipe_carbs='".$recipe_carbs."',recipe_ingredients='".$recipe_ingredients."',recipe_ingredients_value='".$ingrdientsLines."',recipe_serving='".$servings."',original_serving='".$original_serving."',recipe_id='".$new_recipe_id."',source_recipe_url='".$source_recipe_url."',direction='".$direction."',favorite_status='".$favorite_status."',blocked_status='".$blocked_status."' WHERE user_id='".$user_id."' and meal_type='".$meal_type."' and recipe_id='".$old_recipe_id."' and recipe_current_date='".$recipe_current_date."'";
				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

		$result['Status'] = 'Success';
		$result['ingrdientsLines'] = $ingrdientsLines;
	        $result['msg'] = $regenerat_succ;
                $result['favorite_status'] = $favorite_status;
	        $result['blocked_status'] = $blocked_status;

	      
        }
        else
        {
		    $result['Status'] = 'Failed';
			$result['msg'] = $nofound_recipeuserid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}



/* 
********Plan History API***********
*/

private function get_plan_history()
{
		if($this->get_request_method() != "POST")
		{
			$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
			$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";
		$input_dateenter = "";
		$founddata_date = "";
		$futuremeal_notavai = "";
		$show_currentdatesdata = "";
		$nofound_userid = "";
		$nofound_mealplan = "";
		$invalid_userid = "";

		if('es' == $applang){
				$input_userid = "Please enter your user id.";
				$input_dateenter = "Por favor ingrese su fecha.";
				$founddata_date = "Fecha y datos anteriores encontrados.";
				$futuremeal_notavai = "Las comidas futuras no están disponibles.";
				$show_currentdatesdata = "Mostrar datos de fechas actuales.";
				$nofound_userid = "No se ha encontrado la identificación de usuario.";
				$nofound_mealplan = "Ningún plan de comida encontrado para este día.";
				$invalid_userid = "ID de usuario invalido.";
		}
		else{
				$input_userid = "Por favor, introduzca su ID de usuario.";
				$input_dateenter = "Please enter your date.";
				$founddata_date = "Previous date and data found.";
				$futuremeal_notavai = "Future meals is not available.";
				$show_currentdatesdata = "show current dates data.";
				$nofound_userid = "User id not found.";
				$nofound_mealplan = "No meal plan found for this day.";
				$invalid_userid = "Invalid user id.";
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

        
        if(isset($this->_request['history_plan_date']))
		$history_plan_date = $this->_request['history_plan_date'];

		if( empty($history_plan_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_dateenter;
			$this->response($this->json($error), 404);
		}


		if(!empty($user_id) && !empty($history_plan_date))
		{
	     global $wpdb;
	    $current_date=date("Y/m/d");
	     $yummly_response_query = "select * from " . $wpdb->prefix . "meal_plan where user_id='".$user_id."' and recipe_current_date='".$history_plan_date."'";
         $yummly_response_result = $wpdb->get_results($yummly_response_query. " order by id DESC");
	     if($yummly_response_result && $current_date!=$history_plan_date)
         {
          $meal_types = array("Breakfast","Lunch","Dinner","Snacks");
	      $count_meal_type = count($meal_types);
	      for ($j=0; $j <$count_meal_type; $j++) 
	      { 
	      $yummly_response = "select * from " . $wpdb->prefix . "meal_plan where user_id='".$user_id."' and recipe_current_date='".$history_plan_date."' and meal_type='".$meal_types[$j]."'";
          $yummly_response = $wpdb->get_results($yummly_response. " order by id DESC");
          $array = json_decode(json_encode($yummly_response), True);
        		$finalRescArr = array();

			   for ($i=0; $i <count($array); $i++) 
			   {
			    if($array[$i]['meal_type'] == $meal_types[$j] ){
			    	$finalRescArr[$meal_types[$j]][] = $array[$i];
			    }
			   }
               $result['Status'] = 'Success';
               $result['msg']=$founddata_date;
			  $result['Plan_History_Details'] [$meal_types[$j]]= $finalRescArr[$meal_types[$j]];
			} 
			
			 $plan_query = "select * from " . $wpdb->prefix . "plan_history where user_id='".$user_id."' and old_date='".$history_plan_date."'";
			  $plan_query_result = $wpdb->get_row($plan_query. " order by id DESC");
              
             $result['target_calories']=$plan_query_result->old_calories;
             $result['target_protien']=$plan_query_result->old_protien;
             $result['target_fats']=$plan_query_result->old_fats;
             $result['target_carbs']=$plan_query_result->old_carbs;

	       }
	       else if($current_date < $history_plan_date)
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$futuremeal_notavai;

	         }
	       else if($current_date == $history_plan_date)
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$show_currentdatesdata;

	         	 $meal_types = array("Breakfast","Lunch","Dinner","Snacks");
	             $count_meal_type = count($meal_types);
	            for ($j=0; $j <$count_meal_type; $j++) 
	            { 
	             $yummly_response = "select * from " . $wpdb->prefix . "meal_plan where user_id='".$user_id."' and recipe_current_date='".$current_date."' and meal_type='".$meal_types[$j]."'";
                 $yummly_response = $wpdb->get_results($yummly_response. " order by id DESC");
                 $array = json_decode(json_encode($yummly_response), True);
        		$finalRescArr = array();

			   for ($i=0; $i <count($array); $i++) 
			   {
			    if($array[$i]['meal_type'] == $meal_types[$j] ){
			    	$finalRescArr[$meal_types[$j]][] = $array[$i];
			    }
			   }
               $result['Status'] = 'Success';
			  $result['Yummly_Response_Details'] [$meal_types[$j]]= $finalRescArr[$meal_types[$j]];
			} 
              $plan_query = "select * from " . $wpdb->prefix . "plan_history where user_id='".$user_id."' and old_date='".$history_plan_date."'";
			  $plan_query_result = $wpdb->get_row($plan_query. " order by id DESC");
              
             $result['target_calories']=$plan_query_result->old_calories;
             $result['target_protien']=$plan_query_result->old_protien;
             $result['target_fats']=$plan_query_result->old_fats;
             $result['target_carbs']=$plan_query_result->old_carbs;

	         }
	         else 
	         {
	         	$result['Status'] = 'Failed';	
	         	$result['msg']=$nofound_mealplan;
	         }
       }
       else
       {
	    $result['Status'] = 'Failed';
	    $result['msg'] = $nofound_userid;
        }	
        $this->response($this->json($result), 200);
		// If invalid inputs "Bad Request" status message and reason
		$error = array('Status' => "Failed", "msg" => $invalid_userid);
		$this->response($this->json($error), 400);
}



/* 
********Edit Serving***********
*/

private function edit_serving()
{
	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_mealtype = "";
	$input_recipeid = "";
	$input_recipedate = "";
	$invalid_userid = "";

	if('es' == $applang){
		$input_userid = "Por favor, introduzca su ID de usuario.";
		$input_mealtype = "Ingrese su tipo de comida.";
		$input_recipeid = "Ingrese el ID de la receta.";
		$input_recipedate = "Por favor envíe la fecha de la receta.";
		$invalid_userid = "ID de usuario invalido.";
	}
	else{
		$input_userid = "Please enter your user id.";
		$input_mealtype = "Please enter your meal type.";
		$input_recipeid = "Please enter recipe id.";
		$input_recipedate = "Please send recipe date.";
		$invalid_userid = "Invalid user id.";
	}
     
                if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if( empty($user_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_userid;
			$this->response($this->json($error), 404);
		}

		if(isset($this->_request['meal_type']))
		$meal_type = $this->_request['meal_type'];

	       if( empty($meal_type) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_mealtype;
			$this->response($this->json($error), 404);
		}
        
                if(isset($this->_request['recipe_id']))
		$recipe_id = $this->_request['recipe_id'];

	        if( empty($recipe_id) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipeid;
			$this->response($this->json($error), 404);
		} 

                if(isset($this->_request['recipe_date']))
		$recipe_date = $this->_request['recipe_date'];

	        if( empty($recipe_date) )
		{
			$error['Status'] = 'Failed';	
			$error['msg'] = $input_recipedate;
			$this->response($this->json($error), 404);
		} 
         
                if(isset($this->_request['recipe_calories']))
		$recipe_calories = $this->_request['recipe_calories'];

		if(isset($this->_request['recipe_protien']))
		$recipe_protien = $this->_request['recipe_protien'];

	        if(isset($this->_request['recipe_fats']))
		$recipe_fats = $this->_request['recipe_fats'];

		if(isset($this->_request['recipe_carbs']))
		$recipe_carbs = $this->_request['recipe_carbs'];
        
                if(isset($this->_request['recipe_ingredients_value']))
		$recipe_ingredients_value = $this->_request['recipe_ingredients_value'];
        
                if(isset($this->_request['recipe_serving']))
		$recipe_serving = $this->_request['recipe_serving'];

                if(!empty($user_id) and !empty($meal_type) and !empty($recipe_id))
		{
		global $wpdb; 
                $recipe_current_date = $recipe_date;
	 	$sql_edit_serving ="UPDATE ".$wpdb->prefix."meal_plan set recipe_calories='".$recipe_calories."',recipe_protien='".$recipe_protien."',recipe_fats='".$recipe_fats."',recipe_carbs='".$recipe_carbs."',recipe_ingredients_value='".$recipe_ingredients_value."',recipe_serving='".$recipe_serving."',recipe_id='".$recipe_id."' WHERE user_id='".$user_id."' and meal_type='".$meal_type."' and recipe_id='".$recipe_id."' and recipe_current_date='".$recipe_current_date."'";
		$row_sql_edit_serving=$wpdb->get_row($sql_edit_serving);
		$result['Status'] = 'Success';
	    }else
	    {
	    	$result['Status'] = 'Failed';
	    }

    $this->response($this->json($result), 200);
    // If invalid inputs "Bad Request" status message and reason
    $error = array('Status' => "Failed", "msg" => $invalid_userid);
	$this->response($this->json($error), 400);
}	


private function creategrocery(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_startdate = "";
	$input_enddate = "";
	$grocery_listcreate = "";
	$grocery_listnotcreate = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_startdate = "Ingrese la fecha de inicio.";
			$input_enddate = "Por favor, introduzca la fecha de finalización.";
			$grocery_listcreate = "Lista de comestibles creada.";
			$grocery_listnotcreate = "Lista de comestibles no creada.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_startdate = "Please enter start date.";
			$input_enddate = "Please enter end date.";
			$grocery_listcreate = "Grocery list created.";
			$grocery_listnotcreate = "Grocery list not created.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_startdate']))
	$plan_startdate = $this->_request['plan_startdate'];

	if(empty($plan_startdate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_startdate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_enddate']))
	$plan_enddate = $this->_request['plan_enddate'];

	if(empty($plan_enddate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_enddate;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$sql_get_data ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."meal_plan WHERE  user_id ='".$user_id."' AND recipe_current_date >= '".$plan_startdate."' AND recipe_current_date <= '".$plan_enddate."'";
	$recipe_result = $wpdb->get_results($sql_get_data, ARRAY_A);

	$grocery_data = $recipe_result;
	$liste_aisle = array();
	$liste_value = array();

		for ($i=0 ; $i < count($grocery_data) ; $i++ ) { 
	
	$jsongrocey_data = json_decode($grocery_data[$i]['recipe_ingredients_value'],true);
	//echo '<br>'.count($jsongrocey_data);
	//print_r($jsongrocey_data);

	// Getting aisle Start

	for ($j=0; $j < count($jsongrocey_data); $j++) { 
		
		//echo '<br>aisle - '.$jsongrocey_data[$j]['aisle'];

		if(!in_array($jsongrocey_data[$j]['aisle'], $liste_aisle, true)){
			array_push($liste_aisle, $jsongrocey_data[$j]['aisle']);
		}

		
	}

	// Getting aisle End

	// Geeting ingredents name start
	for ($m=0; $m < count($jsongrocey_data); $m++) { 
		
		if(!in_array($jsongrocey_data[$m]['name'], $liste_value, true)){
			array_push($liste_value, $jsongrocey_data[$m]['name']);

    	}

	}
	// Getting ingredents name end
}

	$add_ingredents_inaisle = array();
	$ingredents_value = array();
	$final_unit = array();

//	foreach ($liste_aisle as $liste_aisle_value) { 
//		
//		for ($k=0 ; $k < count($grocery_data) ; $k++ ) { 
//		$ingredents_get = json_decode($grocery_data[$k]['recipe_ingredients_value'],true);
//
//			for ($l=0; $l < count($ingredents_get); $l++) {
//
//				$ingredents_aisle = $ingredents_get[$l]['aisle'];
//				if($liste_aisle_value == $ingredents_aisle){
//
//					if($ingredents_get[$l]['id'] != "" || $ingredents_get[$l]['id'] != null){
//
//					$ingredents_value['id'] = $ingredents_get[$l]['id'];
//					$ingredents_value['aisle'] = $ingredents_get[$l]['aisle'];
//					$ingredents_value['name'] = $ingredents_get[$l]['name'];
//					$ingredents_value['amount'] = $ingredents_get[$l]['amount'];
//					$ingredents_value['unit'] = $ingredents_get[$l]['unit'];
//					$ingredents_value['image'] = $ingredents_get[$l]['image'];
//					$add_ingredents_inaisle[$ingredents_value['name']][] = $ingredents_value;
//					unset($ingredents_value);
//
//					}
//
//				}
//			}
//		}
//	}
//
//	$finalarray = array();
//	$list_array = array();
//	for ($l=0; $l<count($liste_value); $l++) {
//		
//		$ingre_value = $liste_value[$l];
//		$ingre_actual_value = $add_ingredents_inaisle[$ingre_value];
//		if($ingre_actual_value[0]['id'] != "" || $ingre_actual_value[0]['id'] != null){
//			$a = 0;
//			for ($n=0; $n < count($ingre_actual_value); $n++) { 
//				
//				$a = $a + $ingre_actual_value[$n]['amount'];
//
//				$list_array['id'] = $ingre_actual_value[0]['id'];
//				$list_array['name'] = str_replace("'","",$ingre_actual_value[0]['name']);
//				$list_array['unit'] = $ingre_actual_value[0]['unit'];
//				$list_array['image'] = $ingre_actual_value[0]['image'];
//				$list_array['aisle'] = $ingre_actual_value[0]['aisle'];
//
//			}
//			$list_array['amount'] = $a;
//			// $finalarray[$list_array['aisle']][$ingre_actual_value[0]['name']] = $list_array;
//			$finalarray[$list_array['aisle']][] = $list_array;
//			unset($list_array);
//		}
//	}
//
//	// Convert measurement START
//	 $convert_arraykeys = array_keys($finalarray);
//	 $convertarray = array();
//	 $final_convertarray = array();
//	 for ($ll=0; $ll<count($convert_arraykeys); $ll++) {
//	 	$valuesfinal = $finalarray[$convert_arraykeys[$ll]];
//	 	for ($nn=0; $nn < count($finalarray[$convert_arraykeys[$ll]]); $nn++) { 
//
//	 		$convert_value = convert_measurement($valuesfinal[$nn]['name'],$valuesfinal[$nn]['amount'],$valuesfinal[$nn]['unit']);
//
//	 		if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
//
//	 			$convertvalue_unit = $valuesfinal[$nn]['unit'];
//				$convertvalue_amount = $valuesfinal[$nn]['amount'];
//	 		}
//	 		else{
//				$convertvalue_unit = $convert_value['targetunit'];
//				$convertvalue_amount = $convert_value['targetamount'];
//			}
//
//			$convertarray['id'] = $valuesfinal[$nn]['id'];
//	 		$convertarray['name'] = $valuesfinal[$nn]['name'];
//			$convertarray['unit'] = $convertvalue_unit;
//	 		$convertarray['image'] = $valuesfinal[$nn]['image'];
//			$convertarray['aisle'] = $valuesfinal[$nn]['aisle'];
//			$convertarray['amount'] = $convertvalue_amount;
//	 		$final_convertarray[$convert_arraykeys[$ll]][] = $convertarray;
//	 	}
//		
//	 	unset($convertarray);
//	 }

        	foreach ($liste_aisle as $liste_aisle_value) { 
		
		for ($k=0 ; $k < count($grocery_data) ; $k++ ) { 
		$ingredents_get = json_decode($grocery_data[$k]['recipe_ingredients_value'],true);

			for ($l=0; $l < count($ingredents_get); $l++) {

				$ingredents_aisle = $ingredents_get[$l]['aisle'];
				if($liste_aisle_value == $ingredents_aisle){

					if($ingredents_get[$l]['id'] != "" || $ingredents_get[$l]['id'] != null){

					$ingredents_value['id'] = $ingredents_get[$l]['id'];
					$ingredents_value['aisle'] = $ingredents_get[$l]['aisle'];
					$ingredents_value['name'] = $ingredents_get[$l]['name'];
					$ingredents_value['amount'] = $ingredents_get[$l]['amount'];
					$ingredents_value['unit'] = $ingredents_get[$l]['unit'];
					$ingredents_value['image'] = $ingredents_get[$l]['image'];
					$add_ingredents_inaisle[$ingredents_value['name']][] = $ingredents_value;
					unset($ingredents_value);

					}

				}
			}
		}
	}
	
	$finalarray = array();
	$list_array = array();
//            echo "<pre>";
//       //  print_r($ingredents_value) ;
//        echo "</pre>";
//        
//        echo "hello";
//        
//         echo "<pre>";
//         //print_r($liste_value) ;
//        echo "</pre>";
     //   exit;
	for ($l=0; $l<count($liste_value); $l++) {
		
		$ingre_value = $liste_value[$l];
		 $ingre_actual_value = $add_ingredents_inaisle[$ingre_value];
                 
//          echo "hello2";
//        
//        echo "<pre>";
//         print_r($ingre_actual_value) ;
//        echo "</pre>";
//        
		if($ingre_actual_value[0]['id'] != "" || $ingre_actual_value[0]['id'] != null){
			$a = 0;
			for ($n=0; $n < count($ingre_actual_value); $n++) { 
				
				echo $a = $a + $ingre_actual_value[$n]['amount'];

				$list_array['id'] = $ingre_actual_value[0]['id'];
				$list_array['name'] = str_replace("'", "", $ingre_actual_value[0]['name']);
				$list_array['unit'] = $ingre_actual_value[0]['unit'];
				$list_array['image'] = $ingre_actual_value[0]['image'];
				$list_array['aisle'] = $ingre_actual_value[0]['aisle'];

			}
			$list_array['amount'] = $a;
			// $finalarray[$list_array['aisle']][$ingre_actual_value[0]['name']] = $list_array;
			$finalarray[$list_array['aisle']][] = $list_array;
			unset($list_array);
		}
	}        
        
	 
	// Convert measurement START
	$convert_arraykeys = array_keys($finalarray);
	$convertarray = array();
	 $final_convertarray = array();
	 for ($ll=0; $ll<count($convert_arraykeys); $ll++) {
	 	$valuesfinal = $finalarray[$convert_arraykeys[$ll]];
	 	for ($nn=0; $nn < count($finalarray[$convert_arraykeys[$ll]]); $nn++) { 

			$convert_value = convert_measurement($valuesfinal[$nn]['name'],$valuesfinal[$nn]['amount'],$valuesfinal[$nn]['unit']);
			if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){

				$convertvalue_unit = $valuesfinal[$nn]['unit'];
				$convertvalue_amount = $valuesfinal[$nn]['amount'];
			}
	 		else{
	 			$convertvalue_unit = $convert_value['targetunit'];
				$convertvalue_amount = $convert_value['targetamount'];
			}
			$convertarray['id'] = $valuesfinal[$nn]['id'];
			$convertarray['name'] = $valuesfinal[$nn]['name'];
			$convertarray['unit'] = $convertvalue_unit;
	 		$convertarray['image'] = $valuesfinal[$nn]['image'];
	 		$convertarray['aisle'] = $valuesfinal[$nn]['aisle'];
			$convertarray['amount'] = $convertvalue_amount;
			$final_convertarray[$convert_arraykeys[$ll]][] = $convertarray;
	 	}
		
	 	unset($convertarray);
	 }

	// Convert measurement END  
        
        
	// Convert measurement END
	$recipe_ingredients_data = json_encode($final_convertarray);
        
	$last_modified = date('Y/m/d');

	$sql_grocery_list ="insert into ".$wpdb->prefix."grocerylist(user_id,start_date,end_date,recipe_ingredients_value,last_modified) VALUES ('".$user_id."','".$plan_startdate."','".$plan_enddate."','".$recipe_ingredients_data."','".$last_modified."')";
	$sql_grocerylist_result = $wpdb->query($sql_grocery_list);

	if($sql_grocerylist_result){

		$results['Status'] = 'Success';
		$results['msg'] = $grocery_listcreate;
		$results['last_modified'] = $last_modified;
		//$results['grocerylist'] = $finalarray;
		$this->response($this->json($results), 200);

	}else{

		$results['Status'] = 'faild';
		$results['msg'] = $grocery_listnotcreate;
		$this->response($this->json($results), 200);

	}
	

}


private function get_grocery_dates(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$grodate_list = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$grodate_list = "Lista de fechas de comestibles.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$grodate_list = "Grocery dates list.";	
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$sql_grocery_dates ="SELECT start_date,end_date,last_modified FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."'";
	$sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates, ARRAY_A);

	$results['Status'] = 'Success';
	$results['msg'] = $grodate_list;
	$results['grocerydatelist'] = $sql_grocery_datesresult;
	$this->response($this->json($results), 200);
}


private function get_grocerydates_data(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_startdate = "";
	$input_enddate = "";
	$grodata_list = "";
	$nogrodate_list = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_startdate = "Ingrese la fecha de inicio.";
			$input_enddate = "Por favor, introduzca la fecha de finalización.";
			$grodata_list = "Lista de datos de comestibles.";
			$nogrodate_list = "No hay lista de fechas de comestibles.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_startdate = "Please enter start date.";
			$input_enddate = "Please enter end date.";
			$grodata_list = "Grocery data list.";
			$nogrodate_list = "There is no grocery dates list.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_startdate']))
	$plan_startdate = $this->_request['plan_startdate'];

	if(empty($plan_startdate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_startdate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_enddate']))
	$plan_enddate = $this->_request['plan_enddate'];

	if(empty($plan_enddate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_enddate;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$sql_grocery_dates_list ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates_list);

	if($sql_grocery_datesresult){

		$sql_pantry_dates_list ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
		$sql_pantry_datesresult = $wpdb->get_results($sql_pantry_dates_list);


		$results['Status'] = 'Success';
		$results['msg'] = $grodata_list;
		$results['grocerydatedata'] = $sql_grocery_datesresult;
		$results['pantrydatedata'] = $sql_pantry_datesresult;
		$this->response($this->json($results), 200);

	}
	else{

		$results['faild'] = 'Failed';
		$results['msg'] = $nogrodate_list;
		$this->response($this->json($results), 200);
	}

	
	
}	

private function grocery_delete_all(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$alllist_deleted = "";
	$alllistnot_deleted = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$alllist_deleted = "Se eliminó toda la lista.";
			$alllistnot_deleted = "Toda la lista no se ha eliminado.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$alllist_deleted = "All list deleted.";
			$alllistnot_deleted = "All list not deleted.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$sql_grocerylist_delete ="DELETE FROM ".$wpdb->prefix."grocerylist WHERE user_id='".$user_id."'";
	$sql_grocerylistdelete_result = $wpdb->query($sql_grocerylist_delete);

	$sql_pantrylist_delete ="DELETE FROM ".$wpdb->prefix."pantrylist WHERE user_id='".$user_id."'";
	$sql_pantrylistdelete_result = $wpdb->query($sql_pantrylist_delete);

	if($sql_grocerylistdelete_result){

		$results['Status'] = 'Success';
		$results['msg'] = $alllist_deleted;
		$this->response($this->json($results), 200);

	}
	else{

		$error['Status'] = 'Failed';
		$error['msg'] = $alllistnot_deleted;
		$this->response($this->json($error), 200);
	}
	
}

private function grocery_delete_parlist(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_startdate = "";
	$input_enddate = "";
	$listdeleted = "";
	$listnotdelted = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_startdate = "Ingrese la fecha de inicio.";
			$input_enddate = "Por favor, introduzca la fecha de finalización.";
			$listdeleted = "Lista eliminada.";
			$listnotdelted = "Lista no eliminada.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_startdate = "Please enter start date.";
			$input_enddate = "Please enter end date.";
			$listdeleted = "List deleted.";
			$listnotdelted = "List not deleted.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_startdate']))
	$plan_startdate = $this->_request['plan_startdate'];

	if(empty($plan_startdate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_startdate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_enddate']))
	$plan_enddate = $this->_request['plan_enddate'];

	if(empty($plan_enddate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_enddate;
	$this->response($this->json($error), 404);
	}

	global $wpdb;
	$sql_grocery_par_list_delete ="DELETE FROM ".$wpdb->prefix."grocerylist WHERE user_id = '".$user_id."' AND  start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$sql_grocery_par_listdelete_result = $wpdb->query($sql_grocery_par_list_delete);

	$sql_pantry_par_list_delete ="DELETE FROM ".$wpdb->prefix."pantrylist WHERE user_id = '".$user_id."' AND  start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$sql_pantry_par_listdelete_result = $wpdb->query($sql_pantry_par_list_delete);

	if($sql_grocery_par_listdelete_result){

		$results['Status'] = 'Success';
		$results['msg'] = $listdeleted;
		$this->response($this->json($results), 200);

	}
	else{

		$error['Status'] = 'Failed';
		$error['msg'] = $listnotdelted;
		$this->response($this->json($error), 200);
	}
	
}


private function move_groceryto_pantry(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_startdate = "";
	$input_enddate = "";
	$input_asile = "";
	$input_searchname = "";
	$itmmov_pantry = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_startdate = "Ingrese la fecha de inicio.";
			$input_enddate = "Por favor, introduzca la fecha de finalización.";
			$input_asile = "Introduzca el pasillo de búsqueda.";
			$input_searchname = "Introduzca el nombre de la búsqueda.";
			$itmmov_pantry = "Elemento movido a la despensa.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_startdate = "Please enter start date.";
			$input_enddate = "Please enter end date.";
			$input_asile = "Please enter search aisle.";
			$input_searchname = "Please enter search name.";
			$itmmov_pantry = "Item moved to pantry.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_startdate']))
	$plan_startdate = $this->_request['plan_startdate'];

	if(empty($plan_startdate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_startdate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_enddate']))
	$plan_enddate = $this->_request['plan_enddate'];

	if(empty($plan_enddate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_enddate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['ingredent_search_aisle']))
	$ingredent_search_aisle = $this->_request['ingredent_search_aisle'];

	if(empty($ingredent_search_aisle))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_asile;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['ingredent_search_name']))
	$ingredent_search_name = $this->_request['ingredent_search_name'];

	if(empty($ingredent_search_name))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_searchname;
	$this->response($this->json($error), 404);
	}

	global $wpdb;

	$sql_grocery_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates, ARRAY_A);

	$get_ingredents_value = json_decode($sql_grocery_datesresult[0]['recipe_ingredients_value'],TRUE);

	

	// Loop for searching according to aisle start
	$new_get_ingredents_value = array();
	$moved_ingredent = array();
	$final_moved_ingredent = array();

	$search_aisle = explode("*$#",$ingredent_search_aisle);
	$search_name = explode("*$#",$ingredent_search_name);
	$aisle_keys = array_keys($get_ingredents_value);

	for ($bb=0; $bb < count($aisle_keys); $bb++) {

		//echo '<br>'.$aisle_keys[$bb];
		$get_aislevalue = $get_ingredents_value[$aisle_keys[$bb]];
		//print_r($get_aislevalue);

		for ($cc=0; $cc < count($get_aislevalue) ; $cc++) { 

			$get_search_aisle = $search_aisle;
			$get_search_name = $search_name;
			for ($dd=0; $dd < count($get_search_aisle); $dd++) { 
				
				for ($ee=0; $ee < count($get_search_name); $ee++) { 
					
					if($get_search_aisle[$dd] == $get_aislevalue[$cc]['aisle'] && $search_name[$ee] == $get_aislevalue[$cc]['name']){

							$moved_ingredent['id'] = $get_aislevalue[$cc]['id'];
							$moved_ingredent['name'] = $get_aislevalue[$cc]['name'];
							$moved_ingredent['aisle'] = $get_aislevalue[$cc]['aisle'];
							$moved_ingredent['unit'] = $get_aislevalue[$cc]['unit'];
							$moved_ingredent['image'] = $get_aislevalue[$cc]['image'];
							$moved_ingredent['amount'] = $get_aislevalue[$cc]['amount'];

							$final_moved_ingredent[$get_search_aisle[$dd]][] = $moved_ingredent;
							// deleting particular ingredent from array
							array_splice($get_aislevalue, $cc, 1);
							}

				}
			}
		}
		//print_r($get_aislevalue); // print deleting particular ingredent from array
		$new_get_ingredents_value[$aisle_keys[$bb]] = $get_aislevalue; 

	}

	// Check pantry data already exist start

	$sql_pantry_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_pantry_datesresult = $wpdb->get_results($sql_pantry_dates, ARRAY_A);

	if($sql_pantry_datesresult){

		$pantry_ingredents_value = json_decode($sql_pantry_datesresult[0]['recipe_ingredients_value'],TRUE);

		$final_new_ing_array = array();
		$pantry_aisle = array_keys($pantry_ingredents_value);

		for ($ii=0; $ii < count($search_aisle); $ii++) { 
			
			//echo '<br>'.$search_aisle[$ii];

			if(array_key_exists($search_aisle[$ii], $pantry_ingredents_value)){

				//echo 'Yes';
				for ($kk=0; $kk < count($final_moved_ingredent[$search_aisle[$ii]]); $kk++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle']){

					//create new array of final with aisle name
						$ifyes_data_get =array();
						$ifyes_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['id'];
						$ifyes_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle'];
						$ifyes_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['name'];
						$ifyes_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['unit'];
						$ifyes_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['image'];
						$ifyes_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['amount'];
						//$final_new_ing_array[] = $ifyes_data_get;
						array_push($pantry_ingredents_value[$search_aisle[$ii]],$ifyes_data_get);
	 	 			
					
				}	
				unset($ifyes_data_get);	
			}
		}
			else{

				//echo '<br>No';
				for ($jj=0; $jj < count($final_moved_ingredent[$search_aisle[$ii]]); $jj++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle']){

						//create new array of final with aisle name
						$array_data_get =array();
						$array_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['id'];
						$array_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle'];
						$array_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['name'];
						$array_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['unit'];
						$array_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['image'];
						$array_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['amount'];
						$final_new_ing_array[] = $array_data_get;
					}
					
					
				}
				$pantry_ingredents_value[$search_aisle[$ii]] = $final_new_ing_array;
				unset($array_data_get);
				unset($final_new_ing_array);
			}
		}

	}

	//print_r($pantry_ingredents_value); die();

	// Check pantry data already exist end
// Loop for searching according to aisle end

	$last_modified = date('Y/m/d');

	$update_grocery_list="UPDATE ".$wpdb->prefix."grocerylist set recipe_ingredients_value='".json_encode($new_get_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$update_grocery_list_result = $wpdb->query($update_grocery_list);

	if($sql_pantry_datesresult){

		$update_pantry_list="UPDATE ".$wpdb->prefix."pantrylist set recipe_ingredients_value='".json_encode($pantry_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
		$sql_pantry_result = $wpdb->query($update_pantry_list);
	}
	else{

		$sql_pantry_insert ="insert into ".$wpdb->prefix."pantrylist(user_id,start_date,end_date,recipe_ingredients_value,last_modified) VALUES ('".$user_id."','".$plan_startdate."','".$plan_enddate."','".json_encode($final_moved_ingredent)."','".$last_modified."')";
		$sql_pantry_result = $wpdb->query($sql_pantry_insert);
	}
	

	if($update_grocery_list_result && $sql_pantry_result){

		$afterupdate_grocerydates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
		$afterupdate_grocery_datesresult = $wpdb->get_results($afterupdate_grocerydates, ARRAY_A);

		$afterupdate_ingredents = $afterupdate_grocery_datesresult;

			$results['Status'] = 'Success';
			$results['last_modified'] = $last_modified;
			$results['groceryupdatedlist'] = $afterupdate_ingredents;
			//$results['ingredent_search_aisle'] = $ingredent_search_aisle;
			//$results['ingredent_search_name'] = $ingredent_search_name;
			//$results['request'] = $this->_request['ingredent_search_aisle'];
			$results['msg'] = $itmmov_pantry;
			$this->response($this->json($results), 200);

	}

}


private function move_pantryto_grocery(){

	if($this->get_request_method() != "POST")
	{
	$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
	$this->response($this->json($error), 400);
	}

	if(isset($this->_request['applang']))
	$applang = $this->_request['applang'];
	
	$input_userid = "";
	$input_startdate = "";
	$input_enddate = "";
	$input_ingredentstring = "";
	$itmmov_grocery = "";
	$itmnotmov_grocery = "";

	if('es' == $applang){
			$input_userid = "Por favor, introduzca su ID de usuario.";
			$input_startdate = "Ingrese la fecha de inicio.";
			$input_enddate = "Por favor, introduzca la fecha de finalización.";
			$input_ingredentstring = "Por favor, envíe la cadena de ingredientes.";
			$itmmov_grocery = "El artículo se trasladó al supermercado.";
			$itmnotmov_grocery = "El artículo no se traslada al supermercado.";
	}
	else{
			$input_userid = "Please enter your user id.";
			$input_startdate = "Please enter start date.";
			$input_enddate = "Please enter end date.";
			$input_ingredentstring = "Please send ingredents string.";
			$itmmov_grocery = "Item moved to grocery.";
			$itmnotmov_grocery = "Item not moved to grocery.";
	}

	if(isset($this->_request['user_id']))
	$user_id = $this->_request['user_id'];

	if(empty($user_id))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_userid;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_startdate']))
	$plan_startdate = $this->_request['plan_startdate'];

	if(empty($plan_startdate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_startdate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['plan_enddate']))
	$plan_enddate = $this->_request['plan_enddate'];

	if(empty($plan_enddate))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_enddate;
	$this->response($this->json($error), 404);
	}

	if(isset($this->_request['ingredent_jsonstring']))
	$ingredent_jsonstring = $this->_request['ingredent_jsonstring'];

	if(empty($ingredent_jsonstring))
	{
	$error['Status'] = 'Failed';	
	$error['msg'] = $input_ingredentstring;
	$this->response($this->json($error), 404);
	}

	global $wpdb;

	$sql_pantrytogrocery_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_pantrytogrocery_datesresult = $wpdb->get_results($sql_pantrytogrocery_dates, ARRAY_A);

	$sql_pantry_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_pantry_datesresult = $wpdb->get_results($sql_pantry_dates, ARRAY_A);

	$pantry_ingredents_value = json_decode($sql_pantry_datesresult[0]['recipe_ingredients_value'],TRUE);

	// $jso = json_decode('{"Baking":[{"name":"baking powder","unit":"teaspoons","image":"https://spoonacular.com/cdn/ingredients_100x100/white-powder.jpg","aisle":"Baking","amount":6},{"name":"flour","unit":"cups","image":"https://spoonacular.com/cdn/ingredients_100x100/flour.png","aisle":"Baking","amount":8}],"Milk, Eggs, Other Dairy":[{"name":"butter","unit":"cup","image":"https://spoonacular.com/cdn/ingredients_100x100/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":2.5},{"name":"egg","unit":"","image":"https://spoonacular.com/cdn/ingredients_100x100/egg.jpg","aisle":"Milk, Eggs, Other Dairy","amount":2}]}',TRUE);

	//main code start
	$jso = json_decode(str_replace('\\', "", $ingredent_jsonstring),TRUE); 
	//print_r($jso); die();
	$get_ingredents_value = json_decode($sql_pantrytogrocery_datesresult[0]['recipe_ingredients_value'],TRUE);	

	$moved_aisle_keys = array_keys($jso);
	$get_ingredent_aisle_keys = array_keys($get_ingredents_value);
	//array_key_exists($key, $array_name);
	$final_moved_array = array();
	$inserted_newelement_array = array();
	$final_inserted_newelement_array = array();
	$semi_addinto_org_array = array();
	$final_addinto_org_array = array();
	for ($ff=0; $ff < count($moved_aisle_keys); $ff++) {

		$get_search_aisle = $moved_aisle;
	 	$get_search_name = $moved_name;
	 	$get_aislevalue = $get_ingredents_value[$moved_aisle_keys[$ff]];
	 	$jso_data = $jso[$moved_aisle_keys[$ff]];
	 	if(array_key_exists($moved_aisle_keys[$ff], $get_ingredents_value)){

	 		for ($gg=0; $gg < count($jso[$moved_aisle_keys[$ff]]); $gg++) { 
	 			
	 			$inserted_newelement_array['id'] = $jso_data[$gg]['id'];
	 			$inserted_newelement_array['name'] = $jso_data[$gg]['name'];
	 			$inserted_newelement_array['unit'] = $jso_data[$gg]['unit'];
	 			$inserted_newelement_array['image'] = $jso_data[$gg]['image'];
	 			$inserted_newelement_array['aisle'] = $jso_data[$gg]['aisle'];
	 			$inserted_newelement_array['amount'] = $jso_data[$gg]['amount'];
	 			array_push($get_ingredents_value[$moved_aisle_keys[$ff]],$inserted_newelement_array);
	 			unset($inserted_newelement_array);
	 		}
	 		
	 		

	 	}else{

	 		for ($hh=0; $hh < count($jso[$moved_aisle_keys[$ff]]); $hh++) {

	 			$semi_addinto_org_array['id'] = $jso_data[$hh]['id'];
	 			$semi_addinto_org_array['name'] = $jso_data[$hh]['name'];
	 			$semi_addinto_org_array['unit'] = $jso_data[$hh]['unit'];
	 			$semi_addinto_org_array['image'] = $jso_data[$hh]['image'];
	 			$semi_addinto_org_array['aisle'] = $jso_data[$hh]['aisle'];
	 			$semi_addinto_org_array['amount'] = $jso_data[$hh]['amount'];
	 			$final_addinto_org_array[] = $semi_addinto_org_array;
	 			
	 			
			}	
			$get_ingredents_value[$moved_aisle_keys[$ff]] = $final_addinto_org_array; 		
			unset($final_addinto_org_array);
	 		unset($semi_addinto_org_array);
	 		

	 }



	}
//print_r($get_ingredents_value);

	//main code end

// Remove moved element from pantry json Start

	$search_aisle = array_keys($jso);
	$search_name = array();
	

	for ($ll=0; $ll < count($search_aisle); $ll++) { 

		//echo 'Hi yes - '; print_r($jso[$search_aisle[$ll]]);

		for ($mm=0; $mm < count($jso[$search_aisle[$ll]]); $mm++) { 

			//echo '<br>Hi yes name -  '.$jso[$search_aisle[$ll]][$mm]['name'];
			$search_name[] = $jso[$search_aisle[$ll]][$mm]['name'];
		}
		
	}

	$new_pantry_ingredents_value = array();
	$pantry_moved_ingredent = array();
	$final_pantry_moved_ingredent = array();

	$pantry_search_aisle = $search_aisle;
	$pantry_search_name = $search_name;
	$aisle_keys = array_keys($pantry_ingredents_value);

	for ($bb=0; $bb < count($aisle_keys); $bb++) {

		//echo '<br>'.$aisle_keys[$bb];
		$get_aislevalue = $pantry_ingredents_value[$aisle_keys[$bb]];
		//print_r($get_aislevalue);

		for ($cc=0; $cc < count($get_aislevalue) ; $cc++) { 

			$get_search_aisle = $pantry_search_aisle;
			$get_search_name = $pantry_search_name;
			for ($dd=0; $dd < count($get_search_aisle); $dd++) { 
				
				for ($ee=0; $ee < count($get_search_name); $ee++) { 
					
					if($get_search_aisle[$dd] == $get_aislevalue[$cc]['aisle'] && $pantry_search_name[$ee] == $get_aislevalue[$cc]['name']){

							$pantry_moved_ingredent['id'] = $get_aislevalue[$cc]['id'];
							$pantry_moved_ingredent['name'] = $get_aislevalue[$cc]['name'];
							$pantry_moved_ingredent['aisle'] = $get_aislevalue[$cc]['aisle'];
							$pantry_moved_ingredent['unit'] = $get_aislevalue[$cc]['unit'];
							$pantry_moved_ingredent['image'] = $get_aislevalue[$cc]['image'];
							$pantry_moved_ingredent['amount'] = $get_aislevalue[$cc]['amount'];

							$final_pantry_moved_ingredent[$get_search_aisle[$dd]][] = $pantry_moved_ingredent;
							// deleting particular ingredent from array
							array_splice($get_aislevalue, $cc, 1);
							}

				}
			}
		}
		//echo '<br>print deleting particular ingredent from array';print_r($get_aislevalue); // print deleting particular ingredent from array
		$new_pantry_ingredents_value[$aisle_keys[$bb]] = $get_aislevalue; 

	}

	//echo "<br>moved element final array - ";print_r($new_pantry_ingredents_value);

// Remove moved element from pantry json End	

	$last_modified = date('Y/m/d');

	$update_pantrytogrocery_list="UPDATE ".$wpdb->prefix."grocerylist set recipe_ingredients_value='".json_encode($get_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$update_pantrytogrocery_list_result = $wpdb->query($update_pantrytogrocery_list);

	$update_pantry_list="UPDATE ".$wpdb->prefix."pantrylist set recipe_ingredients_value='".json_encode($new_pantry_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$update_pantryt_list_result = $wpdb->query($update_pantry_list);

	if($update_pantrytogrocery_list_result && $update_pantryt_list_result){

		$after_pantry_updates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
		$after_pantry_updates_result = $wpdb->get_results($after_pantry_updates, ARRAY_A);

		$results['Status'] = 'Success';
		$results['last_modified'] = $last_modified;
		$results['pantryupdatedlist'] = $after_pantry_updates_result;
		//$results['ingredent_search_aisle'] = $ingredent_search_aisle;
		//$results['ingredent_search_name'] = $ingredent_search_name;
		//$results['request'] = $this->_request['ingredent_search_aisle'];
		$results['msg'] = $itmmov_grocery;
		$this->response($this->json($results), 200);
	}
	else{

		$results['Status'] = 'Failed';
		$results['msg'] = $itmnotmov_grocery;
		$this->response($this->json($results), 200);
	}
	
}


private function workoutPlanner(){

		if($this->get_request_method() != "POST")
		{
		$error = array('Status' => "Not Acceptable", "msg" => "Invalid Format");
		$this->response($this->json($error), 400);
		}

		if(isset($this->_request['applang']))
		$applang = $this->_request['applang'];

		$input_userid = "";

		if('es' == $applang){
				$input_userid = "Por favor, introduzca su ID de usuario.";
		}
		else{
				$input_userid = "Please enter your user id.";	
		}

		if(isset($this->_request['user_id']))
		$user_id = $this->_request['user_id'];

		if(empty($user_id))
		{
		$error['Status'] = 'Failed';	
		$error['msg'] = $input_userid;
		$this->response($this->json($error), 404);
		}

		// Getting plan start and end date 
		global $wpdb;
		$query = "select payment_date,plan_expire_date from nu_payments where user_id='".$user_id."'";
		$query_result = $wpdb->get_results($query, ARRAY_A);
		//echo '<pre>'; print_r($query_result);

		$plan_start_date = $query_result[0]['payment_date'];
		$plan_end_date = $query_result[0]['plan_expire_date'];
		$plan_end_date = $plan_end_date = date('Y-m-d', strtotime($query_result[0]['plan_expire_date']. ' + 1 days'));

		// Getting weeks from start & end date

		//$week = week_startday($plan_start_date,$plan_end_date);
		//$firstday = $week['startday'];
		//$lastday = $week['endday'];
		$firstday = date("Y-m-d", strtotime('monday this week', strtotime($plan_start_date)));   
		$lastday = date("Y-m-d", strtotime('sunday this week', strtotime($plan_end_date)));
		$begin = new DateTime($firstday);
    	$end = date('Y-m-d', strtotime($lastday. ' + 1 days'));
    	$end = new DateTime($end);
        $date_array = array();
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date){
            $date_array[] = $date->format("Y/m/d");
        }
        
        $getweekarr = array_chunk($date_array, 7); 
		

		// Getting video list

		global $wpdb;
 	
		$sql_workout_videolist = 'SELECT youtube_video_id,workout_date,user_id,workout_count,workout_name from ' . $wpdb->prefix . 'add_workout WHERE user_id = "'.$user_id.'" and workout_date BETWEEN "'.$plan_start_date.'" AND "'.$query_result[0]['plan_expire_date'].'"';
		$row_workout_videolist=$wpdb->get_results($sql_workout_videolist, ARRAY_A);

		$videos = $row_workout_videolist;
		//echo '<pre>'; print_r($videos);
		$videolistarray = array();
		$finelvideolistarray = array();
		for ($i=0; $i < count($videos); $i++) { 
		    //echo $a[$i]['workout_date'];
		    //echo $a[$i]['workout_count'];
		    //echo $a[$i]['workout_name'].'<br>';
		    $videolistarray['workout_date'] = $videos[$i]['workout_date'];
		    $videolistarray['workout_count'] = $videos[$i]['workout_count'];
		    $videolistarray['workout_name'] = $videos[$i]['workout_name'];
		    $videolistarray['youtube_video_id'] = $videos[$i]['youtube_video_id'];
		    $finelvideolistarray[] = $videolistarray;
		    unset($videolistarray);
		  }

		//echo '<pre>new'; print_r($finelvideolistarray);
 		$results['Status'] = 'Success';
 		$results['plan_start_date'] = $query_result[0]['payment_date'];
 		$results['plan_end_date'] = $query_result[0]['plan_expire_date'];
 		$results['weeks'] = $getweekarr;
 		$results['videolist'] = $finelvideolistarray;
 		$this->response($this->json($results), 200);

}
	/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}



	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>
