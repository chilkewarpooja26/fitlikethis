<?php
add_shortcode('napapp_title_with_image', 'napapp_title_with_image_sc');

function napapp_title_with_image_sc($atts, $content = null){
    extract(shortcode_atts(array(
        'title_page'            => '',
        'text_position'         => 'center',
        'custom_title_color'    => '',
        'banner_bg'             => '',
        'title_icon'             => '',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
    if( $text_position == 'left' ) $alignment = "text-left";
    elseif( $text_position == 'right' ) $alignment = "text-right";
    else $alignment = "text-center";
?>
    <div class="<?php echo $alignment; ?> section-title <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( esc_attr( $animation_duration ) ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
        <?php if( $banner_bg != '' && !empty($banner_bg) ) :  ?>
            <?php $img = wp_get_attachment_image_src($atts['banner_bg'], 68, 69);  ?>
            <img src="<?php echo esc_url($img[0]); ?>" alt="Image" class="img-responsive">
        <?php endif; ?>
        <?php if( $title_icon != '' && !empty( $title_icon )  ) : ?>
            <span class="title-icon" style="color: <?php echo esc_html( $custom_title_color ); ?>"><i class="<?php echo esc_html( $title_icon ); ?>"></i></span>
        <?php endif; ?>
        <h1 style="color: <?php echo $custom_title_color; ?>"><?php echo ( $title_page != "" ) ? esc_attr($title_page, 'napapp') : " " ; ?></h1>
    </div>
<?php  return ob_get_clean();
}

add_shortcode( 'napapp_image_with_content', 'napapp_image_with_content_sc' );

function napapp_image_with_content_sc($atts, $content = null){
    extract(shortcode_atts(array(
        'image'                 => '',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
?>
    <div class="row">
        <div class="col-sm-6" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <?php $img = wp_get_attachment_image_src($atts['image'], 555, 369);  ?>
            <img src="<?php echo esc_url($img[0]); ?>" alt="Image" class="img-responsive">
        </div>
        <div class="col-sm-6" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <div class="<?php echo $custom_css_class; ?>">
                <?php echo  $content ; ?>
            </div>
        </div>
    </div>

<?php  return ob_get_clean();
}

add_shortcode( 'napapp_special_content_box', 'napapp_special_content_box_sc' );

function napapp_special_content_box_sc($atts, $content = null){
    extract(shortcode_atts(array(
        'text_position'         => '',
        'animation_duration'    => '',
        'animation_delay'    => '',
        'animation_style'    => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
?>
<?php if( $text_position == 'left' ) $class = "text-left";
    else $class = "text-right";
 ?>
    <div class="special-content <?php echo $class; ?>  <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
        <?php echo  $content ; ?>
    </div>
<?php  return ob_get_clean();
}

add_shortcode( 'napapp_video_player', 'napapp_video_player_sc' );

function napapp_video_player_sc($atts, $content = null){
    extract(shortcode_atts(array(
        'image'              => '',
        'video_link'         => '',
        'animation_duration' => '',
        'animation_delay'    => '',
        'animation_style'    => '',
        'custom_css_class'   => '',
    ), $atts));
    ob_start();
?>
    <div class="video <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
        <a class="video-link" href="<?php echo esc_url( $video_link ) ?>"><i class="fa fa-youtube-play"></i></a>
        <?php $img = wp_get_attachment_image_src($atts['image'], 1024, 569);  ?>
        <img src="<?php echo esc_url($img[0]); ?>" alt="Image" class="img-responsive">
    </div>
<?php  return ob_get_clean();
}

/* FEATURED BOX SHORTCODE */

add_shortcode( 'napapp_feature_box_shortcode', 'napapp_feature_box' );
function napapp_feature_box( $atts, $content = null ){

    extract(shortcode_atts(array(
        'nap_fb_image'      => '',
        'nap_fb_icon'       => '',
        'fb_icon_color'     => '#000',
        'fb_alignment'      => 'text-center',
        'nap_fb_title'       => '',
        'animation_duration' => '',
        'animation_delay'    => '',
        'animation_style'    => '',
        'custom_css_class'  => '',
    ), $atts));

    if( $fb_alignment == 'left' ) $alignment = "text-left";
    elseif( $fb_alignment == 'right' ) $alignment = "text-right";
    else $alignment = "text-center";

    ob_start();
?>

    <div class="<?php echo $custom_css_class; ?>">
        <div class="feature <?php echo esc_attr( $alignment ); ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <?php if( $nap_fb_image != '' && !empty($nap_fb_image) ) :  ?>
            <?php $img = wp_get_attachment_image_src($atts['nap_fb_image'], 68, 69);  ?>
                <img src="<?php echo esc_url($img[0]); ?>" alt="Image" class="img-responsive">
            <?php endif; ?>
            <?php if( $nap_fb_icon != '' && !empty( $nap_fb_icon )  ) : ?>
                <span class="feature-icon" style="color: <?php echo esc_html( $fb_icon_color ); ?>"><i class="<?php echo esc_html( $nap_fb_icon ); ?>"></i></span>
            <?php endif; ?>
            <h2><?php echo ( $nap_fb_title != "" ) ? esc_attr($nap_fb_title, 'napapp') : " " ; ?></h2>
            <p><?php echo $content ; ?></p>
        </div>
    </div>
<?php  return ob_get_clean();
}

add_shortcode('napapp_image_gallery','napapp_image_gallery_shortcode');

function napapp_image_gallery_shortcode($atts, $content = null){

    extract(shortcode_atts(array(
        'nap_images'    => '',
        'animation_duration' => '',
        'animation_delay'    => '',
        'animation_style'    => '',
        'custom_css_class'  => '',
    ), $atts));
    ob_start();
?>
<div id="screenshot-slider" class="owl-carousel <?php echo $custom_css_class; ?>">
    <?php
        $get_images = $atts['nap_images'];
        $images = explode(',', $get_images); ?>
        <?php foreach ( $images as $image ) : ?>
            <?php  $img = wp_get_attachment_image_src($image , 'full'); ?>
    <div class="screenshot" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
        <a class="image-link" href="<?php echo esc_url($img[0]); ?>"><img  class="img-responsive" src="<?php echo $img[0]; ?>" alt="Image"></a>
    </div>
    <?php endforeach; ?>
</div>

<?php  return ob_get_clean();
}

/* PRICE PLAN SHORTCODE */

add_shortcode( 'napapp_price_list', 'napapp_price_list_sc' );
function napapp_price_list_sc( $atts, $content = null ){

    extract(shortcode_atts(array(
        'nap_price'         => '',
        'nap_price_title'   => '',
        'nap_featured'      => '',
        'nap_btn'           => '',
        'nap_btn_link'      => '',
        'animation_duration'=> '',
        'animation_delay'   => '',
        'animation_style'   => '',
        'custom_css_class'  => '',
    ), $atts));
    ob_start();
?>
    <?php if( $nap_featured == 'yes') $featured= "featured"; else $featured = ''; ?>
    <div class="price <?php echo $featured; ?> <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
        <h1><?php echo ( $nap_price != "" ) ? esc_attr($nap_price, 'napapp') : " " ; ?></h1>
        <h2><?php echo ( $nap_price_title != "" ) ? esc_attr($nap_price_title, 'napapp') : " " ; ?></h2>
        <?php echo $content; ?>
        <a href="<?php echo ( $nap_btn_link != "" ) ? esc_attr($nap_btn_link, 'napapp') : "#" ; ?>" class="btn btn-primary btn-animated"><?php echo ( $nap_btn != "" ) ? esc_attr($nap_btn, 'napapp') : "Buy Now" ; ?></a>
    </div>
<?php  return ob_get_clean();
}

add_shortcode( 'napapp_testimonial_shortcode', 'napapp_testimonial_sc' );
function napapp_testimonial_sc ( $atts, $content = null ){
    extract(shortcode_atts(array(
        'testimonial_details'   => '',
        'max'                   => '5',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
?>
<?php
    $detail = vc_param_group_parse_atts( $atts['testimonial_details'] );
    $count = sizeof($detail);
    ?>
    <div id="testimonial-slider" class="carousel carousel-fade slide" data-ride="carousel">
        <div class="carousel-inner text-center <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <?php for ($i=0; $i < $count ; $i++) : ?>
            <div class="item <?php echo ( $i == 0 ) ? esc_attr( 'active' ) : ''; ?>">
                <?php if( !empty( $detail[$i]['testimonial_image'] ) ) : ?>
                    <?php $img_url = wp_get_attachment_image_src($detail[$i]['testimonial_image'], 75, 75); ?>
                    <img  class="img-responsive" src="<?php echo esc_url( $img_url[0] ); ?>" alt="Image">
                <?php endif; ?>
                <h2><?php echo ( $detail[$i]['testimonial_name'] != "" ) ? esc_attr($detail[$i]['testimonial_name'], 'napapp') : " " ; ?></h2>
                <?php
                $rating = $detail[$i]['testimonial_rating'];
                if( !empty( $rating ) ) :
                    if( $max == NULL ) {
                        $max = 5;
                    }
                    if( is_float( $rating ) ) {
                        $rating = preg_replace( '/\.?0+$/', '', (int)$rating );
                    }
                    $empty_rating = $max - $rating;
                    if( is_float( $empty_rating ) ) {
                        $filled = floor( $rating );
                        $half = 1;
                        $empty = floor($empty_rating);
                    } else {
                        $filled = $rating;
                        $half = 0;
                        $empty = $empty_rating;
                    }

                    if( $max < $filled ) {
                        $filled = $max;
                    }
                    if( !ctype_digit( strval( $empty ) ) ) {
                        $empty = 0;
                    }

                    $ssr_html = '<ul class="list-inline">';
                    $ssr_html .= str_repeat( '<li><i class="fa fa-star" aria-hidden="true"></i></li>', (int)$filled );
                    $ssr_html .= str_repeat( '<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>', $half );
                    $ssr_html .= str_repeat( '<li><i class="fa fa-star-o" aria-hidden="true"></i><li>', $empty );
                    $ssr_html .= "</ul>";
                    echo $ssr_html;
                endif;
                ?>
                <p><?php echo ( $detail[$i]['testimonial_comment'] != "" ) ? $detail[$i]['testimonial_comment'] : " " ; ?></p>
            </div>
            <?php endfor; ?>
        </div>
    </div>
<?php  return ob_get_clean();
}

add_shortcode( 'napapp_team_slider', 'napapp_team_slider_sc' );
function napapp_team_slider_sc ( $atts, $content = null ){
    extract(shortcode_atts(array(
        'team_details'          => '',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
?>
<?php
    $detail = vc_param_group_parse_atts( $atts['team_details'] );
    $count = sizeof($detail);
    ?>

    <div id="team-slider" class="owl-carousel">
    <?php for ($i=0; $i < $count ; $i++) : ?>
        <div class="team <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <?php if( !empty( $detail[$i]['team_image'] ) ) : ?>
                <?php $img_url = wp_get_attachment_image_src($detail[$i]['team_image'], 174, 174); ?>
                <img  class="img-responsive" src="<?php echo esc_url( $img_url[0] ); ?>" alt="Image">
            <?php endif; ?>
            <div class="team-info">
                <?php if( ! empty($detail[$i]['team_name']) ){ ?>
                    <h2><?php echo esc_html( $detail[$i]['team_name'] ); ?></h2>
                <?php } else echo ''; ?>
                <?php if( ! empty($detail[$i]['team_position']) ) { ?>
                    <p><?php echo esc_html( $detail[$i]['team_position'] ); ?></p>
                <?php } else echo ''; ?>
                <ul class="list-inline social-icons">
                <?php $social = vc_param_group_parse_atts( $detail[$i]['team_social_icon'] );
                        $social_count = sizeof($social);
                    for ($j=0; $j < $social_count ; $j++) {  ?>
                        <li><a href="<?php echo esc_url( $social[$j]['social_link'] ); ?>"><i class="<?php echo esc_attr( $social[$j]['icon_fontawesome'] ); ?>"></i></a></li>
                <?php  } ?>
                </ul>
            </div>
        </div>
    <?php endfor; ?>
    </div>
<?php  return ob_get_clean();
}

add_shortcode('napapp_twitter_feed', 'napapp_twitter_feed_shortcode');

function napapp_twitter_feed_shortcode($atts , $content = null){
    extract( shortcode_atts( array(
        'number_of_post'        => '3',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));

    ob_start();
    $theme_option = get_option('napapp_theme_options');

    $TweetPHP = new TweetPHP(array(
      'consumer_key'              => $theme_option['_twitter_consumer_key'],
      'consumer_secret'           => $theme_option['_twitter_consumer_secret'],
      'access_token'              => $theme_option['_twitter_access_token'],
      'access_token_secret'       => $theme_option['_twitter_access_token_secret'],
      'twitter_screen_name'       => $theme_option['_twitter_screen_name']
    ));
    $tweet_array = $TweetPHP->get_tweet_array();
    ?>
    <div class="col-sm-8 col-sm-offset-2">
        <div class="twitter-slider owl-carousel">
            <?php for( $i = 0 ; $i < $number_of_post; $i++ ){ ?>
            <div class="twitter-feed text-center <?php echo $custom_css_class; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
                <p><?php echo $TweetPHP->autolink($tweet_array[$i]['text']); ?></p>
                <?php $date = explode(" ", $tweet_array[$i]['created_at']) ; ?>
                <h4><a href="https://twitter.com/<?php echo esc_html( $theme_option['_twitter_screen_name'] ); ?>">@<?php echo esc_html( $theme_option['_twitter_screen_name'] ); ?></a> - <?php echo esc_attr__( $date[1] )." ".esc_attr__($date[2], 'napapp')."th, ".esc_attr__($date[5], 'napapp'); ?></h4>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

add_shortcode( 'napapp_call_to_action', 'napapp_call_to_action_sc' );

function napapp_call_to_action_sc ( $atts, $content = null ){
    extract(shortcode_atts(array(
        'cta_icons'             => '',
        'cta_alignment'         => 'text-center',
        'animation_duration'    => '',
        'animation_delay'       => '',
        'animation_style'       => '',
        'custom_css_class'      => '',
    ), $atts));
    ob_start();
?>
<?php
    $icons = vc_param_group_parse_atts( $atts['cta_icons'] );
    $count = sizeof($icons);
    if( $cta_alignment == 'left' ) $alignment = "text-left";
    elseif( $cta_alignment == 'right' ) $alignment = "text-right";
    else $alignment = "text-center";
    ?>
    <div class="col-sm-12">
        <div class="download-apps <?php echo esc_attr( $alignment ); ?> <?php echo esc_attr( $custom_css_class ) ; ?>" data-aos-duration="<?php echo esc_attr( $animation_duration ); ?>" data-aos-delay="<?php echo esc_attr( $animation_delay ); ?>" data-aos="<?php echo esc_attr( $animation_style ); ?>">
            <?php echo $content; ?>
            <div class="apps">
                <ul class="list-inline">
                <?php for ($i=0; $i < $count ; $i++) : ?>
                    <li data-aos-duration="<?php echo esc_attr( $icons[$i]['cta_animation_duration'] ); ?>" data-aos-delay="<?php echo esc_attr( $icons[$i]['cta_animation_delay'] ); ?>" data-aos="<?php echo esc_attr( $icons[$i]['cta_animation_style'] ); ?>">
                        <a href="<?php echo esc_url( $icons[$i]['cta_link'] ); ?>"><i class="<?php echo esc_attr( $icons[$i]['cta_icon'] ); ?>"></i></a>
                    </li>
                <?php endfor; ?>
                </ul>
            </div>
        </div>
    </div>
<?php  return ob_get_clean();
}
