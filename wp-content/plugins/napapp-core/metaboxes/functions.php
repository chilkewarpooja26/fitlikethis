<?php

if ( file_exists( NA_ROOT . '/init.php' ) ) {
	require_once  NA_ROOT . '/init.php';
} elseif ( file_exists(  NA_ROOT . 'CMB2.php' ) ) {
	require_once NA_ROOT . '/CMB2.php';
}

if ( !function_exists( 'napapp_post_meta' ) ) {

	add_action( 'cmb2_admin_init', 'napapp_post_meta' );
	function napapp_post_meta() {

		$prefix = '_na_';

		$na_posts = new_cmb2_box( array(
	        'id'           => $prefix . 'pagefw_setup',
	        'title'        => 'One Page Settings',
	        'object_types' => array( 'page' ), // post type
	        'show_on'      => array( 'key' => 'page-template', 'value' => 'tpl-onepage.php' ),
	        'context'      => 'normal', //  'normal', 'advanced', or 'side'
	        'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	        'show_names'   => true, // Show field names on the left
	    ) );

		$na_posts->add_field( array(
			'name' => esc_html__( 'Enable/Disable Banner', 'napapp' ),
			'desc' => esc_html__( 'check to enable/disable banner in frontpage', 'napapp' ),
			'id'   => $prefix . 'ed_banner',
			'type' => 'checkbox',
		) );

		$na_posts->add_field( array(
			'name' => esc_html__( 'Move Banner Below Navigation', 'napapp' ),
			'desc' => esc_html__( 'check to move banner below navigation', 'napapp' ),
			'id'   => $prefix . 'mv_banner',
			'type' => 'checkbox',
			'default' => false,
		) );

		$na_posts->add_field( array(

			'name'       => esc_html__( 'Section ID', 'napapp' ),
			'desc'       => esc_html__( 'this section will show at the very first portion of the page', 'napapp' ),
			'id'         => $prefix . 'section_id',
			'type'       => 'text',
			'default'    => 'home-section',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Banner Background', 'napapp' ),
			'desc' => esc_html__( 'Upload banner background image', 'napapp' ),
			'id'   => $prefix . 'banner_bg',
			'type' => 'file',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Banner Callout Image ', 'napapp' ),
			'desc' => esc_html__( 'Upload callout image for the banner Ex: logo image or something', 'napapp' ),
			'id'   => $prefix . 'banner_callout',
			'type' => 'file',
		) );

		$na_posts->add_field( array(

			'name'       => esc_html__( 'Banner Content', 'napapp' ),
			'id'         => $prefix . 'banner_content',
			'type'       => 'wysiwyg',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Callout Button 1 Text ', 'napapp' ),
			'desc' => esc_html__( 'Enter button text. Ex: Free Trail', 'napapp' ),
			'id'   => $prefix . 'cta_btn_1_text',
			'type' => 'text',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Callout Button 1 Link ', 'napapp' ),
			'desc' => esc_html__( 'Enter button link here. Ex: #', 'napapp' ),
			'id'   => $prefix . 'cta_btn_1_link',
			'default'=> '#',
			'type' => 'text',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Callout Button 2 Text ', 'napapp' ),
			'desc' => esc_html__( 'Enter button text. Ex: Buy Now', 'napapp' ),
			'id'   => $prefix . 'cta_btn_2_text',
			'type' => 'text',
		) );

		$na_posts->add_field( array(

			'name' => esc_html__( 'Callout Button 2 Link ', 'napapp' ),
			'desc' => esc_html__( 'Enter button link here. Ex: http://example.com', 'napapp' ),
			'id'   => $prefix . 'cta_btn_2_link',
			'default'=> '#',
			'type' => 'text',
		) );

		$group_field_id = $na_posts->add_field( array(
			'id'          => $prefix . 'social_icons_list',
			'type'        => 'group',
			'description' => esc_html__( 'Choose Social Icons To Be Displayed On Header Secion', 'napapp' ),
			'options'     => array(
				'group_title'   => esc_html__( 'Social Icon {#}', 'napapp' ), // {#} gets replaced by row number
				'add_button'    => esc_html__( 'Add Another Icon', 'napapp' ),
				'remove_button' => esc_html__( 'Remove Icon', 'napapp' ),
				'sortable'      => true, // beta
				'closed'     => true, // true to have the groups closed by default
			),
		) );

		$na_posts->add_group_field( $group_field_id, array(
		  'name'        => esc_html__( 'Select Icon', 'napapp' ),
		  'id'          => $prefix . 'icon',
		  'type'        => 'fontawesome_icon', // This field type
		) );

		$na_posts->add_group_field( $group_field_id, array(
			'name'       => esc_html__( 'Icon URL', 'napapp' ),
			'id'         => $prefix . 'icon_url',
			'type'       => 'text_url',
		) );
	}
}
