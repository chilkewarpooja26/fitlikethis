<?php
/**
* Plugin Name: Napapp Core
* Plugin URI: http://plugins.themeregion.com/napapp
* Description: Core functionalities of Napapp theme. The theme's functionalities depends on this plugin.
* Version:  1.0
* Author: ThemeRegion Team
* Author URI: http://themeregion.com/
* License:  GPL2
*/

// Define Constants
defined( 'NA_ROOT' ) or define( 'NA_ROOT', dirname( __FILE__ ) );
defined( 'NA_URI' ) or define( 'NA_URI', get_template_directory_uri() );
defined( 'NA_VERSION' ) or define( 'NA_VERSION', '1.0' );

if ( ! class_exists( 'Napapp_Core' ) ) {

	class Napapp_Core {

		public function napapp_load_theme_backbone() {
			require_once NA_ROOT . '/theme-option/ReduxCore/framework.php';
			require_once NA_ROOT . '/theme-option/options.php';
			require_once NA_ROOT . '/metaboxes/init.php';
			require_once NA_ROOT . '/metaboxes/functions.php';
			require_once NA_ROOT . '/metaboxes/cmb2-fontawesome-picker.php';
			require_once NA_ROOT . '/twitter-tweet/TweetPHP.php';
		}

		public function napapp_load_shortcodes() {

			if ( class_exists( 'Vc_Manager' ) ) {
				require_once NA_ROOT . '/custom-visual/vc-mapping.php';
				require_once NA_ROOT . '/custom-visual/vc-shortcode.php';
			}

		}

		/**
		 * Load plugin textdomain.
		 */
		function napapp_core_load_textdomain() {
		  	load_plugin_textdomain( 'napapp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}

	}

	$core = new Napapp_Core;
	$core->napapp_core_load_textdomain();
	$core->napapp_load_theme_backbone();
	$core->napapp_load_shortcodes();
}
