<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "napapp_theme_options";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'napapp_theme_options/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'submenu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'NapApp Options', 'napapp' ),
        'page_title'           => __( 'NapApp Options', 'napapp' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        'forced_dev_mode_off'  => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => 'napapp-options',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /* As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Header', 'napapp' ),
        'id'               => 'na_header',
        'desc'             => __( 'Settings for header!', 'napapp' ),
        'subsection'       => false,
        'icon'             => 'el el-arrow-up',
        'fields'           => array(
            array(
                'id'       => 'na_logo_header',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Logo', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Preferred image size: 105x31', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/logo.png' ),
            ),
            array(
                'id'       => 'na_page_preloader',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Page Preloader', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Preferred GIF size: 66x66', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/preloader.gif' ),
            ),
            array(
                'id'       => 'na_favicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Favicon', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Upload image size: 16x16', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/ico/favicon.ico' ),
            ),
            array(
                'id'       => 'na_apple_fav_57',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Apple Touch Icon 57x57', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Upload image size: 57x57', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/ico/apple-touch-icon-57-precomposed.png' ),
            ),
            array(
                'id'       => 'na_apple_fav_72',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Apple Touch Icon 72x72', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Upload image size: 72x72', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/ico/apple-touch-icon-72-precomposed.png' ),
            ),
            array(
                'id'       => 'na_apple_fav_114',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Apple Touch Icon 114x114', 'napapp' ),
                'compiler' => 'true',
                'desc'     => __( 'Upload image size: 114x114', 'napapp' ),
                'default'  => array( 'url' => NA_URI . '/assets/images/ico/apple-touch-icon-114-precomposed.png' ),
            ),
            array(
                'id'       => 'na_head_btn_title',
                'type'     => 'text',
                'title'    => __( 'Header Button Title', 'napapp' ),
                'default'  => __( 'Download', 'napapp' ),
            ),
            array(
                'id'       => 'na_head_btn_link',
                'type'     => 'text',
                'title'    => __( 'Header Button URL', 'napapp' ),
                'default'  => __( '#', 'napapp' ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Styling', 'napapp' ),
        'id'               => 'na_styling',
        'desc'             => __( 'Choose different styles to change the look of the theme!', 'napapp' ),
        'subsection'       => false,
        'icon'             => 'el el-brush',
        'fields'           => array(
            array(
                'id'       => 'na_skin',
                'type'     => 'select',
                'title'    => __( 'Select Theme Skin', 'napapp' ),
                'desc'     => __( 'Choose your preferred theme skin for styling your theme', 'napapp' ),
                'options'  => array(
                    'default'  => 'Default',
                    'blue'     => 'Blue',
                    'blue-flat'=> 'Blue Flat',
                    'red'      => 'Red',
                    'dark'     => 'Dark',
                    'custom'   => 'Custom Skin',
                ),
                'default'  => 'default',
            ),
            array(
                'id'       => 'na_custom_skin',
                'type'     => 'color',
                'title'    => __( 'Custom Skin Color', 'napapp'),
                'subtitle' => __( 'Pick a color for the theme (default: #a0d48c).', 'napapp' ),
                'default'  => '#a0d48c',
                'validate' => 'color',
                'required' => array('na_skin','=','custom'),
            ),
            array(
                'id'        => 'na_skin_color_rgba',
                'type'      => 'color_rgba',
                'title'     => 'Background Overlay',
                'subtitle'  => 'Set color and alpha channel',
                'desc'      => 'Change overlay color with RGBA according to your custom skin!',
                'default'   => array(
                    'color'     => '#a0d48c',
                    'alpha'     => 0.9
                ),
                // These options display a fully functional color palette.  Omit this argument
                // for the minimal color picker, and change as desired.
                'options'       => array(
                    'show_input'                => true,
                    'show_initial'              => true,
                    'show_alpha'                => true,
                    'show_palette'              => true,
                    'show_palette_only'         => false,
                    'show_selection_palette'    => true,
                    'max_palette_size'          => 10,
                    'allow_empty'               => true,
                    'clickout_fires_change'     => false,
                    'choose_text'               => 'Choose',
                    'cancel_text'               => 'Cancel',
                    'show_buttons'              => true,
                    'use_extended_classes'      => true,
                    'palette'                   => null,  // show default
                    'input_text'                => 'Select Color'
                ),
                'required' => array('na_skin','=','custom'),
            ),
            array(
                'id'          => 'na_body_typo',
                'type'        => 'typography',
                'title'       => __( 'Body Typography', 'napapp' ),
                'google'      => true,
                'subsets'     => false,
                'line-height' => false,
                'text-align'  => false,
                'font-weight' => false,
                'units'       =>'px',
                'default'     => array(
                    'color'       => '#4f4f4f',
                    'font-family' => 'Open Sans',
                    'google'      => true,
                    'font-size'   => '16px',
                ),
            ),
            array(
                'id'            => 'na_head_typo',
                'type'          => 'typography',
                'title'         => __( 'Global Heading Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-size'     => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-family' => 'Ubuntu',
                    'font-weight' => '500',
                    'google'      => true,
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h1_typo',
                'type'          => 'typography',
                'title'         => __( 'H1 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '36px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h2_typo',
                'type'          => 'typography',
                'title'         => __( 'H2 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '26px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h3_typo',
                'type'          => 'typography',
                'title'         => __( 'H3 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '20px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h4_typo',
                'type'          => 'typography',
                'title'         => __( 'H4 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '18px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h5_typo',
                'type'          => 'typography',
                'title'         => __( 'H5 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '16px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
            array(
                'id'            => 'na_h6_typo',
                'type'          => 'typography',
                'title'         => __( 'H6 Typography', 'napapp' ),
                'google'        => true,
                'text-transform'=> true,
                'subsets'       => false,
                'line-height'   => false,
                'text-align'    => false,
                'font-family'   => false,
                'units'         =>'px',
                'default'       => array(
                    'color'       => '#4f4f4f',
                    'font-size'   => '14px',
                    'font-weight' => 'bold',
                    'text-transform'=> 'capitalize',
                ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Custom Code', 'napapp' ),
        'id'               => 'na_custom_code',
        'desc'             => __( 'Advanced coding section for altering themes settings/styles!', 'napapp' ),
        'subsection'       => false,
        'icon'             => 'el el-cog',
        'fields'           => array(
            array(
                'id'       => 'na_css_editor',
                'type'     => 'ace_editor',
                'title'    => __( 'CSS Code', 'napapp' ),
                'subtitle' => __( 'Paste your CSS code here.', 'napapp' ),
                'mode'     => 'css',
                'theme'    => 'monokai'
            ),
            array(
                'id'       => 'na_js_editor_header',
                'type'     => 'ace_editor',
                'title'    => __( 'Header Javascript Code', 'napapp' ),
                'subtitle' => __( 'Paste your js code here without script tag. This code will be placed just before head tag closed', 'napapp' ),
                'mode'     => 'js',
                'theme'    => 'monokai'
            ),
            array(
                'id'       => 'na_js_editor_footer',
                'type'     => 'ace_editor',
                'title'    => __( 'Footer Javascript Code', 'napapp' ),
                'subtitle' => __( 'Paste your js code here without script tag. This code will be placed just before body tag closed', 'napapp' ),
                'mode'     => 'js',
                'theme'    => 'monokai'
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer', 'napapp' ),
        'id'               => 'footer_setting',
        'desc'             => __( 'Settings for footer!', 'napapp' ),
        'subsection'       => false,
        'icon'             => 'el el-arrow-down',
        'fields'           => array(
            array(
                'id'       => 'na_copyright',
                'title'    => __( 'Enter copyright by default or html', 'napapp' ),
                'type'     => 'editor',
                'args'     => array(
                    'teeny'            => true,
                    'textarea_rows'    => 10
                ),
                'default'  => 'Copyright © 2017 <a href="#">Nap App</a>.'
            ),
            array(
                'id'       => 'na_enable_footer_nav',
                'type'     => 'switch',
                'title'    => __( 'Enable Navigation In Footer', 'napapp' ),
                'subtitle' => __( 'Switch it to enable/disable navigation in footer section', 'napapp' ),
                'default'  => true,
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Twitter Settings', 'napapp' ),
        'id'               => 'twitter_setting',
        'desc'             => __( 'Settings for twitter!', 'napapp' ),
        'subsection'       => false,
        'icon'             => 'el el-twitter',
        'fields'           => array(
            array(
                'type'    => 'subheading',
                'content'   => __('Get your Twitter API Key from <a href="https://apps.twitter.com/">Here</a>', 'napapp'),
            ),
            array(
                'id'      => '_twitter_consumer_key',
                'type'    => 'text',
                'title'   => __('Consumer Key', 'napapp'),
                'default' => '',
            ),
            array(
                'id'      => '_twitter_consumer_secret',
                'type'    => 'text',
                'title'   => __('Consumer Secret', 'napapp'),
                'default' => '',
            ),
            array(
                'id'      => '_twitter_access_token',
                'type'    => 'text',
                'title'   => __('Access Token', 'napapp'),
                'default' => '',
            ),
            array(
                'id'      => '_twitter_access_token_secret',
                'type'    => 'text',
                'title'   => __('Access Token Secret', 'napapp'),
                'default' => '',
            ),
            array(
                'id'      => '_twitter_screen_name',
                'type'    => 'text',
                'title'   => __('Twitter Username', 'napapp'),
                'default' => '',
            ),
        )
    ) );

    $section = array(
        'icon'   => 'el el-list-alt',
        'title'  => __( 'Documentation', 'napapp' ),
        'fields' => array(
            array(
                'id'       => 'na_docs',
                'type'     => 'raw',
                'markdown' => true,
                //'content_path' => NA_URI  . '/README.md', // FULL PATH, not relative please
                'content' => '
=== Nap App ===

Author: ThemeRegion Team <br/>
Tags: translation-ready, custom-background, theme-options, custom-menu, threaded-comments, one-column, two-columns, sticky-post, right-sidebar

Requires at least: 4.0
Tested up to: 4.7.2
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A beautiful WordPress base app landing page.

== Description ==

This is a beautiful WordPress base app landing page theme, if you like. This is a theme meant for hacking so don\'t use me as a Parent Theme. Instead try turning this into the next, most awesome, WordPress theme out there. That\'s what this theme is here for.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme\'s .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Nap App includes support for Infinite Scroll in Jetpack.
This also includes Visual Composer plugin support
                ',
            ),
        ),
    );
    Redux::setSection( $opt_name, $section );


    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

    if ( !function_exists( 'napapp_importer_settings' ) ) {
    	function napapp_importer_settings( $demo_active_import , $demo_directory_path ) {
    		reset( $demo_active_import );
    		$current_key = key( $demo_active_import );

    		/************************************************************************
    		* Setting Menus
    		*************************************************************************/
    		// If it's demo1 - demo6
    		$wbc_menu_array = array( 'demo1' );
    		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
    			$top_menu = get_term_by( 'name', 'Temp Menu', 'nav_menu' );
    			if ( isset( $top_menu->term_id ) ) {
    				set_theme_mod( 'nav_menu_locations', array(
    						'primary-nav' => $top_menu->term_id,
    						'footer-nav'  => $top_menu->term_id
    					)
    				);
    			}
    		}
    		/************************************************************************
    		* Set HomePage
    		*************************************************************************/
    		// array of demos/homepages to check/select from
    		$wbc_home_pages = array(
    			'demo1' => 'FrontPage',
    		);
    		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
    			$page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
    			if ( isset( $page->ID ) ) {
    				update_option( 'page_on_front', $page->ID );
    				update_option( 'show_on_front', 'page' );
    			}
    		}
    	}
        add_action( 'wbc_importer_after_content_import', 'napapp_importer_settings', 10, 2 );
    }
