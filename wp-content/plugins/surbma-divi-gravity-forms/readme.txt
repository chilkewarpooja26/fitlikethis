=== Surbma - Divi & Gravity Forms ===
Contributors: Surbma
Donate link: http://surbma.com/wordpress-plugins/
Tags: divi, divi theme, gravityforms, gravity forms
Requires at least: 4.0
Tested up to: 4.4
Stable tag: 1.3.3
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Responsive Divi form styles for Gravity Forms.

== Description ==

This plugin adds the same styling for Gravity Forms as Divi Theme has for its own forms. This is a very lightweight plugin with minified css to reduce the loading time as low as possible.

= Responsive style =

It also adds responsive styles to Gravity Forms. So you can use complex forms, halves or thirds fields, your form will look great on mobile screens also.

You have to buy the Divi Theme and the Gravity Forms Plugin to use this plugin:

- [Divi by Elegant Themes](http://surbma.com/go/elegantthemes/) (affiliate link)
- [Gravity Forms](http://surbma.com/go/gravityforms/) (affiliate link)

**My plugins for Divi theme:**

- [Surbma - Divi Extras](https://wordpress.org/plugins/surbma-divi-extras/)
- [Surbma - Divi & Gravity Forms](https://wordpress.org/plugins/surbma-divi-gravity-forms/)
- [Surbma - Divi Project Shortcodes](https://wordpress.org/plugins/surbma-divi-project-shortcodes/)

**Do you want to contribute or help improving this plugin?**

You can find it on GitHub: [https://github.com/Surbma/surbma-divi-gravity-forms](https://github.com/Surbma/surbma-divi-gravity-forms)

**You can find my other plugins and projects on GitHub:**

[https://github.com/Surbma](https://github.com/Surbma)

Please feel free to contribute, help or recommend any new features for my plugins, themes and other projects.

**Do you want to know more about me?**

Visit my webpage: [Surbma.com](http://surbma.com/)

== Installation ==

1. Upload the `surbma-divi-gravity-forms` folder to the `/wp-content/plugins/` directory.
1. Activate the Surbma - Divi & Gravity Forms plugin through the 'Plugins' menu in WordPress.
1. That's it.

== Frequently Asked Questions ==

= What does Surbma mean? =

It is the reverse version of my last name. ;)

== Changelog ==

= 1.3.3 =

- Fixed button text color setting. (Props Tevya)
- Tested with WordPress 4.4 version.
- Checked for PHP 7 compatibility.

= 1.3.2 =

- Fixed an undefined variable. (Props Tevya)

= 1.3.1 =

- Fix for submit button. (Props agiuliano)

= 1.3.0 =

- Form buttons are now controlled by Divi's Buttons Style options in the customizer. I have made it safe to update without loosing any styling, but please check your forms after updating the plugin! Especially if you have custom css for these buttons. Any feedback is welcome!
- Fixed textdomain path for localization.
- Minor code fixes.

= 1.2.5 =

- Fixed responsive styling for Gravity Forms CSS Ready Classes. (Props Guilamu)

= 1.2.4 =

- Tested with WordPress 4.2 version.
- Fixed date and time fields.

= 1.2.3 =

- Fixes in description.

= 1.2.2 =

- Added links in the plugin description to my other plugins for Divi theme. Give them a try!

= 1.2.1 =

- Changed description.

= 1.2.0 =

- Responsive styles added! :)
- Minor fixes in css.
- Changed description.

= 1.1.1 =

- Fix localization.

= 1.1.0 =

- Prevent direct access to plugin's file.
- Check for Divi Theme and Gravity Forms plugin, if they already installed and activated.

= 1.0.1 =

- Added Hungarian language.

= 1.0.0 =

- Initial release.
- First commit to the official WordPress repo.
