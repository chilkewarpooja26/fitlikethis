<?php

	/***
	***	@Error processing hook : login
	***/
	add_action('um_submit_form_errors_hook_login', 'um_submit_form_errors_hook_login', 10);
	function um_submit_form_errors_hook_login( $args ){
		global $ultimatemember;
		
		$is_email = false;
		
		$form_id = $args['form_id'];
		$mode = $args['mode'];

		if ( isset( $args['username'] ) && $args['username'] == '' ) {
			$ultimatemember->form->add_error( 'username',  __('Please enter your  email.','ultimatemember') );
		}
		
		if ( isset( $args['user_login'] ) && $args['user_login'] == '' ) {
			$ultimatemember->form->add_error( 'user_login',  __('Please enter your username','ultimatemember') );
		}
		
		if ( isset( $args['user_email'] ) && $args['user_email'] == '' ) {
			$ultimatemember->form->add_error( 'user_email',  __('Please enter your email.','ultimatemember') );
		}
		
		if ( isset( $args['username'] ) ) {
			$field = 'username';
			if ( is_email( $args['username'] ) ) {
				$is_email = true;
				$data = get_user_by('email', $args['username'] );
				$user_name = (isset ( $data->user_login ) ) ? $data->user_login : null;
			} else {
				$user_name  = $args['username'];
			}
		} else if ( isset( $args['user_email'] ) ) {
				$field = 'user_email';
				$is_email = true;
				$data = get_user_by('email', $args['user_email'] );
				$user_name = (isset ( $data->user_login ) ) ? $data->user_login : null;
		} else {
				$field = 'user_login';
				$user_name = $args['user_login'];
		}
		
		if ( !username_exists( $user_name ) ) {
			if ( $is_email ) {
				$ultimatemember->form->add_error( $field,  __(' Sorry, we can\'t find an account with that email address.','ultimatemember') );
			} else {
				$ultimatemember->form->add_error( $field,  __(' Please enter valid email address.','ultimatemember') );
			}
		} else {
			if ( $args['user_password'] == '' ) {
				$ultimatemember->form->add_error( 'user_password',  __('Please enter your password.','ultimatemember') );
			}
		}
		
		$user = get_user_by( 'login', $user_name );

                /* 30 Nov 2016  */
		if ( $args['user_password'] == '' ) {
				$ultimatemember->form->add_error( 'user_password',  __('Please enter your password.','ultimatemember') );
			}

		if ( $user && wp_check_password( $args['user_password'], $user->data->user_pass, $user->ID) ) {
			$ultimatemember->login->auth_id = username_exists( $user_name );
		} else {
			$ultimatemember->form->add_error( 'user_password',  __('Please enter correct password.','ultimatemember') );
		}
		
	}
	
	/***
	***	@login checks thru the frontend login
	***/
	add_action('um_submit_form_errors_hook_logincheck', 'um_submit_form_errors_hook_logincheck', 9999 );
	function um_submit_form_errors_hook_logincheck($args){
		global $ultimatemember;
		
		// Logout if logged in
		if ( is_user_logged_in() ) {
			wp_logout();
		}

		$user_id = ( isset( $ultimatemember->login->auth_id ) ) ? $ultimatemember->login->auth_id : '';
		um_fetch_user( $user_id );
		
		$status = um_user('account_status'); // account status
		switch( $status ) {
		
			// If user can't login to site...
			case 'inactive':
			case 'awaiting_admin_review':
			case 'awaiting_email_confirmation':
			case 'rejected':
				um_reset_user();
				exit( wp_redirect(  add_query_arg( 'err', esc_attr( $status ), $ultimatemember->permalinks->get_current_url() ) ) );
				break;
				
		}
		
		if ( isset( $args['form_id'] ) && $args['form_id'] == $ultimatemember->shortcodes->core_login_form() &&  $ultimatemember->form->errors && !isset( $_POST[ $ultimatemember->honeypot ] ) ) {
			exit( wp_redirect( um_get_core_page('login') ) );
		}
		
	}
	
	/***
	***	@store last login timestamp
	***/
	add_action('um_on_login_before_redirect', 'um_store_lastlogin_timestamp', 10);
	function um_store_lastlogin_timestamp( $user_id ) {
		delete_user_meta( $user_id, '_um_last_login' );
		update_user_meta( $user_id, '_um_last_login', current_time( 'timestamp' ) );
	}
	
	add_action( 'wp_login', 'um_store_lastlogin_timestamp_' );
	function um_store_lastlogin_timestamp_( $login ) {
		$user = get_user_by('login',$login);
		$user_id = $user->ID;
		delete_user_meta( $user_id, '_um_last_login' );
		update_user_meta( $user_id, '_um_last_login', current_time( 'timestamp' ) );
	}
	
	/***
	***	@login user
	***/
	add_action('um_user_login', 'um_user_login', 10);
	function um_user_login($args){
		global $ultimatemember;
		extract( $args );

		$rememberme = ( isset($args['rememberme']) ) ? 1 : 0;
		
		if ( um_get_option('deny_admin_frontend_login') && strstr( um_user('wp_roles' ), 'administrator' ) )
			wp_die( __('This action has been prevented for security measures.','ultimatemember') );
		
		$ultimatemember->user->auto_login( um_user('ID'), $rememberme );
		
		// Hook that runs after successful login and before user is redirected
		do_action('um_on_login_before_redirect', um_user('ID') );
	
		if ( isset( $args['redirect_to'] ) && ! empty( $args['redirect_to']  ) ) {
			//exit( wp_redirect(  urldecode( $args['redirect_to'] ) ) );
		     global $wpdb;
                     $user_id=um_user('ID');
                     $payment_status= get_user_meta($user_id,"payment_status", true);
                     $user_type= get_user_meta($user_id,"user_type", true);
                     if($user_type=='User' && $payment_status=='1')
                       {
                        $url = get_home_url() . '/dashboard';wp_redirect($url);exit();
                       }else if($user_type=='User' && $payment_status=='' || $payment_status=='0')
                       {
                        wp_redirect( home_url() ); exit();
                       }else
                       {
                        $url = get_home_url() . '/nutritionists-dashboard';wp_redirect($url);exit();
                       }

		}
		
		// Role redirect
		$after = um_user('after_login');
		switch( $after ) {
			
			case 'redirect_admin':
				exit( wp_redirect( admin_url() ) );
				break;
				
			case 'redirect_profile':
				exit( wp_redirect( um_user_profile_url() ) );
				break;
			
			case 'redirect_url':
				exit( wp_redirect( um_user('login_redirect_url') ) );
				break;
				
			case 'refresh':
				exit( wp_redirect( $ultimatemember->permalinks->get_current_url() ) );
				break;
				
		}
	
	}
	
	/***
	***	@form processing
	***/
	add_action('um_submit_form_login', 'um_submit_form_login', 10);
	function um_submit_form_login($args){
		global $ultimatemember;
		
		if ( !isset($ultimatemember->form->errors) ) {
			do_action( 'um_user_login', $args );
		}
		
		do_action('um_user_login_extra_hook', $args );
		
	}

	/***
	***	@Show the submit button
	***/
	add_action('um_after_login_fields', 'um_add_submit_button_to_login', 1000);
	function um_add_submit_button_to_login($args){
		global $ultimatemember;

		// DO NOT add when reviewing user's details
		if ( $ultimatemember->user->preview == true && is_admin() ) return;
		
		$primary_btn_word = $args['primary_btn_word'];
		$primary_btn_word = apply_filters('um_login_form_button_one', $primary_btn_word, $args );
		
		$secondary_btn_word = $args['secondary_btn_word'];
		$secondary_btn_word = apply_filters('um_login_form_button_two', $secondary_btn_word, $args );
		
		$secondary_btn_url = ( isset( $args['secondary_btn_url'] ) && $args['secondary_btn_url'] ) ? $args['secondary_btn_url'] : um_get_core_page('register');
		$secondary_btn_url = apply_filters('um_login_form_button_two_url', $secondary_btn_url, $args );
		
		?>
		
		<div class="um-col-alt">

			<?php if ( isset( $args['show_rememberme'] ) && $args['show_rememberme'] ) {
					echo $ultimatemember->fields->checkbox('rememberme', __('Keep me signed in','ultimatemember') );
					echo '<div class="um-clear"></div>';
			} ?>

			<?php if ( isset($args['secondary_btn']) && $args['secondary_btn'] != 0 ) { ?>
			<div class="um-left um-half"><input type="submit" value="<?php echo $primary_btn_word; ?>" id="loginpaypalbutton" class="um-button" /></div>
                <?php

if (isset($_GET['status']))
		{
			//echo $_POST['status'];exit;
			if($_POST['paypalurl']!="")
			{ 
				//echo $_POST['paypalurl'];exit;

			
				 $url1=$_POST['paypalurl'].'?a3='.$_POST['a3'].'&p3='.$_POST['p3'].'&t3='.$_POST['t3'].'&business='.$_POST['business'].'&cmd='.$_POST['cmd'].'&currency_code='.$_POST['currency_code'].'&cancel_return='.$_POST['cancel_return'].'&return='.$_POST['return'];
	    
	    //echo $url;exit;
	    	//wp_redirect( $url);exit;
				
			?>
<div class="um-right um-half"><a href=<?php site_url()?>"./paypal-registration/?status=1" id="registerpaypalbutton" class="um-button um-alt">REGISTER</a></div>
			<?php }
		}







else {?>
			<div class="um-right um-half"><a href="<?php echo $secondary_btn_url; ?>" class="um-button um-alt"><?php echo $secondary_btn_word; ?></a></div>
			<?php } ?>
			<?php } else { ?>
			
			<div class="um-center"><input type="submit" value="<?php echo $args['primary_btn_word']; ?>" class="um-button" /></div>
			
			<?php } ?>
			
			<div class="um-clear"></div>
			
		</div>
	
		<?php
	}

	/***
	***	@Display a forgot password link
	***/
	add_action('um_after_login_fields', 'um_after_login_submit', 1001);
	function um_after_login_submit( $args ){ 
		global $ultimatemember;

		if ( $args['forgot_pass_link'] == 0 ) return;
	
	?>
		
		<div class="um-col-alt-b">
			<a href="<?php echo um_get_core_page('password-reset'); ?>" class="um-link-alt"><?php _e('Forgot your password?','ultimatemember'); ?></a>
		
            <a href="<?php echo get_home_url();?>/wp-login.php?loginFacebook=1&redirect=<?php echo get_home_url();?>" onclick="window.location = '<?php echo get_home_url();?>/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;"> <img src="<?php echo get_home_url();?>/wp-content/uploads/2016/06/facebook-login-icon.png" /> </a>
		</div>
		
		<?php
	}
	
	/***
	***	@Show Fields
	***/
	add_action('um_main_login_fields', 'um_add_login_fields', 100);
	function um_add_login_fields($args){
		global $ultimatemember;
		
		echo $ultimatemember->fields->display( 'login', $args );
		
	}
