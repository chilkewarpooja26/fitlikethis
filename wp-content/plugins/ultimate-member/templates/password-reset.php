<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo $form_id; ?>">

	<div class="um-form">
	
		<form method="post">
		
			<?php
			
			if ( !isset( $ultimatemember->password->reset_request ) ) {
			
				do_action('um_reset_password_page_hidden_fields', $args );
				
				do_action('um_reset_password_form', $args );
				
				do_action("um_after_form_fields", $args);
			
			} else {
			
				echo '<div class="um-field-block">';
				
				echo '<div class="check-inbox">' . __('<p>Check your email. We just sent you an email with a link to set up your new password.</p>','ultimatemember') . '</div>';
				?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email-icon.png" class="img-responsive secimage">
				<?php 
				echo '</div>';
				
			}
			
			?>

		</form>
	
	</div>
	
</div>
