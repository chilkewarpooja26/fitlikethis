<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo $form_id; ?> um-role-<?php echo um_user('role'); ?> ">

	<div class="um-form">
	<?php
        global $wpdb;
        $user_id = um_user('ID'); 
        $query = "select * from " . $wpdb->prefix . "payments where user_id=" . $user_id;
        $results = $wpdb->get_row($query);
        $age = '';
        ?>
		<?php do_action('um_profile_before_header', $args ); ?>
		
		<?php if ( um_is_on_edit_profile() ) { ?><form method="post" action=""><?php } ?>
		
			<?php do_action('um_profile_header_cover_area', $args ); ?>
			
			<?php do_action('um_profile_header', $args ); ?>
			
			<?php do_action('um_profile_navbar', $args ); ?>
			
			<?php
				
			$nav = $ultimatemember->profile->active_tab;
			$subnav = ( get_query_var('subnav') ) ? get_query_var('subnav') : 'default';
				
			print "<div class='um-profile-body $nav $nav-$subnav'>";
				
				// Custom hook to display tabbed content
				do_action("um_profile_content_{$nav}", $args);
				do_action("um_profile_content_{$nav}_{$subnav}", $args);
				
			print "</div>";
				
			?>
		
		<?php if ( um_is_on_edit_profile() ) { ?></form><?php } ?>
	
	</div>
	
</div>

<?php if (get_user_meta($user_id, 'age', true) == '') { ?>
    <script>

        var age_user = document.getElementById("age-239");
        age_user.value = "<?php echo $results->age; ?>";

        var age_weight = document.getElementById("weight-239");
        age_weight.value = "<?php echo $results->weight; ?>";
var age_gender = document.getElementsByName("gender");
        age_gender.value = "<?php echo $results->gender; ?>";
//alert(age_gender.value);

    </script>
<?php } ?>
