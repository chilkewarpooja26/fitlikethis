<?php
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

	if (isset($_POST["editOptions"])) {
		if(check_admin_referer('wp_translate','wp_translate')) {
			$wpTranslateOptions['default_language'] = sanitize_text_field($_POST["defaultLanguage"]);
			if (isset($_POST["trackingEnabled"]))
				$wpTranslateOptions['tracking_enabled'] = true;
			else
				$wpTranslateOptions['tracking_enabled'] = false;
			
			$wpTranslateOptions['tracking_id'] = sanitize_text_field($_POST["trackingId"]);
			
			if (isset($_POST["autoDisplay"]))
				$wpTranslateOptions['auto_display'] = true;
			else
				$wpTranslateOptions['auto_display'] = false;
				
			if (isset($_POST["excludeMobile"]))
				$wpTranslateOptions['exclude_mobile'] = true;
			else
				$wpTranslateOptions['exclude_mobile'] = false;
				
			update_option("wpTranslateOptions", $wpTranslateOptions);
			?>  
			<div class="updated"><p><strong><?php _e('WP Translate settings have been saved.', 'wp-translate' ); ?></strong></p></div>  
			<?php
		}
	}
	$wpTranslateOptions = get_option("wpTranslateOptions");
?>
<div id='wrap'>
	<div id="wpt-left-column">
	<div class="wpt-divider first"><h1><?php _e('WP Translate Pro', 'wptranslate-pro'); ?></h1></div>
	<div class="wp-translate-settings-wrap">
	<p>You can also use the <a href="<?php echo get_site_url().'/wp-admin/widgets.php'; ?>">WP Translate Widget</a> to insert the drop down list of languages globally instead of the default tool bar.</p>
	<p><strong><a href="http://labs.hahncreativegroup.com/wp-translate-pro/?src=wpt" target="_blank"><?php _e('WP Translate Pro', 'wp-translate'); ?></a></strong><br /><em><?php _e('Pro features include: Enhanced widget positioning and ad free', 'wp-translate'); ?>...</em></p>
	</div>
	<div class="wp-translate-settings-wrap">
	<h2><?php _e('Settings', 'wptranslate-pro'); ?></h2>	
    <form name="wp_translate_settings_form" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post">
    <input type="hidden" name="editOptions" vale="true" />
	<?php wp_nonce_field('wp_translate','wp_translate'); ?>
    <div class="wpt-setting-label-wrap">
		<?php _e('Default Language', 'wptranslate-pro'); ?>
	</div>
	<div class="wpt-setting-value-wrap">
		<select id="defaultLanguage" name="defaultLanguage">
			<option value="auto" <?php if($wpTranslateOptions['default_language'] == 'auto') echo esc_attr('selected'); ?>>Detect language</option>
			<option value="af" <?php if($wpTranslateOptions['default_language'] == 'af') echo esc_attr('selected'); ?>>Afrikaans</option>
			<option value="sq" <?php if($wpTranslateOptions['default_language'] == 'sq') echo esc_attr('selected'); ?>>Albanian</option>
			<option value="ar" <?php if($wpTranslateOptions['default_language'] == 'ar') echo esc_attr('selected'); ?>>Arabic</option>
			<option value="hy" <?php if($wpTranslateOptions['default_language'] == 'hy') echo esc_attr('selected'); ?>>Armenian</option>
			<option value="az" <?php if($wpTranslateOptions['default_language'] == 'az') echo esc_attr('selected'); ?>>Azerbaijani</option>
			<option value="eu" <?php if($wpTranslateOptions['default_language'] == 'eu') echo esc_attr('selected'); ?>>Basque</option>
			<option value="be" <?php if($wpTranslateOptions['default_language'] == 'be') echo esc_attr('selected'); ?>>Belarusian</option>
			<option value="bg" <?php if($wpTranslateOptions['default_language'] == 'bg') echo esc_attr('selected'); ?>>Bulgarian</option>
			<option value="ca" <?php if($wpTranslateOptions['default_language'] == 'ca') echo esc_attr('selected'); ?>>Catalan</option>
			<option value="zh-CN" <?php if($wpTranslateOptions['default_language'] == 'zh-CN') echo esc_attr('selected'); ?>>Chinese (Simplified)</option>
			<option value="zh-TW" <?php if($wpTranslateOptions['default_language'] == 'zh-TW') echo esc_attr('selected'); ?>>Chinese (Traditional)</option>
			<option value="hr" <?php if($wpTranslateOptions['default_language'] == 'hr') echo esc_attr('selected'); ?>>Croatian</option>
			<option value="cs" <?php if($wpTranslateOptions['default_language'] == 'cs') echo esc_attr('selected'); ?>>Czech</option>
			<option value="da" <?php if($wpTranslateOptions['default_language'] == 'da') echo esc_attr('selected'); ?>>Danish</option>
			<option value="nl" <?php if($wpTranslateOptions['default_language'] == 'nl') echo esc_attr('selected'); ?>>Dutch</option>
			<option value="en" <?php if($wpTranslateOptions['default_language'] == 'en') echo esc_attr('selected'); ?>>English</option>
			<option value="et" <?php if($wpTranslateOptions['default_language'] == 'et') echo esc_attr('selected'); ?>>Estonian</option>
			<option value="tl" <?php if($wpTranslateOptions['default_language'] == 'tl') echo esc_attr('selected'); ?>>Filipino</option>
			<option value="fi" <?php if($wpTranslateOptions['default_language'] == 'fi') echo esc_attr('selected'); ?>>Finnish</option>
			<option value="fr" <?php if($wpTranslateOptions['default_language'] == 'fr') echo esc_attr('selected'); ?>>French</option>
			<option value="gl" <?php if($wpTranslateOptions['default_language'] == 'gl') echo esc_attr('selected'); ?>>Galician</option>
			<option value="ka" <?php if($wpTranslateOptions['default_language'] == 'ka') echo esc_attr('selected'); ?>>Georgian</option>
			<option value="de" <?php if($wpTranslateOptions['default_language'] == 'de') echo esc_attr('selected'); ?>>German</option>
			<option value="el" <?php if($wpTranslateOptions['default_language'] == 'el') echo esc_attr('selected'); ?>>Greek</option>
			<option value="ht" <?php if($wpTranslateOptions['default_language'] == 'ht') echo esc_attr('selected'); ?>>Haitian Creole</option>
			<option value="iw" <?php if($wpTranslateOptions['default_language'] == 'iw') echo esc_attr('selected'); ?>>Hebrew</option>
			<option value="hi" <?php if($wpTranslateOptions['default_language'] == 'hi') echo esc_attr('selected'); ?>>Hindi</option>
			<option value="hu" <?php if($wpTranslateOptions['default_language'] == 'hu') echo esc_attr('selected'); ?>>Hungarian</option>
			<option value="is" <?php if($wpTranslateOptions['default_language'] == 'is') echo esc_attr('selected'); ?>>Icelandic</option>
			<option value="id" <?php if($wpTranslateOptions['default_language'] == 'id') echo esc_attr('selected'); ?>>Indonesian</option>
			<option value="ga" <?php if($wpTranslateOptions['default_language'] == 'ga') echo esc_attr('selected'); ?>>Irish</option>
			<option value="it" <?php if($wpTranslateOptions['default_language'] == 'it') echo esc_attr('selected'); ?>>Italian</option>
			<option value="ja" <?php if($wpTranslateOptions['default_language'] == 'ja') echo esc_attr('selected'); ?>>Japanese</option>
			<option value="ko" <?php if($wpTranslateOptions['default_language'] == 'ko') echo esc_attr('selected'); ?>>Korean</option>
			<option value="lv" <?php if($wpTranslateOptions['default_language'] == 'lv') echo esc_attr('selected'); ?>>Latvian</option>
			<option value="lt" <?php if($wpTranslateOptions['default_language'] == 'lt') echo esc_attr('selected'); ?>>Lithuanian</option>
			<option value="mk" <?php if($wpTranslateOptions['default_language'] == 'mk') echo esc_attr('selected'); ?>>Macedonian</option>
			<option value="ms" <?php if($wpTranslateOptions['default_language'] == 'ms') echo esc_attr('selected'); ?>>Malay</option>
			<option value="mt" <?php if($wpTranslateOptions['default_language'] == 'mt') echo esc_attr('selected'); ?>>Maltese</option>
			<option value="no" <?php if($wpTranslateOptions['default_language'] == 'no') echo esc_attr('selected'); ?>>Norwegian</option>
			<option value="fa" <?php if($wpTranslateOptions['default_language'] == 'fa') echo esc_attr('selected'); ?>>Persian</option>
			<option value="pl" <?php if($wpTranslateOptions['default_language'] == 'pl') echo esc_attr('selected'); ?>>Polish</option>
			<option value="pt" <?php if($wpTranslateOptions['default_language'] == 'pt') echo esc_attr('selected'); ?>>Portuguese</option>
			<option value="ro" <?php if($wpTranslateOptions['default_language'] == 'ro') echo esc_attr('selected'); ?>>Romanian</option>
			<option value="ru" <?php if($wpTranslateOptions['default_language'] == 'ru') echo esc_attr('selected'); ?>>Russian</option>
			<option value="sr" <?php if($wpTranslateOptions['default_language'] == 'sr') echo esc_attr('selected'); ?>>Serbian</option>
			<option value="sk" <?php if($wpTranslateOptions['default_language'] == 'sk') echo esc_attr('selected'); ?>>Slovak</option>
			<option value="sl" <?php if($wpTranslateOptions['default_language'] == 'sl') echo esc_attr('selected'); ?>>Slovenian</option>
			<option value="es" <?php if($wpTranslateOptions['default_language'] == 'es') echo esc_attr('selected'); ?>>Spanish</option>
			<option value="sw" <?php if($wpTranslateOptions['default_language'] == 'sw') echo esc_attr('selected'); ?>>Swahili</option>
			<option value="sv" <?php if($wpTranslateOptions['default_language'] == 'sv') echo esc_attr('selected'); ?>>Swedish</option>
			<option value="th" <?php if($wpTranslateOptions['default_language'] == 'th') echo esc_attr('selected'); ?>>Thai</option>
			<option value="tr" <?php if($wpTranslateOptions['default_language'] == 'tr') echo esc_attr('selected'); ?>>Turkish</option>
			<option value="uk" <?php if($wpTranslateOptions['default_language'] == 'uk') echo esc_attr('selected'); ?>>Ukrainian</option>
			<option value="ur" <?php if($wpTranslateOptions['default_language'] == 'ur') echo esc_attr('selected'); ?>>Urdu</option>
			<option value="vi" <?php if($wpTranslateOptions['default_language'] == 'vi') echo esc_attr('selected'); ?>>Vietnamese</option>
			<option value="cy" <?php if($wpTranslateOptions['default_language'] == 'cy') echo esc_attr('selected'); ?>>Welsh</option>
			<option value="yi" <?php if($wpTranslateOptions['default_language'] == 'yi') echo esc_attr('selected'); ?>>Yiddish</option>
		</select>
	</div>    	
	<div class="wpt-setting-label-wrap"><?php _e('Exclude from Mobile Browsers', 'wptranslate-pro'); ?></div>
	<div class="wpt-setting-value-wrap"><input type="checkbox" name="excludeMobile" value="true"<?php echo ($wpTranslateOptions['exclude_mobile']) ? " checked='yes'" : ""; ?> /></div>	
	<div class="wpt-setting-label-wrap"><?php _e('Toolbar Auto Display', 'wptranslate-pro'); ?></div>
	<div class="wpt-setting-value-wrap"><input type="checkbox" name="autoDisplay" value="true"<?php echo ($wpTranslateOptions['auto_display']) ? " checked='yes'" : ""; ?> /></div>    
	<div class="wpt-divider"><h3><?php _e('Translation Tracking - Google Analytics', 'wptranslate-pro'); ?></h3></div>
    <div class="wpt-setting-label-wrap"><?php _e('Tracking enabled', 'wptranslate-pro'); ?></div> 
	<div class="wpt-setting-value-wrap"><input type="checkbox" name="trackingEnabled" value="true"<?php echo ($wpTranslateOptions['tracking_enabled']) ? " checked='yes'" : ""; ?> /></div>    
    <div class="wpt-setting-label-wrap"><?php _e('Tracking ID (UA#)', 'wptranslate-pro'); ?></div>
	<div class="wpt-setting-value-wrap"><input type="text" name="trackingId" value="<?php echo esc_attr($wpTranslateOptions['tracking_id']); ?>" /></div>
    <div class="wpt-divider"><p class="major-publishing-actions"><input type="submit" name="Submit" class="button-primary" value="<?php _e('Save Settings', 'wptranslate-pro'); ?>" /></p></div>
    </form>
	</div>
	</div>
	<div id="wpt-right-column">
		<div id="rss" class="wp-translate-settings-wrap">
		</div>
        <div class="wp-translate-settings-wrap">                
			<p><strong><?php _e('Try WP Easy Gallery Pro', 'wp-translate'); ?></strong><br /><em><?php _e('Pro Features include: Multi-image uploader, Enhanced admin section for easier navigation, Image preview pop-up, and more', 'wp-translate'); ?>...</em></p>
			<p><a href="http://labs.hahncreativegroup.com/wordpress-gallery-plugin/?src=wpt" target="_blank"><img src="https://labs.hahncreativegroup.com/wp-content/uploads/2012/02/WP-Easy-Gallery-Pro_200x160.gif" width="200" height="160" border="0" alt="WP Easy Gallery Pro"></a></p>
			<p><strong><?php _e('Try Custom Post Donations Pro', 'wp-translate'); ?></strong><br /><em><?php _e('This WordPress plugin will allow you to create unique customized PayPal donation widgets to insert into your WordPress posts or pages and accept donations. Features include: Multiple Currencies, Multiple PayPal accounts, Custom donation form display titles, and more.', 'wp-translate'); ?></em></p>
			<p><a href="http://labs.hahncreativegroup.com/wordpress-paypal-plugin/?src=wpt" target="_blank"><img src="https://labs.hahncreativegroup.com/wp-content/uploads/2017/04/CustomPostDonationsPro-200x400v4.png" width="200" height="400" alt="Custom Post Donations Pro" border="0"></a></p>
			<p><strong><?php _e('Try Email Obfuscate', 'wp-translate'); ?></strong><br /><em><?php _e('Email Obfuscate is a Lightweight WordPress/jQuery plugin that prevents spam-bots from harvesting your email addresses by dynamically obfuscating email addresses on your site.', 'wp-translate'); ?></em><br /><a href="http://codecanyon.net/item/wordpressjquery-email-obfuscate-plugin/721738?ref=HahnCreativeGroup" target="_blank">Email Obfuscate Plugin</a></p>
			<p><a href="http://codecanyon.net/item/wordpressjquery-email-obfuscate-plugin/721738?ref=HahnCreativeGroup" target="_blank"><img alt="WordPress/jQuery Email Obfuscate Plugin - CodeCanyon Item for Sale" border="0" class="landscape-image-magnifier preload no_preview" height="80" src="<?php echo plugins_url( '/images/WordPress-Email-Obfuscate_thumb_80x80.jpg', __FILE__ ); ?>" title="" width="80"></a></p>
			<p><em><?php _e('Please consider making a donation for the continued development of this plugin. Thank you.', 'wp-translate'); ?></em></p>
			<p><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EJVXJP3V8GE2J" target="_blank"><img src="<?php echo plugins_url( '/images/btn_donateCC_LG.gif', __FILE__ ); ?>" border="0" alt="PayPal - The safer, easier way to pay online!"><img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"></a></p>
		</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){			
	jQuery.ajax({url: "<?php echo plugins_url( '/rss.php', __FILE__ ); ?>",success:function(result){
		jQuery("#rss").html(result);
  }});
});
</script>
</div>