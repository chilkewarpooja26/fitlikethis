 <?php

// /* SIGN IN */
// add_action('wp_ajax_nutrafit_signin_db', 'nutrafit_signin_func');
// add_action('wp_ajax_nopriv_nutrafit_signin_db', 'nutrafit_signin_func');

// function nutrafit_signin_func() {

//     extract($_POST);
//     $error = false;

//     if (!username_exists($email) && !email_exists($email)) {
//         $error = true;
//         $error_type = 'email';
//         $message = 'Email doesn\'t exists. Please enter valid one';
//     } else if (empty($password)) {
//         $error = true;
//         $error_type = 'password';
//         $message = 'Please enter your password';
//     } else {

//         $emailTemp = $email;

//         if (!username_exists($email)) {

//             $user = get_user_by('email', $email);
//             $email = $user->user_login;
//         }

//         $remember = $remember == 'yes' ? true : false;

//         $creds = array();
//         $creds['user_login'] = $email;
//         $creds['user_password'] = $password;
//         $creds['remember'] = $remember;
//         $user = get_user_by('login', $email);
//         $meta = get_user_meta($user->ID);

//         if (is_wp_error($user)) {
//             $error = true;
//             $error_type = 'common';
//             $message = 'Please enter valid username or email and password';
//         } else {

//             $role = userRole(array('user_id' => $user->ID));
//             if (!in_array($role, array('nutrafit', 'user', 'administrator'))) {
//                 $error = true;
//                 $error_type = 'common';
//                 $message = 'You don\'t have enough permission to access this site';
//             } else if ($meta['account_status'][0] == 'inactive') {

//                 $error = true;
//                 $error_type = 'common';
//                 $message = 'Your account is still in inactive status. Please wait for admin approval';
//             } else if ($meta['account_status'][0] == 'block') {

//                 $error = true;
//                 $error_type = 'common';
//                 $message = 'Your account has been blocked by admin. Please contact with administrator for more details';
//             } else if ($meta['account_status'][0] == 'active') {

//                 $user = wp_signon($creds, false);
//                 if ($remember) {
//                     setcookie('nutrafit_email', $emailTemp, time() + 1209600, COOKIEPATH, COOKIE_DOMAIN, false);
//                     setcookie('nutrafit_password', base64_encode($password), time() + 1209600, COOKIEPATH, COOKIE_DOMAIN, false);
//                     setcookie('nutrafit_remember', $remember, time() + 1209600, COOKIEPATH, COOKIE_DOMAIN, false);
//                 } else {
//                     setcookie('nutrafit_email', null, -1, COOKIEPATH, COOKIE_DOMAIN, false);
//                     setcookie('nutrafit_password', null, -1, COOKIEPATH, COOKIE_DOMAIN, false);
//                     setcookie('nutrafit_remember', null, -1, COOKIEPATH, COOKIE_DOMAIN, false);
//                 }
//                 $message = 'You have successfully login. Please wait..';
//                 $url = $redirect_to;
//                 ;
//             }
//         }
//     }

//     $results['status'] = $error ? 'error' : 'success';
//     $results['error_type'] = !empty($error_type) ? $error_type : '';
//     $results['message'] = $message;
//     $results['url'] = $url;

//     echo json_encode($results);
//     die();
// }

// /* SIGN UP */
// add_action('wp_ajax_nutrafit_signup_db', 'nutrafit_signup_func');
// add_action('wp_ajax_nopriv_nutrafit_signup_db', 'nutrafit_signup_func');

// function nutrafit_signup_func() {
//     global $nutrafit, $Mail;
//     extract($_POST);
//     $error = false;
//     if (empty($usertype)) {
//         $error = true;
//         $error_type = 'usertype';
//         $message = 'Please choose user type.';
//         /* } elseif (empty($username)) {
//           $error = true;
//           $error_type = 'username';
//           $message = 'Please enter your username';
//           } elseif (username_exists($username)) {
//           $error = true;
//           $error_type = 'username';
//           $message = 'This username is already exists. Please try new one'; */
//     } else if (empty($first_name)) {
//         $error = true;
//         $error_type = 'first-name';
//         $message = 'Please enter your name';
//     } else if (!is_email($email)) {
//         $error = true;
//         $error_type = 'email';
//         $message = 'Please enter a valid email';
//     } else if (email_exists($email)) {
//         $error = true;
//         $error_type = 'email';
//         $message = 'This email is already exists. Please try new one';
//     } else if (empty($password)) {
//         $error = true;
//         $error_type = 'password';
//         $message = 'Please enter your password';
//     } else if (empty($address)) {
//         $error = true;
//         $error_type = 'address';
//         $message = 'Please enter your address';
//     } else if (empty($gender)) {
//         $error = true;
//         $error_type = 'gender';
//         $message = 'Please select your gender';
//           } else if (empty($weight)) {
//           $error = true;
//           $error_type = 'weight';
//           $message = 'Please enter your weight'; 
//     } else if (empty($age)) {
//         $error = true;
//         $error_type = 'age';
//         $message = 'Please enter your age';
//         /* } else if (empty($goal)) {
//           $error = true;
//           $error_type = 'goal';
//           $message = 'Please enter your goal';
//           } else if (empty($maintain_weight)) {
//           $error = true;
//           $error_type = 'maintain-weight';
//           $message = 'Please enter your maintain weight';
//           } else if (empty($gain_weight)) {
//           $error = true;
//           $error_type = 'gain_weight';
//           $message = 'Please enter your gain weight';
//           } else if (empty($loose_weight)) {
//           $error = true;
//           $error_type = 'loose_weight';
//           $message = 'Please enter your loose weight'; */
//         /* } else if ($tac == 'no') {
//           $error = true;
//           $error_type = 'tac';
//           $message = 'You have to agree the terms and condition'; */
//     } else {
//         $loginuser = explode('@', $email);
//         $login_username = str_shuffle($loginuser[0]);
//         $user_id = wp_create_user($login_username, $password, $email);
//         $user = new WP_User($user_id);
//         $user->remove_role('subscriber');
//         $user->set_role($usertype);

//         update_user_meta($user_id, 'account_status', 'active');
//         update_user_meta($user_id, 'first_name', strip_tags($first_name));
// //        update_user_meta($user_id, 'last_name', $last_name);
//         update_user_meta($user_id, 'address', strip_tags($address));
//         update_user_meta($user_id, 'gender', $gender);
//         update_user_meta($user_id, 'weight', $weight);
//         update_user_meta($user_id, 'age', $age);
//         update_user_meta($user_id, 'goal', $goal);
// //        update_user_meta($user_id, 'maintain_weight', $maintain_weight);
// //        update_user_meta($user_id, 'gain_weight', $gain_weight);
// //        update_user_meta($user_id, 'loose_weight', $loose_weight);
// //        update_user_meta($user_id, 'contact_number', $contact_number);



//         if (is_wp_error($user_id))
//             $message = $user_id->get_error_message();
//         else {

//             // $results = $Mail->activationLink(array('user_id'=>$user_id));

//             update_user_meta($user_id, 'activate_account_token', $results['token']);
//             update_user_meta($user_id, 'activate_account_token_time', time());
//             update_user_meta($user_id, 'activate_account_link_status', 'active');

            
//             $message = 'You have successfully registered. We have send you a mail with activation link. Please check...';
        
            
//         }
        
//     }

//     $results['status'] = $error ? 'error' : 'success';
//     $results['error_type'] = !empty($error_type) ? $error_type : '';
//     $results['message'] = $message;

//     echo json_encode($results);
    
//     die();
// }

// /* FORGOT PASSWORD */
// add_action('wp_ajax_nutrafit_forgot_password_db', 'nutrafit_forgot_password_func');
// add_action('wp_ajax_nopriv_nutrafit_forgot_password_db', 'nutrafit_forgot_password_func');

// function nutrafit_forgot_password_func() {
//     global $nutrafit, $Mail;
//     extract($_POST);
//     $error = false;

//     if (!email_exists($email)) {
//         $error = true;
//         $error_type = 'email';
//         $message = 'Email doesn\'t exists. Please enter valid email';
//     } else {

// //        $results = $Mail->forgot_password_link(array('email' => $email));
//         $user = get_user_by('email', $email);
//         $meta = get_post_meta($user->ID);
//         $token = mt_rand(00000, 99999);
//         $token = md5($token);
//         $token = substr($token, 0, 10);
//         $link = add_query_arg(array('token' => $token), get_permalink(2));
//         $to = $user->user_email;
//         $subject = 'Reset Password';
//         $headers = array('From: NutraFit <noreply@nutrafit.com>' . "\r\n");
//         $message = '<p>Hi ' . $user->user_login . ',</p>';
//         $message .= '<p>Here is your reset password link. Please click on it or copy paste this link to your browsers address bar</p>';
//         $message .= '<p><a href="' . $link . '">' . $link . '<a/></p>';
//         $message .= '<p>Thanks & regards</p>';
//         $message .= '<p>' . get_bloginfo('name') . '</p>';
        
//        add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
//         wp_mail($to, $subject, $message, $headers);
//         remove_filter('wp_mail_content_type', 'set_html_content_type');


//         update_user_meta($user->ID, 'forgot_password_token', $token);
//         update_user_meta($user->ID, 'forgot_password_token_time', time());
//         update_user_meta($user->ID, 'forgot_password_reset_status', 'unset');

//         $message = 'We have send a reset password link. Please check your mail.';
//     }

//     $results['status'] = $error ? 'error' : 'success';
//     $results['error_type'] = !empty($error_type) ? $error_type : '';
//     $results['message'] = $message;

//     echo json_encode($results);
//     die();
// }

 ?>
