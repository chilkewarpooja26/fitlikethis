<?php
global $post;
$wpdb;
if ($_GET['id'] != "") {
    $id = $_GET['id'];
} else {
    $id = 0;
}

$parent = $wpdb->get_results("SELECT parent FROM `av_term_taxonomy` WHERE `term_id`='$id'");
$pid = $parent[0]->parent;
$help_center_parent = get_terms(AV_TX_HELP_CENTER, array('hide_empty' => false, 'orderby' => 'id', 'order' => 'ASC', 'parent' => $pid));
$term = get_the_terms($post->ID, 'av-help-center-category');
$termnm = $term[0]->name;
?>
<section class="serch-contain-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php
                require_once 'search/index.php';
                $term_parent_id = $wpdb->get_results('SELECT parent FROM `av_term_taxonomy` WHERE `term_id`="' . $_GET['id'] . '"');
                $postpid = $term_parent_id[0]->parent;

                $main_parent_id = $wpdb->get_results('SELECT parent FROM `av_term_taxonomy` WHERE `term_id`="' . $postpid . '"');
                $mainpid = $main_parent_id[0]->parent;
                $bk = $mainpid;
                if ($postpid != "0") {
                    $top = $mainpid . ',' . $postpid;
                } else {
                    $top = "";
                }
                $datadd = "data-bk='" . $bk . "'";
                ?>
            </div>
        </div>
    </div>

</section>

<section>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="left_section_main">
                    <div class="left_tab_section col-md-3 bhoechie-tab-menu sub-menu-tab-2 pd-L-0">
                        <?php if ($bk != "") { ?>
                            <a id='back' data-b="b" style="cursor:pointer;" <?= $datadd ?> data-rem="<?= $postpid ?>"><img src="<?= get_bloginfo("template_url"); ?>/images/Back.png"/><b style="color:#009AA6;"><?php _e("Back", 'avamera'); ?></b></a><?php } ?>

                        <img src="<?= get_bloginfo("template_url"); ?>/images/loading36.gif" style="display: none;" id="imgg" />

                        <ul class="nav nav-tabs list-group" id="maincathelp">

                            <?php
                            foreach ($help_center_parent as $cat) {

                                $sesult = $wpdb->get_results("SELECT * FROM av_term_taxonomy where taxonomy='av-help-center-category' and  parent='" . $cat->term_id . "'");
                                $num = $wpdb->num_rows;
                                if ($num > 0) {
                                    $datad = "data-idd='$cat->term_id'";
                                    $ddchangg = '';
                                } else {
                                    $datad = '';
                                    $ddchangg = "data-show='$cat->term_id'";
                                }
                                ?>
                                <li class="open-close howitwork">
                                    <?php if ($cat->term_id == "1169" || $cat->term_id == "1175") { ?>
                                        <a <?= $ddchangg ?> <?= $datad ?> href="<?php echo home_url(); ?>/help-center-contact-us/"  class="flip list-group-item <?php if($post->ID=='1487' || $post->ID=='2329') echo 'current' ?> " id="<?php echo $cat->term_id; ?>" ><?php echo $cat->name; ?>
                                        </a>
                                    <?php } else {
                                        ?>
                                        <a <?= $ddchangg ?> <?= $datad ?> <?php
                                        if ($cat->term_id == $_GET['id']) {
                                            echo "style='background-color:#009aa6 !important; color:white !important;'";
                                        }
                                        ?> style="cursor:pointer;" class="flip list-group-item" id="<?php echo $cat->term_id; ?>" ><?php echo $cat->name; ?>
                                        </a>
                                    <?php } ?>
                                    <ul style="display: none;" class="accountopen in-out" id="insu_top">
                                        <?php
                                        $help_center_subcat = get_terms(AV_TX_HELP_CENTER, array('hide_empty' => false, 'orderby' => 'id', 'order' => 'ASC', 'parent' => $cat->term_id));

                                        foreach ($help_center_subcat as $subcat) {
                                            ?>

                                            <li><a data-idd="<?= $subcat->term_id; ?>" href="javascript:;" class="flip list-group-item" id="<?= $subcat->term_id; ?>" onclick="gethelppost(<?php echo $subcat->term_id; ?>);"><?php echo $subcat->name; ?></a>
                                                <ul class="sub_next pane<?= $subcat->term_id; ?>" <?php if ($termnm = $subcat->name) { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                                    <?php
                                                    $help_center_subsubcat = get_terms(AV_TX_HELP_CENTER, array('hide_empty' => false, 'orderby' => 'id', 'order' => 'ASC', 'parent' => $subcat->term_id));
                                                    foreach ($help_center_subsubcat as $subsubcat) {
                                                        ?>
                                                        <li><a href="javascript:;" class="list-group-item"  onclick="gethelppost(<?php echo $subsubcat->term_id; ?>);"><?php echo $subsubcat->name; ?></a></li>

                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>				
                                </li>
                            <?php } ?>
                        </ul>
                        <input type="hidden" name="parids" id="parids" VALUE="<?= $top ?>"  />
                    </div>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {

                            $("#topmenu li").last().addClass("active");

                            $('#tipue_drop_input').blur(function() {

                                var value = $('#tipue_drop_input').val();

                                $.ajax({
                                    type: "POST",
                                    url: '<?= get_bloginfo("template_url"); ?>/ajaxsearch_helpcenter.php?value=' + value + '&lang=<?php echo ICL_LANGUAGE_CODE ?>',
                                    success: function(response) {
                                        //alert(response);

                                        $('.tab-pane').html(response);


                                    },
                                    error: function() {
                                    }
                                });


                            });
                            $('#tipue_drop_input').keypress(function(e) {
                                //alert(e.keyCode);
                                if (e.which == 13) {//Enter key pressed

                                    $('#tipue_drop_content').hide();
                                    $('#tipue_drop_input').blur();//Trigger search button click event

                                }
                            });

                            $("a[data-show]").click(function() {
                                var idd = $(this).data('show');



                                if (!$(this).hasClass("current"))
                                {
                                    $("ul#maincathelp li a[class*=current]").removeClass('current');

                                    $(this).addClass("current");
                                }

                                gethelppost(idd);

                            });
                            $("a[data-idd]").click(function() {

                                $("#imgg").show();
                                var termidd = $(this).data('idd');

                                $("#parids").val(termidd);



                                $.ajax({
                                    type: "POST",
                                    url: '<?= get_bloginfo("template_url"); ?>/ajaxmenu.php?termidd=' + termidd + '&lang=<?php echo ICL_LANGUAGE_CODE ?>',
                                    success: function(response) {

                                        //$("#imgg").hide();
                                        $('#maincathelp').html(response);
                                    },
                                    error: function() {
                                    }
                                });




                            });

                            $("a[data-bk]").click(function() {
                                $("#imgg").show();
                                var backid = $(this).data('bk');
                                var rem = $(this).data('rem');

                                $.ajax({
                                    type: "POST",
                                    url: '<?= get_bloginfo("template_url"); ?>/ajaxmenu.php?backid=' + backid + "&rem=" + rem + '&lang=<?php echo ICL_LANGUAGE_CODE ?>',
                                    success: function(response) {
                                        $("a[data-b]").hide();
                                        $("#imgg").hide();
                                        $('#maincathelp').html(response);
                                    },
                                    error: function() {
                                    }
                                });
                            });

                        });
                    </script>
