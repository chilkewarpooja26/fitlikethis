<!-- START CAR OWNER EDIT PROFILE, VERIFICATION & PAYOUT PRIFRENCES INFORMATION HERE -->
<div id="car-owner-renter-information">
    <div class="car-owner-information <?php if ($class == '') echo 'hide'; ?>">
        <!-- START MENU VALIDATION TAB-->
        <ul id ="myTab" role="tablist" class="nav nav-tabs">
            <?php if ($checkUser[0] != '') { ?>
                <li class = "<?php
                echo $checkUser[0];
                if ($class == $checkUser[0])
                    echo ' active';
                ?>">
                    <a href="#edit-profile-specification" data-toggle="tab">
                        <?php _e('Personal details', 'avamera') ?>
                    </a>
                </li>
            <?php } if ($checkUser[1] != '') { ?>
                <li class="<?php
                echo $checkUser[1];
                if ($class == $checkUser[1])
                    echo ' active';
                ?>"><a href="#verification-driving-phone" data-toggle="tab"><?php _e('Verifications', 'avamera') ?></a></li>
                    <?php
                } if ($checkUser[2] != '' && $message->post_author == USER_ID) {
                    ?>
                <li class="<?php
                echo $checkUser[2];
                if ($class == $checkUser[2])
                    echo ' active';
                ?>"><a href="#payment-method-renter" data-toggle="tab"><?php _e('Payment Method', 'avamera') ?></a></li>
                    <?php
                } if ($checkUser[3] != '' && $message->post_author != USER_ID) {
                    ?>
                <li class="<?php
                echo $checkUser[3];
                if ($class == $checkUser[3])
                    echo ' active';
                ?>"><a href="#payout-method-owner" data-toggle="tab"><?php _e('Payout Method', 'avamera') ?></a></li>
                    <?php
                }
                ?>
        </ul>
        <!-- EDIT MENU VALIDATION TAB-->

        <!--EDIT PROFILE PERSONAL DETAILS -->
        <div class="tab-content" id="menu-tab-content">
            <?php
            if ($checkUser[0] != '') {
                ?>
                <div id="edit-profile-specification" class="tab-pane fade <?php
                echo $checkUser[0];
                if ($class == $checkUser[0])
                    echo ' active in';
                ?>">
                    <form class="form-horizontal verify_edit_profile cd-form" name="verify_edit_profile" id="verify_edit_profile" action="" method="post" enctype="multipart/form-data">
                        <p class="common-info" id="">
                            <span class="common-error-message"> 
                                <?php
                                _e("Please add missing information below", "avamera");
                                ?>
                            </span>
                        </p>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('First Name', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="first_name" id="first_name" value="<?php echo $userMeta['first_name'][0] ?>" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Last Name', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="last_name" id="last_name" value="<?php echo $userMeta['last_name'][0] ?>" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-sm-3"> <?php _e('I Am', 'avamera'); ?> :   <a class='my-tool-tip' data-toggle="tooltip" data-placement="top" title="<?php _e("Private", "avamera") ?>"> <!-- The class CANNOT be tooltip... --><i class="fa fa-lock" ></i></a></label>
                            <div class="col-sm-3 pd-0">
                                <div class="select-style">
                                    <select name="gender" id="gender" class="select_single">
                                        <option value=""><?php _e('Gender', 'avamera'); ?></option>
                                        <option value="male" <?php echo $userMeta['gender'][0] == 'male' ? 'selected="selected"' : '' ?>><?php _e('Male', 'avamera'); ?></option>
                                        <option value="female" <?php echo $userMeta['gender'][0] == 'female' ? 'selected="selected"' : '' ?>><?php _e('Female', 'avamera'); ?></option>
                                        <option value="other" <?php echo $userMeta['gender'][0] == 'other' ? 'selected="selected"' : '' ?>><?php _e('Other', 'avamera'); ?></option>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>
                            </div>
                            <p class="info"><?php _e('We use this data for analysis and never share it with other users.', 'avamera'); ?></p>

                        </div>

                        <div class="form-group DOB">
                            <label class="col-sm-3"><?php _e('Date of Birth', 'avamera'); ?> : <a class='my-tool-tip' data-toggle="tooltip" data-placement="top" title="<?php _e('Private', 'avamera'); ?>"> <i class="fa fa-lock" ></i></a></label>
                            <div class="col-sm-9 pd-0">
                                <div class="select-style">
                                    <select name="birth_day" id="birth_day">
                                        <option value=""><?php _e('Day', 'avamera'); ?></option>
                                        <?php for ($i = 1; $i <= 31; $i++): ?>
                                            <option <?php echo $userMeta['birth_day'][0] == $i ? 'selected="selected"' : '' ?> value="<?php echo $i ?>"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>
                                <div class="select-style">
                                    <select name="birth_month" id="birth_month">
                                        <?php for ($i = 0; $i < COUNT($MonthList); $i++): ?>
                                            <option <?php echo $userMeta['birth_month'][0] == $i ? 'selected="selected"' : '' ?> value="<?php echo $i ?>"><?php echo $MonthList[$i] ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>

                                <div class="select-style">
                                    <select name="birth_year" id="birth_year">
                                        <option value=""><?php _e('Year', 'avamera'); ?></option>
                                        <?php for ($i = (date("Y") - 23); $i >= (date("Y") - 100); $i--): ?>
                                            <option <?php echo $userMeta['birth_year'][0] == $i ? 'selected="selected"' : '' ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>
                            </div>
                            <p class="info"><?php _e('We use this data for analysis and never share it with other users.', 'avamera'); ?></p>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">
                                <?php _e('Where You Live', 'avamera'); ?> :
                            </label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" placeholder="" name="address" id="address" value="<?php echo $userMeta['address'][0] ?>" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Email', 'avamera'); ?> : <a class='my-tool-tip' data-toggle="tooltip" data-placement="top" title="<?php _e('Private', 'avamera'); ?>"> <i class="fa fa-lock"></i></a>
                            </label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="email" id="email" value="<?php echo $user->user_email ?>" /> 
                                <span class="cd-error-message">Error text here</span>
                            </div>
                            <p class="info"><?php _e('We won’t share your private email address with other Avamera users.', 'avamera'); ?></p>
                        </div>
                        <div class="success"><span>Saved Successfully</span></div>
                        <div class="form-group form-footer">
                            <div class="col-sm-12 pd-0">
                                <button type="submit" class="submit-btn buttons"><?php _e('Next Step', 'avamera'); ?></button>
                                <img src="<?php echo AV_LOADER_CYCLE_2 ?>" class="hide loader" />                        
                            </div>
                        </div>
                    </form>
                </div>
            <?php } if ($checkUser[1] != '') { ?>
                <!-- VERIFICATION PERSONAL DETAILS -->
                <div id="verification-driving-phone" class="tab-pane fade <?php
                echo $checkUser[1];
                if ($class == $checkUser[1])
                    echo ' active in';
                ?>">
                    <form class="form-horizontal verify_mobile_photo_driving cd-form" name="verify_mobile_photo_driving" id="verify_mobile_photo_driving" action="" method="post" enctype="multipart/form-data">
                        <p class="common-info">
                            <span class="common-error-message"> 
                                <?php
                                _e("Please add missing information below", "avamera");
                                ?>
                            </span>
                        </p>
                        <h4 class="inner_head_tag text-left"><?php _e('Profile photo', 'avamera'); ?></h4>
                        <div class="profile-photo-upload">
                            <ul class="profile_image_gallery">
                                <?php
                                if ($userMeta['profile_image'][0] > 0):
                                    $image = wp_get_attachment_image_src($userMeta['profile_image'][0], 'thumbnail');
                                    ?>
                                    <li class="featured">
                                        <span class="file">
                                            <div class="image-single" id="<?php echo $userMeta['profile_image'][0] ?>">
                                                <span>
                                                    <img src="<?php echo $image[0] ?>" class="uploaded_image" />
                                                </span>
                                            </div>
                                            <div id="<?php echo $userMeta['profile_image'][0] ?>" class="delete-tags">
                                                <a href="javascript:void(0)" class="delete-attachment"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </span>
                                    </li>
                                <?php endif; ?>
                                <?php if (COUNT($profileImageGallery) > 0): ?>
                                    <?php
                                    for ($i = 0; $i < COUNT($profileImageGallery); $i++):
                                        $image = wp_get_attachment_image_src($profileImageGallery[$i], 'thumbnail');
                                        ?>
                                        <li class="squ_img_wra">
                                            <span class="file">
                                                <div class="image-single" id="<?php echo $profileImageGallery[$i] ?>">
                                                    <span>
                                                        <img src="<?php echo $image[0] ?>" class="uploaded_image" />
                                                    </span>

                                                </div>
                                                <div id="<?php echo $profileImageGallery[$i] ?>" class="delete-tags">
                                                    <a href="javascript:void(0)" class="delete-attachment"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </span>
                                        </li>
                                    <?php endfor; ?>
                                <?php endif; ?>
                                <p class="upload-txt"><?php echo stripslashes($AvameraUserOptions['profile_text']) ?></p>
                            </ul>
                            <input type="hidden" id="profile_image_gallery" value="<?php echo COUNT($profileImageGallery) + 1 ?>" />
                            <input type="hidden" id="max_profile_image_gallery" value="<?php echo $AvameraUserOptions['profile_image'] ?>" />
                            <div class="profile_image"></div>
                            <div class="filelists">
                                <ul class="filelist queue"></ul>
                            </div>
                            <span id="profile_photo_error"></span>
                            <span class="cd-error-message">Error text here</span>
                        </div>
                        <hr>
                        <h4 class="inner_head_tag text-left"><?php _e('Driving license', 'avamera'); ?></h4>
                        <div class="profile_license" id="drive">
                            <div class="validation-dl-front col-lg-offset-2 col-lg-5">
                                <ul class="driving_licence_gallery_front">
                                    <?php if (COUNT($frontDrivingLicenceGallery) > 0) : ?>
                                        <?php
                                        for ($i = 0; $i < COUNT($frontDrivingLicenceGallery); $i++):
                                            $image = wp_get_attachment_image_src($frontDrivingLicenceGallery[$i], 'full');
                                            ?>
                                            <li>
                                                <span class="file">
                                                    <div id="<?php echo $frontDrivingLicenceGallery[$i] ?>">
                                                        <img src="<?php echo $image[0] ?>" class="uploaded_image" />
                                                        <a href="javascript:void(0)" class="delete-front-licence"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </span>
                                            </li>
                                        <?php endfor; ?>
                                        <?php
                                        if ($userMeta['driving_license_verification'][0] == 'verified')
                                            echo ' <div class="aproval-icon"><i class="fa fa-check-circle-o"></i> ' . __('Verified', 'avamera') . ' </div>';
                                        elseif ($userMeta['driving_license_verification'][0] == 'declined')
                                            echo '<div class="declined-icon"><i class="fa fa-ban"></i> ' . __('Declined', 'avamera') . ' </div>';
                                        ?>
                                    <?php endif; ?>

                                    <p class="upload-txt"><?php // echo stripslashes($AvameraUserOptions['verification_text'])                                                                                                             ?></p>
                                </ul>
                                <input type="hidden" id="driving_licence_gallery_front" value="<?php echo COUNT($frontDrivingLicenceGallery) ?>" />
                                <input type="hidden" id="max_driving_licence_gallery_front" value="<?php echo $AvameraUserOptions['verification_image'] ?>" />
                                <div class="take-capture">
                                    <a href="javascript:;" data-toggle="modal" data-target="#frontCaptureModal" class="btn btn-launch" onClick="setup();"><?php _e('Take Picture', 'avamera'); ?></a>
                                </div>
                                <div class="or-separator"><span><?php _e('or', 'avamera'); ?></span><hr></div>
                                <div class="dl_front"></div>
                                <div class="filelists">
                                    <ul class="filelist_front_dl queues"></ul>
                                </div>
                                <span id="front_license"></span>
                                <span class="cd-error-message">Error text here</span>
                            </div>

                            <div class="validation-dl-back col-lg-5">
                                <ul class="driving_licence_gallery_back">
                                    <?php if (COUNT($backDrivingLicenceGallery) > 0) : ?>
                                        <?php
                                        for ($i = 0; $i < COUNT($backDrivingLicenceGallery); $i++):
                                            $image = wp_get_attachment_image_src($backDrivingLicenceGallery[$i], 'full');
                                            ?>
                                            <li>
                                                <span class="file">
                                                    <div id="<?php echo $backDrivingLicenceGallery[$i] ?>">
                                                        <img src="<?php echo $image[0] ?>" class="uploaded_image" />
                                                        <a href="javascript:void(0)" class="delete-back-licence"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </span>
                                            </li>
                                        <?php endfor; ?>

                                        <?php
                                        if ($userMeta['driving_license_verification'][0] == 'verified')
                                            echo ' <div class="aproval-icon"><i class="fa fa-check-circle-o"></i> ' . __('Verified', 'avamera') . ' </div>';
                                        elseif ($userMeta['driving_license_verification'][0] == 'declined')
                                            echo '<div class="declined-icon"><i class="fa fa-ban"></i> ' . __('Declined', 'avamera') . ' </div>';
                                        ?>

                                    <?php endif; ?>
                                    <p class="upload-txt"><?php // echo stripslashes($AvameraUserOptions['verification_text'])                                                                                     ?></p>
                                </ul>
                                <input type="hidden" id="driving_licence_gallery_back" value="<?php echo COUNT($backDrivingLicenceGallery) ?>" />
                                <input type="hidden" id="max_driving_licence_gallery_back" value="<?php echo $AvameraUserOptions['verification_image'] ?>" />
                                <div class="take-capture">
                                    <a href="javascript:;" data-toggle="modal" data-target="#backCaptureModal" class="btn btn-launch" onClick="back_setup();"><?php _e('Take Picture', 'avamera'); ?></a>
                                </div>
                                <div class="or-separator"><span><?php _e('or', 'avamera'); ?></span><hr></div>
                                <div class="dl_back"></div>
                                <div class="filelists">
                                    <ul class="filelist_back_dl queuesb"></ul>
                                </div>
                                <span id="back_license"></span>
                                <span class="cd-error-message">Error text here</span>
                            </div>
                            <label class="col-lg-6 pd-L-0 control-label"><?php _e('You have to be 23 years old to rent a car at Avamera.', 'avamera'); ?></label> 
                        </div>
                        <div class="form-group ad-fild-wrap" id="mobile-phone"> 
                            <h4 class="inner_head_tag text-left"><?php _e('Phone Number', 'avamera'); ?></h4>


                            <div class="col-sm-9 pd-0">
                                <?php
                                $mob_verification = get_user_meta(USER_ID, 'mobile_verification', true);
                                if (count($mob_verification) && isset($mob_verification['verify_status']) && $mob_verification['verify_status'] == 'yes') {
                                    ?>
                                    <div class="verified_phone"> 
                                        <span class="phone">+<?php echo mobilenumber(substr($mob_verification['verified_mobile'], 2), 2, -3); ?></span> 
                                        <span><i class="fa fa-check-circle-o"></i><?php _e('Verified', 'avamera'); ?> </span>
                                        <span class="remove_phone">X</span>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <span><?php _e('No phone number entered', 'avamera'); ?></span>
                                <?php } ?>
                                <span class="ad-fild" id="phone_number"><a href="javascript:;" class="flip2"><i class="fa fa-plus-square-o"></i> <?php _e('Add a phone number', 'avamera'); ?></a></span>
                                <span class="cd-error-message">Error text here</span>
                                <!--Select field-->

                                <div class="phone-number-verify-widget edit-profile pane2">
                                    <div class="pnaw-step1">
                                        <div id="phone-number-input-widget-0b356c9d" class="phone-number-input-widget">
                                            <label for="phone_country"><?php _e('Choose a country', 'avamera'); ?>:</label>
                                            <div class="select">
                                                <select name="phone_country" id="phone_country" autocomplete="off">
                                                    <option value="47" data-prefix="NO">Norway</option>
                                                    <?php
                                                    for ($i = 0; $i < COUNT($countries); $i++):
                                                        if ($countries[$i]->phonecode != 0) {
                                                            ?>
                                                            <option data-prefix="<?php echo $countries[$i]->countryCode; ?>" <?php echo $countries[$i]->phonecode == $userMeta['phone_country'][0] ? 'selected="selected"' : '' ?> value="<?php echo $countries[$i]->phonecode ?>"><?php echo $countries[$i]->countryName ?></option>
                                                            <?php
                                                        }
                                                    endfor;
                                                    ?>
                                                </select>
                                            </div>

                                            <label for="tel-mobile"><?php _e('Add a phone number', 'avamera'); ?>:</label>

                                            <div class="pniw-number-container clearfix">
                                                <?php
                                                if ($userMeta['phone_country'][0]) {
                                                    $phCode = '+ ' . $userMeta['phone_country'][0];
                                                } else {
                                                    $phCode = '+47';
                                                }
                                                ?>
                                                <div class="pniw-number-prefix"><?php echo $phCode; ?></div>
                                                <input type="tel" maxlength="10" autocomplete="off" id="tel-mobile" name="mobile" class="pniw-number" value="<?php echo $userMeta['mobile'][0]; ?>" onkeypress="return validateQty(event);">
                                            </div>
                                        </div>

                                        <div class="pnaw-verify-container">
                                            <a rel="sms" id="via-sms" href="javascript:;" class="btn btn-primary"><?php _e('Verify via SMS', 'avamera'); ?></a>
                                            <!--<a rel="call" href="javascript:;" class="btn btn-primary">Verify via Call</a>-->
                                            <!--<a href="javascript:;" class="why-verify"><?php _e('Why Verify?', 'avamera'); ?></a>-->
                                        </div>

                                    </div>
                                    <img src="<?php echo AV_LOADER_CYCLE ?>" class="hide loader-verification" />  
                                    <div class="pnaw-step2 hide">
                                        <p class="message"></p>
                                        <label for="phone_number_verification"><?php _e('Please enter the 4-digit code', 'avamera'); ?>:</label>
                                        <input type="text" autocomplete="off" maxlength="4" id="phone_number_verification" onkeypress="return validateQty(event);">
                                        <span class="cd-error-message">Error text here</span>
                                        <div class="pnaw-verify-container">
                                            <a rel="verify" id="verify" href="javascript:;" class="btn btn-primary">
                                                <?php _e('Verify', 'avamera'); ?>
                                            </a>
                                            <a href="javascript:;" rel="cancel" id="cancel-verify" class="cancel">
                                                <?php _e('Cancel', 'avamera'); ?>
                                            </a>
                                        </div>
                                        <p class="cancel-message"><small><?php _e("If the SMS dosen’t arrive, click cancel and re&#45;enter your phone number", 'avamera'); ?></small></p>

                                    </div>
                                    <div class="pnaw-step3">
                                        <div class="success_verified"></div>
                                    </div>

                                </div>
                            </div>
                           <!-- <p class="info"><?php _e('This is only shared once you have a confirmed booking with another Avamera user.', 'avamera'); ?></p>-->
                        </div>
                        <div class="success"><span>Saved Successfully</span></div>
                        <div class="form-group form-footer">
                            <div class="col-sm-12 pd-0">
                                <button type="submit" class="submit-btn buttons"><?php _e('Next Step', 'avamera'); ?></button>
                                <img src="<?php echo AV_LOADER_CYCLE_2 ?>" class="hide loader" />                        
                            </div>
                        </div>
                    </form>
                </div>

                <!-- PAYMENT METHOD PERSONAL DETAILS -->
                <?php
            } if ($checkUser[2] != '' && $message->post_author == USER_ID) {
                ?>
                <div id="payment-method-renter" class="payment-methods-conversation tab-pane fade <?php
                echo $checkUser[2];
                if ($class == $checkUser[2])
                    echo ' active in';
                ?>">
                    <div class="credit-card-box">
                        <div class="header_info">
                            <p class="fieldset" id="">
                                <span class="common-error-message"> <?php _e("Please add missing information below", "avamera"); ?></span>
                            </p>
                        </div>
                        <div class="credit_card_input_form">
                            <form role="form" name="credit_card_form" class="form-horizontal credit_card_form cd-form" id="credit_card_form" method="post" enctype="multipart/form-data" >
                                <div class="form-group">
                                    <img class="img-responsive" src="<?= get_stylesheet_directory_uri() . '/images/accepted_c22e0.png'; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="cardNumber" class="col-sm-3 pd-L-0 control-label"><?php _e("Card Number", "avamera"); ?> :</label>
                                    <div class="col-md-9 pd-0">
                                        <input type="tel" class="form-control has-padding" data-stripe="number" id="card_number" placeholder="" autocomplete="cc-number" required autofocus />
                                        <span class="cd-error-message">Error text here</span>
                                    </div>
                                </div>

                                <div class="form-group DOB">
                                    <label class="col-sm-3 pd-L-0 control-label">
                                        <span class="hidden-xs"><?php _e("Expires", "avamera"); ?></span><span class="visible-xs-inline"><?php _e("EXP", "avamera"); ?></span> <?php _e("on", "avamera"); ?> : 
                                    </label>
                                    <div class="col-sm-9 pd-0">
                                        <div class="select-style">
                                            <select  data-stripe="exp-month" id="exp_month" required autofocus>
                                                <option value=""><?php _e("Month", "avamera"); ?></option>
                                                <?php for ($i = 1; $i < 13; $i++): ?>
                                                    <option value="<?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?> </option>
                                                <?php endfor; ?>
                                            </select>
                                            <span class="cd-error-message">Error text here</span>
                                        </div>
                                        <div class="select-style">
                                            <select data-stripe="exp-year" id="exp_year" required autofocus>
                                                <option value=""><?php _e("Year", "avamera"); ?></option>
                                                <?php for ($i = date("Y"); $i <= date("Y") + 25; $i++): ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php endfor; ?>
                                            </select>
                                            <span class="cd-error-message">Error text here</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 pd-L-0 control-label"><?php _e("CVV Code", "avamera"); ?> :</label>
                                    <div class="col-sm-3 pd-0">
                                        <input type="tel" class="form-control has-padding" data-stripe="cvc" id="card_cvv" placeholder="CVV" autocomplete="cc-csc" required autofocus/>
                                        <span class="cd-error-message">Error text here</span>
                                    </div>
                                </div>
                                <div class="form-group half_section">
                                    <label class="col-sm-3 pd-L-0 control-label"><?php _e("First Name", "avamera"); ?> :</label>
                                    <div class="col-sm-9 pd-0">
                                        <input type="text" class="form-control has-padding" placeholder="First Name" name="first_name" id="first_name" value="" />
                                        <span class="cd-error-message">Error text here</span>
                                    </div>
                                </div>
                                <div class="form-group half_section">
                                    <label class="col-sm-3 pd-L-0 control-label"><?php _e("Last Name", "avamera"); ?> :</label>
                                    <div class="col-sm-9 pd-0">
                                        <input type="text" class="form-control has-padding" name="last_name" placeholder="Last Name" id="last_name" value="" />
                                        <span class="cd-error-message">Error text here</span>
                                    </div>
                                </div>
                                <div class="form-group half_section">
                                    <label class="col-sm-3 pd-L-0 control-label"><?php _e("Postal Code", "avamera"); ?> :</label>
                                    <div class="col-sm-9 pd-0">
                                        <input type="text" class="form-control has-padding" name="postal_code" placeholder="Postal Code" id="postal_code" value="" />
                                        <span class="cd-error-message">Error text here</span>
                                    </div>
                                </div>
                                <div class="form-group half_section">
                                    <label class="col-sm-3 pd-L-0 control-label"><?php _e("Country", "avamera"); ?> :</label>
                                    <div class="col-sm-9 pd-0">
                                        <div class="select-style">
                                            <select class="select_single" name="country_code" id="country_code" >
                                                <option value=""><?php _e("Please select your country", "avamera"); ?></option>
                                                <?php for ($i = 0; $i < COUNT($countries); $i++): ?>
                                                    <option <?php echo $countries[$i]->countryCode == $userMeta['country_code'][0] ? 'selected="selected"' : '' ?> value="<?php echo $countries[$i]->countryCode ?>"><?php echo $countries[$i]->countryName ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--                                <div class="form-group">
                                                                    <div class="col-sm-12 pd-0">
                                                                        <div class="icheckbox_minimal-grey mg-top-10">
                                                                            <input type="checkbox" value="yes" class="icheck" id="default_status" name="default_status" checked="checked"> 
                                                                            <span class="cd-error-message">Error text here</span>
                                                                            <label for="accept_terms"><?php _e("Please save my credit card for future transactions", "avamera"); ?></label>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                                <div class="form-footer">
                                    <div class="col-sm-12">
                                        <button type="submit" name="cardBtn" class="submit-btn buttons"><?php _e("Save", "avamera"); ?></button>
                                        <img src="<?php echo AV_LOADER_CYCLE_2 ?>" class="hide loader" />                        
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>                

                </div>

                <!--PAYOUT PREFRENCES PERSONAL DETAILS-->
                <?php
            } if ($checkUser[3] != '' && $message->post_author != USER_ID) {
                ?>
                <div id="payout-method-owner" class="payout-methods-conversation tab-pane fade <?php
                echo $checkUser[3];
                if ($class == $checkUser[3])
                    echo ' active in';
                ?>">
                    <form class="form-horizontal add_payout_method_profile cd-form" name="add_payout_method_profile" id="add_payout_method_profile" action="" method="post" enctype="multipart/form-data">
                        <p class="common-info">
                            <span class="common-error-message"> 
                                <?php _e('Please add missing information below', 'avamera') ?>
                            </span>
                        </p>
                        <p class="fieldset" id="common">
                            <span class="common-success-message hide"></span>
                            <span class="common-error-message hide"></span>
                        </p>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('First Name', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="first_name" id="first_name" value="" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Last Name', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="last_name" id="last_name" value="" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Account number', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="acc_number" id="acc_number" value="" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Bank Name', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="bank_name" id="bank_name" value="" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>

                        <div class="form-group DOB">
                            <label class="col-sm-3"><?php _e('Date of Birth', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <div class="select-style">
                                    <select name="birth_day" id="birth_day">
                                        <option value=""><?php _e('Day', 'avamera'); ?></option>
                                        <?php for ($i = 1; $i <= 31; $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>
                                <div class="select-style">
                                    <select name="birth_month" id="birth_month">
                                        <?php for ($i = 0; $i < COUNT($MonthList); $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo $MonthList[$i] ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>

                                <div class="select-style">
                                    <select name="birth_year" id="birth_year">
                                        <option value=""><?php _e('Year', 'avamera'); ?></option>
                                        <?php for ($i = (date("Y") - 23); $i >= (date("Y") - 100); $i--): ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <span class="cd-error-message">Error text here</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">
                                <?php _e('Address', 'avamera'); ?> :
                            </label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" placeholder="" name="address" id="address" value="" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('City', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <input type="text" class="form-control has-padding" name="city" id="city" value="<?php echo $userMeta['city'][0] ?>" />
                                <span class="cd-error-message">Error text here</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Country', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <div class="select-style">
                                    <select name="country_code" id="country_code" >
                                        <option value=""><?php _e('Please select your country', 'avamera'); ?></option>
                                        <?php for ($i = 0; $i < COUNT($countries); $i++): ?>
                                            <option value="<?php echo $countries[$i]->countryCode ?>"><?php echo $countries[$i]->countryName ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"><?php _e('Currency', 'avamera'); ?> :</label>
                            <div class="col-sm-9 pd-0">
                                <div class="select-style">
                                    <select class="select_single" name="currency" id="currency" >
                                        <option value=""><?php _e('Please select your currency', 'avamera'); ?></option>
                                        <?php for ($i = 0; $i < COUNT($currencies); $i++): ?>
                                        <option value="<?php echo $currencies[$i]->currency_code ?>"><?php echo strtoupper($currencies[$i]->currency_code) ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="success"><span>Saved Successfully</span></div>
                        <div class="form-group form-footer">
                            <div class="col-sm-12 pd-0">
                                <button type="submit" class="submit-btn buttons"><?php _e('Save', 'avamera'); ?></button>
                                <img src="<?php echo AV_LOADER_CYCLE_2 ?>" class="hide loader" />                        
                            </div>
                        </div>
                    </form>

                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- END CAR OWNER EDIT PROFILE, VERIFICATION & PAYOUT PRIFRINCES INFORMATION HERE -->