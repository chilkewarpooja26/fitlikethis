<?php
ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_WARNING);
include_once '../../../../../wp-load.php';

// GLOBAL VARIABLE
global $Avamera, $AvameraSiteOptions, $Database, $wpdb, $current_user;

/*
  $filename = "balance_csv.csv";
  $fp = fopen('php://output', 'w');

  $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='avamera' AND TABLE_NAME='av_fullstripe_payments'";
  $result = $wpdb->get_results($query);

  foreach ($result as $row) {
  $header[] = $row->COLUMN_NAME;
  }

  header('Content-type: application/csv');
  header('Content-Disposition: attachment; filename=' . $filename);
  fputcsv($fp, $header);

  $query = "select * from " . $wpdb->prefix . "fullstripe_payments where car_owner_id=" . $current_user->ID;
  $result = $wpdb->get_results($query,ARRAY_N);
  foreach ($result as $row) {
  fputcsv($fp, $row);
  }
  exit;
 */

// The function header by sending raw excel
header("Content-type: application/vnd-ms-excel");
// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=balance_export.xls");
// Add data table
?>
<table border="1">
    <tr>
        <th>NO.</th>
        <th>CHARGE ID</th>
        <th>TRANSFER ID</th>
        <th>BALANCE TRANSACTION</th>
        <th>INBOX ID</th>
        <th>RENTER NAME</th>
        <th>OWNER NAME</th>
        <th>CAR ID</th>
        <th>DESCRIPTION</th>
        <th>BOOKING PAYOUT</th>
        <th>PAID DATE</th>
        <th>AVAMERA SERVICE FEE</th>
        <th>ADDITIONAL PAYOUT</th>
        <th>PAID DATE</th>
    </tr>
    <?php
//query get data
    $sql = "select event_id, transfer_id, balance_transaction, msg_id, renter_id, car_owner_id, car_id, description, amount, application_fee, deposite_amt, amt_ownr_clrnc_date, extra_owner_price, extr_onwr_price_clrnc_date from " . $wpdb->prefix . "fullstripe_payments where car_owner_id=" . $current_user->ID;
    $result = $wpdb->get_results($sql);
    $no = 1;
    foreach ($result as $row) {
        if ($row->amt_ownr_clrnc_date != '0000-00-00') {
            echo '
		<tr>
			<td>' . $no . '</td>
			<td>' . $row->event_id . '</td>
			<td>' . $row->transfer_id . '</td>
			<td>' . $row->balance_transaction . '</td>
			<td>' . $row->msg_id . '</td>
			<td>' . get_user_meta($row->renter_id, 'first_name', true) . '</td>
			<td>' . get_user_meta($row->car_owner_id, 'first_name', true) . '</td>
			<td>' . $row->car_id . '</td>
			<td>' . $row->description . '</td>
			<td>' . ($row->amount - ($row->deposite_amt + $row->application_fee)) . '</td>
			<td>' . date('M d,  Y', strtotime($row->amt_ownr_clrnc_date)) . '</td>
                                                                    <td>' . $row->application_fee . '</td>
			<td>' . $row->extra_owner_price . '</td>
                                                                  <td>' . date('M d,  Y', strtotime($row->extr_onwr_price_clrnc_date)) . '</td>
		</tr>             
		';
            $no++;
        }
    }
    ?>
</table>

