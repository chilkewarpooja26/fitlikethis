<?php 
global $post, $sitepress, $wpdb;
$sitepress->switch_lang(ICL_LANGUAGE_CODE, true);
$sql = 'select p.post_title,p.guid,p.ID from '.$wpdb->prefix.'posts as p
LEFT JOIN '.$wpdb->prefix.'icl_translations as lt
ON p.ID = lt.element_id
WHERE p.post_status="publish"
AND p.post_type="'.AV_PT_HELP_CENTER.'"
AND lt.language_code = "'.ICL_LANGUAGE_CODE.'"';
$res = $wpdb->get_results($sql);
//$res=$wpdb->get_results("SELECT post_title,guid,ID FROM  `av_posts` WHERE  `post_type` =  'av-help-center' AND `post_status`='publish'");
$arr='[';
for($i=0;$i<count($res);$i++)
{
    $parent=$wpdb->get_results('SELECT term_taxonomy_id FROM `av_term_relationships` WHERE `object_id`="'.$res[$i]->ID.'"');
   
     $url='"'.$res[$i]->guid.'&id='.$parent[0]->term_taxonomy_id.'"';
 $arr.='
     {"title": "'.$res[$i]->post_title.'", "thumb": "'.get_stylesheet_directory_uri().'/images/img/Page_Icon_1.png", "text": "'.$res[$i]->post_content.'", "url": '.$url.'},';
}

 $arr.=']';
 
?>


<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/search/inc/normalize.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/search/inc/standard.css">


<script type="text/javascript" >
var tipuedrop = {"pages":<?=$arr?>};

</script>



<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/search/tipuedrop/tipuedrop.css">
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/search/tipuedrop/tipuedrop.js"></script>


<div class="block" style="padding-top: 39px;width:500px;" width="100%">

    <input type="text" name="q" id="tipue_drop_input" autocomplete="off"  style="width:500px;" placeholder="<?php _e('How can we help you?','avamera'); ?>">
<div id="tipue_drop_content"></div>


</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var a= $('#tipue_drop_input').tipuedrop();
});
</script>

