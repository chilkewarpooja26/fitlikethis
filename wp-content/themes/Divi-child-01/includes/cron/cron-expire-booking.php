<?php

/* If car owner not response with in 24 hours than autometically expired inquiry/request */
include_once '../../../../../wp-load.php';
include_once'../../../../../wp-includes/wp-db.php';
include_once '../../../../../wp-includes/pluggable.php';
// GLOBAL VARIABLE
global $wpdb, $Avamera, $AvameraSiteOptions, $Database, $wpdb, $current_user;

// GETTING ALL MESSAGES
$args = array(
    'numberposts' => -1,
    'post_type' => 'message',
    'post_status' => 'publish'
);

$messages = get_posts($args);

foreach ($messages as $key => $msg) {
//  GET MESSAGE STATUS
    $message_status = get_post_meta($msg->ID, 'message_status', true);


    if ((get_post_meta($msg->ID, 'booking_type', true) == 'contact_host' && $message_status['action'] == 'Inquiry') || (get_post_meta($msg->ID, 'booking_type', true) == 'request' && $message_status['action'] == 'Reservation')) {

        $pickup_date = date('Y-m-d H:i', strtotime(get_post_meta($msg->ID, 'pickupDate', true)));
        $return_date = date('Y-m-d H:i', strtotime(get_post_meta($msg->ID, 'returnDate', true)));
        /*
          $strtime = date('Y-m-d H:i', strtotime($msg->post_date));
          $starttime = strtotime($strtime);
          $oneday = 60 * 60 * 24;
          $date = ($starttime + $oneday);
         */
        $date = date('Y-m-d H:i', strtotime($msg->post_date . '+1 day'));
        $date = strtotime($date);
        if ($date <= (time())) {

            $post_author = $msg->post_author;
            $receiverID = $post_author;

            $carID = get_post_meta($msg->ID, 'car_id', true);
            $car_details = get_post($carID);
            $sendarID = $car_details->post_author;

            $replyData = array(
                'sendar_id' => $senderID,
                'receiver_id' => $receiverID,
                'subject' => $car_details->post_title,
                'message' => '',
                'send_date' => date('Y-m-d H:i:s'),
                'status' => 'Expired'
            );
            add_post_meta($msg->ID, 'replyData', $replyData);
            $action_data = array(
                'action' => 'expired',
                'status' => 'Expired',
            );
            update_post_meta($msg->ID, 'message_status', $action_data);




            /*
              // SEND EMAIL AND PHONE MSG
              $receiver_user = get_user_by('id', $receiverID);
              $sender_details = get_user_by('id', $sendarID);
              $to = $receiver_user->data->user_email;
              $to_name = $receiver_details->data->display_name;
              $listpageid = icl_object_id(484, 'page', false, ICL_LANGUAGE_CODE);
              $rentpageid = icl_object_id(354, 'page', false,ICL_LANGUAGE_CODE);
              $Mandrill = new Mandrill(get_option('mandrill_api_key'));
              // Reciver Email
              $mandrillMessage = array(
              'to' => array(array('email' => $to, 'name' => $to_name)),
              'merge_vars' => array(array(
              'rcpt' => $to,
              'vars' => array(
              array('name' => 'TONAME', 'content' => $to_name),
              array('name' => 'FROMNAME', 'content' => $sender_details['first_name'][0]),
              array('name' => 'LISTCAR', 'content' => get_permalink($listpageid)),
              array('name' => 'RENTCAR', 'content' => get_permalink($rentpageid)),
              array('name' => 'INBOX', 'content' => get_page_link(get_page_by_title('Inbox')->ID),
              ),
              ))));
              if (ICL_LANGUAGE_CODE == 'nb')
              $template_name = 'N-11 Close to pick-up';
              else
              $template_name = 'E-11 Close to pick-up';
              $Mandrill->messages->sendTemplate($template_name, '', $mandrillMessage);
              // SENDER EMAIL
              $mandrillMessage = array(
              'to' => array(array('email' => $sender_details->data->user_email, 'name' => $sender_details['first_name'][0])),
              'merge_vars' => array(array(
              'rcpt' => $to,
              'vars' => array(
              array('name' => 'TONAME', 'content' =>$sender_details['first_name'][0]),
              array('name' => 'FROMNAME', 'content' =>  $to_name),
              array('name' => 'LISTCAR', 'content' => get_permalink($listpageid)),
              array('name' => 'RENTCAR', 'content' => get_permalink($rentpageid)),
              array('name' => 'INBOX', 'content' => get_page_link(get_page_by_title('Inbox')->ID),
              ),
              ))));
              if (ICL_LANGUAGE_CODE == 'nb')
              $template_name = 'N-11 Close to pick-up';
              else
              $template_name = 'E-11 Close to pick-up';
              $Mandrill->messages->sendTemplate($template_name, '', $mandrillMessage);
              /*

              /*  MOBILE INBOX MESSAGE */
            /*
              $mobile_verify = get_user_meta($receiverID, 'mobile_verification', true);
              if (count($mobile_verify) && isset($mobile_verify['verify_status']) && $mobile_verify['verify_status'] == 'yes') :
              $sms = new CMSMS();
              $phone = $mobile_verify['verified_mobile'];
              $sms->sendMessage("$phone", __('Hello ', 'avamera') . $to_name . ", " . __("Your booking request expired ",'avamera') . $car_details->post_title . "");
              endif;

             */
        }
        echo '<br> This booking is expired ' . $msg->ID;
    }
    echo '<br> This booking is expired ' . $msg->ID;
}


// Cron 3rd for rating and reviews after 14 days he has not written
if ($messages) {
    foreach ($messages as $key => $msg) {
        $message_status = get_post_meta($msg->ID, 'message_status', true);
        if ($message_status['action'] == 'completed_car' || $message_status['action'] == 'rated_car' || $message_status['action'] == 'closed') {
            $msgmeta = get_post_meta($msg->ID);
            foreach ($msgmeta['replyData'] as $replydata) {
                $replydata = unserialize($replydata);
                if ($replydata['status'] == 'Booking Completed') {
                    $your_date = strtotime($replydata['send_date']);
                }
            }
            $cid = $msgmeta['car_id'][0];
            $from_id = $msg->post_author;
            $get_car = get_post($cid);
            $to_id = $get_car->post_author;

            $now = time(); // or your date as well
            $datediff = $now - $your_date;
            $fourteenDays = floor($datediff / (60 * 60 * 24));

            $renter_rating = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix . "rate_review WHERE car_id='" . $cid . "' AND to_id='" . $to_id . "' AND msg_id='" . $msg->ID . "'");
            $owner_rating = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix . "rate_review WHERE car_id='" . $cid . "' AND to_id='" . $from_id . "' AND msg_id='" . $msg->ID . "'");

            if ($fourteenDays >= 14) {

                if ($renter_rating == 0) {
                    $wpdb->insert('av_rate_review', array('to_id' => $to_id, 'from_id' => $from_id, 'car_id' => $cid, 'Overall_experience' => '', 'Communication' => '', 'Accuracy' => '', 'Renslighet' => '', 'Pick_up_Return' => '', 'avarage_rate' => '', 'comment' => '', 'msg_id' => $msg->ID, 'review_by' => '0', 'date' => date('Y-m-d H:i:s')));
                }

                if ($owner_rating == 0) {
                    $wpdb->insert('av_rate_review', array('to_id' => $from_id, 'from_id' => $to_id, 'car_id' => $cid, 'Overall_experience' => '', 'Communication' => '', 'Accuracy' => '', 'Renslighet' => '', 'Pick_up_Return' => '', 'avarage_rate' => '', 'comment' => '', 'msg_id' => $msg->ID, 'review_by' => '1', 'date' => date('Y-m-d H:i:s')));
                }
            }
        }
    }
}
?> 