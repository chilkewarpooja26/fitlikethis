<?php

ini_set('display_errors', '1');
error_reporting(E_ALL & ~E_WARNING);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* PICKUP PAYMENT CRON JOB SET  */
include_once '../../../../../wp-load.php';
include_once'../../../../../wp-includes/wp-db.php';
include_once '../../../../../wp-includes/pluggable.php';

// GLOBAL VARIABLE
global $wpdb, $Avamera, $AvameraSiteOptions, $Database, $wpdb, $current_user;
$stripe_secret_key = trim(get_option('stripe_secret_key'));
$stripe_publishable_key = trim(get_option('stripe_publishable_key'));
// GETTING ALL MESSAGES
$messages = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "postmeta WHERE meta_key='owner_received_payment'");
if ($messages) {
    echo '<pre>';
    $docpath = get_home_path();
    require_once($docpath . 'wp-content/themes/avamera/includes/stripe-php/init.php');
    \Stripe\Stripe::setApiKey(trim($stripe_secret_key));
    foreach ($messages as $key => $msg) {
        $msg->ID = $msg->post_id;
        //  GET MESSAGE STATUS
        $message_status = get_post_meta($msg->ID, 'owner_received_payment', true);

        if ($message_status["status_car"] == "pickup_car_status" && ($message_status['payment_status'] == 'no' || $message_status['payment_status'] == '')) {

//  PICKUP DATE
            //  Check 24 Hours
            $strtime = date('Y-m-d H:i', strtotime($message_status['picked_date']));
            $starttime = strtotime($strtime);
            $oneday = 60 * 60 * 24;
            $date = ($starttime + $oneday);
            if ($date <= (time())) {
                echo "Message ID " . $msg->ID;
                $rentalPrice = get_post_meta($msg->ID, 'rentalPrice', true);
                $price = ($rentalPrice * get_option('owner_avamera_charge')) / 100;
                $finalPrice = $rentalPrice - $price;
                $msg_author = $msg->post_author;
                $carID = get_post_meta($msg->ID, 'car_id', true);
                $car_details = get_post($carID);
                $sendarID = $msg_author;
                $receiverID = $car_details->post_author;
                $get_payout_methods = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "payout_preferences WHERE user_id='" . $receiverID . "' AND stripe_bank_acc_id !='' AND default_status='1'");


                if (get_post_meta($msg->ID, 'balance_transaction', true)) {

                    // Check available balance in stripe account
                    $strpeBalance = \Stripe\BalanceTransaction::retrieve(get_post_meta($msg->ID, 'balance_transaction', true));
                    
                    echo '<br> Created On '. date('Y-m-d h:i:s', $strpeBalance['created']) ;
                    echo "<br> Balance Avalable on  " . date('Y-m-d h:i:s', $strpeBalance['available_on']) . " And Status " . $strpeBalance['status'];
//                    print_r($strpeBalance); die;
                    if (($strpeBalance['available_on'] <= time()) && $strpeBalance['status'] == 'available') {
                        echo "<br>Balance Avalable and Transaction Maid " . $msg->ID;
                        // start stripe refund code 
                        /* RETURN PRICE TO CAR OWNER  */
                        echo "<br>----------------------------<br> Owner DB ID - " . $receiverID;
                        echo "<br>Bank Acc ID - " . $get_payout_methods->stripe_bank_acc_id;
                        echo "<br> Strip Acc ID - " . $get_payout_methods->stripe_acc_id;
                        echo "<br> Amount to transfer  - " . round($finalPrice * 100);
                        echo "<br> currency - " . $get_payout_methods->currency;
                        try {
                            $re = \Stripe\Transfer::create(
                                            array(
                                        "amount" => round($finalPrice * 100),
                                        "currency" => $get_payout_methods->currency,
//                            "application_fee" => round($price * 100),
                                        "destination" => $get_payout_methods->stripe_bank_acc_id
                                            ), array("stripe_account" => $get_payout_methods->stripe_acc_id)
                            );

                            //send the file, this line will be reached if no error was thrown above
                            if ($re['id']) {
                                $pickdata = array(
                                    'status_car' => 'pickup_car_status',
                                    'picked_date' => $message_status['picked_date'],
                                    'payment_status' => 'yes',
                                );
                                update_post_meta($msg->ID, 'owner_received_payment', $pickdata);

                                $transaction_id = get_post_meta($msg->ID, 'transaction_id', true);
                                $update_data = array(
                                    'amt_ownr_clrnc_date' => date('Y-m-d'),
                                );
                                $where = array('payment_id' => $transaction_id);
                                $wpdb->update('av_fullstripe_payments', $update_data, $where, $format = null, $where_format = null);


                                /* RETURN PAYMENT FUNCTIONALITY HERE */
                                echo "<br>Debug 1";
                                // SEND EMAIL AND PHONE MSG
                                /*
                                $receiver_user = get_user_by('id', $receiverID);
                                $sender_details = get_user_by('id', $sendarID);
                                $to = $receiver_user->data->user_email;
                                $to_name = $receiver_details->data->display_name;
                                $subject = 'Pickup payment ' . $car_details->post_title;
                                $messages = "<p>Hi $to_name,</p>";
                                $messages .= "<p>payment received " . $car_details->post_title . "</p>";
                                $messages .= "<p> From: <a href='" . get_author_posts_url($senderID) . "'>" . $sender_details->data->display_name . "</a></p>";
//        $messages .= "<p><a href='" . get_page_link(get_page_by_title('Reserved')->ID) . "'> Pickup the car </a></p>";
                                $mail_sent = $Avamera->expressMail(array('to' => $to, 'subject' => $subject, 'user_message' => $messages, 'from' => $sender_details->data->display_name));
                                if ($mail_sent)
                                    echo "<br>Email has been sent successfully";
                                else {
                                    echo "<br>Email failed";
                                } */
                                /*  MOBILE INBOX MESSAGE Reminder to pick up the payment To Owner */
                               /* $mobile_verify = get_user_meta($receiverID, 'mobile_verification', true);
                                if (count($mobile_verify) && isset($mobile_verify['verify_status']) && $mobile_verify['verify_status'] == 'yes') {
                                    $sms = new CMSMS();
                                    $phone = $mobile_verify['verified_mobile'];
                                    $msg_sent = $sms->sendMessage($phone, __('Hello ', 'avamera') . $to_name . ", " . "Payment for " . $car_details->post_title . " car booking has been done and transfered to your bank account.");
                                    if ($msg_sent)
                                        echo "<br>SMS has been sent successfully";
                                    else
                                        echo "<br>SMS failed";
                                } */
                            }
                        }  //catch the errors in any way you like
                        catch (Exception $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            print('<br>Status is:' . $e->getHttpStatus() . "\n");
                            print('<br>Type is:' . $err['type'] . "\n");
                            print('<br>Code is:' . $err['code'] . "\n");
                            // param is '' in this case
                            print('<br>Param is:' . $err['param'] . "\n");
                            print('<br>Message is:' . $err['message'] . "\n");
                            $error = true;
                            $key = 'common';
                            $success_message = __($err['message'], 'avamera');
                        }
                    } else {
                        echo "<br>It seems there are no balance in stripe account to transfer balance";
                    }
                }
                // Update Payment Status
            } else {
                echo "<br>It seems that there are no transactions in the queue for automatic transfer";
            }
        } else {
            echo "<br>Message Status If condition failed ";
        }
    }
} else {
    echo "<br>It seems that there are no transactions in the queue for automatic transfer";
}


// The below script will execute when Partial payouts from deposit amount from a booking transaction has not been
//  transfered to Car Owner's bank account due to insufficient balance on Stirpe.  

$get_all_deliver_used_prices = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "postmeta WHERE meta_key='stripe_amount_after_deliver_car_paid_amt'");
if (count($get_all_deliver_used_prices)) {
    $docpath = get_home_path();
    require_once($docpath . 'wp-content/themes/avamera/includes/stripe-php/init.php');
    Stripe\Stripe::setApiKey(trim(get_option('stripe_secret_key')));
    foreach ($get_all_deliver_used_prices as $deliversOwnerPrice) {
        echo "<br><br><br><br><br>Addtional Message ID " . $deliversOwnerPrice->post_id;
        $msg_id = $deliversOwnerPrice->post_id;
        $meta_val = unserialize($deliversOwnerPrice->meta_value);
        $strpeBalance = \Stripe\BalanceTransaction::retrieve($meta_val['balance_transaction']);
       echo '<br> Additional Created On '. date('Y-m-d h:i:s', $strpeBalance['created']) ;
        echo "<br> Additional Balance Avalable on  " . date('Y-m-d h:i:s', $strpeBalance['available_on']) . " And Status " . $strpeBalance['status'];
        if (($strpeBalance['available_on'] <= time()) && $strpeBalance['status'] == 'available') {
            echo "<br>Addtional Balance Avalable and Transaction Maid " . $msg_id;
            try {
                $re = \Stripe\Transfer::create(
                                array(
                            "amount" => round($meta_val['create_price'] * 100),
                            "currency" => $meta_val['currency'],
                            "destination" => $meta_val['destination']
                                ), array("stripe_account" => $meta_val['stripe_account'])
                );
                if ($re['id']) {
                    delete_post_meta($msg_id, 'stripe_amount_after_deliver_car_paid_amt');
                    $deliveryExtraPrice = array(
                        'create_price' => $meta_val['create_price'],
                        "paid_date" => date('Y-m-d H:i:s'),
                    );
                    update_post_meta($msg_id, 'car_owner_extra_price_after_deliver', $deliveryExtraPrice);
                    $transaction_id = get_post_meta($msg_id, 'transaction_id', true);
                    $update_data = array(
                        'extra_owner_price' => $meta_val['create_price'],
                        'extr_onwr_price_clrnc_date' => date('Y-m-d'),
                    );
                    $where = array('payment_id' => $transaction_id);
                    $wpdb->update('av_fullstripe_payments', $update_data, $where, $format = null, $where_format = null);

                    echo 'Car Owner extra 0.5 % price payment completed successfully';
                }
            } catch (Exception $e) {
                $body = $e->getJsonBody();
                $err = $body['error'];
                print('<br>Status is:' . $e->getHttpStatus() . "\n");
                print('<br>Type is:' . $err['type'] . "\n");
                print('<br>Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('<br>Param is:' . $err['param'] . "\n");
                print('<br>Message is:' . $err['message'] . "\n");
                $error = true;
                $key = 'common';
                $success_message = __($err['message'], 'avamera');
            }
        } else {
            echo "<br>Payment - 0.5 :-It seems there are no balance in stripe account to transfer balance";
        }
    }
} else {
    echo "<br>Payment - 0.5 :-It seems that there are no transactions in the queue for automatic transfer";
}

    