<?php

/* PICKUP CRON JOB SET  */
include_once '../../../../../wp-load.php';
include_once'../../../../../wp-includes/wp-db.php';
include_once '../../../../../wp-includes/pluggable.php';
// GLOBAL VARIABLE
global $wpdb, $Avamera, $AvameraSiteOptions, $Database, $wpdb, $current_user;

// GETTING ALL MESSAGES
$args = array(
    'post_type' => 'message',
    'post_status' => 'publish'
);

$messages = get_posts($args);

foreach ($messages as $key => $msg) {
//  GET MESSAGE STATUS
    $message_status = get_post_meta($msg->ID, 'message_status', true);


    if ($message_status['action'] == 'special_offer' || $message_status['action'] == 'confirmed' || $message_status['action'] == 'Reservation') {

//  PICKUP DATE
        $pickup_date = date('Y-m-d H:i', strtotime(get_post_meta($msg->ID, 'pickupDate', true)));
        $current_date = date('Y-m-d H:i', strtotime('-1 hour'));

        if (strtotime($pickup_date) >= strtotime($current_date)) {
//         PICKUP DATA STATUS AND MAINTAIN THE RECORD IN DATA BASE

            $post_author = $msg->post_author;
            $receiverID = $post_author;
            $carID = get_post_meta($msg->ID, 'car_id', true);
            $car_details = get_post($carID);
            $sendarID = $car_details->post_author;

            $replyData = array(
                'sendar_id' => $senderID,
                'receiver_id' => $receiverID,
                'subject' => $car_details->post_title,
                'message' => '',
                'send_date' => date('Y-m-d H:i:s'),
                'status' => 'Pickup car'
            );
            add_post_meta($mid, 'replyData', $replyData);
            $action_data = array(
                'action' => 'pickup_car',
                'status' => 'Pickup car',
            );
            update_post_meta($mid, 'message_status', $action_data);
            // SEND EMAIL AND PHONE MSG
            $receiver_user = get_user_by('id', $receiverID);
            $sender_details = get_user_by('id', $sendarID);
            $to = $receiver_user->data->user_email;
            $to_name = $receiver_details->data->display_name;
            $listpageid = icl_object_id(484, 'page', false, $_REQUEST['lang']);
            $rentpageid = icl_object_id(354, 'page', false, $_REQUEST['lang']);
            $Mandrill = new Mandrill(get_option('mandrill_api_key'));
            $mandrillMessage = array(
//                        'subject' => __('Contact To Owner') . $msgTitle,
//                        'from_email' => 'express@avamera.com',
//                        "from_name" => $sender_details['first_name'][0] . ' (Avamera)',
                'to' => array(array('email' => $to, 'name' => $to_name)),
                'merge_vars' => array(array(
                        'rcpt' => $to,
                        'vars' => array(
                            array('name' => 'TONAME', 'content' => $to_name),
                            array('name' => 'FROMNAME', 'content' => $sender_details['first_name'][0]),
                            array('name' => 'LISTCAR', 'content' => get_permalink($listpageid)),
                            array('name' => 'RENTCAR', 'content' => get_permalink($rentpageid)),
                            array('name' => 'INBOX', 'content' => get_page_link(get_page_by_title('Inbox')->ID),
                            ),
            ))));
            if (ICL_LANGUAGE_CODE == 'nb')
                $template_name = 'N-11 Close to pick-up';
            else
                $template_name = 'E-11 Close to pick-up';
            $Mandrill->messages->sendTemplate($template_name, '', $mandrillMessage);

            /*
              $subject = 'Pick up the car'. $car_details->post_title;
              $messages = "<p>Hi $to_name,</p>";
              $messages .= "<p>Pick up the car ".$car_details->post_title."</p>";
              $messages .= "<p> From: <a href='" . get_author_posts_url($senderID) . "'>" . $sender_details->data->display_name . "</a></p>";
              $messages .= "<p><a href='" . get_page_link(get_page_by_title('Reserved')->ID) . "'> Pickup the car </a></p>";
              $Avamera->expressMail(array('to' => $to, 'subject' => $subject, 'user_message' => $messages, 'from' => $sender_details->data->display_name));
             */
            /*  MOBILE INBOX MESSAGE */
            $mobile_verify = get_user_meta($receiverID, 'mobile_verification', true);
            if (count($mobile_verify) && isset($mobile_verify['verify_status']) && $mobile_verify['verify_status'] == 'yes') :
                $sms = new CMSMS();
                $mobMes = substr($msgContent, 1, 100);
                $phone = $mobile_verify['verified_mobile'];
                $sms->sendMessage("$phone", __('Hello ','avamera'). $to_name.", "."Your pick up time for booked car." . $car_details->post_title." is near." );
            endif;
        }
    }
}
?>