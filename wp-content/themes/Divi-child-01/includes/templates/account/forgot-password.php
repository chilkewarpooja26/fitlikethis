<div id="cd-reset-password"> <!-- reset password form -->
    <p class="cd-form-message"><?php _e('Lost your password? Please enter your email address. You will receive a link to create a new password.', 'avamera'); ?></p>

    <form class="cd-form">
        <p class="fieldset">
            <span class="common-success-message hide"></span>
        </p>
        <p class="fieldset">
            <label class="image-replace cd-email" for="reset-email"><?php _e('E-mail', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="<?php _e('E-mail', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
            <input class="full-width has-padding" id="reset-submit" type="submit" value="<?php _e('Reset password', 'avamera'); ?>">
            <img src="<?php echo AV_LOADER_CYCLE_2 ?>" id="loader" class="H-loader" style="display: none;" />
        </p>
    </form>

    <p class="cd-form-bottom-message"><a href="#0"><?php _e('Back to log-in', 'avamera'); ?></a></p>
</div> <!-- cd-reset-password -->