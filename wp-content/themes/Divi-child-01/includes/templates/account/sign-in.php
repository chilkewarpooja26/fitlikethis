<div id="cd-login"> <!-- log in form -->
    <form class="cd-form">
        <p class="fieldset" id="common">
            <span class="common-success-message hide"></span>
            <span class="common-error-message hide"></span>
        </p>
        <?php
        if ($AvameraSiteOptions['social_media_account'] == 'yes') {
            $url = urlencode(home_url());
            ?>
            <div class="wp-social-login-widget">

                <div class="wp-social-login-connect-with"><?php _e('Connect with:', 'avamera'); ?></div>

                <div class="wp-social-login-provider-list">

                    <a data-provider="Facebook" class="wp-social-login-provider wp-social-login-provider-facebook" title="<?php _e('Connect with Facebook', 'avamera'); ?>" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                        <span> <?php _e('Sign in with Facebook','avamera'); ?></span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" title="<?php _e('Connect with Facebook', 'avamera'); ?>" alt="Facebook">
                    </a>

                    <a data-provider="Google" class="wp-social-login-provider wp-social-login-provider-google" title="<?php _e('Connect with Google', 'avamera'); ?>" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Google&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                        <span> <?php _e('Sign in with Google','avamera'); ?></span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/google.png" title="<?php _e('Connect with Google', 'avamera'); ?>" alt="Google">
                    </a>

    <!--                <a data-provider="LinkedIn" class="wp-social-login-provider wp-social-login-provider-linkedin" title="Connect with LinkedIn" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=LinkedIn&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" title="Connect with LinkedIn" alt="LinkedIn">
                    </a>-->

                </div> 

                <div class="wp-social-login-widget-clearing"></div>

            </div>
        <?php } ?>
        <?php // $AvameraSiteOptions['social_media_account'] == 'yes' ? do_shortcode('[wordpress_social_login]') : ''; ?>
        <div class="or-separator"><span><?php _e('or', 'avamera') ?></span><hr /></div>
        <p class="fieldset">
            <label class="image-replace cd-email" for="signin-email"><?php _e('Username / E-mail', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="<?php _e('E-mail', 'avamera'); ?>" value="<?php echo $_COOKIE['avamera_email']; ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
            <label class="image-replace cd-password" for="signin-password"><?php _e('Password', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signin-password" type="password"  placeholder="<?php _e('Password', 'avamera'); ?>" value="<?php echo base64_decode($_COOKIE['avamera_password']); ?>">
            <a href="javascript:void(0)" class="hide-password"><?php _e('Show', 'avamera'); ?></a>
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
        <div class="icheckbox_minimal-grey">
            <input type="checkbox" id="remember-me" <?php echo $_COOKIE['avamera_remember'] ? 'checked="checked"' : '' ?>>
            <label for="remember-me"><?php _e('Remember me', 'avamera'); ?></label>
        </div>
        </p>

        <p class="fieldset">
            <input type="hidden" name="redirect_to" id="redirect_to" value="<?php echo esc_attr($_SERVER["REQUEST_URI"]); ?>" />
            <input class="full-width" id="signin-submit" type="submit" value="<?php _e('Login', 'avamera'); ?>">
            <img src="<?php echo AV_LOADER_CYCLE_2 ?>" id="loader" class="H-loader" style="display: none;" />
        </p>
    </form>

    <p class="cd-form-bottom-message"><a href="#0"><?php _e('Forgot your password?', 'avamera'); ?></a></p>
    <!-- <a href="#0" class="cd-close-form">Close</a> -->
</div> <!-- cd-login -->
