<div id="cd-signup"> <!-- sign up form -->
    <form class="cd-form">
        <p class="fieldset">
            <span class="common-success-message hide"></span>
        </p>
        <?php 
        if($AvameraSiteOptions['social_media_account'] == 'yes') {
            $url = urlencode(home_url());
            ?>
        <div class="wp-social-login-widget">

            <div class="wp-social-login-connect-with"><?php _e('Connect with:', 'avamera'); ?></div>

            <div class="wp-social-login-provider-list">

                <a data-provider="Facebook" class="wp-social-login-provider wp-social-login-provider-facebook" title="<?php _e('Connect with Facebook', 'avamera'); ?>" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Facebook&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                   <span> <?php _e('Sign up with Facebook','avamera'); ?></span> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" title="<?php _e('Connect with Facebook', 'avamera'); ?>" alt="Facebook">
                </a>

                <a data-provider="Google" class="wp-social-login-provider wp-social-login-provider-google" title="<?php _e('Connect with Google', 'avamera'); ?>" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=Google&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                    <span> <?php _e('Sign up with Google','avamera'); ?></span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/google.png" title="<?php _e('Connect with Google', 'avamera'); ?>" alt="Google">
                </a>

<!--                <a data-provider="LinkedIn" class="wp-social-login-provider wp-social-login-provider-linkedin" title="Connect with LinkedIn" href="<?php echo home_url(); ?>/wp-login.php?action=wordpress_social_authenticate&amp;mode=login&amp;provider=LinkedIn&amp;redirect_to=<?php echo $url; ?>" rel="nofollow">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin.png" title="Connect with LinkedIn" alt="LinkedIn">
                </a>-->

            </div> 

            <div class="wp-social-login-widget-clearing"></div>

        </div>
        <?php } ?>

        <?php // $AvameraSiteOptions['social_media_account'] == 'yes' ? do_shortcode('[wordpress_social_login]') : ''; ?>
        <div class="or-separator"><span><?php _e('or', 'avamera'); ?></span><hr /></div>
<!--        <p class="fieldset">
            <label class="image-replace cd-username" for="signup-username"><?php _e('Username', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="<?php _e('Username', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>-->

        <p class="fieldset">
            <label class="image-replace cd-email" for="signup-first-name"><?php _e('First Name', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-first-name" type="first-name" placeholder="<?php _e('First Name', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
            <label class="image-replace cd-email" for="signup-last-name"><?php _e('Last Name', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-last-name" type="last-name" placeholder="<?php _e('Last Name', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
            <label class="image-replace cd-email" for="signup-email"><?php _e('E-mail', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="<?php _e('E-mail', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>

        <p class="fieldset">
            <label class="image-replace cd-password" for="signup-password"><?php _e('Password', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="<?php _e('Password', 'avamera'); ?>">
            <a href="javascript:void(0)" class="hide-password"><?php _e('Hide', 'avamera'); ?></a>
            <span class="cd-error-message">Error message here!</span>
        </p>

<!--        <p class="fieldset">
            <label class="image-replace cd-email" for="signup-contact-number"><?php _e('Contact Number', 'avamera'); ?></label>
            <input class="full-width has-padding has-border" id="signup-contact-number" type="contact-number" placeholder="<?php _e('Contact Number', 'avamera'); ?>">
            <span class="cd-error-message">Error message here!</span>
        </p>-->

        <p class="fieldset">
            <?php //$MC4WP_Registration_Form_Integration->output_checkbox(); ?>
        </p>
        <p class="fieldset">
        <div class="icheckbox_minimal-grey">
            <input type="checkbox" id="accept-terms" checked="checked">
            <label for="accept-terms"><?php _e('I agree to the', 'avamera'); ?> <a href="<?php echo get_permalink($AvameraPageOptions['terms_and_conditions']) ?>" target="_blank"><?php _e('Terms and Conditions', 'avamera'); ?></a></label>
            <span class="cd-error-message tag-error">Error message here!</span>
        </div>
        </p>

        <p class="fieldset">
            <input type="hidden" name="redirect_to" id="redirect_to" value="<?php echo esc_attr($_SERVER["REQUEST_URI"]); ?>" />
            <input class="full-width has-padding" id="signup-submit" type="submit" value="<?php _e('Create account', 'avamera'); ?>">
            <img src="<?php echo AV_LOADER_CYCLE_2 ?>" id="loader" class="H-loader" style="display: none;" />
        </p>
    </form>

    <!-- <a href="#0" class="cd-close-form">Close</a> -->
</div> <!-- cd-signup -->
