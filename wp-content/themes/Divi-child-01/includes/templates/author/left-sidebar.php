<?php
global $current_user;
get_currentuserinfo();
$postAuthor = get_user_by('id', $post->post_author);
?>

<script type="text/javascript">
    function send() {
        if ($('#msgcontent').val() == '' || $('#msgtitle').val() == '') {
            if ($('#msgtitle').val() == '')
                $('#msgtitle').css('border-color', 'red');
            if ($('#msgcontent').val() == '')
                $('#msgcontent').css('border-color', 'red');
        } else {
            $('#msgtitle').css('border-color', '');
            $('#msgcontent').css('border-color', '');
            var senderID = <?php echo $current_user->ID; ?>;
            var receiverID = <?php echo $postAuthor->ID; ?>;
            var msgTitle = $('#msgtitle').val();
            var msgContent = $('#msgcontent').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                data: {'action': 'send_message', 'senderID': senderID, 'receiverID': receiverID, 'msgTitle': msgTitle, 'msgContent': msgContent},
                success: function(resp) {
                    // alert($.trim(resp));
                    if ($.trim(resp) == 'success') {
                        $('#msgdiv').html('<p>Your message was successfully delivered.</p>');
                    }
                }
            });
        }
    }
</script>
<div class="col-md-3" id="auther_info_section">
    <div class="panel text-center">
        <div class="author-img-big left_side_bar"><span><img src="<?php echo $Avamera->profileImage(array('attachment_id' => $authorMeta['profile_image'][0])) ?>" /></span></div>
        <div class="panel-body">
            <!--<div class="author-name">John Elliot</div>-->
            <div class="star">

                <?php
//                $userid = $current_user->ID;
                show_rating_user($author->ID);
                ?>
                <div class="clr"></div>
            </div>
            <!--<a href="javascript:void(0)" class="link">View Profile</a>-->

            <?php
            /*
              if($current_user->ID != $author->ID){?>
              <?php if($current_user->ID != 0) { ?>
              <div class="full-width-btn">
              <!--<a href="javascript:void(0)" onclick="jQuery('#msgdiv').toggle(250); jQuery('#msgtitle').focus();">Send Message</a>-->
              <a href="javascript:;" onclick="jQuery('#msgtitle').focus();" data-toggle="modal" data-target="#massageModal"> Send Message</a>
              </div>
              <?php } else { ?>
              <div class="full-width-btn"><a href="javascript:void(0)" onclick="alert('You need to login to send a message to this user!');">Send Message</a></div>
              <?php } ?>
              <?php
              }
             * */
            ?>
        </div>
    </div><!-- panel -->
    <!--    <div id="msgdiv" class="panel text-center" style="display: none;">
          <div class="panel-body">
            <input type="text" id="msgtitle" class="form-control has-padding" style="margin-bottom: 5px;" placeholder="Your message title" />
            <textarea rows="10" id="msgcontent" class="form-control has-padding" placeholder="Type your message here"></textarea>
            <div class="full-width-btn">
              <a id="send" href="javascript:void(0)" onclick="send();">Send</a>
            </div>
          </div>
        </div>-->
    <?php
    $user_ID =$author->ID;
    $linked_verification = $wpdb->get_row("select * from av_social_verification where user_id = " . $user_ID . " and provider='linkedin'");

    $facebook_verification = $wpdb->get_row("select * from av_social_verification where user_id = " . $user_ID . " and provider='facebook'");

    $goggle_verification = $wpdb->get_row("select * from av_social_verification where user_id = " . $user_ID . " and provider='google'");

    $email_verification = $wpdb->get_row("select * from av_social_verification where user_id = " . $user_ID . " and verifyemailstatus ='1'");
    $mob_verification = get_user_meta($user_ID, 'mobile_verification', true);
    ?>
    <div class="panel">
        <div class="panel-header"><h3><?php _e('Verifications', 'avamera'); ?></h3></div>
        <div class="panel-body">
            <ul class="overview users_verifications">
              <!--  <li><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/globe.png" /></i>Online: <?php echo $Avamera->timePassed($authorMeta['last_login'][0]) ?></li>-->

                <li <?php if ($authorMeta['address'][0] != '') { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>> 
                    <i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Address Verified', 'avamera'); ?>
                </li>
                <li <?php if (count($mob_verification) && isset($mob_verification['verify_status']) && $mob_verification['verify_status'] == 'yes') { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>> 
                    <i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Phone Number Verified', 'avamera'); ?>
                </li>
                <li <?php if ($authorMeta['driving_license_verification'][0] == 'verified') { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>> 
                    <i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Driver License Verified', 'avamera'); ?>
                </li>
                <li <?php if (count($email_verification) > 0) { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>> 
                    <i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Email Address Verified', 'avamera'); ?>
                </li>

                <li <?php if (count($facebook_verification) > 0) { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Facebook Approved', 'avamera'); ?></li>

                <li <?php if (count($linked_verification) > 0) { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Linkedin Approved', 'avamera'); ?></li>

                <li <?php if (count($goggle_verification) > 0) { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick2.png" /></i><?php _e('Google Approved', 'avamera'); ?></li>


                <!--<li><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/mail.png" /></i><?php echo $Avamera->userEmail(array('email' => $user->user_email)) ?></li>-->
            </ul>
        </div>
    </div><!-- panel -->
    <div class="panel">
        <div class="panel-header"><h3><?php _e('About me', 'avamera'); ?></h3></div>
        <div class="panel-body"><?php echo apply_filters('the_content', stripslashes($authorMeta['description'][0])) ?></div>
    </div><!-- panel -->


</div>

<!--// Start Message Popup-->
<div class="modal fade" id="massageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php _e('Send Message to', 'avamera'); ?> <span class="name"><?php echo $authorMeta['first_name'][0] . ' ' . $authorMeta['last_name'][0] ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel text-center">
                            <div class="author-img-big"><img src="<?php echo $Avamera->profileImage(array('attachment_id' => $authorMeta['profile_image'][0])) ?>" /></div>
                            <div class="panel-body">
                                <!--<div class="author-name">John Elliot</div>-->
                                <div class="star">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    </ul>
                                    <div class="clr"></div>
                                </div>

                            </div>
                        </div><!-- panel -->
                        <div class="panel">
                            <div class="panel-header"><h3>Profile Overview</h3></div>
                            <div class="panel-body">
                                <ul class="overview">
                                    <li><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/globe.png" /></i>Online: <?php echo $Avamera->timePassed($authorMeta['last_login'][0]) ?></li>
                                    <li <?php if (count($facebook_verification) > 0) { ?> class="verify_yes" <?php } else { ?> class="verify_no" <?php } ?>><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/tick.png" /></i>Facebook approved</li>
                                    <li><i><img src="<?php echo get_stylesheet_directory_uri() ?>/images/mail.png" /></i><?php echo $Avamera->userEmail(array('email' => $user->user_email)) ?></li>
                                </ul>
                            </div>
                        </div><!-- panel -->
                    </div>
                    <div class="col-md-6">
                        <div id="msgdiv" class="author-info-bottom">
                            <div class="">
                                <div class="header-view"> When are you renting </div>
                                <!--                                                            <button class="btn btn-default"> Pick up</button>
                                                                                            <button class="btn btn-default"> Return</button>-->
                                <input type="text" placeholder="Your message title" style="margin-bottom: 5px;" class="form-control has-padding" id="msgtitle">
                                <textarea placeholder="Type your message here" class="form-control has-padding" id="msgcontent" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <!--<div class="full-width-btn">-->
                <a class="btn btn-primary" onclick="send();" href="javascript:void(0)" id="send">Send Message</a>
                <!--</div>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!--// End Message Popup-->
