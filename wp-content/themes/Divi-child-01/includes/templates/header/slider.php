<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        var element = document.getElementById('address');
        element.oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("<?php _e('Please set location', 'avamera'); ?>");
            }
        };
        element.oninput = function(e) {
            e.target.setCustomValidity("");
        };
    })
</script>
<div class="ava" id="home_banner">
<div class="ava_background" aria-hidden="true" data-native-currency="INR">
<div class="ava-slideshow">
<div class="ava-slideshow_list">
<div class="ava-slide">
    <img alt="avamera-home-slider" class="ava_slider-bg" src="<?php echo $AvameraHomeOptions['banner_background_src'] ?>" />
</div>
</div>
</div>
</div>
<div class="search-car">
        <?php // _e(stripslashes($AvameraHomeOptions['banner_brief']), 'avamera');  ?>
        <h1><?php _e('Rent the car - own the memories','avamera'); ?></h1>
        <h4><?php _e('Rent the perfect car at low price, friendlier and fully insured','avamera'); ?></h4>
        <form class="search-filter ws-validate" method="get" action="<?php echo get_permalink($AvameraPageOptions['listing_page']) ?>">
            <input type="text" class="form-control" name="address" id="address" placeholder="<?php _e('Write an address or a city', 'avamera'); ?>" required />
            <?php /*
            <div class='input-group date' id='datetimepicker-pickup'>
                <input class="datepicker form-control" name="pickup" id="pickup" placeholder="<?php _e('Pick up', 'avamera'); ?>" />
                <span class="calendar"><a href="javascript:;"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/calender.png" /></a></span>
            </div>
            <div class='input-group date' id='datetimepicker-pickup'>
                <input class="datepicker form-control" name="return" id="return" placeholder="<?php _e('Return', 'avamera'); ?>" />
                <span class="calendar"><a href="javascript:;"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/calender.png" /></a></span>
            </div> 
            
             */ ?>
            <button type="submit" class="search-btn"><?php _e('Search cars', 'avamera'); ?></button>
            <!--<input type="hidden" name="city" id="locality" value="<?php echo $_GET['city'] ?>" />--> 
        </form>
        <div class="HW-btn"><a href="javascript:void(0)" class="btn2"><?php _e('How it Works', 'avamera'); ?></a></div>
    </div>
</div>