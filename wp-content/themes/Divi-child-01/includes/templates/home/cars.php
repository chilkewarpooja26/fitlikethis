<?php
global $current_user, $post, $wpdb;
get_currentuserinfo();
$all_cars = get_posts(array('post_type' => 'av-car', 'post_status' => 'publish', 'posts_per_page' => -1));

# Spherical Law of Cosines

function distance_slc($lat1, $lon1, $lat2, $lon2) {
    error_reporting(0);
    $earth_radius = 3960.00; # in miles
    $delta_lat = $lat_2 - $lat_1;
    $delta_lon = $lon_2 - $lon_1;
    $distance = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($delta_lon));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;
    $distance = round($distance, 4);

    return $distance;
} 

if (!empty($all_cars)) {
    foreach ($all_cars as $key => $car) {
        $all_car_location_arr[$key]['id'] = $car->ID;
        $all_car_location_arr[$key]['car_name'] = $car->post_title;
        $all_car_location_arr[$key]['lat'] = get_post_meta($car->ID, 'lat', true);
        $all_car_location_arr[$key]['lng'] = get_post_meta($car->ID, 'lng', true);
        $all_car_location_arr[$key]['daily_rate'] = get_post_meta($car->ID, 'daily_rate', true);
        // find out average Rating for the car 
        $resultt = $wpdb->get_results("SELECT AVG(avarage_rate) as avgg FROM av_rate_review where car_id='" . $car->ID . "' and review_by='0'");
        $all_car_location_arr[$key]['avg_rate'] = $resultt[0]->avgg;
    }

// $lat_1 = "47.117828";
    $lat_1 = $_COOKIE['currentLocationLat'];
// $lon_1 = "-88.545625";
    $lon_1 = $_COOKIE['currentLocationLng'];
// $lat_2 = "47.122223";
// $lon_2 = "-88.568781";
   
    /* $distances = array();
    foreach ($all_car_location_arr as $key => $car_location) {
        $lat_2 = $car_location['lat'];
        $lon_2 = $car_location['lng'];
        $distances[$key]['distance'] = distance_slc($lat_1, $lon_1, $lat_2, $lon_2);
        $distances[$key]['car_id'] = $car_location['id'];
        // $slc_distance = distance_slc($lat_1, $lon_1, $lat_2, $lon_2);
    }

    sort($distances);

    $cars = array();

     if (count($distances) < 3) {
      for ($i = 0; $i < count($distances); $i++) {
      $cars[$i] = get_post($distances[$i]['car_id']);
      }
      } else {
      for ($i = 0; $i < 3; $i++) {
      $cars[$i] = get_post($distances[$i]['car_id']);
      }
      }  */

    $args = array(
        'meta_key' => 'feature_car',
        'meta_value' => 'yes',
        'post_type' => AV_PT_CAR,
        'post_status' => 'publish',
        'orderby' => 'rand',
        'posts_per_page' => 3
    );
    $cars = get_posts($args);
}
?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery.raty.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.wishpop').click(function() {
            var currentUserID = <?php echo $current_user->ID; ?>;
            if (currentUserID != 0) {
                var car_id = $(this).data('carid');
                $.fancybox({
                    href: '<?php echo get_page_link(get_page_by_title('Wish List Popup')->ID); ?>?car_id=' + car_id,
                    type: 'iframe',
                    width: 600,
                    height: 560,
                    fitToView: false,
                    autoSize: false,
                    scrolling: 'no'
                });
            } else {
                alert('<?php _e('Please login to add this car to your Wish List!', 'avamera'); ?>');
            }
        });
    });
</script>
<?php if (!empty($cars)) : ?>
    <div class="title-holder">

        <h2><?php _e('Newly registered cars', 'avamera'); ?></h2>
        <p><?php _e(stripslashes($AvameraHomeOptions['car_near_you_description']), 'avamera'); ?></p>
    </div>
    <?php
    for ($i = 0; $i < COUNT($cars); $i++):
        $carMore = $Database->carMore(array('car_id' => $cars[$i]->ID));
        $ownerMeta = get_user_meta($cars[$i]->post_author);

        $car_id = $cars[$i]->ID;
        $user_id = $current_user->ID;
        $wish_count = $wpdb->get_var("SELECT COUNT(*) FROM av_wishlist WHERE car_id = $car_id AND user_id = $user_id");
        ?>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <input type="hidden" id="wish_count_<?php echo $car_id; ?>" value="<?php echo $wish_count; ?>" />
            <div class="car-item">
                <a hreflang="<?php echo ICL_LANGUAGE_CODE; ?>" href="<?php echo get_permalink($cars[$i]->ID) ?>">
                    <div class="picture">
                        <ul class="bxslider">
                            <?php if (has_post_thumbnail($cars[$i]->ID)): ?><li><div class="thumb-ratio"><div class="thumb-ratio-content"><?php echo wp_get_attachment_image(get_post_thumbnail_id($cars[$i]->ID), 'avamera-thumb'); ?></div></div></li><?php endif; ?>
                            <?php
                            for ($j = 0; $j < COUNT($carMore['imageGallery']); $j++):
                                $image = wp_get_attachment_image_src($carMore['imageGallery'][$j], 'avamera-thumb');
                                ?>
                                <li><div class="thumb-ratio"><div class="thumb-ratio-content"><img alt="<?php echo $cars[$i]->post_title; ?>" src="<?php echo $image[0] ?>" height="250px" /></div></div></li>
                            <?php endfor; ?>
                        </ul>

                        <div class="top">
                            <a hreflang="<?php echo ICL_LANGUAGE_CODE; ?>" href="javascript:void(0);" class="wishpop" data-carid="<?php echo $cars[$i]->ID; ?>">
                                <div class="like pull-left">
                                    <i class="checked fa fa-heart"></i>
                                    <i class="un-checked fa fa-heart"  id="wish_<?php echo $cars[$i]->ID; ?>" <?php if ($wish_count > 0) { ?> style="color:#ffc000;" <?php } ?>></i>
                                    <i class="white fa fa-heart-o"></i>
                                </div>
                            </a>
                            <div class="star pull-right">
                                <?php show_rating($cars[$i]->ID); ?>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price-time"><?php echo $Avamera->price(array('price' => $carMore['meta']['daily_rate'][0], 'decimal' => 0)) ?> <span>/ <?php _e('day', 'avamera'); ?></span></div>
                            <div class="price-time"><?php echo $Avamera->price(array('price' => $carMore['meta']['hourly_rate'][0], 'decimal' => 0)) ?> <span>/ <?php _e('hour', 'avamera'); ?></span></div>
                        </div>
                    </div>
                </a>
                <?php
                //                        $carMore['meta']['address'][0]
                $carAddress = explode(',', $carMore['meta']['address'][0]);
                ?>
                <div class="car-info">
                    <h3><a hreflang="<?php echo ICL_LANGUAGE_CODE; ?>" href="<?php echo get_permalink($cars[$i]->ID) ?>"><?php echo $cars[$i]->post_title ?></a> <span><?php echo $carMore['year'][0] ?></span></h3>
                    <!--<p><?php echo substr(stripslashes($cars[$i]->post_excerpt), 0, 30) ?> </p>-->
                    <label>
                        <?php
                        if ($carMore['meta']['city'][0] != '') {
                            echo $carMore['meta']['city'][0];
                        } else {
                            $carAddress = explode(',', $carMore['meta']['address'][0]);
                            if (count($carAddress) <= 2) {
                                echo $carMore['meta']['address'][0];
                            } else {
                                for ($ad = 0; $ad < COUNT($carAddress); $ad++):
                                    if ($ad >= (COUNT($carAddress) - 2)) {
                                        echo $carAddress[$ad];
                                        if ($ad != (count($carAddress) - 1))
                                            echo ', ';
                                    }
                                endfor;
                            }
                        }
                        ?>
                    </label>
                    <div class="author-img">
                        <a hreflang="<?php echo ICL_LANGUAGE_CODE; ?>" href="<?php echo get_author_posts_url($cars[$i]->post_author); ?>">
                            <img alt="<?php echo $ownerMeta['first_name'][0]; ?>" src="<?php echo $Avamera->profileImage(array('attachment_id' => $ownerMeta['profile_image'][0])) ?>" />
                        </a>
                    </div>
                    <div class="car-owner-name">
                        <a hreflang="<?php echo ICL_LANGUAGE_CODE; ?>" href="<?php echo get_author_posts_url($cars[$i]->post_author); ?>"><?php echo $ownerMeta['first_name'][0]; ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endfor; ?>
    <div class="clr"></div>
<?php endif; ?>
