<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
    <div class="col-md-6 col-sm-6 col-xs-12 pd-L-0 xs-pd">
        <div class="panel">
            <div class="panel-body">
                <div class="LS-info">
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center pd-0">
                        <i class="drivers"><img alt="driver" src="<?php echo get_stylesheet_directory_uri() ?>/images/drivers.png" /></i>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 text-center-xs pd-R-0">
                        <h4><?php _e('Total Drivers', 'avamera'); ?></h4>
                        <h2><?php echo $totalUser ?>+</h2>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 pd-R-0 xs-pd">
        <div class="panel">
            <div class="panel-body">
                <div class="LS-info">
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center pd-0">
                        <i class="city"><img alt="city" src="<?php echo get_stylesheet_directory_uri() ?>/images/city.png" /></i>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 text-center-xs pd-R-0">
                        <h4><?php _e('Cities', 'avamera'); ?></h4>
                        <h2><?php echo $totalCity ?>+</h2>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 pd-L-0 xs-pd">
        <div class="panel">
            <div class="panel-body">
                <div class="LS-info">
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center pd-0">
                        <i class="cars"><img alt="cars" src="<?php echo get_stylesheet_directory_uri() ?>/images/cars.png" /></i>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 text-center-xs pd-R-0">
                        <h4><?php _e('Total Cars', 'avamera'); ?></h4>
                        <h2><?php echo $totalCar ?>+</h2>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 pd-R-0 xs-pd">
        <div class="panel">
            <div class="panel-body">
                <div class="LS-info">
                    <div class="col-md-3 col-sm-3 col-xs-12 text-center pd-0">
                        <i class="countries"><img alt="avamera-countris" src="<?php echo get_stylesheet_directory_uri() ?>/images/countries.png" /></i>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 text-center-xs pd-R-0">
                        <h4><?php _e('Countries', 'avamera'); ?></h4>
                        <h2><?php echo $totalCountry ?>+</h2>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clr"></div>