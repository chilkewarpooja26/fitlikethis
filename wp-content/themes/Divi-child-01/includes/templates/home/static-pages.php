<div class="col-md-6 pd-0 text-center cover boxes" style="background-image:url(<?php echo $howToTravelImage[0] ?>);">
    <div class="box-item">
        <h1><?php _e($howToTravel->post_title, 'avamera'); ?></h1>
        <div class="my-btn"><a href="<?php echo get_permalink($howToTravel->ID) ?>"><?php _e('Learn More', 'avamera'); ?></a></div>
    </div>
</div><!-- boxes -->
<div class="col-md-6 pd-0 text-center cover boxes" style="background-image:url(<?php echo $oneLessStrangerImage[0] ?>);">
    <div class="box-item">
        <h1><?php _e($oneLessStranger->post_title, 'avamera'); ?></h1>
        <div class="my-btn"><a href="<?php echo get_permalink($oneLessStranger->ID) ?>"><?php _e('Learn More', 'avamera'); ?></a></div>
    </div>
</div><!-- boxes -->
<div class="col-md-7 pd-0 text-center cover boxes" style="background-image:url(<?php echo $aSaenseOfBelongingImage[0] ?>);">
    <div class="box-item">
        <?php $aSaenseOfBelongingID = icl_object_id(55, 'page', false, ICL_LANGUAGE_CODE); ?>
        <h1><?php _e($aSaenseOfBelonging->post_title, 'avamera'); ?></h1>
        <div class="my-btn"><a href="<?php echo get_permalink($aSaenseOfBelongingID) ?>"><?php _e('Learn More', 'avamera'); ?></a></div>
    </div>
</div><!-- boxes -->
<div class="col-md-5 pd-0 text-center cover boxes" style="background-image:url(<?php echo $howToHostImage[0] ?>);">
    <div class="box-item">
        <h1><?php _e($howToHost->post_title, 'avamera'); ?></h1>
        <div class="my-btn"><a href="<?php echo get_permalink($howToHost->ID) ?>"><?php _e('Learn More', 'avamera'); ?></a></div>
    </div>
</div>