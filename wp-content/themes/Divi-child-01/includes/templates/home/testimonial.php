<div class="container-fluid testi-bg">
    <div class="row">
    <div class="container">
        <div class="testi-wrap cover">
            <h2 class="U-say"><?php _e('Trust, a new perspective!','avamera'); ?></h2>
            <div class="testimonials-slider">
                <?php for($i=0; $i<COUNT($testimonials); $i++):
                        
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($testimonials[$i]->ID), 'full');
                        $meta  = get_post_meta($testimonials[$i]->ID);
                    ?>
                <div class="slide">
                    <div class="col-md-3 text-center">
                        <div class="author-thumbnail"><div><div style="background-image: url('<?php echo $image[0] ?>')"></div></div></div>
                        <div class="author-name"><?php echo $testimonials[$i]->post_title ?></div>
                        <p><?php echo $meta['designation'][0] ?></p>
                    </div>
                    <div class="col-md-9">
                        <div class="testimonials-context"><?php echo stripslashes($testimonials[$i]->post_content) ?></div>
                        <!--<div class="post-date"><small><?php echo $Avamera->timePassed(strtotime($testimonials[$i]->post_date)) ?></small></div>-->
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
        </div>
    </div>
</div>