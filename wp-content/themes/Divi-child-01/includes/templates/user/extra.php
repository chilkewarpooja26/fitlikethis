<?php
$popup = get_user_meta(USER_ID, 'popup', true);
?>
<?php if ($popup == 'yes'): ?>  

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var options = {
                "backdrop": "static"
            };
            $('#basicModal').modal(options);
            
            $('form#how_did_get_find_form').submit(function(){
                var loader = $('form#how_did_get_find_form .loader');
                var button = $('form#how_did_get_find_form .buttons');
                var values = $('form#how_did_get_find_form').serializeArray();
                
                var data = {
                    action: 'avamera_how_did_find_us_db',
                    values: values
                };
                
                loader.removeClass('hide');
                button.addClass('hide');
                $.ajax({
                    url: avamera.ajaxurl,
                    type: 'POST',
                    data: data,
                    success: function(response) {

                        loader.addClass('hide');
                        button.removeClass('hide');

                        var obj = jQuery.parseJSON(response);
                        window.location.href = obj.url;   
                    },
                    error: function(e) {

                    }
                });
                
//                return false;
            });
            
        });
    </script>
<?php 
$AvameraSiteOptions = get_option('AvameraSiteOptions');
$list = explode(',', $AvameraSiteOptions['how_did_you_get_find_list']);
$list = is_array($list) ? array_map('trim', $list) : array();

?>
    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <form name="how_did_get_find_form" id="how_did_get_find_form" method="post" action="" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Avamera</h4>
                </div>
                <div class="modal-body">
                	<div class="form-group">
                    	<label class="col-sm-5 pd-L-0 control-label"><?php _e('How Did You Find Us', 'avamera'); ?> :</label>
                        <div class="col-sm-7 pd-0">
                        	<div class="select-style">
                                <select name="how_did_find_us">
                                <?php for($i=0; $i<COUNT($list); $i++): ?>
                                    <option value="<?php echo $list[$i] ?>"><?php echo $list[$i] ?></option>
                                <?php endfor; ?>
                                </select>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <button type="submit" class="submit-btn buttons"><?php _e('Submit', 'avamera'); ?></button>
                    <img src="<?php echo AV_LOADER_CYCLE_2 ?>" class="loader hide" />
                </div>
            </div>
        </form>
        </div>
    </div>
<?php endif; ?>