<?php 
/*
Template Name: Grocery Template 
*/
get_header(); ?>

          <div id="main-content" class="inner-pages-section">
            <div class="sub-banner dash-sect">
              <img src="http://exceptionaire.co/nutrafitDivi/wp-content/themes/Divi-child-01/images/inner-banner.jpg" alt="inner-banner-image">
              <div class="page-title-head">
                <h1 class="entry-title main_title">Groceries</h1>
              </div>
            </div>

            <section class="groceries">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 dash-sect-title"><h1>Grocery Lists</h1></div>

                  <div class="col-md-12 col-sm-12 list-create">
                    <div class="col-sm-8 col-md-8 list-create-inner">
                      <div class="top-btn">
                      <div class="date-picker">
                            <div class="well configurator hidden">
                               
                              <form>
                              <div class="row">

                                <div class="col-md-4">

                                  <div class="form-group">
                                    <label for="parentEl">parentEl</label>
                                    <input type="text" class="form-control" id="parentEl" value="" placeholder="body">
                                  </div>

                                  <div class="form-group">
                                    <label for="startDate">startDate</label>
                                    <input type="text" class="form-control" id="startDate" value="">
                                  </div>

                                  <div class="form-group">
                                    <label for="endDate">endDate</label>
                                    <input type="text" class="form-control" id="endDate" value="">
                                  </div>

                                  <div class="form-group">
                                    <label for="minDate">minDate</label>
                                    <input type="text" class="form-control" id="minDate" value="" placeholder="MM/DD/YYYY">
                                  </div>

                                  <div class="form-group">
                                    <label for="maxDate">maxDate</label>
                                    <input type="text" class="form-control" id="maxDate" value="" placeholder="MM/DD/YYYY">
                                  </div>

                                </div>
                                <div class="col-md-4">

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="autoApply"> autoApply
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="singleDatePicker"> singleDatePicker
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showDropdowns"> showDropdowns
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showWeekNumbers"> showWeekNumbers
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showISOWeekNumbers"> showISOWeekNumbers
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePicker"> timePicker
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePicker24Hour"> timePicker24Hour
                                    </label>
                                  </div>

                                  <div class="form-group">
                                    <label for="timePickerIncrement">timePickerIncrement (in minutes)</label>
                                    <input type="text" class="form-control" id="timePickerIncrement" value="1">
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePickerSeconds"> timePickerSeconds
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="dateLimit"> dateLimit (with example date range span)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="ranges"> ranges (with example predefined ranges)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="locale"> locale (with example settings)
                                    </label>
                                    <label id="rtl-wrap">
                                      <input type="checkbox" id="rtl"> RTL (right-to-left)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="alwaysShowCalendars"> alwaysShowCalendars
                                    </label>
                                  </div>

                                </div>
                                <div class="col-md-4">

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="linkedCalendars" checked="checked"> linkedCalendars
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="autoUpdateInput" checked="checked"> autoUpdateInput
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showCustomRangeLabel" checked="checked"> showCustomRangeLabel
                                    </label>
                                  </div>

                                  <div class="form-group">
                                    <label for="opens">opens</label>
                                    <select id="opens" class="form-control">
                                      <option value="right" selected>right</option>
                                      <option value="left">left</option>
                                      <option value="center">center</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="drops">drops</label>
                                    <select id="drops" class="form-control">
                                      <option value="down" selected>down</option>
                                      <option value="up">up</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="buttonClasses">buttonClasses</label>
                                    <input type="text" class="form-control" id="buttonClasses" value="btn btn-sm">
                                  </div>

                                  <div class="form-group">
                                    <label for="applyClass">applyClass</label>
                                    <input type="text" class="form-control" id="applyClass" value="btn-success">
                                  </div>

                                  <div class="form-group">
                                    <label for="cancelClass">cancelClass</label>
                                    <input type="text" class="form-control" id="cancelClass" value="btn-default">
                                  </div>

                                </div>

                              </div>
                              </form>

                            </div>

                            <div class="row">

                              <div class="col-md-12 demo">
                                 <label class="cal-text">Create New List</label>
                                <input type="text" id="config-demo" name="daterange" class="form-control" placeholder="Create New List" value="">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                              </div>

                              <div class="col-md-6 hidden">
                                <h4>Configuration</h4>

                                <div class="well">
                                  <textarea id="config-text" style="height: 300px; width: 100%; padding: 10px"></textarea>
                                </div>
                              </div>

                            </div>

                          </div>
                      </div>
                     <!--  <div class="left-btn">
                        <button type="button" class="btn btn-primary">Create New List</button>
                      </div> -->
                      <div class="right-btn">   
                        <button type="button" class="btn btn-primary">Delete All Lists</button>
                      </div>

                      <div class="col-md-12 col-sm-12 date-list">
                        <div class="col-md-8 col-sm-8">
                          <p>
                            <span>March 1st -</span>
                            <span>March 3rd</span>
                            <span class="label">Last Modified - 28 Feb</span>
                          </p>
                        </div>
                        <div class="col-md-4 col-sm-4 delet-btn">
                          <button type="button" class="btn btn-primary">Delete</button> 
                        </div>
                      </div>

                      <div class="col-md-12 col-sm-12 date-list">
                        <div class="col-md-8 col-sm-8">
                          <p>
                            <span>March 4th -</span>
                            <span>March 15th</span>
                          </p>
                        </div>
                        <div class="col-md-4 col-sm-4 delet-btn">
                          <button type="button" class="btn btn-primary">Delete</button> 
                        </div>
                      </div>

                      <div class="col-md-12 col-sm-12 date-list">
                        <div class="col-md-8 col-sm-8">
                          <p>
                            <span>March 16th -</span>
                            <span>March 17th</span>
                          </p>
                        </div>
                        <div class="col-md-4 col-sm-4 delet-btn">
                          <button type="button" class="btn btn-primary">Delete</button> 
                        </div>
                      </div>

                      <div class="col-md-12 col-sm-12 date-list">
                        <div class="col-md-8 col-sm-8">
                          <p>
                            <span>March 18th -</span>
                            <span>March 30th</span>
                          </p>
                        </div>
                        <div class="col-md-4 col-sm-4 delet-btn">
                          <button type="button" class="btn btn-primary">Delete</button> 
                        </div>
                      </div>
                    </div><!--create-inner--> 

                      <div class="col-md-12 col-sm-12 grocery-moving-content">
                        <div class="col-md-6 col-sm-6 moving-left">
                          <div class="grocery-inner">
                            <h1>Grocery List</h1>

                            <div class="grocery-content">
                              <div class="col-sm-12 col-md-12 gro-top">
                                <p class="move-title">Move to Pantry</p>
                              </div>

                              <div class="col-sm-12 col-md-12 gro-bottom" id="boxscroll5">
                                <ul>
                                    <li>
                                      <label for="select_all">
                                          <input type="checkbox" name="check[]" class="checkbox" id="select_all"/>
                                          <i></i> <span> Select All</span> 
                                      </label>
                                    </li>
                                    <li class="title-text">Dairy Products</li>
                                    <li>
                                      <label for="check-one">
                                          <input type="checkbox" name="check[]" class="checkbox" id="check-one">
                                          <i></i> <span> <img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Milk</span> 
                                      </label>
                                    </li>
                                    <li><label for="check-two">
                                          <input type="checkbox" name="check[]" class="checkbox" id="check-two">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block">Yogurt</span> 
                                      </label></li>
                                    <li>
                                    <label for="check-three">
                                          <input type="checkbox" name="check[]" class="checkbox" id="check-three">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Cottage Cheese</span> 
                                      </label></li>
                                    <li class="title-text">Fats &amp; Oils </li>  
                                    <li><label for="check-four">
                                          <input type="checkbox" name="check[]" class="checkbox" id="check-four">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Olive Oil</span> 
                                      </label></li>
                                    <li><label for="check-five">
                                          <input type="checkbox" name="check[]" class="checkbox" id="check-five">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Mayonnaise</span> 
                                      </label></li>
                                  </ul>

                                <script>
                                  //select all checkboxes
                                  $("#select_all").change(function(){  //"select all" change
                                      $(".checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
                                  });

                                  //".checkbox" change
                                  $('.checkbox').change(function(){
                                      //uncheck "select all", if one of the listed checkbox item is unchecked
                                      if(false == $(this).prop("checked")){ //if this item is unchecked
                                          $("#select_all").prop('checked', false); //change "select all" checked status to false
                                      }
                                      //check "select all" if all checkbox items are checked
                                      if ($('.checkbox:checked').length == $('.checkbox').length ){
                                          $("#select_all").prop('checked', true);
                                      }
                                  }); 
                                </script>

                              </div>

                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 moving-right">
                          <div class="grocery-inner">
                            <h1>Pantry</h1>

                            <div class="grocery-content">
                              <div class="col-sm-12 col-md-12 gro-top">
                                <p class="move-title">Move to Grocery</p>
                              </div>

                              <div class="col-sm-12 col-md-12 gro-bottom" id="boxscroll6">
                                  <ul>
                                    <li>
                                      <label for="select_all1">
                                          <input type="checkbox" name="check[]" class="checkbox1" id="select_all1"/>
                                          <i></i> <span> Select All</span> 
                                      </label>
                                    </li>
                                    <li class="title-text">Dairy Products</li>
                                    <li>
                                      <label for="check-six">
                                          <input type="checkbox" name="check[]" class="checkbox1" id="check-six">
                                          <i></i> <span> <img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Tofu</span> 
                                      </label>
                                    </li>
                                    <li><label for="check-seven">
                                          <input type="checkbox" name="check[]" class="checkbox1" id="check-seven">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Lowfat Milk</span> 
                                      </label></li>
                                    <li>
                                    <label for="check-eight">
                                          <input type="checkbox" name="check[]" class="checkbox1" id="check-eight">
                                          <i></i> <span><img src="https://spoonacular.com/cdn/ingredients_100x100/lemon-juice.jpg" class="img-block"> Fish Oil</span> 
                                      </label>
                                    </li>
                                  </ul>
                              </div>

                              <script>
                                  //select all checkboxes
                                  $("#select_all1").change(function(){  //"select all" change
                                      $(".checkbox1").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
                                  });

                                  //".checkbox" change
                                  $('.checkbox1').change(function(){
                                      //uncheck "select all", if one of the listed checkbox item is unchecked
                                      if(false == $(this).prop("checked")){ //if this item is unchecked
                                          $("#select_all1").prop('checked', false); //change "select all" checked status to false
                                      }
                                      //check "select all" if all checkbox items are checked
                                      if ($('.checkbox1:checked').length == $('.checkbox1').length ){
                                          $("#select_all1").prop('checked', true);
                                      }
                                  }); 
                                </script>

                            </div>
                          </div>      
                        </div><!--moving-right-->
                      </div><!--grocery-moving-content-->

                    
                  </div>
                </div> 
        </div>              
            </section>



  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/css/daterangepicker.css" />
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.nicescroll.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('#config-text').keyup(function() {
          eval($(this).val());
        });
        
        $('.configurator input, .configurator select').change(function() {
          updateConfig();
        });

        $('.demo i').click(function() {
          $(this).parent().find('input').click();
        });

        $('#startDate').daterangepicker({
          singleDatePicker: true,
          startDate: moment().subtract(6, 'days')
        });

        $('#endDate').daterangepicker({
          singleDatePicker: true,
          startDate: moment()
        });

        updateConfig();

        function updateConfig() {
          var options = {};

          if ($('#singleDatePicker').is(':checked'))
            options.singleDatePicker = true;
          
          if ($('#showDropdowns').is(':checked'))
            options.showDropdowns = true;

          if ($('#showWeekNumbers').is(':checked'))
            options.showWeekNumbers = true;

          if ($('#showISOWeekNumbers').is(':checked'))
            options.showISOWeekNumbers = true;

          if ($('#timePicker').is(':checked'))
            options.timePicker = true;
          
          if ($('#timePicker24Hour').is(':checked'))
            options.timePicker24Hour = true;

          if ($('#timePickerIncrement').val().length && $('#timePickerIncrement').val() != 1)
            options.timePickerIncrement = parseInt($('#timePickerIncrement').val(), 10);

          if ($('#timePickerSeconds').is(':checked'))
            options.timePickerSeconds = true;
          
          if ($('#autoApply').is(':checked'))
            options.autoApply = true;

          if ($('#dateLimit').is(':checked'))
            options.dateLimit = { days: 7 };

          if ($('#ranges').is(':checked')) {
            options.ranges = {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            };
          }

          if ($('#locale').is(':checked')) {
            $('#rtl-wrap').show();
            options.locale = {
              direction: $('#rtl').is(':checked') ? 'rtl' : 'ltr',
              format: 'MM/DD/YYYY HH:mm',
              separator: ' - ',
              applyLabel: 'Apply',
              cancelLabel: 'Cancel',
              fromLabel: 'From',
              toLabel: 'To',
              customRangeLabel: 'Custom',
              daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
              monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
              firstDay: 1
            };
          } else {
            $('#rtl-wrap').hide();
          }

          if (!$('#linkedCalendars').is(':checked'))
            options.linkedCalendars = false;

          if (!$('#autoUpdateInput').is(':checked'))
            options.autoUpdateInput = false;

          if (!$('#showCustomRangeLabel').is(':checked'))
            options.showCustomRangeLabel = false;

          if ($('#alwaysShowCalendars').is(':checked'))
            options.alwaysShowCalendars = true;

          if ($('#parentEl').val().length)
            options.parentEl = $('#parentEl').val();

          if ($('#startDate').val().length) 
            options.startDate = $('#startDate').val();

          if ($('#endDate').val().length)
            options.endDate = $('#endDate').val();
          
          if ($('#minDate').val().length)
            options.minDate = $('#minDate').val();

          if ($('#maxDate').val().length)
            options.maxDate = $('#maxDate').val();

          if ($('#opens').val().length && $('#opens').val() != 'right')
            options.opens = $('#opens').val();

          if ($('#drops').val().length && $('#drops').val() != 'down')
            options.drops = $('#drops').val();

          if ($('#buttonClasses').val().length && $('#buttonClasses').val() != 'btn btn-sm')
            options.buttonClasses = $('#buttonClasses').val();

          if ($('#applyClass').val().length && $('#applyClass').val() != 'btn-success')
            options.applyClass = $('#applyClass').val();

          if ($('#cancelClass').val().length && $('#cancelClass').val() != 'btn-default')
            options.cancelClass = $('#cancelClass').val();

          $('#config-text').val("$('#demo').daterangepicker(" + JSON.stringify(options, null, '    ') + ", function(start, end, label) {\n  console.log(\"New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')\");\n});");

          // $('#config-demo').daterangepicker(options, function(start, end, label) { console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); });

           $(function() {
            $('input[name="daterange"]').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                locale: {
                    format: 'YYYY/MM/DD'
                }
            });
        });
          
        }

      });
      </script>     
       
      <script type="text/javascript">
         $(document).ready(function() {
     
            $("#boxscroll5").niceScroll({touchbehavior:false,cursorcolor:"#00F",cursoropacitymax:0.7,cursorwidth:6,background:"#ccc",autohidemode:false});
            $("#boxscroll6").niceScroll({touchbehavior:false,cursorcolor:"#00F",cursoropacitymax:0.7,cursorwidth:6,background:"#ccc",autohidemode:false});
          });
      </script>

             <?php get_footer(); ?>

