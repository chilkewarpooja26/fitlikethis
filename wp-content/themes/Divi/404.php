<?php get_header(); ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<div class="col-sm-12 text-center page-not-found"> <span class="not-found font-montserrat">page not found</span> 
        			<span class="head-404">404</span>
					<h4>Page doesn’t exist. Go to our 
					<a href="<?php echo get_home_url();?>" class="font-montserrat">HOMEPAGE</a>
					</h4>
        		</div>
			</div> 
		</div> 
	</div> 
</div> 

<?php get_footer(); ?>
