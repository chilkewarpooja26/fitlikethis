<?php


// Set up the ORM library
require_once('../../../wp-config.php');
require_once('../../../idiorm.php');
$user_id = get_current_user_id();

$host = 'localhost';
$user = 'etpl2012_nutrafi';
$pass = 'nutrafit123';
$database = 'etpl2012_nutrafitdivi';
ORM::configure("mysql:host=$host;dbname=$database");
ORM::configure('username', $user);
ORM::configure('password', $pass);
ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));


header('Content-Type: application/json');

if (isset($_GET['start']) AND isset($_GET['end'])) {
	
	global $wpdb;
	$query_sub_plan_duration = "select payment_date from " . $wpdb->prefix . "payments where user_id='".$user_id."' ";
	$row_sub_plan_duration=$wpdb->get_row($query_sub_plan_duration . " order by payment_id DESC");
	$plan_start_date=$row_sub_plan_duration->payment_date;
        //$first_entry_date= date('Y-m-d', strtotime($plan_start_date));

	$current_date = date("Y-m-d");
	$data = array();

	//Select the results with Idiorm
	$results = ORM::for_table('nu_weight_tracker')
			->where_gte('date', $plan_start_date)
			->where_lte('date', $current_date)
			->where('user_id', $user_id)
			->find_array();;


	// Build a new array with the data
	foreach ($results as $key => $value) {
		$data[$key]['label'] = $value['current_date'];
		$data[$key]['value'] = $value['current_lbs_weight'];
	}

	echo json_encode($data);
}
