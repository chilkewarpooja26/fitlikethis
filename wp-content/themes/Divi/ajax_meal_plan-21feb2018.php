<?php
require_once('../../../wp-config.php');
global $wpdb; 

global $addmore_secondhit_info;
$addmore_secondhit_info = array();
// Block recipe start
if (isset($_POST['blk_recipe_id']) && isset($_POST['blk_user_id']))
{
	// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$_POST['blk_user_id']."'";
			$getdate = $wpdb->get_row($getplans_date);
	// Get plan date end

    $alredy_exist_item= "select * from " . $wpdb->prefix . "block_items where recipe_id='".$_POST['blk_recipe_id']."' and user_id='".$_POST['blk_user_id']."' ";
   $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);

  $alredy_favorites_item= "select recipe_id from " . $wpdb->prefix . "favorites_items where recipe_id='".$_POST['blk_recipe_id']."' and user_id='".$_POST['blk_user_id']."' ";
            $alredy_favorites_item_row = $wpdb->get_row($alredy_favorites_item);
           
    if($alredy_favorites_item_row->recipe_id!='')
    {

     echo json_encode(array("status"=>"success","recipe_status"=>"already_favorites","msg"=>"Recipe already added as favorites."));
	}
            elseif($alredy_exist_item_row->recipe_id=='')
  {
        $plan_expire_date=plan_expire_date($_POST['blk_user_id']); 
	$sql = "INSERT INTO nu_block_items (recipe_id,user_id,recipe_name,recipe_image,recipe_calories,recipe_protien, 	recipe_fats,recipe_carbs,plan_expire_date) VALUES ('".$_POST['blk_recipe_id']."','".$_POST['blk_user_id']."','".$_POST['blk_user_rname']."','".$_POST['blk_user_rimage']."','".$_POST['blk_user_rcalories']."','".$_POST['blk_user_rprotien']."','".$_POST['blk_user_rfats']."','".$_POST['blk_user_rcarbs']."','".$plan_expire_date."')";
	$res = $wpdb->get_row($sql);

	$sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='1' WHERE user_id='".$_POST['blk_user_id']."' and recipe_id='".$_POST['blk_recipe_id']."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
	$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);


	echo json_encode(array("status"=>"success","recipe_status"=>"Block","rec_id"=>$_POST['blk_recipe_id'],"msg"=>"Item blocked sucessfully."));
  }else
  {
  	$sql = "Delete from nu_block_items where recipe_id='".$_POST['blk_recipe_id']."' and user_id='".$_POST['blk_user_id']."'";

	$res = $wpdb->get_row($sql);

	$sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='0' WHERE user_id='".$_POST['blk_user_id']."' and recipe_id='".$_POST['blk_recipe_id']."' and recipe_current_date >= '".$getdate->payment_date."' AND  recipe_current_date <= '".$getdate->plan_expire_date."'";
	$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

  	echo json_encode(array("status"=>"success","recipe_status"=>"Unblock","rec_id"=>$_POST['blk_recipe_id'],"msg"=>"Item Unblocked sucessfully."));
  }

}
// Block recipe End

// Delete block recipe start
if (isset($_POST['delblock_recipe_id']) && isset($_POST['delblock_user_id']))
{
	// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$_POST['delblock_user_id']."'";
			$getdate = $wpdb->get_row($getplans_date);
	// Get plan date end

	$sql = "Delete from nu_block_items  where recipe_id='".$_POST['delblock_recipe_id']."' and user_id='".$_POST['delblock_user_id']."'";

	$res = $wpdb->get_row($sql);

	$sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set blocked_status='0' WHERE user_id='".$_POST['delblock_user_id']."' and recipe_id='".$_POST['delblock_recipe_id']."' and recipe_current_date >= '".$getdate->payment_date."' AND  recipe_current_date <= '".$getdate->plan_expire_date."'";
	$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);


	if($_POST['delblock_recipe_id']!='')
	{
		echo json_encode(array("status"=>"success","msg"=>"Item deleted sucessfully."));
	}
   else
	{
		echo json_encode(array("status"=>"error","msg"=>"Failed."));
	}
}
// Delete block recipe end

// Favorites recipe start
if (isset($_POST['fav_recipe_id']) && isset($_POST['fav_user_id']))
{

	// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$_POST['fav_user_id']."'";
			$getdate = $wpdb->get_row($getplans_date);
	// Get plan date end

	$alredy_exist_item= "select * from " . $wpdb->prefix . "favorites_items where recipe_id='".$_POST['fav_recipe_id']."' and user_id='".$_POST['fav_user_id']."' ";
   $alredy_exist_item_row = $wpdb->get_row($alredy_exist_item);
   
   $alredy_block_item= "select recipe_id from " . $wpdb->prefix . "block_items where recipe_id='".$_POST['fav_recipe_id']."' and user_id='".$_POST['fav_user_id']."' ";
            $alredy_block_item_row = $wpdb->get_row($alredy_block_item);
           
           if($alredy_block_item_row->recipe_id!='')
            {
echo json_encode(array("status"=>"success","recipe_status"=>"already_blocked","msg"=>"Recipe already added as blocked."));
	       }
           elseif($alredy_exist_item_row->recipe_id=='')
  {
        $plan_expire_date=plan_expire_date($_POST['fav_user_id']);
	$sql = "INSERT INTO nu_favorites_items (recipe_id,user_id,recipe_name,recipe_image,recipe_calories,recipe_protien, 	recipe_fats,recipe_carbs,plan_expire_date) VALUES ('".$_POST['fav_recipe_id']."','".$_POST['fav_user_id']."','".$_POST['fav_user_rname']."','".$_POST['fav_user_rimage']."','".$_POST['fav_user_rcalories']."','".$_POST['fav_user_rprotien']."','".$_POST['fav_user_rfats']."','".$_POST['fav_user_rcarbs']."','".$plan_expire_date."')";
	$res = $wpdb->get_row($sql);

    
    $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='1' WHERE user_id='".$_POST['fav_user_id']."' and recipe_id='".$_POST['fav_recipe_id']."' and recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."' ";
  $row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

	echo json_encode(array("status"=>"success","recipe_status"=>"Favorites","rec_id"=>$_POST['fav_recipe_id'],"msg"=>"Item favorited sucessfully."));
   }
   else
   {
   	$sql = "Delete from nu_favorites_items where recipe_id='".$_POST['fav_recipe_id']."' and user_id='".$_POST['fav_user_id']."'";
	$res = $wpdb->get_row($sql);

    $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='0' WHERE user_id='".$_POST['fav_user_id']."' and recipe_id='".$_POST['fav_recipe_id']."' and  recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
	$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

	  echo json_encode(array("status"=>"error","recipe_status"=>"Unfavorites","rec_id"=>$_POST['fav_recipe_id'],"msg"=>"Item unfavorited sucessfully."));
   }
}
// Favorites recipe end

// Delete Favorites recipe start
if (isset($_POST['delfav_recipe_id']) && isset($_POST['delfav_user_id']))
{
	// Get plan date start
 		global $wpdb;
		    $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$_POST['delfav_user_id']."'";
			$getdate = $wpdb->get_row($getplans_date);
	// Get plan date end

	$sql = "Delete from nu_favorites_items  where recipe_id='".$_POST['delfav_recipe_id']."' and user_id='".$_POST['delfav_user_id']."'";

	$res = $wpdb->get_row($sql);

	$sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set favorite_status='0' WHERE user_id='".$_POST['delfav_user_id']."' and recipe_id='".$_POST['delfav_recipe_id']."' and  recipe_current_date >= '".$getdate->payment_date."' AND recipe_current_date <= '".$getdate->plan_expire_date."'";
	$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);

	if($_POST['delfav_recipe_id']!='')
	{
		echo json_encode(array("status"=>"success","msg"=>"Item deleted sucessfully."));
	}
   else
	{
		echo json_encode(array("status"=>"error","msg"=>"Failed."));
	}
}
// Delete Favorites recipe end

// Meal plan  Start

// Delete recipe start
if (isset($_POST['del_recipe_id']) && isset($_POST['del_user_id']) && isset($_POST['del_meal_type']))
{
	$current_date = $_POST['del_recipe_date'];
	$sql = "Delete from nu_meal_plan  where recipe_id='".$_POST['del_recipe_id']."' and user_id='".$_POST['del_user_id']."' and meal_type='".$_POST['del_meal_type']."' and recipe_current_date='".$current_date."'";

	$res = $wpdb->get_row($sql);

	$get_macro_nutrients = macro_nutrients($_POST['del_user_id'],$current_date); 
	$cla_sum_total = round($get_macro_nutrients[0]['recipe_calories']) ;
	$prot_sum_total = round($get_macro_nutrients[0]['recipe_protien']);
	$fat_sum_total = round($get_macro_nutrients[0]['recipe_fats']);
	$carb_sum_total = round($get_macro_nutrients[0]['recipe_carbs']);

	
	if($_POST['del_recipe_id']!='')
	{
		echo json_encode(array("status"=>"success","msg"=>"Item deleted sucessfully.", "recipe_id"=>$_POST['del_recipe_id'], "recipe_calories"=> $cla_sum_total, "recipe_protien"=> $prot_sum_total, "recipe_fats"=> $fat_sum_total, "recipe_carbs"=> $carb_sum_total));
	}
   else
	{
		echo json_encode(array("status"=>"error","msg"=>"Failed."));
	}
}
// Delete recipe end

// regenerate start revised on 16-02-2017
if (isset($_POST['rege_meal_type']) && isset($_POST['rege_recipe_id']) && isset($_POST['user_id']))
{
	
	global $wpdb;
$alredy_fav_exist_item= "select recipe_id from ".$wpdb->prefix."favorites_items where user_id='".$_POST['user_id']."'";
$alredy_fav_exist_item_row = $wpdb->get_results($alredy_fav_exist_item,ARRAY_A);

$alredy_regblock_item= "select recipe_id from ".$wpdb->prefix."block_items where user_id='".$_POST['user_id']."'";
$alredy_regblockitem_row = $wpdb->get_results($alredy_regblock_item,ARRAY_A);

//echo "string <pre>"; print_r($alredy_fav_exist_item_row);
$checkfavrecipe = array();
for ($favck=0; $favck < count($alredy_fav_exist_item_row); $favck++) { 
	
	$checkfavrecipe[] = $alredy_fav_exist_item_row[$favck]['recipe_id'];
}

$checkblockrecipe = array();
for ($bck=0; $bck < count($alredy_regblockitem_row); $bck++) { 
	
	$checkblockrecipe[] = $alredy_regblockitem_row[$bck]['recipe_id'];
}



	// $url = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/random?limitLicense=false&number=1';

	$url = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/'.$_POST['rege_recipe_id'].'/similar';

			    $ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL, $url);
			   // curl_setopt($ch, CURLOPT_HTTPGET, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh',
			    'Accept: application/json'
			    ));

			    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0'); 
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			    $body = curl_exec($ch);
			    $info = curl_getinfo($ch);
			    $error = curl_errno($ch);
			    // print_r('Curl error: ' . curl_error($ch));

			    curl_close($ch); 

			    $addmore_response = array();
			    $final_response = array();
			    
			    $spoon_result_array = json_decode($body, TRUE);
			    $result_array = array_rand($spoon_result_array);
			    $spoon_result_array = $spoon_result_array[$result_array];

			    // echo $spoon_result_array['title'];
			    // echo $spoon_result_array['id'];

			    // echo '<pre>'; print_r($spoon_result_array);
			    // die();
			    // echo "<pre>"; print_r($spoon_result_array['recipes'][0]['analyzedInstructions']);
			    // die();

			    $spoonacula_getsechit = spoonacula_getsechit($spoon_result_array['id']);

			    // Check favorite and block msg start
			    $favmsg = 'notfav';
			    $blockmsg = 'notblock';
			    if(in_array($spoon_result_array['id'], $checkfavrecipe)){

			    	$favmsg = 'isfavourite';
			    }

			    if(in_array($spoon_result_array['id'], $checkblockrecipe)){

			    	$blockmsg = 'isblock';
			    }
			    // Check favorite msg end

				  $spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);

				  // echo $spoonacula_getsechit['servings'];
				  // echo '//'.$spoonacula_getsechit['spoonacularSourceUrl'];
				  // echo '//<pre>'; print_r($spoonacula_getsechit['extendedIngredients']);
				  // echo '//<pre>'; print_r($spoonacula_getsechit['analyzedInstructions']);
				  // //echo '<pre>'; print_r($nutrients);
				  //  die();

				  $nutrients = $spoonacula_getsechit['nutrition']['nutrients'];

				  $countattr = count($nutrients);
				  $stringnutrition = array();
				  for($i=0;$i<$countattr;$i++)
				  {

				    if("Calories" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['calories'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
				    }
				    if("Fat" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
				    {
				       $stringnutrition['fat'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
				    }        
				    if("Carbohydrates" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['carbohydrates'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
				    }
				      if("Protein" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
				    {
				       $stringnutrition['protein'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
				    }
				  }


			 $regrecipe_calories = $stringnutrition['calories'];
			 $regrecipe_fat = $stringnutrition['fat'];
			 $regrecipe_carbohydrates = $stringnutrition['carbohydrates'];
			 $regrecipe_protein = $stringnutrition['protein'];
			 $regrecipe_id = $spoon_result_array['id'];
			 $regrecipe_title = $spoon_result_array['title'];
			 $regrecipe_image = 'https://spoonacular.com/recipeImages/'.$spoon_result_array['image'];
			 $original_numberofservings= $spoonacula_getsechit['servings'];
			 $regrecipe_servings= 1;
			 $spoonacularSourceUrl= $spoonacula_getsechit['spoonacularSourceUrl'];
			 // echo "<br><pre>regrecipe_extendedIngredients - ";
			 $regrecipe_ingredients = json_encode($spoonacula_getsechit['extendedIngredients']);

			 	// Conversion grocery START

				  $afterconvert = array();
				  $converted = array();

				  $final_ingredientline = json_decode($regrecipe_ingredients,TRUE);
				  
				  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {


				  	$convertvalue_unit = "";
				    $convertvalue_amount = "";
//				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
//				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
//
//				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
//				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
//				      }
//				      else{
//				        $convertvalue_unit = $convert_value['targetunit'];
//				        $convertvalue_amount = $convert_value['targetamount'];
//				      }
//				      unset($convert_value);

				    $afterconvert['id'] = $final_ingredientline[$gg]['id'];
				  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
				  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
				  	//$afterconvert['amount'] = $convertvalue_amount;
				  	//$afterconvert['unit'] = $convertvalue_unit;
                                        $afterconvert['amount'] = $final_ingredientline[$gg]['amount'];
                                        $afterconvert['unit'] = $final_ingredientline[$gg]['unit'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

				  	$converted[$gg] = $afterconvert;
				    unset($afterconvert);
				  }

				  $regrecipe_ingredients = json_encode($converted);
				  unset($converted);


				  // Conversion grocery END

			 //echo "<pre>";print_r($regrecipe_ingredients);

			 // for ($ri=0; $ri < count($spoon_result_array['recipes'][0]['extendedIngredients']); $ri++) { 

			 // 	$ri_image = $regrecipe_ingredients[$ri]['image'];
			 // 	$ri_image = $regrecipe_ingredients[$ri]['name'];
			 // 	$ri_amount = $regrecipe_ingredients[$ri]['amount'];
			 // 	$ri_unit = $regrecipe_ingredients[$ri]['unit'];
			 	
			 // }

			 // echo "<br>regrecipe_analyzedInstructions - ";
			 $regrecipe_instructions = $spoonacula_getsechit['analyzedInstructions'];
			 $steps = array();
			 $stpsinfo = array();
			 
			 for ($regreciperi=0; $regreciperi < count($spoonacula_getsechit['analyzedInstructions']); $regreciperi++) { 
			 	
			 	$count_direction_steps = count($regrecipe_instructions[$regreciperi]['steps']);
			 	$steps_srno = 0;
			 	for ($regsteps=0; $regsteps < $count_direction_steps; $regsteps++) { 
			 		$steps_srno++;
			 		$steps['srno'] = $steps_srno;
			 		$steps['step'] = $regrecipe_instructions[$regreciperi]['steps'][$regsteps]['step'];
			 		$stpsinfo[] = $steps;
			 		unset($steps);
			 	}
			 	
			 }
    	
			 // echo "<pre>"; print_r($stpsinfo);
			 // die();
    	

	if($_POST['rege_meal_type']!='')
	{
        // insert regenerate meal plan recipe to old recipe function in function(.php) Start
           $current_date=$_POST['rege_recipe_date'];
	 	   $sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set recipe_name=('$regrecipe_title'),recipe_image=('$regrecipe_image'),recipe_calories=('$regrecipe_calories'),recipe_protien=('$regrecipe_protein'),recipe_fats=('$regrecipe_fat'),recipe_carbs=('$regrecipe_carbohydrates'),recipe_id=('$regrecipe_id'),recipe_serving=('$regrecipe_servings'),original_serving=('$original_numberofservings'),source_recipe_url=('$spoonacularSourceUrl'),recipe_ingredients_value=('$regrecipe_ingredients'),direction=('$regrecipe_instructions') WHERE meal_type ='".$_POST['rege_meal_type']."' and recipe_id ='".$_POST['rege_recipe_id']."' and user_id ='".$_POST['user_id']."' and recipe_current_date='".$current_date."'";
				$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);
		// insert regenerate meal plan recipe ... End

		$get_macro_nutrients = macro_nutrients($_POST['user_id'],$current_date); 
		$cla_sum_total = round($get_macro_nutrients[0]['recipe_calories'],1) ;
		$prot_sum_total = round($get_macro_nutrients[0]['recipe_protien'],1);
		$fat_sum_total = round($get_macro_nutrients[0]['recipe_fats'],1);
		$carb_sum_total = round($get_macro_nutrients[0]['recipe_carbs'],1);	

		echo json_encode(array("status"=>"success","msg"=>"Item regerated sucessfully.", "recipe_name"=>$regrecipe_title,"recipe_image"=>$regrecipe_image,"recipe_calories"=>round($regrecipe_calories,1),"recipe_protien"=>round($regrecipe_protein,1),"recipe_fats"=>round($regrecipe_fat,1), "recipe_carbs"=>round($regrecipe_carbohydrates,1), "recipe_id"=>$regrecipe_id, "cla_sum_total"=>$cla_sum_total, "prot_sum_total"=>$prot_sum_total, "fat_sum_total"=>$fat_sum_total, "carb_sum_total"=>$carb_sum_total,"recipe_ingredients"=>"dummyhi","recipe_ingredients_lines"=>$regrecipe_ingredients,"recipe_numberofservings"=>$regrecipe_servings,"original_numberofservings"=>$original_numberofservings,"source_recipe_url"=>$spoonacularSourceUrl,"direction"=>$stpsinfo,"favmsg"=>$favmsg,"blockmsg"=>$blockmsg));
	}
   else
	{
		echo json_encode(array("status"=>"error","msg"=>"Failed."));
	}
}
// regenerate End

// Update calories etc on weight change start

if (isset($_POST['formule_user_id']) && isset($_POST['formule_current_weight']))
{
	$weight=$_POST['formule_current_weight'];
	$query_ma  = "SELECT post_id,gender,sub_plan_title FROM ".$wpdb->prefix."payments where user_id = '".$_POST['formule_user_id']."' order by payment_id DESC";
	 $plan_result_kk = $wpdb->get_row($query_ma);
	$plan_id=$plan_result_kk->post_id;
	$gender=$plan_result_kk->gender;
	$sub_plan_title=$plan_result_kk->sub_plan_title;

	$query  = "SELECT ID,post_type FROM nu_posts WHERE ID = '".$plan_id."' and post_type= 'plan'";
	$plan_result = $wpdb->get_row($query);

	$custom_args = array('post_type' => 'plan');
	$custom_query = new WP_Query( $custom_args ); 
	if ($custom_query->have_posts())
	{
		while ($custom_query->have_posts()) 
		{
			$custom_query->the_post();
			if(have_rows('sub_plan') ):
			while ( have_rows('sub_plan') ) : the_row(); 
			$myplan_id=get_the_ID();
			if($myplan_id == $plan_id && $gender=='Male') 
			{
			    $my_sub_plan_title = get_sub_field('sub_plan_title');

				if( have_rows('men') ):
				while ( have_rows('men') ) : the_row();

				$men_formula_for_calories = get_sub_field('formula_for_calories');  
				$men_formula_for_proteins = get_sub_field('formula_for_proteins'); 
				$men_formula_for_carbs = get_sub_field('formula_for_carbs');
				                     
				$cal=$weight*$men_formula_for_calories;
				$pro=$men_formula_for_proteins*$weight;
				$carb=$men_formula_for_carbs*$weight;
				$fat= ($cal-(($pro*4)+($carb*4)))/9;

				$men_calories=round($cal,2);
				$men_pro=round($pro,2);  
				$men_carb=round($carb,2);  
				$men_fat=round($fat,2);    
                
                if($my_sub_plan_title==$sub_plan_title)
                {
				    $sql_accept ="UPDATE  ".$wpdb->prefix."payments set calories='". $men_calories."',protein_gram='".$men_pro."',carb_gram='".$men_carb."',fat_gram='".$men_fat."' WHERE user_id =  '".$_POST['formule_user_id']."' and post_id='".$myplan_id."' and sub_plan_title='".$my_sub_plan_title."'";
					$row=$wpdb->get_row($sql_accept);

					$plan_history_men ="UPDATE ".$wpdb->prefix."plan_history set old_calories='".$men_calories."',old_protien='".$men_pro."',old_carbs='".$men_carb."',old_fats='".$men_fat."'  WHERE user_id =  '".$_POST['formule_user_id']."' and old_date='".date('Y/m/d')."' ";
					$row_plan_history=$wpdb->get_row($plan_history_men);

					echo json_encode(array("status"=>"success"));
                }

				endwhile;//men
				endif;//men   
			}else if($myplan_id ==$plan_id && $gender=='Female') 
			{
				$my_sub_plan_title = get_sub_field('sub_plan_title');

				if( have_rows('women') ):
				while ( have_rows('women') ) : the_row(); 

				$women_formula_for_calories = get_sub_field('women_formula_for_calories');  
                $women_formula_for_proteins = get_sub_field('women_formula_for_proteins'); 
                $women_formula_for_carbs = get_sub_field('women_formula_for_carbs');

				$cal=$weight*$women_formula_for_calories;
				$pro=$women_formula_for_proteins*$weight;
				$carb=$women_formula_for_carbs*$weight;
				$fat= ($cal-(($pro*4)+($carb*4)))/9;

				$women_calories=round($cal,2);
				$women_pro=round($pro,2);  
				$women_carb=round($carb,2);  
				$women_fat=round($fat,2); 
				if($my_sub_plan_title==$sub_plan_title)
                {                           
					$sql_accept ="UPDATE  ".$wpdb->prefix."payments set calories='". $women_calories."',protein_gram='".$women_pro."',carb_gram='".$women_carb."',fat_gram='".$women_fat."'  WHERE user_id =  '".$_POST['formule_user_id']."' and post_id='".$myplan_id."' and sub_plan_title='".$my_sub_plan_title."'";
					$row=$wpdb->get_row($sql_accept);

					$plan_history_men ="UPDATE ".$wpdb->prefix."plan_history set old_calories='".$women_calories."',old_protien='".$women_pro."',old_carbs='".$women_carb."',old_fats='".$women_fat."'  WHERE user_id =  '".$_POST['formule_user_id']."' and old_date='".date('Y/m/d')."' ";
					$row_plan_history=$wpdb->get_row($plan_history_men);

					echo json_encode(array("status"=>"success"));
			    } 
				endwhile;//women
				endif;//women   
			}
			endwhile;//sub plan
			endif;//sub plan
		}//while custom_query
	}//if custom_query
}

// Update calories etc on weight change End

// Add more search start

if (isset($_POST['addmore_recipe_name']) && isset($_POST['addmore_meal_type']))
{
	global $wpdb;

	$delete_data = "DELETE FROM ".$wpdb->prefix."addmore_recipe_info WHERE 1";
	$wpdb->get_results($delete_data);
	$recipe_name = str_replace(" ","%20",$_POST['addmore_recipe_name']);
	// $calories_rangemin = $_POST['calories_rangemin'];
	// $calories_rangemax = $_POST['calories_rangemax'];
	$returnarray = array();
	$mainarray = array();

	// filter work start

	$divdiet_array = $_POST['divdiet_array'];
	$meal_type = $_POST['addmore_meal_type'];
	$diet_count = count($divdiet_array);

	$divallergies_array = $_POST['divallergies_array'];
	$divallergies_count = count($divallergies_array);

	if(isset($_POST['calmin']) || isset($_POST['calmax'])){
	 $enerc_kcal_min = $_POST['calmin'];
	 $enerc_kcal_max = $_POST['calmax'];
	}

	 $diet_string = '';
	 $divallergies_string = '';
	 $finel_string = '';

	 if(isset($_POST['divdiet_array'])){

	 	for ($i=0; $i < $diet_count; $i++) { 
		
		$diet_string .= $divdiet_array[$i].",";

		}

		if ($diet_string) {
			
			$diet = "&diet=".rtrim($diet_string, ',');
		}

	 }

	 if(isset($_POST['divallergies_array'])){

	 	for ($j=0; $j < $divallergies_count; $j++) { 
		
		$divallergies_string .= $divallergies_array[$j].",";

		}

		if ($divallergies_string) {
			
			$intolerances = "&intolerances=".rtrim($divallergies_string, ',');
		}

	 }
	 // filter work end

	if(isset($_POST["countmoresearch"])){
		if( $_POST["countmoresearch"] == ''){
			$countmoresearch = 10;
		}else{
			$countmoresearch = $_POST["countmoresearch"];
		}
		
	}

	 	// 	$finel_string = "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=a69c41a9e555cb555a8757eb512a1d4f&q=".$meal_type."&q=".$recipe_name."".$enerc_kcal_min."".$enerc_kcal_max."".$diet_string."".$divallergies_string."";

			// $jsonapi_recipe_id = file_get_contents("http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=a69c41a9e555cb555a8757eb512a1d4f&q=".$meal_type."&q=".$recipe_name."".$enerc_kcal_min."".$enerc_kcal_max."".$diet_string."".$divallergies_string."");

			//echo "<pre>"; print_r($jsonapi_recipe_id);
	 		$type = "";
	 		if("Breakfast" == $_POST['addmore_meal_type']){ $type = "Breakfast";}
	 		if("Lunch" == $_POST['addmore_meal_type']){ $type = "main+course";}
	 		if("Dinner" == $_POST['addmore_meal_type']){ $type = "main+course";}
	 		if("Snacks" == $_POST['addmore_meal_type']){ $type = "appetizer";}

			$url = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/searchComplex?instructionsRequired=true&limitLicense=false'.$intolerances.$diet.'&maxCalories='.$enerc_kcal_max.'&minCalories='.$enerc_kcal_min.'&number='.$countmoresearch.'&offset=0&query='.$recipe_name.'&ranking=2&type='.$type.'';
			
			    $ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL, $url);
			   // curl_setopt($ch, CURLOPT_HTTPGET, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh',
			    'Accept: application/json'
			    ));

			    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0'); 
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			    $body = curl_exec($ch);
			    $info = curl_getinfo($ch);
			    $error = curl_errno($ch);
			    // print_r('Curl error: ' . curl_error($ch));

			    curl_close($ch); 

			    
			    $spoon_result_array = json_decode($body, TRUE);

			if(!empty($spoon_result_array['results'])){

				// get recipe id from meal plan table for checking it will not contain in response
				$user_id = get_current_user_id();
				$current_date=$_POST['addmore_recipe_date'];
				$query_getrecipe_id  = "SELECT recipe_id FROM nu_meal_plan WHERE user_id=".$user_id." AND meal_type='".$_POST['addmore_meal_type']."' AND recipe_current_date='".$current_date."'";
				$already_exist_result = $wpdb->get_results($query_getrecipe_id);
				$data_recipeid = array();
				foreach ($already_exist_result as $value) {
				  $data_recipeid[] = $value->recipe_id;
				}
				//echo "hi<pre>";print_r($data_recipeid);

			    $addmore_count = count($spoon_result_array['results']);

			    for ($i=0; $i < $addmore_count; $i++) { 
			    
			    	$returnarray['recipe_id'] = $spoon_result_array['results'][$i]['id'];
			    	 if(!in_array($returnarray['recipe_id'],$data_recipeid)){

			    	 		$spoonacula_getsechit = spoonacula_getsechit($returnarray['recipe_id']);

							// $spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);

							// $returnarray['recipe_ingredients_lines'] = json_encode($spoonacula_getsechit['extendedIngredients']);
							// $returnarray['recipe_numberofservings'] = $spoonacula_getsechit['servings'];
							// $returnarray['recipe_source_recipe_url'] = $spoonacula_getsechit['spoonacularSourceUrl'];
							// $addmore_response['direction'] = json_encode($spoonacula_getsechit['analyzedInstructions']);
							// $returnarray['recipe_ingredients'] = 'recipe_ingredients';



				  $spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);

				  $nutrients = $spoonacula_getsechit['nutrition']['nutrients'];

				  $countattr = count($nutrients);
				  $stringnutrition = array();
				  for($j=0;$j<$countattr;$j++)
				  {

				    if("Calories" == $spoonacula_getsechit['nutrition']['nutrients'][$j]['title'] )
				    {
				        $stringnutrition['calories'] = $spoonacula_getsechit['nutrition']['nutrients'][$j]['amount'];
				    }
				    if("Fat" == $spoonacula_getsechit['nutrition']['nutrients'][$j]['title'] )
				    {
				        $stringnutrition['fat'] = $spoonacula_getsechit['nutrition']['nutrients'][$j]['amount'];
				    }        
				    if("Carbohydrates" == $spoonacula_getsechit['nutrition']['nutrients'][$j]['title'] )
				    {
				        $stringnutrition['carbohydrates'] = $spoonacula_getsechit['nutrition']['nutrients'][$j]['amount'];
				    }
				      if("Protein" == $spoonacula_getsechit['nutrition']['nutrients'][$j]['title'] )
				    {
				        $stringnutrition['protein'] = $spoonacula_getsechit['nutrition']['nutrients'][$j]['amount'];
				    }
				  }

					        $returnarray['recipe_name'] = str_replace("'","",$spoon_result_array['results'][$i]['title']);
					    	$returnarray['recipe_image'] = $spoon_result_array['results'][$i]['image'];
					    	// $returnarray['recipe_calories'] = $spoon_result_array['results'][$i]['calories'];
					    	// $returnarray['recipe_protien'] = $spoon_result_array['results'][$i]['protein'];
					    	// $returnarray['recipe_fats'] = $spoon_result_array['results'][$i]['fat'];
					    	// $returnarray['recipe_carbs'] = $spoon_result_array['results'][$i]['carbs'];

					    	// CPFC from second hit

					    	$returnarray['recipe_calories'] = $stringnutrition['calories'];
					    	$returnarray['recipe_protien'] = $stringnutrition['protein'];
					    	$returnarray['recipe_fats'] = $stringnutrition['fat'];
					    	$returnarray['recipe_carbs'] = $stringnutrition['carbohydrates'];
					    	$stringnutrition['protein'] = "";
					    	$stringnutrition['carbohydrates'] = "";
					    	$stringnutrition['fat'] = "";
					    	$stringnutrition['calories'] = "";

					    	$mainarray[] = $returnarray;
					        unset($returnarray);

					    }
			    	
			        

			    }

			    if($countmoresearch == ''){
					$countmoresearch = 10;
				}else{
					$countmoresearch = $countmoresearch;
				}
					
		      $countresult = $spoon_result_array['totalResults'];
			    
			  echo json_encode(array("status"=>"success","search_response"=>$htmladdmore,"search_responseone"=>$mainarray,"finel_string"=>$finel_string, "countmoresearch" => ($countmoresearch + 10),"countresult"=>$countresult  ));
      		}
      		else{
      			echo json_encode(array("status"=>"error","finel_string"=>$finel_string));
      		}
}

// Add more search end


// Result add more new recipe to meal_plan table start

	if (isset($_POST['new_meal_type']) && isset($_POST['new_user_id']) && isset($_POST['new_recipe_id']) )
{
	$query_already_exist  = "SELECT user_id,meal_type,recipe_id FROM ".$wpdb->prefix."meal_plan where user_id = '".$_POST['new_user_id']."' and meal_type = '".$_POST['new_meal_type']."' and recipe_id = '".$_POST['new_recipe_id']."' and recipe_current_date = '".$_POST['new_recipe_date']."'";
	 $already_exist_result = $wpdb->query($query_already_exist);
      if($already_exist_result)
      {
        echo json_encode(array("status"=>"failed","msg"=>'Recipe already exist.'));
      }else
      {
      	//echo "addr- ".$_POST['is_single_food'];

      	$spoonacula_getsechit = "";
      	$new_recipeingrelines_obj = "";
      	$recipe_numberofservings = $_POST['new_servings'];
      	$recipe_source_recipe_url = "";
      	$ingretext = '';
      	$direction = "";
      	
      	if("yes_singlefood" != $_POST['is_single_food']){
      	$spoonacula_getsechit = spoonacula_getsechit($_POST['new_recipe_id']);

		$spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);
                
//                     echo "hello";
//                    echo "<pre>";
//                    print_r($spoonacula_getsechit) ;
//                    echo "</pre>";
//                
		$new_recipeingrelines_obj = $spoonacula_getsechit['extendedIngredients'];

		// Conversion grocery START

				  $afterconvert = array();
				  $converted = array();

				  $final_ingredientline = $new_recipeingrelines_obj;
				  
				  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {

                                    
                                      
//				    $convertvalue_unit = "";
//				    $convertvalue_amount = "";
//				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
//				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
//
//				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
//				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
//				      }
//				      else{
//				        $convertvalue_unit = $convert_value['targetunit'];
//				        $convertvalue_amount = $convert_value['targetamount'];
//				      }
//				      unset($convert_value);
//
//				        $afterconvert['id'] = $final_ingredientline[$gg]['id'];
//				  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
//				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
//				  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
//				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
//				  	$afterconvert['amount'] = $convertvalue_amount;
//				  	$afterconvert['unit'] = $convertvalue_unit;
//				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
//				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
//				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
//
//				  	$converted[$gg] = $afterconvert;
//				    unset($afterconvert);
                                      
                                    
                                      
				    $convertvalue_unit = "";
				    $convertvalue_amount = "";
//				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
//				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
//
//				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
//				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
//				      }
//				      else{
//				        $convertvalue_unit = $convert_value['targetunit'];
//				        $convertvalue_amount = $convert_value['targetamount'];
//				      }
//				      unset($convert_value);

				        $afterconvert['id'] = $final_ingredientline[$gg]['id'];
				  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
				  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
				  //	$afterconvert['amount'] = $convertvalue_amount;
				  //	$afterconvert['unit'] = $convertvalue_unit;
                                        
                                        $afterconvert['amount'] = $final_ingredientline[$gg]['amount'];
                                        $afterconvert['unit'] = $final_ingredientline[$gg]['unit'];
                                        
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

				  	$converted[$gg] = $afterconvert;
				    unset($afterconvert);
				  }

				  $new_recipeingrelines_obj = $converted;
				  unset($converted);

                                 
                                  
                                  //exit;
				  // Conversion grocery END

		$original_numberofservings = $spoonacula_getsechit['servings'];
		$recipe_numberofservings = 1;
		$recipe_source_recipe_url = $spoonacula_getsechit['spoonacularSourceUrl'];
		$direction = json_encode($spoonacula_getsechit['analyzedInstructions']);
		$ingretext = '';
		for ($ril=0; $ril < count($new_recipeingrelines_obj); $ril++) { 

		  // $ingretext .= "<span id='ingredients".$_POST['new_recipe_id']."'>";	
		  $ingretext .= "<li class='ingredientsclass'>";
		  $ingretext .= "<span>".$new_recipeingrelines_obj[$ril]['name']."</span>";
		  $ingretext .= "<span>".round($new_recipeingrelines_obj[$ril]['amount'],1)."&nbsp;&nbsp;".$new_recipeingrelines_obj[$ril]['unitLong']."</span>";
		  $ingretext .= "</li>";
		  // $ingretext .= "</span>";

		  
		}	
		$new_recipeingrelines_obj = json_encode($new_recipeingrelines_obj);
	}

	   $current_date=$_POST['new_recipe_date'];
	   $sql_new_recipe ="insert into ".$wpdb->prefix."meal_plan(user_id,meal_type,recipe_name, recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_id,source_recipe_url,direction,favorite_status,blocked_status,recipe_ingredients,recipe_ingredients_value,recipe_serving,original_serving,recipe_current_date) VALUES ('".$_POST['new_user_id']."','".$_POST['new_meal_type']."','".$_POST['new_recipe_name']."','".$_POST['new_recipe_image']."','".$_POST['new_recipe_calories']."','".str_replace("g","",$_POST['new_recipe_protien'])."','".str_replace("g","",$_POST['new_recipe_fats'])."','".str_replace("g","",$_POST['new_recipe_carbs'])."','".$_POST['new_recipe_id']."','".$recipe_source_recipe_url."','".$direction."','0','0','".$new_recipeingre_obj."','".$new_recipeingrelines_obj."','".$recipe_numberofservings."','".$original_numberofservings."','".$current_date."')";
		$row=$wpdb->query($sql_new_recipe);

		// Get inserted recipe 
		$query_data  = "SELECT direction,recipe_serving,recipe_ingredients_value,source_recipe_url FROM ".$wpdb->prefix."meal_plan where user_id = '".$_POST['new_user_id']."' and meal_type = '".$_POST['new_meal_type']."' and recipe_id = '".$_POST['new_recipe_id']."' and recipe_current_date = '".$current_date."'";
   		$data_result = $wpdb->get_row($query_data);

   		//echo "<pre>"; print_r($data_result->recipe_ingredients_value);

   		$new_recipe_extendedingredents = json_decode($data_result->recipe_ingredients_value,TRUE);
   		// echo "<pre>"; print_r($new_recipe_extendedingredents);
   		$div_extendedingredents = "";
   		$ingredents_title = "";
   		if(count($new_recipe_extendedingredents)){
   			$ingredents_title = '<p class="head-sec">Ingredients</p>';
   		}

   		for ($new_ex=0; $new_ex < count($new_recipe_extendedingredents); $new_ex++) { 

   			// echo "<br>image - ".$new_recipe_extendedingredents[$new_ex]['image'];
   			// echo "<br>name - ".$new_recipe_extendedingredents[$new_ex]['name'];
   			// echo "<br>amount - ".$new_recipe_extendedingredents[$new_ex]['amount'];
   			// echo "<br>unit - ".$new_recipe_extendedingredents[$new_ex]['unit'];

   			$div_extendedingredents .= '<div class="content-sec">
				<div class="col-md-3">
					<img src="'.$new_recipe_extendedingredents[$new_ex]['image'].'">
				</div>
				<div class="col-md-5 ingradient-one">
					<p class="ingradient-title">'.$new_recipe_extendedingredents[$new_ex]['name'].'</p>
				</div>
	            <div class="col-md-4 ingradient-two">
	                <p class="ingradient-title">'.round($new_recipe_extendedingredents[$new_ex]['amount'],1).'&nbsp;&nbsp;'.$new_recipe_extendedingredents[$new_ex]['unitLong'].'</p>
	            </div>
			</div>';
   		}

   		$new_recipe_direction = json_decode($data_result->direction,TRUE);

   		
   		$div_recipe_direction = "";
   		$direction_title = "";
   		$sr_no = 1;

   		if(count($new_recipe_direction)){
   			$direction_title = '<p class="head-sec">Directions</p>';
   		}

   		for($dj=0;$dj<count($new_recipe_direction);$dj++){
              $count_direction_steps = count($new_recipe_direction[$dj]['steps']);
              
              for ($l=0; $l < $count_direction_steps ; $l++) { 
                              $steps_data = $new_recipe_direction[$dj]['steps'];
                              // echo $sr_no."/////".$steps_data[$l]['step'];

                            $div_recipe_direction .= '
                             <div class="direction-sec col-md-12 col-sm-12">
							              <div class="col-md-1 col-sm-1 direction-no">
							              <p>'.$sr_no.'</p>
							              </div>
								            <div class="col-md-11 col-sm-11 direction-data">
								              	<p>'.$steps_data[$l]['step'].'</p>
								            </div>
							   </div>';

                              $sr_no++;
                          }

               }           
               

       $new_recipe_id = "'".$_POST['new_recipe_id']."'";
       $new_user_id = "'".$_POST['new_user_id']."'";
       $new_meal_type = "'".$_POST['new_meal_type']."'";
       $recipe_id_blkcls = "block_items_btn".$_POST['new_recipe_id'];
       $recipe_id_favcls = "favorites_items_btn".$_POST['new_recipe_id'];

       $new_recipe_date = "'".$_POST['new_recipe_date']."'";
       $new_recipe_name = "'".$_POST['new_recipe_name']."'";
       $new_recipe_image = "'".$_POST['new_recipe_image']."'";
       $new_recipe_calories = "'".round($_POST['new_recipe_calories'],1)."'";
       $new_recipe_protien = "'".round($_POST['new_recipe_protien'],1)."'";
       $new_recipe_fats = "'".round($_POST['new_recipe_fats'],1)."'";
       $new_recipe_carbs = "'".round($_POST['new_recipe_carbs'],1)."'";
       $new_recipe_numberofservings = $data_result->recipe_serving;



       if($new_recipe_calories ){ $new_recipe_calories  = $new_recipe_calories ; }else{ $new_recipe_calories = 0; }
       if($new_recipe_protien ){ $new_recipe_protien  = $new_recipe_protien ; }else{ $new_recipe_protien = 0; }
       if($new_recipe_fats ){ $new_recipe_fats  = $new_recipe_fats ; }else{ $new_recipe_fats = 0; }
       if($new_recipe_carbs ){ $new_recipe_carbs  = $new_recipe_carbs ; }else{ $new_recipe_carbs = 0; }

       // Nutrition percentage calculation start

       			$recipe_carbs_pop = str_replace("g","",$_POST['new_recipe_carbs']) * 4 ;
                $recipe_protien_pop = str_replace("g","",$_POST['new_recipe_protien']) * 4 ;
                $recipe_fats_pop = str_replace("g","",$_POST['new_recipe_fats']) * 9 ;
                $caloriesin_grams = $recipe_carbs_pop + $recipe_protien_pop + $recipe_fats_pop;

                if($recipe_carbs_pop != 0){$recipe_carbs_pop = $recipe_carbs_pop*100/$caloriesin_grams;}
                $recipe_carbs_pop = round($recipe_carbs_pop, 1);
                if($recipe_protien_pop != 0){$recipe_protien_pop = $recipe_protien_pop*100/$caloriesin_grams;}
                $recipe_protien_pop = round($recipe_protien_pop, 1);
                if($recipe_fats_pop != 0){$recipe_fats_pop = $recipe_fats_pop*100/$caloriesin_grams;}
                $recipe_fats_pop = round($recipe_fats_pop, 1);

       // Nutrition percentage calculation end
	   if($original_numberofservings){ $dis_org_ser = '<span class="pop-info">Original Recipe Size : '.$original_numberofservings.' Serving(s)</span>';}                 

       $new_recipe_addmore .='<div class="prod-detail-sect" id="'.$_POST['new_recipe_id'].'">
                              <div class="col-lg-12 nutricss">
                              <div class="bind-sect">
                              <div class="col-lg-4">
                              <div class="tooltipnew img-responsive info-sec">
                              <a class="more_info" onclick="dis_popup(this);"><i class="fa fa-info-circle" aria-hidden="true"></i>More Info</a>


                              <div style="display: none;" class="dispbodypopup">
                              	<div class="">
                              		<div class="ingradient-main-sec">
                              		<p class="receip_name">'.$_POST['new_recipe_name'].' '.$dis_org_ser.'</p>

                              		<div class="col-md-4 col-sm-4 reciepe-image">
						            <img class="nutricss" src="'.$_POST['new_recipe_image'].'">
						          	</div>

						          	<div class="col-md-3 col-sm-3 reciepe-data">
							        <p>Calories: <span id="popupcal'.$_POST['new_recipe_id'].'">'.round($_POST['new_recipe_calories'],1).'</span></p>
							        <p>Protein: <span id="popupprotien'.$_POST['new_recipe_id'].'">'.round($_POST['new_recipe_protien'],1).'g</span></p>
							        <p>Fat: <span id="popupfat'.$_POST['new_recipe_id'].'">'.round($_POST['new_recipe_fats'],1).'g</span></p>
							        <p>Carbs: <span id="popupcarbs'.$_POST['new_recipe_id'].'">'.round($_POST['new_recipe_carbs'],1).'g</span></p>
							        </div>

							        <div class="pieclass" id="piechart'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'"></div>
							        <script type="text/javascript">
						                google.charts.load("current", {"packages":["corechart"]});
						              google.charts.setOnLoadCallback(drawChart);
						              function drawChart() {
						                var recipe_carbs=60;
						                var recipe_protien=15;
						                var recipe_fats=22;
						                var data = google.visualization.arrayToDataTable([
						                  ["Task", "Hours per Day"],
						                  ["Fats", '.round($recipe_fats_pop,1).'],
						                  ["Carbs", '.round($recipe_carbs_pop,1).'],
						                  ["Protein", '.round($recipe_protien_pop,1).'],
						                  
						                ]);

						                var options = {
						                 chartArea:{left:0,top:2},
						                 colors: ["#47B650", "#28aae2", "#6538C9"]
						                };
						                var piechart = "piechart'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'";
						                var chart = new google.visualization.PieChart(document.getElementById(piechart));
						                chart.draw(data, options);
						              }

						            </script>

						            

						            <div class="col-md-12 col-sm-12 ingradients-content">
						            	'.$ingredents_title.'
						            	<div id="popingrdent'.$_POST['new_recipe_id'].'" class="col-md-12 col-sm-12 ingradients-data">
						            	'.$div_extendedingredents.'
						            	</div>
							        </div>

							        <div class="col-md-12 col-sm-12 directions">
            							'.$direction_title.'  
            							
            							'.$div_recipe_direction.'

            						</div>	



                              		</div>
                              	</div>
                              </div>



                              <img src="'.$_POST['new_recipe_image'].'" class="nutricss" />
                                <ul style="list-style-type: none;" class="tooltiptext tooltip-left">
							    <li><span>'.$_POST['new_recipe_name'].'</span></li>
							    <li><span class="white-cal">Calories:</span><span id="tooltipcal'.$_POST['new_recipe_id'].'" class="white-cal-text">'.str_replace("'","",$new_recipe_calories).'</span></li>
							    <li><span class="purple-pro">Protein:</span><span id="tooltipprotien'.$_POST['new_recipe_id'].'" class="purple-pro-text">'.str_replace("'","",$new_recipe_protien).'g</span></li>
							    <li><span class="green-fat">Fats:</span><span id="tooltipfat'.$_POST['new_recipe_id'].'" class="green-fat-text">'.str_replace("'","",$new_recipe_fats).'g</span></li>
							    <li><span class="cyan-carb">Carbs:</span><span id="tooltipcarbs'.$_POST['new_recipe_id'].'" class="cyan-carb-text">'.str_replace("'","",$new_recipe_carbs).'g</span></li>
							    <hr class="hrclass">';
		$new_recipe_addmore .= "<span id='ingredients".$_POST['new_recipe_id']."'>";
		$new_recipe_addmore .= $ingretext;
		$new_recipe_addmore .= '</span>';
		$new_recipe_addmore .= '</ul>
                              </div>
                              </div>
                              <div class="col-lg-8 meal-right">
                              <div class="servings">
							      <form class="servings" id="">
							          <div class="form-group serving-sec">
							          	<div class="red-app">
							          		<span class="recinfoforserv"> For</span>
							                <input type="text" onclick="(this.select())" onkeyup="edit_servings(this.value,'.$new_recipe_id.','.$new_meal_type.','.$new_recipe_date.');" id="servings'.$_POST['new_recipe_id'].'" name="servings'.$_POST['new_recipe_id'].'" value='.$new_recipe_numberofservings.'>
							                <label>Serving(s)</label>
							            </div>
							                
							          </div> 
							      </form>
							  </div>
                              <div class="icon-section">
                              <a href="javascript:void(0)" id="" class="food-ico" title="Remove this food"><span id="deletegif'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'" class="glyphicon glyphicon-remove" onclick="my_delete_items('.$new_recipe_id.','.$new_user_id.','.$new_meal_type.','.$new_recipe_date.')"></span>
                              	<span id="deletegif'.$_POST['new_recipe_id'].'" style="display: none;">
		                     <img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" />
		                     </span>
                              </a>

                              <a href="javascript:void(0)" id='.$recipe_id_favcls.' class="food-ico
              " title="Add to Favorites" onclick="my_favorites_items('.$new_recipe_id.','.$new_user_id.','.$new_recipe_name.','.$new_recipe_image.','.$new_recipe_calories.','.$new_recipe_protien.','.$new_recipe_fats.','.$new_recipe_carbs.','.$new_recipe_date.','.$new_meal_type.')"><span class="glyphicon glyphicon-star" id="favoritgif'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'"></span><span id="favoritgif'.$_POST['new_recipe_id'].'" style="display: none;"><img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" /></span></a>

                              <a href="javascript:void(0)" id='.$recipe_id_blkcls.' class="food-ico" title="Block this food" onclick="my_block_items('.$new_recipe_id.','.$new_user_id.','.$new_recipe_name.','.$new_recipe_image.','.$new_recipe_calories.','.$new_recipe_protien.','.$new_recipe_fats.','.$new_recipe_carbs.','.$new_recipe_date.','.$new_meal_type.')"><span id="blockgif'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'" class="glyphicon glyphicon-ban-circle"></span><span id="blockgif'.$_POST['new_recipe_id'].'" style="display: none;"><img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" /></span></a>
                     
                              <a href="javascript:void(0)" class="food-ico" title="Generate new food"><span id="regenartegif'.$_POST['new_recipe_id'].$_POST['new_meal_type'].'" class="glyphicon glyphicon-retweet" onclick="my_regenerate_items('.$new_recipe_id.','.$new_meal_type.','.$new_user_id.',\''.$_POST['new_recipe_date'].'\')"></span><span id="regenartegif'.$_POST['new_recipe_id'].'" style="display: none;">
                     <img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" />
                     </span></a>
                              </div>
                              <div class="nutricss">
                              <div class="red-app">
                              <h3>'.$_POST['new_recipe_name'].'</h3>
                               '.$dis_org_ser.'
                              </div>
                              
                                <span id="mealloader-'.$recipe_id.'" style="display: none;">
                     <img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif">
                     </span>
                               

                              <ul>
                              <li>
                              <span>Calories</span>
                              <p id="cal'.$_POST['new_recipe_id'].'">'.str_replace("'","",$new_recipe_calories) .'</p>
                              </li>
                              <li class="purple">
                              <span>P</span>
                              <p id="protien'.$_POST['new_recipe_id'].'">'.str_replace('g','',str_replace("'","",$new_recipe_protien)).'g</p>
                              </li>
                              <li class="grenn">
                              <span>F</span>
                              <p id="fat'.$_POST['new_recipe_id'].'">'.str_replace('g','',str_replace("'","",$new_recipe_fats)).'g</p>
                              </li>
                              <li class="orange">
                              <span>C</span>
                              <p id="carb'.$_POST['new_recipe_id'].'">'.str_replace('g','',str_replace("'","",$new_recipe_carbs)).'g</p>
                              </li>
                              </ul>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>';
     }
	
	  if($row)
	  {

	  	$get_macro_nutrients = macro_nutrients($_POST['new_user_id'],$_POST['new_recipe_date']); 
		$cla_sum_total = round($get_macro_nutrients[0]['recipe_calories'],1) ;
		$prot_sum_total = round($get_macro_nutrients[0]['recipe_protien'],1);
		$fat_sum_total = round($get_macro_nutrients[0]['recipe_fats'],1);
		$carb_sum_total = round($get_macro_nutrients[0]['recipe_carbs'],1);

       echo json_encode(array("status"=>"success","addmore_search_response"=>$new_recipe_addmore,"msg"=>'New recipe added sucessfully.',"cla_sum_total"=>$cla_sum_total,"prot_sum_total"=>$prot_sum_total,"fat_sum_total"=>$fat_sum_total,"carb_sum_total"=>$carb_sum_total));
	  }
	
}
// Result add more new recipe to meal_plan table end

// Nutritionix Single Food Start

if (isset($_POST['singlefood_mealtype']) && isset($_POST['snfdcalories_rangemin']) && isset($_POST['snfdcalories_rangemax']) && isset($_POST['single_recipename']) && isset($_POST['single_userid']))
{
	$single_recipename = str_replace(' ', '+', $_POST['single_recipename']);

	// if(isset($_POST["countmoresearchsingle"])){
		if( $_POST["countmoresearchsingle"] == ''){
			$countmoresearchsingle = 10;
		}else{
			$countmoresearchsingle = $_POST["countmoresearchsingle"];
		}
		
	// }

		// Intolarence for single food START
		$divallergies_single = "";
		if(isset($_POST['divallergies_array'])){

		$divallergies_count = count($_POST['divallergies_array']);
		$divallergies_array = $_POST['divallergies_array'];	

	 	for ($j=0; $j < $divallergies_count; $j++) { 
		
		$divallergies_single .= $divallergies_array[$j].",";

		}

		if ($divallergies_single) {
			
			$intolerances = "&intolerances=".rtrim($divallergies_single, ',');
		}

	 }

	$url = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/autocomplete?metaInformation=true&query='.$single_recipename.'&number='.$countmoresearchsingle.''.$intolerances.''; 

			    $ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL, $url);
			   // curl_setopt($ch, CURLOPT_HTTPGET, true);
			    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh',
			    'Accept: application/json'
			    ));

			    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0'); 
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			    $body = curl_exec($ch);
			    $info = curl_getinfo($ch);
			    $error = curl_errno($ch);
			    // print_r('Curl error: ' . curl_error($ch));

			    curl_close($ch); 

			    $addmore_response = array();
			    $final_response = array();
			    
			    $spoon_result_array = json_decode($body, TRUE);


			   // echo "sa<pre>"; print_r($spoon_result_array); die();

			    

	if(!empty($spoon_result_array)){

		      $newresult = json_decode($single_recipe_id , true);

		      //echo "<pre>"; print_r($newresult['hits']);
		      $htmladdmore='';

		      // get recipe id from meal plan table for checking it will not contain in response
				$user_id = get_current_user_id();
				$current_date=$_POST['single_recipe_date'];
				$query_getrecipe_id  = "SELECT recipe_id FROM nu_meal_plan WHERE user_id=".$user_id." AND meal_type='".$_POST['singlefood_mealtype']."' AND recipe_current_date='".$current_date."'";
				$already_exist_result = $wpdb->get_results($query_getrecipe_id);
				$data_recipeid = array();
				foreach ($already_exist_result as $value) {
				  $data_recipeid[] = $value->recipe_id;
				}

		      for ($slr=0; $slr < count($spoon_result_array); $slr++) { 
		      // single food second hit	
		      $singlefoodinfo = singlefoodinfo($spoon_result_array[$slr]['id']);
			  $singlefood_nutrition = $singlefoodinfo['nutrition'];
			    //echo "<pre>"; print_r($singlefood_nutrition['nutrients']);

			    $nutrients = $singlefood_nutrition['nutrients'];

				  $countattr = count($nutrients);
				  $stringnutrition = array();
				  for($i=0;$i<$countattr;$i++)
				  {

				    if("Calories" == $singlefood_nutrition['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['calories'] = $singlefood_nutrition['nutrients'][$i]['amount'];
				    }
				    if("Fat" == $singlefood_nutrition['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['fat'] = $singlefood_nutrition['nutrients'][$i]['amount'];
				    }        
				    if("Carbohydrates" == $singlefood_nutrition['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['Carbohydrates'] = $singlefood_nutrition['nutrients'][$i]['amount'];
				    }
				      if("Protein" == $singlefood_nutrition['nutrients'][$i]['title'] )
				    {
				        $stringnutrition['protein'] = $singlefood_nutrition['nutrients'][$i]['amount'];
				    }
				  }

				$singlefood_mealtype = "'".$_POST['singlefood_mealtype']."'";
			    $Single_food_id = "'".$spoon_result_array[$slr]['id']."'";
			    $Single_food_name = "'".$spoon_result_array[$slr]['name']."'";
			    $Single_food_image = "'"."https://spoonacular.com/cdn/ingredients_100x100/".$singlefoodinfo['image']."'";
				$singlefood_protine = "'".$stringnutrition['protein']."'";
				$singlefood_carbs = "'".$stringnutrition['Carbohydrates']."'";
				$singlefood_fats = "'".$stringnutrition['fat']."'";
				$singlefood_calories = "'".$stringnutrition['calories']."'";
				$singlefood_servings = "'"."1"."'";
				$is_single_food = "'".$_POST['is_single_food']."'";
				$templateurl = get_stylesheet_directory_uri();
				$single_recipe_date = "'".$_POST['single_recipe_date']."'";

				// Piechart calculation Start
				$recipe_carbs_pop = $stringnutrition['Carbohydrates'];
				$recipe_protien_pop = $stringnutrition['protein'];
				$recipe_fats_pop = $stringnutrition['fat'];

				if($recipe_carbs_pop != 0 || $recipe_protien_pop != 0 || $recipe_fats_pop != 0){
				$recipe_carbs_pop = $recipe_carbs_pop * 4 ;
                $recipe_protien_pop = $recipe_protien_pop * 4 ;
                $recipe_fats_pop = $recipe_fats_pop * 9 ;
                $caloriesin_grams = $recipe_carbs_pop + $recipe_protien_pop + $recipe_fats_pop;

                $recipe_carbs_pop = $recipe_carbs_pop*100/$caloriesin_grams;
                $recipe_carbs_pop = $recipe_carbs_pop;
                $recipe_protien_pop = $recipe_protien_pop*100/$caloriesin_grams;
                $recipe_protien_pop = $recipe_protien_pop;
                $recipe_fats_pop = $recipe_fats_pop*100/$caloriesin_grams;
                $recipe_fats_pop = $recipe_fats_pop;
            	}
				// Piechart calculation End
		
				// echo "<br>Single_food_id".$Single_food_id;
				// echo "<br>Single_food_name".$Single_food_name;
				// echo "<br>Single_food_image".$Single_food_image;
				// echo "<br>singlefood_protine".$singlefood_protine;
				// echo "<br>singlefood_carbs".$singlefood_carbs;
				// echo "<br>singlefood_fats".$singlefood_fats;
				// echo "<br>singlefood_calories".$singlefood_calories;
				if(!in_array($spoon_result_array[$slr]['id'],$data_recipeid)){
					if(round($stringnutrition['Carbohydrates'],1) != 0 || round($stringnutrition['protein'],1) != 0 || round($stringnutrition['fat'],1) != 0){
		           $htmladdmore .='
			                   <div class="result-set1" id="'.$spoon_result_array[$slr]['id'].'">
			                   		<a class="add_pluse" onclick="add_recipe('.$singlefood_mealtype.','.$Single_food_name.','.$Single_food_image.','.$singlefood_calories.','.$singlefood_protine.','.$singlefood_carbs.','.$singlefood_fats.','.$Single_food_id.','.$singlefood_servings.','.$is_single_food.','.$single_recipe_date.')"><i class="fa fa-plus" aria-hidden="true"></i><span id="regenartegif'.$spoon_result_array[$slr]['id'].'" style="display:none;"><img class="add_pluse_gif" src="'.$templateurl.'/images/addrecipegif.gif" /></span></a>
				                   <div class="img-sec">
				                   <img src='.$Single_food_image.' alt="" title="">
				                   </div>
				                   <h4>'.$spoon_result_array[$slr]['name'].'</h4>
				                   <p class="cal-title-sec" id="itemcal'.$spoon_result_array[$slr]['id'].'">Cal : '.round($stringnutrition['calories'],1).'</p>
				                   <div class="carb-details">
					                   <ul>
						                   <li class="purple">
						                   <span>P</span>
						                   <p id="itemprotein'.$spoon_result_array[$slr]['id'].'">'.round($stringnutrition['protein'],1).'g</p>
						                   </li>
						                   <li class="green">
						                   <span>F</span>
						                   <p id="itemfat'.$spoon_result_array[$slr]['id'].'">'.round($stringnutrition['fat'],1).'g</p>
						                   </li>
						                   <li class="blue">
						                   <span>C</span>
						                   <p id="itemcarb'.$spoon_result_array[$slr]['id'].'">'.round($stringnutrition['Carbohydrates'],1).'g</p>
						                   </li>
						                </ul>
				                   </div>

				                   <div style="display: none;" class="dispbodypopup">
				                   <div class="">
                              		<div class="ingradient-main-sec">
                              		<p class="receip_name">'.$spoon_result_array[$slr]['name'].'</p>

                              		<div class="col-md-4 col-sm-4 reciepe-image">
						            <img class="nutricss" src="https://spoonacular.com/cdn/ingredients_100x100/'.$singlefoodinfo['image'].'">
						          	</div>

						          	<div class="col-md-3 col-sm-3 reciepe-data">
							        <p>Calories: <span id="popupcal'.$spoon_result_array[$slr]['id'].'">'.$stringnutrition['calories'].'</span></p>
							        <p>Protein: <span id="popupprotien'.$spoon_result_array[$slr]['id'].'">'.$stringnutrition['protein'].'g</span></p>
							        <p>Fat: <span id="popupfat'.$spoon_result_array[$slr]['id'].'">'.$stringnutrition['fat'].'g</span></p>
							        <p>Carbs: <span id="popupcarbs'.$recipe_id.'">'.$stringnutrition['Carbohydrates'].'g</span></p>
							        </div>

							        <div class="pieclass" id="piechart'.$spoon_result_array[$slr]['id'].$_POST['singlefood_mealtype'].'"></div>
							        <script type="text/javascript">
						                google.charts.load("current", {"packages":["corechart"]});
						              google.charts.setOnLoadCallback(drawChart);
						              function drawChart() {
						                
						                var data = google.visualization.arrayToDataTable([
						                  ["Task", "Hours per Day"],
						                  ["Fats", '.round($recipe_fats_pop,1).'],
						                  ["Carbs", '.round($recipe_carbs_pop,1).'],
						                  ["Protein", '.round($recipe_protien_pop,1).'],
						                  
						                ]);

						                var options = {
						                 chartArea:{left:0,top:2},
						                 colors: ["#47B650", "#28aae2", "#6538C9"]
						                };
						                var piechart = "piechart'.$spoon_result_array[$slr]['id'].$_POST['singlefood_mealtype'].'";
						                var chart = new google.visualization.PieChart(document.getElementById(piechart));
						                chart.draw(data, options);
						              }

						            </script>

									</div>
                              	</div>
				                   </div>
				                   <a onclick="dis_popup(this);" class="Add_rec"><span><i aria-hidden="true" class="fa fa-info-circle"></i> Single food more info</span></a>
				                   </div>';
			               }
		      				}
			               } // end for loop  

				if($countmoresearchsingle == ''){
					$countmoresearchsingle = 10;
				}else{
					$countmoresearchsingle = $countmoresearchsingle;
				}

			          //$htmladdmore .= '<button type="button" onclick="nutritionix_singlefood("'.$_POST['singlefood_mealtype'].',"'.$_POST['single_recipe_date'].'"); ">Load more</button>';

               $htmladdmore .= '<input type="hidden" value="'.($countmoresearchsingle + 10).'" id="countmoresearchsingleBreakfast" >';
               $htmladdmore .= '<input type="hidden" value="'.($countmoresearchsingle + 10).'" id="countmoresearchsingleLunch" >';
               $htmladdmore .= '<input type="hidden" value="'.($countmoresearchsingle + 10).'" id="countmoresearchsingleDinner" >';
               $htmladdmore .= '<input type="hidden" value="'.($countmoresearchsingle + 10).'" id="countmoresearchsingleSnacks" >';

		      

		      echo json_encode(array("status"=>"success","msg"=>'Single Food Ok.',"snfdcalories_rangemin"=>$snfdcalories_rangemin,"snfdcalories_rangemax"=>$snfdcalories_rangemax,"single_recipename"=>$single_recipename,"search_response"=>$htmladdmore,"countresult"=>count($spoon_result_array)));

		      
				

	}else{
		echo json_encode(array("status"=>"error"));
}	

}

// Nutritionix Single Food End

// diet allarigies start

if(isset($_POST['meal_type'])){


	$divdiet_array = $_POST['divdiet_array'];
	$meal_type = $_POST['meal_type'];
	$diet_count = count($divdiet_array);

	$divallergies_array = $_POST['divallergies_array'];
	$divallergies_count = count($divallergies_array);

	 $diet_string = '';
	 $divallergies_string = '';
	 $finel_string = '';

	 $calmin = $_POST['calmin'];
	 $calmax = $_POST['calmax'];

	if (isset($_POST['divdiet_array']) && isset($_POST['divallergies_array'])) {

	 	for ($i=0; $i < $diet_count; $i++) { 
		
		$diet_string .= "&allowedDiet[]=".$divdiet_array[$i];

		}

		for ($j=0; $j < $divallergies_count; $j++) { 
		
		$divallergies_string .= "&allowedAllergy=".$divallergies_array[$j];

		}

		if(isset($_POST['calmin']) || isset($_POST['calmax'])){
				$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."&nutrition.ENERC_KCAL.max=".$calmax."&nutrition.ENERC_KCAL.min=".$calmin."".$divallergies_string."".$diet_string."";
		}
		else{

			$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."".$divallergies_string."".$diet_string."";
		}
		
	 	
	 	
	 }
	 elseif (isset($_POST['divallergies_array'])) {


	for ($j=0; $j < $divallergies_count; $j++) { 
		
		$divallergies_string .= "&allowedAllergy=".$divallergies_array[$j];

	}

	if(isset($_POST['calmin']) || isset($_POST['calmax'])){

		$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."&nutrition.ENERC_KCAL.max=".$calmax."&nutrition.ENERC_KCAL.min=".$calmin."".$divallergies_string."";
		}
		else{

				$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."".$divallergies_string."";

		}

	 	
	 }
	 elseif (isset($_POST['divdiet_array'])) {

	 	for ($i=0; $i < $diet_count; $i++) { 
		
		$diet_string .= "&allowedDiet[]=".$divdiet_array[$i];

		}


	if(isset($_POST['calmin']) || isset($_POST['calmax'])){
	 	$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."&nutrition.ENERC_KCAL.max=".$calmax."&nutrition.ENERC_KCAL.min=".$calmin."".$diet_string."";
	 }
	 else{
	 		$finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type."".$diet_string."";

	 }

	 }
	 else{ $finel_string .= $finel_string .= "http://api.yummly.com/v1/api/recipes?_app_id=e90a1582&_app_key=daf4c05cda5471adf8ee4699ebe9968e&q=".$meal_type.""; }

	echo json_encode(array("status"=>"success","finel_string"=>$finel_string,"divdiet_array"=>$divdiet_array,"divallergies_array"=>$divallergies_array));

	

}

//diet allergies end

// planning calender history start
if (isset($_POST['history_user_id']) && isset($_POST['history_plan_date']))
{
	global $wpdb;
	$history_plan_date=date("Y/m/d",strtotime($_POST['history_plan_date']));

	$plan_payment  = "SELECT calories,carb_gram,protein_gram,fat_gram FROM ".$wpdb->prefix."payments where user_id = '".$_POST['history_user_id']."' order by payment_id DESC";
	$row_plan_payment = $wpdb->get_row($plan_payment,ARRAY_A);

	//print_r($row_plan_payment); 

	$update_planhistory="UPDATE " . $wpdb->prefix ."plan_history set old_calories='".$row_plan_payment['calories']."', old_protien='".$row_plan_payment['protein_gram']."', old_fats='".$row_plan_payment['fat_gram']."', old_carbs='".$row_plan_payment['carb_gram']."' where user_id='".$_POST['history_user_id']."' and  old_date='".$history_plan_date."'";

	$update_planhistoryresult = $wpdb->get_row($update_planhistory);

	$plan_history= "select * from ".$wpdb->prefix."meal_plan, ".$wpdb->prefix."plan_history where  ".$wpdb->prefix."meal_plan.user_id='".$_POST['history_user_id']."' and ".$wpdb->prefix."meal_plan.recipe_current_date ='".$history_plan_date."' and ".$wpdb->prefix."plan_history.user_id='".$_POST['history_user_id']."' and ".$wpdb->prefix."plan_history.old_date ='".$history_plan_date."'";

	$query1 = "select * from " . $wpdb->prefix . "payments where user_id=".$_POST['history_user_id']; 
$row1=$wpdb->get_row($query1 . " order by payment_id DESC");
	
      $get_macro_nutrients = macro_nutrients($_POST['history_user_id'],$history_plan_date); 
		$recipe_caloriesnew = round($get_macro_nutrients[0]['recipe_calories'],1) ;
		$recipe_protiennew = round($get_macro_nutrients[0]['recipe_protien'],1);
		$recipe_fatsnew = round($get_macro_nutrients[0]['recipe_fats'],1);
		$recipe_carbsnew = round($get_macro_nutrients[0]['recipe_carbs'],1);
	
           $before_caloriess = $row1->calories;    
           $before_carbs = $row1->carb_gram;    
           $before_proteins = $row1->protein_gram;    
           $before_fats = $row1->fat_gram;     
         
           
           $before_calories_half = $before_caloriess/2;   
           $before_protien_half    = $before_proteins/2;   
           $before_fat_half    = $before_fats/2; 
           $before_carb_half    = $before_carbs/2;    

	// $plan_history= "select * from nu_meal_plan where  nu_meal_plan.user_id='".$_POST['history_user_id']."'  and nu_meal_plan.recipe_current_date ='".$history_plan_date."'";

           
  
	// For allergies start

	$allergies_list_query = "SELECT meta_value FROM ".$wpdb->prefix."usermeta WHERE meta_key = 'allergiesone' AND user_id='".$_POST['history_user_id']."'";
    $allergies_list_query_result = $wpdb->get_results($allergies_list_query, 'ARRAY_A');
    $alrgy = unserialize($allergies_list_query_result[0]['meta_value']);
    $alrgy_count = count($alrgy);
    $allergyselect = '[';
    for ($i=0; $i < $alrgy_count ; $i++) { 

    	$allergyselect .= "'".$alrgy[$i]."',";
    }
    $allergyselect .= ']';
   
	// For allergies end
    $baseimgurl = get_stylesheet_directory_uri();

	$exist_result = $wpdb->get_row($plan_history);

	$plan_history_row = $wpdb->get_results($plan_history);

	$template_url = get_stylesheet_directory_uri();

	$sum_achieve_calories = 0;
	$sum_achieve_protien = 0;
	$sum_achieve_fats = 0;
	$sum_achieve_carbs = 0;
	$htmlplan_history='';

	$meal_plan_html .= '<ul class="nav nav-pills">
		      <li class="active"><a href="#Breakfast" data-toggle="pill">Breakfast</a></li>
		      <li class=""><a href="#Lunch" data-toggle="pill">Lunch</a></li>
		      <li class=""><a href="#Dinner" data-toggle="pill">Dinner</a></li>
		      <li class=""><a href="#Snacks" data-toggle="pill">Snacks</a></li>
		  </ul>';

	$meal_plan_html .= '<div class="col-lg-12">';
    $meal_plan_html .= '<div class="tab-content">';	  

    $baseurl = get_stylesheet_directory_uri();

	$plans = array("Breakfast","Lunch","Dinner","Snacks");
	foreach($plans as $define_meal_type)
	{
		//echo $meal_type;
		$inactive_class = '';
		if('Breakfast' == $define_meal_type){$inactive_class = 'in active';}
		//$meal_plan_html .='<h3 class="break-title">'.$define_meal_type.'</h3>';
		$meal_plan_html .='<div class="tab-pane fade '.$inactive_class.'" id="'.$define_meal_type.'">';
		$meal_plan_html .= '<div id=""><br>';
		foreach ($plan_history_row as $plan_history_data) 
		{
			$dynamic_meal_type = $plan_history_data->meal_type;
			if($define_meal_type==$dynamic_meal_type)
			{	
				$recipe_ingredients_value = json_decode($plan_history_data->recipe_ingredients_value,TRUE);

                                
                                
				$recipe_direction = json_decode($plan_history_data->direction,TRUE);
				//echo'<pre>'; print_r($recipe_ingredients_value);
				$userid = $plan_history_data->user_id;
				$recipe_id = $plan_history_data->recipe_id;
				$recipe_meal_type = $plan_history_data->meal_type;
				$recipe_name = $plan_history_data->recipe_name;
				$recipe_image = $plan_history_data->recipe_image;
				$recipe_calories = $plan_history_data->recipe_calories;
				$recipe_protien = $plan_history_data->recipe_protien;
				$recipe_fats = $plan_history_data->recipe_fats;
				$recipe_carbs = $plan_history_data->recipe_carbs;
				$recipe_serving = $plan_history_data->recipe_serving;
				$original_numberofservings = $plan_history_data->original_serving;
				$is_favourite = $plan_history_data->favorite_status;
				$is_block = $plan_history_data->blocked_status;
				$div_extendedingredents = '';
				$div_recipe_direction = '';
				$ingretext = '';
				$regenerate_items = '';

				// Piechart calculation Start
				if($recipe_carbs != 0 || $recipe_protien != 0 || $recipe_fats != 0){
				$recipe_carbs_pop = $recipe_carbs * 4 ;
                $recipe_protien_pop = $recipe_protien * 4 ;
                $recipe_fats_pop = $recipe_fats * 9 ;
                $caloriesin_grams = $recipe_carbs_pop + $recipe_protien_pop + $recipe_fats_pop;

                $recipe_carbs_pop = $recipe_carbs_pop*100/$caloriesin_grams;
                $recipe_carbs_pop = round($recipe_carbs_pop, 1);
                $recipe_protien_pop = $recipe_protien_pop*100/$caloriesin_grams;
                $recipe_protien_pop = round($recipe_protien_pop, 1);
                $recipe_fats_pop = $recipe_fats_pop*100/$caloriesin_grams;
                $recipe_fats_pop = round($recipe_fats_pop, 1);
            }
            else{
            	$recipe_carbs_pop = 0;
            	$recipe_protien_pop = 0;
            	$recipe_fats_pop = 0;

            }
				// Piechart calculation End

				// Set class favourite and block items START
                if($is_favourite) { $is_favourite = 'favrecip'; }
                if($is_block) { $is_block = 'blkrecip'; }
				// Set class favourite and block items END

				if($history_plan_date >= date('Y/m/d')){
				$regenerate_items = '<a href="javascript:void(0)" class="food-ico" title="Generate new food"><span id="regenartegif'.$recipe_id.$recipe_meal_type.'" class="glyphicon glyphicon-retweet" onclick="my_regenerate_items(\''.$recipe_id.'\',\''.$recipe_meal_type.'\',\''.$userid.'\',\''.$history_plan_date.'\')"></span><span id="regenartegif'.$recipe_id.'" style="display: none;">
                <img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" /></span></a>';
            	}

            	$ingredents_title = "";
            	if($recipe_ingredients_value){
            		$ingredents_title = '<p class="head-sec">Ingredients</p>';

				for ($ril=0; $ril < count($recipe_ingredients_value); $ril++) { 
                                    
                                

				  // $ingretext .= "<span id='ingredients".$_POST['new_recipe_id']."'>";	
				  $ingretext .= "<li class='ingredientsclass'>";
				  $ingretext .= "<span>".$recipe_ingredients_value[$ril]['name']."</span>";
				 // $ingretext .= "<span>".round($recipe_ingredients_value[$ril]['amount'],2)."&nbsp;&nbsp;".$recipe_ingredients_value[$ril]['unit']."</span>";
                                 $ingretext .= "<span>".round($recipe_ingredients_value[$ril]['amount'],2)."&nbsp;&nbsp;".$recipe_ingredients_value[$ril]['unit']."</span>";
				  $ingretext .= "</li>";
				  // $ingretext .= "</span>";

				  $div_extendedingredents .= '<div class="content-sec">
						<div class="col-md-3">
							<img src="'.$recipe_ingredients_value[$ril]['image'].'">
						</div>
						<div class="col-md-5 ingradient-one">
							<p class="ingradient-title">'.$recipe_ingredients_value[$ril]['name'].'</p>
						</div>
			            <div class="col-md-4 ingradient-two">
			                <p class="ingradient-title">'.round($recipe_ingredients_value[$ril]['amount'],1).'&nbsp;&nbsp;'.$recipe_ingredients_value[$ril]['unit'].'</p>
			            </div>
						</div>';
				}
			}
				$sr_no = 1;
				$direction_title = "";
				if($recipe_direction){
					$direction_title = '<p class="head-sec">Directions</p>';

   				for($dj=0;$dj<count($recipe_direction);$dj++){
              			$count_direction_steps = count($recipe_direction[$dj]['steps']);
              			for ($l=0; $l < $count_direction_steps ; $l++) { 
                              $steps_data = $recipe_direction[$dj]['steps'];
                              // echo $sr_no."/////".$steps_data[$l]['step'];

                            $div_recipe_direction .= '
                             <div class="direction-sec col-md-12 col-sm-12">
							              <div class="col-md-1 col-sm-1 direction-no">
							              <p>'.$sr_no.'</p>
							              </div>
								            <div class="col-md-11 col-sm-11 direction-data">
								              	<p>'.$steps_data[$l]['step'].'</p>
								            </div>
							   </div>';

                              $sr_no++;
                        }

               }
           }
				$sum_achieve_calories+= $plan_history_data->recipe_calories;
				$achieve_calories = $sum_achieve_calories;
				$target_calories= $plan_history_data->old_calories;		     
				$sum_achieve_protien+= $plan_history_data->recipe_protien;
				$achieve_protien= $sum_achieve_protien;
				$target_protien= $plan_history_data->old_protien;
				$sum_achieve_fats+= $plan_history_data->recipe_fats;
				$achieve_fats = $sum_achieve_fats;
				$target_fats = $plan_history_data->old_fats;
				$sum_achieve_carbs+= $plan_history_data->recipe_carbs;
				$achieve_carbs = $sum_achieve_carbs;
				$target_carbs= $plan_history_data->old_carbs;
				$dis_org_serv ="";
				if($original_numberofservings){ $dis_org_serv = "<span class='pop-info'>Original Recipe Size : ".$original_numberofservings." Serving(s)</span>";}

				$meal_plan_html .='<div class="prod-detail-sect" id="'.$recipe_id.'">
                              <div class="col-lg-12 nutricss">
                              <div class="bind-sect">
                              <div class="col-lg-4">
                              <div class="tooltipnew img-responsive info-sec">
                              <a class="more_info" onclick="dis_popup(this);"><i class="fa fa-info-circle" aria-hidden="true"></i>More Info</a>


                              <div style="display: none;" class="dispbodypopup">
                              	<div class="">
                              		<div class="ingradient-main-sec">
                              		<p class="receip_name">'.$recipe_name.''.$dis_org_serv.'</p>

                              		<div class="col-md-4 col-sm-4 reciepe-image">
						            <img class="nutricss" src="'.$recipe_image.'">
						          	</div>

						          	<div class="col-md-3 col-sm-3 reciepe-data">
							        <p>Calories: <span id="popupcal'.$recipe_id.'">'.round($recipe_calories,1).'</span></p>
							        <p>Protein: <span id="popupprotien'.$recipe_id.'">'.round($recipe_protien,1).'g</span></p>
							        <p>Fat: <span id="popupfat'.$recipe_id.'">'.round($recipe_fats,1).'g</span></p>
							        <p>Carbs: <span id="popupcarbs'.$recipe_id.'">'.round($recipe_carbs,1).'g</span></p>
							        </div>

							        <div class="pieclass" id="piechart'.$recipe_id.$recipe_meal_type.'"></div>
							        <script type="text/javascript">
						                google.charts.load("current", {"packages":["corechart"]});
						              google.charts.setOnLoadCallback(drawChart);
						              function drawChart() {
						                var recipe_carbs='.round($recipe_carbs_pop,1).';
						                var recipe_protien='.round($recipe_protien_pop,1).';
						                var recipe_fats='.round($recipe_fats_pop,1).';
						                var data = google.visualization.arrayToDataTable([
						                  ["Task", "Hours per Day"],
						                  ["Fats", recipe_fats],
						                  ["Carbs", recipe_carbs],
						                  ["Protein", recipe_protien],
						                  
						                ]);

						                var options = {
						                 chartArea:{left:0,top:2},
						                 colors: ["#47B650", "#28aae2", "#6538C9"]
						                };
						                var piechart = "piechart'.$recipe_id.$recipe_meal_type.'";
						                var chart = new google.visualization.PieChart(document.getElementById(piechart));
						                chart.draw(data, options);
						              }

						            </script>

						            

						            <div class="col-md-12 col-sm-12 ingradients-content">
						            	'.$ingredents_title.'
						            	<div id="popingrdent'.$recipe_id.'" class="col-md-12 col-sm-12 ingradients-data">
						            	'.$div_extendedingredents.'
						            	</div>
							        </div>

							        <div class="col-md-12 col-sm-12 directions">
            							'.$direction_title.'  
            							
            							'.$div_recipe_direction.'

            						</div>	



                              		</div>
                              	</div>
                              </div>



                              <img src="'.$recipe_image.'" class="nutricss" />
                                <ul style="list-style-type: none;" class="tooltiptext tooltip-left">
							    <li><span>'.$recipe_name.'</span></li>
							    <li><span class="white-cal">Calories:</span><span id="tooltipcal'.$recipe_id.'" class="white-cal-text">'.round(str_replace("'","",$recipe_calories),1).'</span></li>
							    <li><span class="purple-pro">Protein:</span><span id="tooltipprotien'.$recipe_id.'" class="purple-pro-text">'.round(str_replace("'","",$recipe_protien),1).'g</span></li>
							    <li><span class="green-fat">Fats:</span><span id="tooltipfat'.$recipe_id.'" class="green-fat-text">'.round(str_replace("'","",$recipe_fats),1).'g</span></li>
							    <li><span class="cyan-carb">Carbs:</span><span id="tooltipcarbs'.$recipe_id.'" class="cyan-carb-text">'.round(str_replace("'","",$recipe_carbs),1).'g</span></li>
							    <hr class="hrclass">';
		$meal_plan_html .= "<span id='ingredients".$recipe_id."'>";
		$meal_plan_html .= $ingretext;
		$meal_plan_html .= '</span>';
		$meal_plan_html .= '</ul>
                              </div>
                              </div>
                              <div class="col-lg-8 meal-right">
                              <div class="servings">
							      <form class="servings" id="">
							          <div class="form-group serving-sec">
							          	<div class="red-app">
							          		<span class="forserv"> For</span>
							                <input type="text" onclick="(this.select())" onkeyup="edit_servings(this.value,\''.$recipe_id.'\',\''.$recipe_meal_type.'\',\''.$history_plan_date.'\');" id="servings'.$recipe_id.'" name="servings'.$recipe_id.'" value='.$recipe_serving.'>
							                <label>Serving(s)</label>
							            </div>
							                
							          </div> 
							      </form>
							  </div>
                              <div class="icon-section">
                              <a href="javascript:void(0)" id="" class="food-ico" title="Remove this food"><span id="deletegif'.$recipe_id.$recipe_meal_type.'" class="glyphicon glyphicon-remove" onclick="my_delete_items(\''.$recipe_id.'\',\''.$userid.'\',\''.$recipe_meal_type.'\',\''.$history_plan_date.'\')"></span><span id="deletegif'.$recipe_id.'" style="display: none;"><img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" />
			                     </span>
                              	 </a>

                              <a href="javascript:void(0)" id=favorites_items_btn'.$recipe_id.' class="food-ico '.$is_favourite.'
              " title="Add to Favorites" onclick="my_favorites_items(\''.$recipe_id.'\',\''.$userid.'\',\''.$recipe_name.'\',\''.$recipe_image.'\',\''.$recipe_calories.'\',\''.$recipe_protien.'\',\''.$recipe_fats.'\',\''.$recipe_carbs.'\',\''.$history_plan_date.'\',\''.$recipe_meal_type.'\')"><span id="favoritgif'.$recipe_id.$recipe_meal_type.'" class="glyphicon glyphicon-star"></span><span id="favoritgif'.$recipe_id.'" style="display: none;"><img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" /></span></a>

                              <a href="javascript:void(0)" id=block_items_btn'.$recipe_id.' class="food-ico '.$is_block.'" title="Block this food" onclick="my_block_items(\''.$recipe_id.'\',\''.$userid.'\',\''.$recipe_name.'\',\''.$recipe_image.'\',\''.$recipe_calories.'\',\''.$recipe_protien.'\',\''.$recipe_fats.'\',\''.$recipe_carbs.'\',\''.$history_plan_date.'\',\''.$recipe_meal_type.'\')"><span id="blockgif'.$recipe_id.$recipe_meal_type.'" class="glyphicon glyphicon-ban-circle"></span><span id="blockgif'.$recipe_id.'" style="display: none;"><img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif" /></span></a>';
                
                $meal_plan_html .=$regenerate_items;
                              
                $meal_plan_html .='</div>
                              <div class="nutricss">
                              <div class="red-app">
                              <h3>'.$recipe_name.'</h3>
                              '.$dis_org_serv.'
                              </div>
                                <span id="mealloader-'.$recipe_id.'" style="display: none;">
                     <img class="gifregenerate" src="'.get_stylesheet_directory_uri().'/images/regeneatelod.gif">
                     </span>
                              <ul>
                              <li>
                              <span>Calories</span>
                              <p id="cal'.$recipe_id.'">'.round(str_replace("'","",$recipe_calories),1) .'</p>
                              </li>
                              <li class="purple">
                              <span>P</span>
                              <p id="protien'.$recipe_id.'">'.round(str_replace('g','',str_replace("'","",$recipe_protien)),1).'g</p>
                              </li>
                              <li class="grenn">
                              <span>F</span>
                              <p id="fat'.$recipe_id.'">'.round(str_replace('g','',str_replace("'","",$recipe_fats)),1).'g</p>
                              </li>
                              <li class="orange">
                              <span>C</span>
                              <p id="carb'.$recipe_id.'">'.round(str_replace('g','',str_replace("'","",$recipe_carbs)),1).'g</p>
                              </li>
                              </ul>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>';
			}//define_meal_type
		}//plan_history_row
		$meal_plan_html .= '<div id="addmore_search_response'.$define_meal_type.'"> </div>';
		$meal_plan_html .= '<div class="col-lg-12 collaspsed-container">
  <a type="button" onclick="addnicescroll(\''.$define_meal_type.'\');" class="addmore" data-toggle="collapse" data-target="#addmore_'.$define_meal_type.'"><span data-toggle="modal" data-target="#Mealplanpopup" class="glyphicon glyphicon-plus addicon"></span> <h2>Add Food</h2></a>
<div id="addmore_'.$define_meal_type.'" class="collapse colspan-show-hide">
    <div class="search-filter">
      <form method="post" id="form_add_more" class="form_add_more form_add_more'.$define_meal_type.'">
          <div class="up-sec">
              
              <div class="form-group search-sec">
                <input type="text"  name="recipe_name'.$define_meal_type.'" id="recipe_name'.$define_meal_type.'"/>
                <input type="hidden"  name="recipe_name2'.$define_meal_type.'" id="recipe_name2'.$define_meal_type.'"/>
                <a href="javascript:void(0)"  onclick="addmoresearch(\''.$define_meal_type.'\',\''.$history_plan_date.'\')" id="enter'.$define_meal_type.'">
            <i class="fa fa-search" aria-hidden="true"></i>
            </a>
              </div>
              <div class="error_text_recipe" id="recipe_name_empty'.$define_meal_type.'"></div>

             <h3 class="cal-title">Calorie Range :</h3> 
             <div class="form-group range-slider-sec">
             <input type="text" id="calories_range'.$define_meal_type.'" readonly style="border:0; color:#f6931f; font-weight:bold;">

             <div id="slider-range'.$define_meal_type.'" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
             	<div style="left: 35.227%; width: 64.773%;" class="ui-slider-range ui-corner-all ui-widget-header"></div>
				<span style="left: 35.227%;" tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
				<span style="left: 100%;" tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
             </div>
             </div>

             <div class="form-group calories-butt-sec">
             <input type="text" id="inputrecipebtn'.$define_meal_type.'" value="inputrecipe" style="display: none;" />
             <a class="recipe fill_inputrecipebtn" onclick="addmoresearch(\''.$define_meal_type.'\',\''.$history_plan_date.'\')" id="recipebtn'.$define_meal_type.'" href="javascript:void(0)">Recipe</a>
             <input type="text" id="inputsinglefoodbtn'.$define_meal_type.'" style="display: none;" />
             <a class="sing-food" onclick="nutritionix_singlefood(\''.$define_meal_type.'\',\''.$history_plan_date.'\')" id="singlefoodbtn'.$define_meal_type.'" href="javascript:void(0)">Single Food </a>
             </div>
          </div>        
          <script type="text/javascript" src="'.$baseurl.'/js/jquery.multiselect.js"></script>
     <div class="search-results-sec" id="result" data-title="result-set-sec">
        <div class="form-group rest-allergies-sec" id="divdiet'.$define_meal_type.'" >
              <select id="dietrestrictions'.$define_meal_type.'" multiple="multiple" name="dietrestrictions'.$define_meal_type.'[]">
              <option value="lacto vegetarian">Lacto vegetarian</option>
              <option value="ovo vegetarian">Ovo vegetarian</option>
              <option value="pescetarian">Pescetarian</option>
              <option value="vegan">Vegan</option>
              <option value="vegetarian">Vegetarian</option>
              
              </select>
        </div>
        
        <div class="form-group rest-allergies-sec rest-allergies-sec2" id="divallergies'.$define_meal_type.'">

              <select id="mealallergies'.$define_meal_type.'" multiple="multiple" name="mealallergies'.$define_meal_type.'[]">

              <option value="soy">Soy</option>
              <option value="tree nut">Tree Nuts</option>

              <option value="egg">Egg</option>

              <option value="wheat">Wheat</option>

              <option value="gluten">Gluten</option>

              <option value="peanut">Peanut</option>

              <option value="seafood">Seafood</option>

              <option value="sesame">Sesame</option>

              <option value="dairy">Dairy</option>

              

              </select>

        </div> 
        <script>
        jQuery("#mealallergiesBreakfast").val('.$allergyselect.');
        jQuery("#mealallergiesLunch").val('.$allergyselect.');
        jQuery("#mealallergiesDinner").val('.$allergyselect.');
        jQuery("#mealallergiesSnacks").val('.$allergyselect.');
        	jQuery("#dietrestrictionsBreakfast").multiselect({
            columns: 2,
            search: false,
            selectAll: false,
            placeholder: "Diet Restrictions"
        });
        jQuery("#dietrestrictionsLunch").multiselect({
            columns: 2,
            search: false,
            selectAll: false,
            placeholder: "Diet Restrictions"
        });
        jQuery("#dietrestrictionsDinner").multiselect({
            columns: 2,
            search: false,
            selectAll: false,
            placeholder: "Diet Restrictions"
        });
        jQuery("#dietrestrictionsSnacks").multiselect({
            columns: 2,
            search: false,
            selectAll: false,
            placeholder: "Diet Restrictions"
        });
        jQuery("#mealallergiesBreakfast").multiselect({
          columns: 2,
          search: false,
          selectAll: false,
          placeholder: "Allergies"
      });
      jQuery("#mealallergiesLunch").multiselect({
          columns: 2,
          search: false,
          selectAll: false,
          placeholder: "Allergies"
      });
      jQuery("#mealallergiesDinner").multiselect({
          columns: 2,
          search: false,
          selectAll: false,
          placeholder: "Allergies"
      });
      jQuery("#mealallergiesSnacks").multiselect({
          columns: 2,
          search: false,
          selectAll: false,
          placeholder: "Allergies"
      });
        </script>
      <div id="loadingmessage'.$define_meal_type.'" align="center" style="display:none;width:100%;float:left;text-align: center;"><img src="'.$template_url.'/images/loading.gif" height="60" width="60"/></div>
        <!-- search response start-->
        <div class="form-group result-set-sec" id="search_response'.$define_meal_type.'">      
         
        </div> 
        <!-- <div class="form-group result-set-sec" id="search_responseone<?php echo $meal_type;?>">      
         
        </div>  -->
        <!-- search response end-->

        </div>   
      </form>
    </div>
  
  </div>
</div>';
		$meal_plan_html .= '</div>';
		$meal_plan_html .= '</div>'; // End particular tab info 		
	}//plans
	$meal_plan_html .= '</div>';
	$meal_plan_html .= '</div>';
	                   $calories_cls ='';
	                   $protien_cls ='';
	                   $fats_cls ='';
	                   $carbs_cls ='';

	                   if($achieve_calories > $target_calories){$calories_cls ='opacity:1"';} 
                                                             else { $calories_cls ='style="opacity:1"';}
                        
                       if($achieve_protien > $target_protien){$protien_cls ='style="background: red;opacity:1"';} 
                                                             else {$protien_cls ='style="opacity:1"';}
                        
                       if($achieve_fats > $target_fats){$fats_cls ='style="background: red;opacity:1"';} 
                                                             else{$fats_cls ='style="opacity:1"';}

                       if($achieve_carbs > $target_carbs){$carbs_cls ='style="opacity:1"';} 
                                                             else{$carbs_cls ='style="opacity:1"';}
                         
	$html_plan_history .='
	                      <script>
                              console.log("helllll");	                      	

	                      	$("#slider-rangeBreakfast").slider({ 
						 		range: true,
						 		min :1,     
                                max : '.$target_calories.',
                                values: [ 1, '.$target_calories.'],
                                slide: function( event, ui ) {
							        $( "#calories_rangeBreakfast" ).val(  ui.values[ 0 ]+ "g - " + ui.values[ 1 ] + "g"  );
							    }});
							    $( "#calories_rangeBreakfast" ).val( $( "#slider-rangeBreakfast" ).slider( "values", 0 )+ "g - " + $( "#slider-rangeBreakfast" ).slider( "values", 1 ) + "g" );

						$("#slider-rangeLunch").slider({ 
						 		range: true,
						 		min :1,     
                                max : '.$target_calories.',
                                values: [ 1, '.$target_calories.'],
                                slide: function( event, ui ) {
							        $( "#calories_rangeLunch" ).val(  ui.values[ 0 ]+ "g - " + ui.values[ 1 ] + "g"  );
							    }});
							    $( "#calories_rangeLunch" ).val( $( "#slider-rangeLunch" ).slider( "values", 0 )+ "g - " + $( "#slider-rangeLunch" ).slider( "values", 1 ) + "g" );	  

						$("#slider-rangeDinner").slider({ 
						 		range: true,
						 		min :1,     
                                max : '.$target_calories.',
                                values: [ 1, '.$target_calories.'],
                                slide: function( event, ui ) {
							        $( "#calories_rangeDinner" ).val(  ui.values[ 0 ]+ "g - " + ui.values[ 1 ] + "g"  );
							    }});
							    $( "#calories_rangeDinner" ).val( $( "#slider-rangeDinner" ).slider( "values", 0 )+ "g - " + $( "#slider-rangeDinner" ).slider( "values", 1 ) + "g" );		
                        
                        $("#slider-rangeSnacks").slider({ 
						 		range: true,
						 		min :1,     
                                max : '.$target_calories.',
                                values: [ 1, '.$target_calories.'],
                                slide: function( event, ui ) {
							        $( "#calories_rangeSnacks" ).val(  ui.values[ 0 ]+ "g - " + ui.values[ 1 ] + "g"  );
							    }});
							    $( "#calories_rangeSnacks" ).val( $( "#slider-rangeSnacks" ).slider( "values", 0 )+ "g - " + $( "#slider-rangeSnacks" ).slider( "values", 1 ) + "g" );
						  
						  

	                      $("#ex6caloriesok").slider({   
                                  min: 0,
                                 max : '.$target_calories.',
                                 value: '.$achieve_calories.',
                                     range: "min",
                                 });
                                $( "#ex6caloriesok" ).slider( "disable" ); 
                          </script>
                          <script>$("#ex6proteinsok").slider({  
                                 min: 0,
                                 max : '.$target_protien.',
                                 value: '.$achieve_protien.',
                                      range: "min",
                                 });
                                $( "#ex6proteinsok" ).slider( "disable" ); 
                          </script>  
                          <script>$("#ex6fatsok").slider({
                                 min: 0,
                                 max : '.$target_fats.',
                                 value: '.$achieve_fats.',
                                      range: "min",
                                 });
                                $( "#ex6fatsok" ).slider( "disable" ); 
                          </script>
                          <script>$("#ex6carbsok").slider({ 
                                   min: 0,
                                 max : '.$target_carbs.',
                                 value: '.$achieve_carbs.',
                                      range: "min",
                                 });
                                $( "#ex6carbsok" ).slider( "disable" ); 
                                
                                  
                           if(('.$recipe_caloriesnew.' < '.$before_calories_half.')  ){
                               
                            

                jQuery("#ex6caloriesok .ui-slider-range-min").css("background","rgb(101, 56, 201)");
                

            }else if(('.$recipe_caloriesnew.' > '.$before_calories_half.') && ('.$recipe_caloriesnew.' >=  '.$before_caloriess.') ){
              
                    
                jQuery("#ex6caloriesok .ui-slider-range-min").css("background","red");

            }
            else if(('.$recipe_caloriesnew.' >  '.$before_calories_half.') ){
                
                jQuery("#ex6caloriesok .ui-slider-range-min").css("background","#5aab05");
            }

             if(('.$recipe_protiennew.' < '.$before_protien_half.') ){

                jQuery("#ex6proteinsok .ui-slider-range-min").css("background","rgb(101, 56, 201)");

            }
            else if(('.$recipe_protiennew.' >  '.$before_protien_half.') && ('.$recipe_protiennew.' >=  '.$before_proteins.') ){

                jQuery("#ex6proteinsok .ui-slider-range-min").css("background","red");

            }else if(('.$recipe_protiennew.' >= '.$before_protien_half.') ){

                jQuery("#ex6proteinsok .ui-slider-range-min").css("background","#5aab05");

            }
           

            if(('.$recipe_fatsnew.' < '.$before_fat_half.') ){


                jQuery("#ex6fatsok .ui-slider-range-min").css("background","rgb(101, 56, 201)");
            }
            else if(('.$recipe_fatsnew.' >  '.$before_fat_half.') && ('.$recipe_fatsnew.' >=  '.$before_fats.')  ){
                 
                jQuery("#ex6fatsok .ui-slider-range-min").css("background","red");
            }else if(('.$recipe_fatsnew.' >= '.$before_fat_half.') ){
              
                jQuery("#ex6fatsok .ui-slider-range-min").css("background","#5aab05");
            }

           
            if(('.$recipe_carbsnew.' < '.$before_carb_half.') ){

                jQuery("#ex6carbsok .ui-slider-range-min").css("background","rgb(101, 56, 201)");

            }
            else if(('.$recipe_carbsnew.' >  '.$before_carb_half.') && ('.$recipe_carbsnew.' >=  '.$before_carbs.') ){

                jQuery("#ex6carbsok .ui-slider-range-min").css("background","red");

            }else if(('.$recipe_carbsnew.' >= '.$before_carb_half.' )){

                jQuery("#ex6carbsok .ui-slider-range-min").css("background","#5aab05");

            }


                          </script>
	                      <div class="micro-meal-section col-lg-12 nutricss">
	                      <div class="micro-nutro col-lg-6">
	                      <h2 class="meal-title">Macro-nutrients</h2>
	                      <div class="inner-sect">
	                      <div class="col-lg-12 left-sect total-calories">
	                      <div class="img-top"><img src="'.$baseimgurl.'/images/appl-small.png" class="img-responsive"></div>
	                      <h3 class="com-head"><span id="cla_main_total">'.round($achieve_calories,1).'</span><span class="com-phara"> Total Calories</span></h3>
	                      <div class="range_class">
	                      <div id="ex6caloriesok" '.$calories_cls.' ></div>
	                      </div>
	                      <span class="com-cont"><span id="cla_total">'.round($achieve_calories,1).'</span>/'.$target_calories.'</span>
	                      </div>
	                      <div class="col-lg-12 left-sect total-proteins">
	                      <div class="img-top"><span class="chart-1">p</span></div>
	                      <h3 class="com-head"><span id="prot_main_total">'.round($achieve_protien,1).'</span>g <span class="com-phara">Total Proteins</span></h3>
	                      <div class="range_class">
	                      <div id="ex6proteinsok" '.$protien_cls.' ></div>
	                      </div>
	                      <span class="com-cont"><span id="prot_total">'.round($achieve_protien,1).'</span>/'.$target_protien.'</span>
	                      </div>
	                      <div class="col-lg-12 left-sect total-fats">
	                      <div class="img-top"><span class="chart-1 fat">F</span></div>
	                      <h3 class="com-head"><span id="fat_main_total">'.round($achieve_fats,1).'</span>g <span class="com-phara">Total Fats</span></h3>
	                      <div class="range_class">
	                      <div id="ex6fatsok" '.$fats_cls.' ></div>
	                      </div>
	                      <span class="com-cont"><span id="fat_total">'.round($achieve_fats,1).'</span>/'.$target_fats.'</span>
	                      </div>
	                      <div class="col-lg-12 left-sect total-carbs">
	                      <div class="img-top"><span class="chart-1 carb">C</span></div>
	                      <h3 class="com-head"><span id="carb_main_total">'.round($achieve_carbs,1).'</span>g <span class="com-phara">Total Carbs</span></h3>
	                      <div class="range_class">
	                      <div id="ex6carbsok" '.$carbs_cls.' ></div>
	                      </div>
	                      <span class="com-cont"><span id="carb_total">'.round($achieve_carbs,1).'</span>/'.$target_carbs.'</span>
	                     </div>
	                     </div>
	                     </div>
	                     <div class="micro-meal-palan col-lg-6">
	                     <div id="mealloader"><img src="'.get_stylesheet_directory_uri().'/images/loading.gif"></div>
	                     <div class="inner-sect">'.$meal_plan_html.'</div>
	                     </div>
	                    </div>
	                    </div>
	                    <script>
	                    $(".form_add_moreBreakfast").keypress(function(e){
					        if(e.which == 13){//Enter key pressed
					            $("#enterBreakfast").click();//Trigger search button click event
					        }
					    });
					$(".form_add_moreLunch").keypress(function(e){
					        if(e.which == 13){//Enter key pressed
					            $("#enterLunch").click();//Trigger search button click event
					        }
					    });
					$(".form_add_moreDinner").keypress(function(e){
					        if(e.which == 13){//Enter key pressed
					            $("#enterDinner").click();//Trigger search button click event
					        }
					    });
					$(".form_add_moreSnacks").keypress(function(e){
					        if(e.which == 13){//Enter key pressed
					            $("#enterSnacks").click();//Trigger search button click event
					        }
					    });</script>';
	//echo json_encode(array("status"=>"success","plan_history_response"=> $html_plan_history));
	$current_date=date("Y/m/d");
	$current_date_str = strtotime($current_date);
	$history_plan_date_str = strtotime($history_plan_date);
         $_POST['history_plan_date'] ; 
        $reciep_current_date =  date('Y/m/d', strtotime($_POST['history_plan_date']));
        



if($exist_result)
	{
	 echo json_encode(array("status"=>"success","plan_history_response"=> $html_plan_history,"calories_consume_response"=> $calval ,"current_calories_limit"=>$current_calories_limit));
	}
 //    elseif($current_date_str < $history_plan_date_str)
	// {
	//  echo json_encode(array("status"=>"future_date"));
	// }
	else
	{
	 echo json_encode(array("status"=>"faild"));
	}
}
// planning calender history end


// Add update workout count start
if (isset($_POST['workout_userid']) && isset($_POST['workout_name']) && isset($_POST['workout_videoid']) )
{
	global $wpdb;
	$current_date = date("Y/m/d");
	$add_workout_query = "SELECT * from " . $wpdb->prefix . "add_workout WHERE user_id = '".$_POST['workout_userid']."' and workout_name = '".$_POST['workout_name']."' and youtube_video_id='".$_POST['workout_videoid']."' and workout_date='".$current_date."'";
	$add_workout_result = $wpdb->get_results($add_workout_query,ARRAY_A);

	//echo '<PRE>'; print_r($add_workout_result[0]['workout_count']);  



	if($add_workout_result){
		
		$previous_count=$add_workout_result[0]['workout_count'];
		$new_workout_count=$previous_count+$_POST['workout_count'];

		$update_workout="UPDATE " . $wpdb->prefix . "add_workout set workout_count='".$new_workout_count."' where user_id='".$_POST['workout_userid']."' and workout_name='".$_POST['workout_name']."' and youtube_video_id='".$_POST['workout_videoid']."' and workout_date ='".$current_date."' ";
		$up_row_workcount=$wpdb->get_row($update_workout);

		$video_sumcount = "SELECT sum(workout_count) as workout_count from " . $wpdb->prefix . "add_workout WHERE user_id = '".$_POST['workout_userid']."' and workout_name = '".$_POST['workout_name']."' and youtube_video_id='".$_POST['workout_videoid']."'";
		$video_sumcount_result = $wpdb->get_results($video_sumcount,ARRAY_A);
		$videosumcount = $video_sumcount_result[0]['workout_count'];

		echo json_encode(array("status"=>"success","recipe_status"=>"update_video_count","workout_count"=>$new_workout_count,"videosumcount"=>$videosumcount,"msg"=>"Video count updated sucessfully.","current_date"=>str_replace('/','',$current_date)));
		
	}else{

		$user_present = "SELECT * from " . $wpdb->prefix . "add_workout WHERE user_id = '".$_POST['workout_userid']."'";

			$user_present_res = $wpdb->get_row($user_present);

			if(!$user_present_res)
			{
				
				$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					// Additional headers
					$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

					$htmlContent = 'Good work! First workout is in the books! Keep it up and finish the week strong.';
					

					//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
					//$status = true;
					wp_mail($_POST['workout_userid'],'Logging first workout', $htmlContent, $headers);
			}

		$workout_date=date("Y/m/d");	
		$insert_workout ="INSERT INTO " . $wpdb->prefix . "add_workout(user_id,workout_name,workout_count,youtube_video_id,workout_date) VALUES ('".$_POST['workout_userid']."','".$_POST['workout_name']."','".$_POST['workout_count']."','".$_POST['workout_videoid']."','".$workout_date."')";
		$row_insert_workout=$wpdb->get_row($insert_workout);

		$video_sumcount = "SELECT sum(workout_count) as workout_count from " . $wpdb->prefix . "add_workout WHERE user_id = '".$_POST['workout_userid']."' and workout_name = '".$_POST['workout_name']."' and youtube_video_id='".$_POST['workout_videoid']."'";
		$video_sumcount_result = $wpdb->get_results($video_sumcount,ARRAY_A);
		$videosumcount = $video_sumcount_result[0]['workout_count'];


		echo json_encode(array("status"=>"success","recipe_status"=>"add_video_count","workout_count"=>$_POST['workout_count'],"videosumcount"=>$videosumcount,"msg"=>"Video count added sucessfully.","current_date"=>str_replace('/','',$current_date)));
		
		
	}
	

	// if($add_workout_result->youtube_video_id=='')
	// {
	// 	$workout_date=date("Y/m/d");	
	// 	$insert_workout ="INSERT INTO " . $wpdb->prefix . "add_workout(user_id,workout_name,workout_count,youtube_video_id,workout_date) VALUES ('".$_POST['workout_userid']."','".$_POST['workout_name']."','".$_POST['workout_count']."','".$_POST['workout_videoid']."','".$workout_date."')";
	// 	$row_insert_workout=$wpdb->get_row($insert_workout);
	// 	echo json_encode(array("status"=>"success","recipe_status"=>"add_video_count","workout_count"=>$_POST['workout_count'],"msg"=>"Video count added sucessfully.","current_date"=>str_replace('/','',$current_date)));
					
	// }else
	// {
	// 	$previous_count=$add_workout_result->workout_count;
	// 	$new_workout_count=$previous_count+$_POST['workout_count'];
	// 	$update_workout="UPDATE " . $wpdb->prefix . "add_workout set workout_count='".$new_workout_count."' where user_id='".$_POST['workout_userid']."' and workout_name='".$_POST['workout_name']."' and youtube_video_id='".$_POST['workout_videoid']."'";
	// 	$up_row_workcount=$wpdb->get_row($update_workout);
	// 	echo json_encode(array("status"=>"success","recipe_status"=>"update_video_count","workout_count"=>$new_workout_count,"msg"=>"Video count updated sucessfully.","current_date"=>str_replace('/','',$current_date)));
	// }
}
//  Add update workout count End

// Edit serving ajax start

if (isset($_POST['servingsactualvalue']) && isset($_POST['servingsmealtype']))
{
$current_date = $_POST['servingsdate'];
$userid = get_current_user_id();
$servingsactualvalue = $_POST['servingsactualvalue'];
$servingsmealtype = $_POST['servingsmealtype'];
$servingrecipeid = $_POST['servingrecipeid'];
$servingarray = array();
$final_servingarray = array();
$ingredents_tooltip_html = "";
$ingredents_popup_html = "";

$query_already_exist  = "SELECT recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_serving,recipe_ingredients_value FROM ".$wpdb->prefix."meal_plan where user_id = '".$userid."' and meal_type = '".$_POST['servingsmealtype']."' and recipe_id = '".$_POST['servingrecipeid']."' and recipe_current_date='".$current_date."'";
	 $already_exist_result = $wpdb->get_row($query_already_exist);

	 	$recipeingredients_value = json_decode($already_exist_result->recipe_ingredients_value,true);
	 	$serving_recipe_cal = $already_exist_result->recipe_calories;
		$serving_recipe_protien = $already_exist_result->recipe_protien;
		$serving_recipe_fats = $already_exist_result->recipe_fats;
		$serving_recipe_carbs = $already_exist_result->recipe_carbs; 
		$serving_recipe_serving = $already_exist_result->recipe_serving; 

		$change_value_cal = $servingsactualvalue * $serving_recipe_cal / $serving_recipe_serving;
		$change_value_protien = $servingsactualvalue * $serving_recipe_protien / $serving_recipe_serving;
		$change_value_fats = $servingsactualvalue * $serving_recipe_fats / $serving_recipe_serving;
		$change_value_carbs = $servingsactualvalue * $serving_recipe_carbs / $serving_recipe_serving;
		$change_value_cal = $change_value_cal;
		$change_value_protien = $change_value_protien;
		$change_value_fats = $change_value_fats;
		$change_value_carbs = $change_value_carbs;

		for ($seredit=0; $seredit < count($recipeingredients_value); $seredit++) { 
     // echo "<br>ID - ".$recipeingredients_value[$seredit]['id'];
     // echo "<br>image - ".$recipeingredients_value[$seredit]['image'];
     // echo "<br>name - ".$recipeingredients_value[$seredit]['name'];
     // echo "<br>amount - ".$recipeingredients_value[$seredit]['amount'];
     // echo "<br>unit - ".$recipeingredients_value[$seredit]['unit'];
        $change_value_amount = $recipeingredients_value[$seredit]['amount'];
    //  $change_value_amount = $servingsactualvalue * $recipeingredients_value[$seredit]['amount'] / $serving_recipe_serving;

     $servingarray['id'] = $recipeingredients_value[$seredit]['id'];
     $servingarray['image'] = $recipeingredients_value[$seredit]['image'];
     $servingarray['name'] = $recipeingredients_value[$seredit]['name'];
     $servingarray['amount'] = $change_value_amount; // amount value change
     $servingarray['unit'] = $recipeingredients_value[$seredit]['unit'];
     $servingarray['aisle'] = $recipeingredients_value[$seredit]['aisle'];
     $servingarray['unitShort'] = $recipeingredients_value[$seredit]['unitShort'];
     $servingarray['unitLong'] = $recipeingredients_value[$seredit]['unitLong'];
     $servingarray['originalString'] = $recipeingredients_value[$seredit]['originalString'];
     $servingarray['metaInformation'] = $recipeingredients_value[$seredit]['metaInformation'];

     $final_servingarray[] = $servingarray;
     unset($servingarray);

     $ingredents_tooltip_html .='<li class="ingredientsclass">';
     $ingredents_tooltip_html .='<span>'.$recipeingredients_value[$seredit]['name'].'</span>';
     $ingredents_tooltip_html .='<span>'.round($change_value_amount,1).'&nbsp;&nbsp;'.$recipeingredients_value[$seredit]['unit'].'</span>';
     $ingredents_tooltip_html .= '</li>';

     $ingredents_popup_html .= '<div class="content-sec">';
     $ingredents_popup_html .= '<div class="col-md-3">';
     $ingredents_popup_html .= '<img src="'.$recipeingredients_value[$seredit]['image'].'">';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '<div class="col-md-5 ingradient-one">';
     $ingredents_popup_html .= '<p class="ingradient-title">'.$recipeingredients_value[$seredit]['name'].'</p>';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '<div class="col-md-4 ingradient-two">';
    // $ingredents_popup_html .= '<p class="ingradient-title">'.round($change_value_amount,1).'&nbsp;&nbsp;'.$recipeingredients_value[$seredit]['unit'].'</p>';
     $ingredents_popup_html .= '<p class="ingradient-title">'.round($change_value_amount,1).'&nbsp;&nbsp;'.$recipeingredients_value[$seredit]['unit'].'</p>';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '</div>';
     $change_value_amount = "";


   }

   // final_servingarray converted to json
	$final_servingarray_json = json_encode($final_servingarray);

//echo 'HI<pre>'; print_r($final_servingarray);

      if($already_exist_result)
      {
      		$sql_regerate_meal ="UPDATE ".$wpdb->prefix."meal_plan set recipe_calories='".$change_value_cal."',recipe_protien='".$change_value_protien."',recipe_fats='".$change_value_fats."',recipe_carbs='".$change_value_carbs."',recipe_serving='".$_POST['servingsactualvalue']."',recipe_ingredients_value='".$final_servingarray_json."' WHERE meal_type ='".$_POST['servingsmealtype']."' and recipe_id ='".$_POST['servingrecipeid']."' and user_id ='".$userid."' and recipe_current_date='".$current_date."'";
			//$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);
			$row_sql_regerate_meal = $wpdb->query($sql_regerate_meal);
			//echo "update result = ".$row_sql_regerate_meal;
			
			if($row_sql_regerate_meal){

				$get_macro_nutrients = macro_nutrients($userid,$current_date); 
				$cla_sum_total = round($get_macro_nutrients[0]['recipe_calories'],1) ;
				$prot_sum_total = round($get_macro_nutrients[0]['recipe_protien'],1);
				$fat_sum_total = round($get_macro_nutrients[0]['recipe_fats'],1);
				$carb_sum_total = round($get_macro_nutrients[0]['recipe_carbs'],1);

				echo json_encode(array("status"=>"success","change_value_cal"=>round($change_value_cal,1),"change_value_protien"=>round($change_value_protien,1),"change_value_fats"=>round($change_value_fats,1),"change_value_carbs"=>round($change_value_carbs,1),"ingredents_tooltip_html"=>$ingredents_tooltip_html,"ingredents_popup_html"=>$ingredents_popup_html,"cla_sum_total"=>$cla_sum_total,"prot_sum_total"=>$prot_sum_total,"fat_sum_total"=>$fat_sum_total,"carb_sum_total"=>$carb_sum_total));
			}
			else
		      {

					echo json_encode(array("status"=>"failed","msg"=>'Servings not updated.'));

				}

        		
      }
      else
      {

			echo json_encode(array("status"=>"failed","msg"=>'Servings not edited.'));

		}

// Edit serving ajax end
}

// Add more recipe more info dispaly Start

if (isset($_POST['moreinfo_meal_type']) && isset($_POST['moreinfo_recipeid']))
{
		$search_response_result_size = "";

		global $wpdb;
		$user_id = get_current_user_id();
        $query_already_exist  = "SELECT recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_serving,original_serving,recipe_ingredients_value,direction FROM ".$wpdb->prefix."addmore_recipe_info where user_id = '".$user_id."' and meal_type = '".$_POST['moreinfo_meal_type']."' and recipe_id = '".$_POST['moreinfo_recipeid']."' and recipe_current_date='".$_POST['moreinfo_recipe_date']."'";
		   $already_exist_result = $wpdb->get_row($query_already_exist);

		   // echo '<br>'.$already_exist_result->recipe_calories;
		   // echo '<br>'.$already_exist_result->recipe_protien;
		   // echo '<br>'.$already_exist_result->recipe_fats;
		   // echo '<br>'.$already_exist_result->recipe_carbs;
		   // echo '<br>'.$already_exist_result->recipe_serving;

		   if($already_exist_result != ''){
		   	$recipecal = $already_exist_result->recipe_calories;
		   	$recipecarb = $already_exist_result->recipe_carbs;
		   	$recipeprotien = $already_exist_result->recipe_protien;
		   	$recipefats = $already_exist_result->recipe_fats;
		   	$recipeingredientsvalue = json_decode($already_exist_result->recipe_ingredients_value, TRUE);
		   	$recipedirection = json_decode($already_exist_result->direction, TRUE);
		   	$original_numberofservings = $already_exist_result->original_serving;
		   	$recipeservings = 1;
		   }
		   else{

		   	// Second hit for ingredients and directions Start
			 $spoonacula_getsechit = spoonacula_getsechit($_POST['moreinfo_recipeid']);
			 $spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);
			 //echo '<pre>'; print_r($spoonacula_getsechit['servings']);
			// Second hit for ingredents and directions End
		   	$recipecal = $_POST['moreinfo_recipecal'];
		   	$recipecarb = $_POST['moreinfo_recipecarbs'];
		   	$recipeprotien = $_POST['moreinfo_recipeprotin'];
		   	$recipefats = $_POST['moreinfo_recipefats'];
		   	$recipeingredientsvalue = $spoonacula_getsechit['extendedIngredients'];
		   	$recipedirection = $spoonacula_getsechit['analyzedInstructions'];
		   	$original_numberofservings = $spoonacula_getsechit['servings'];
		   	$recipeservings = 1;

		   }

		   

			// Conversion grocery START

				  $afterconvert = array();
				  $converted = array();

				  $final_ingredientline = $recipeingredientsvalue;
				  
				  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {


				    $convertvalue_unit = "";
				    $convertvalue_amount = "";
//				    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
//				       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
//
//				        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
//				        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
//				      }
//				      else{
//				        $convertvalue_unit = $convert_value['targetunit'];
//				        $convertvalue_amount = $convert_value['targetamount'];
//				      }
//				      unset($convert_value);

				        $afterconvert['id'] = $final_ingredientline[$gg]['id'];
				  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
				  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
				  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
				  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
                                        
				  	//$afterconvert['amount'] = $convertvalue_amount;
				  	//$afterconvert['unit'] = $convertvalue_unit;
                                        
                                        $afterconvert['amount'] = $final_ingredientline[$gg]['amount'];;
                                        $afterconvert['unit'] = $final_ingredientline[$gg]['unit'];;
                                        
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
				  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
				  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

				  	$converted[$gg] = $afterconvert;
				    unset($afterconvert);
				  }

				  $recipeingredientsvalue = $converted;
				  unset($converted);


				  // Conversion grocery END


		 // Ingredents Display Start 
		 $div_extendedingredents = '';
		 $extendedIngredients = $recipeingredientsvalue;
		 //echo $extendedIngredients; die();
		 for ($new_ex=0; $new_ex < count($extendedIngredients); $new_ex++) { 

   				$div_extendedingredents .= '<div class="content-sec">
				<div class="col-md-3">
					<img src="'.$extendedIngredients[$new_ex]['image'].'">
				</div>
				<div class="col-md-5 ingradient-one">
					<p class="ingradient-title">'.$extendedIngredients[$new_ex]['name'].'</p>
				</div>
	            <div class="col-md-4 ingradient-two">
	                <p class="ingradient-title">'.round($extendedIngredients[$new_ex]['amount'],1).'&nbsp;&nbsp;'.$extendedIngredients[$new_ex]['unit'].'</p>
	            </div>
			</div>';
   		}
		 // Ingredents Display End

   		// Direction Display start
   			$div_recipe_direction = "";
   			$analyzedInstructions = $recipedirection;
   			$sr_no = 1;
   		for($dj=0;$dj<count($analyzedInstructions);$dj++){
              	$count_direction_steps = count($analyzedInstructions[$dj]['steps']);
              
              for ($l=0; $l < $count_direction_steps ; $l++) { 
                              	
                              	$steps_data = $analyzedInstructions[$dj]['steps'];
                              	$div_recipe_direction .= '
	                             <div class="direction-sec col-md-12 col-sm-12">
								              <div class="col-md-1 col-sm-1 direction-no">
								              <p>'.$sr_no.'</p>
								              </div>
									            <div class="col-md-11 col-sm-11 direction-data">
									              	<p>'.$steps_data[$l]['step'].'</p>
									            </div>
								   </div>';
								$sr_no++;
                          }

               }

   		// Direction Display End

        // Store second hit data into global array start
        $ingredents_direction_array = array();
                
        $ingredents_direction_array['ingredients'] = $spoonacula_getsechit['extendedIngredients'];
        $ingredents_direction_array['instructions'] = $spoonacula_getsechit['analyzedInstructions'];

        $addmore_secondhit_info[$_POST['moreinfo_recipeid']] = $ingredents_direction_array;
        unset($ingredents_direction_array);
        // echo '<pre>'; print_r($addmore_secondhit_info);
        // die();

        // Stroe second hit data into global array end       

		// Piechart Calculation Start
        $recipe_carbs_pop = $recipecarb * 4 ;
        $recipe_protien_pop = $recipeprotien * 4 ;
        $recipe_fats_pop = $recipefats* 9 ;
        $caloriesin_grams = $recipe_carbs_pop + $recipe_protien_pop + $recipe_fats_pop;

        $recipe_carbs_pop = $recipe_carbs_pop*100/$caloriesin_grams;
        $recipe_protien_pop = $recipe_protien_pop*100/$caloriesin_grams;
        $recipe_fats_pop = $recipe_fats_pop*100/$caloriesin_grams;
        // Piechart calculation End

		// $search_response_result_size .= '<div style="display: none;" class="add_recipe_popup">';
        $search_response_result_size .= '<div class="">';
        $search_response_result_size .= '<div class="ingradient-main-sec">';
        $search_response_result_size .= '<p class="receip_name">'.$_POST['moreinfo_recipename'].'<span class="pop-info">Original Recipe Size : '.$original_numberofservings.' Serving(s)</span></p>';
        $search_response_result_size .= '<p class="popupeditserving"><span class="recinfoforserv"> For</span><input type="number" name="addmoreserving" id="addmoreserving'.$_POST['moreinfo_recipeid'].'" value="'.$recipeservings.'" onkeyup="addmore_editservings(this.value,\''.$_POST['moreinfo_recipeid'].'\',\''.$_POST['moreinfo_meal_type'].'\',\''.$_POST['moreinfo_recipe_date'].'\');" onclick="(this.select())"><span>Serving(s)</span></p>';
        $search_response_result_size .= '<div class="col-md-4 col-sm-4 reciepe-image">';
        $search_response_result_size .= '<img class="nutricss" src="'.$_POST['moreinfo_recipeimage'].'">';
        $search_response_result_size .= '</div>';

        $search_response_result_size .= '<div class="col-md-3 col-sm-3 reciepe-data">';
        $search_response_result_size .= '<p>Calories: <span id="popupcal'.$_POST['moreinfo_recipeid'].'">'.round($recipecal,1).'</span></p>';
        $search_response_result_size .= '<p>Protein: <span id="popupprotien'.$_POST['moreinfo_recipeid'].'">'.round($recipeprotien,1).'g</span></p>';
        $search_response_result_size .= '<p>Fat: <span id="popupfat'.$_POST['moreinfo_recipeid'].'">'.round($recipefats,1).'g</span></p>';
        $search_response_result_size .= '<p>Carbs: <span id="popupcarbs'.$_POST['moreinfo_recipeid'].'">'.round($recipecarb,1).'g</span></p>';
        $search_response_result_size .=	 '</div>';

        $search_response_result_size .= '<div id="changepiechart'.$_POST['moreinfo_recipeid'].$_POST['moreinfo_meal_type'].'">';

        $search_response_result_size .= '<div class="pieclass" id="piechart'.$_POST['moreinfo_recipeid'].$_POST['moreinfo_meal_type'].'"></div>';
        

        $search_response_result_size .= '<script type="text/javascript">';
        $search_response_result_size .= ' google.charts.load("current", {"packages":["corechart"]});';
        $search_response_result_size .= 'function drawChart() {';
        // $search_response_result_size .= 'var recipe_carbs=60;';
        // $search_response_result_size .= 'var recipe_protien=15;';
        // $search_response_result_size .= 'var recipe_fats=22;';
        $search_response_result_size .= '}';
        $search_response_result_size .= 'var data = google.visualization.arrayToDataTable([';
        $search_response_result_size .= '["Task", "Hours per Day"],';
        $search_response_result_size .= '["Fats", '.$recipe_fats_pop.'],';
        $search_response_result_size .= '["Carbs", '.$recipe_carbs_pop.'],';
        $search_response_result_size .= '["Protein", '.$recipe_protien_pop.'],';
        $search_response_result_size .= ']);';

        $search_response_result_size .= 'var options = {';
        $search_response_result_size .= 'chartArea:{left:0,top:2},';
        $search_response_result_size .= 'colors: ["#47B650", "#28aae2", "#6538C9"]';
        $search_response_result_size .= '};';

        $search_response_result_size .= 'var piechart = "piechart'.$_POST['moreinfo_recipeid'].$_POST['moreinfo_meal_type'].'";';
        $search_response_result_size .= 'var chart = new google.visualization.PieChart(document.getElementById(piechart));';
        $search_response_result_size .= 'chart.draw(data, options);';

        $search_response_result_size .= '</script>'; 

        $search_response_result_size .= '</div>'; // end of change piechartid              

        $search_response_result_size .= '<div class="col-md-12 col-sm-12 ingradients-content">
						            	<p class="head-sec">Ingredients</p>
						            	<div id="popingrdent'.$_POST['new_recipe_id'].'" class="col-md-12 col-sm-12 ingradients-data">'.$div_extendedingredents.'</div>
							       		 </div>';

		$search_response_result_size .= '<div class="col-md-12 col-sm-12 directions">
										<p class="head-sec">Directions</p>'.$div_recipe_direction.'</div>';

        $search_response_result_size .= '</div>';
        $search_response_result_size .= '</div>';
        // $search_response_result_size .= '</div>';

        $user_id = get_current_user_id();

        if(!$already_exist_result){

        	$str_direction = str_replace("'","", json_encode($recipedirection));
		  	$sql_new_recipe ="insert into ".$wpdb->prefix."addmore_recipe_info(user_id,meal_type,recipe_name, recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_id,direction,recipe_ingredients_value,recipe_serving,original_serving,recipe_current_date) VALUES ('".$user_id."','".$_POST['moreinfo_meal_type']."','".$_POST['moreinfo_recipename']."','".$_POST['moreinfo_recipeimage']."','".$recipecal."','".$recipeprotien."','".$recipefats."','".$_POST['moreinfo_recipecarbs']."','".$_POST['moreinfo_recipeid']."','".$str_direction."','".json_encode($recipeingredientsvalue)."','".$recipeservings."','".$original_numberofservings."','".$_POST['moreinfo_recipe_date']."')";
			$row=$wpdb->query($sql_new_recipe);

		  } 

        

		echo json_encode(array("status"=>"success","more_info"=>$search_response_result_size));
		

        
}
// Add more recipe more info dispaly End


// Add more pop up edit saervings start


if (isset($_POST['popservingrecipeid']) && isset($_POST['popservingsactualvalue']) && isset($_POST['popservingsmealtype']))
{

		$current_date = $_POST['popservingsdate'];
		$userid = get_current_user_id();
		$servingsactualvalue = $_POST['popservingsactualvalue'];
		$servingsmealtype = $_POST['popservingsmealtype'];
		$servingrecipeid = $_POST['popservingrecipeid'];
		$servingarray = array();
		$final_servingarray = array();
		$ingredents_popup_html = "";

		global $wpdb;
		$query_already_exist  = "SELECT recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_serving,recipe_ingredients_value FROM ".$wpdb->prefix."addmore_recipe_info where user_id = '".$userid."' and meal_type = '".$_POST['popservingsmealtype']."' and recipe_id = '".$_POST['popservingrecipeid']."' and recipe_current_date='".$current_date."'";
		   $already_exist_result = $wpdb->get_row($query_already_exist);

		    $recipeingredients_value = json_decode($already_exist_result->recipe_ingredients_value,true);
		   $serving_recipe_cal = $already_exist_result->recipe_calories;
		   $serving_recipe_protien = $already_exist_result->recipe_protien;
		   $serving_recipe_fats = $already_exist_result->recipe_fats;
		   $serving_recipe_carbs = $already_exist_result->recipe_carbs; 
		   $serving_recipe_serving = $already_exist_result->recipe_serving; 

		   $change_value_cal = "";
		   $change_value_protien = "";
		   $change_value_fats = "";
		   $change_value_carbs = "";
		    if($serving_recipe_cal != 0){
		   	$change_value_cal = $servingsactualvalue * $serving_recipe_cal / $serving_recipe_serving;
		   }else{$change_value_cal=$serving_recipe_cal;}

		   if($serving_recipe_protien != 0){
		   	$change_value_protien = $servingsactualvalue * $serving_recipe_protien / $serving_recipe_serving;
		   }else{$change_value_protien=$serving_recipe_protien;}

		   if($serving_recipe_fats != 0){
		   	$change_value_fats = $servingsactualvalue * $serving_recipe_fats / $serving_recipe_serving;
		   }else{$change_value_fats=$serving_recipe_fats;}

		   if($serving_recipe_carbs != 0){
		   	$change_value_carbs = $servingsactualvalue * $serving_recipe_carbs / $serving_recipe_serving;
		   }else{$change_value_carbs=$serving_recipe_carbs;} 
		    
		    
		    $change_value_cal = round($change_value_cal,1);
		    $change_value_protien = round($change_value_protien,1);
		    $change_value_fats = round($change_value_fats,1);
		    $change_value_carbs = round($change_value_carbs,1);

		    for ($seredit=0; $seredit < count($recipeingredients_value); $seredit++) { 
     // echo "<br>ID - ".$recipeingredients_value[$seredit]['id'];
     // echo "<br>image - ".$recipeingredients_value[$seredit]['image'];
     // echo "<br>name - ".$recipeingredients_value[$seredit]['name'];
     // echo "<br>amount - ".$recipeingredients_value[$seredit]['amount'];
     // echo "<br>unit - ".$recipeingredients_value[$seredit]['unit'];

     $change_value_amount = $servingsactualvalue * $recipeingredients_value[$seredit]['amount'] / $serving_recipe_serving;

     $servingarray['id'] = $recipeingredients_value[$seredit]['id'];
     $servingarray['image'] = $recipeingredients_value[$seredit]['image'];
     $servingarray['name'] = $recipeingredients_value[$seredit]['name'];
     $servingarray['amount'] = round($change_value_amount,2); // amount value change
     $servingarray['unit'] = $recipeingredients_value[$seredit]['unit'];
     $servingarray['aisle'] = $recipeingredients_value[$seredit]['aisle'];
     $servingarray['unitShort'] = $recipeingredients_value[$seredit]['unitShort'];
     $servingarray['unitLong'] = $recipeingredients_value[$seredit]['unitLong'];
     $servingarray['originalString'] = $recipeingredients_value[$seredit]['originalString'];
     $servingarray['metaInformation'] = $recipeingredients_value[$seredit]['metaInformation'];

     $final_servingarray[] = $servingarray;
     unset($servingarray);

     $ingredents_popup_html .= '<div class="content-sec">';
     $ingredents_popup_html .= '<div class="col-md-3">';
     $ingredents_popup_html .= '<img src="'.$recipeingredients_value[$seredit]['image'].'">';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '<div class="col-md-5 ingradient-one">';
     $ingredents_popup_html .= '<p class="ingradient-title">'.$recipeingredients_value[$seredit]['name'].'</p>';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '<div class="col-md-4 ingradient-two">';
     $ingredents_popup_html .= '<p class="ingradient-title">'.round($change_value_amount,1).'&nbsp;&nbsp;'.$recipeingredients_value[$seredit]['unit'].'</p>';
     $ingredents_popup_html .= '</div>';
     $ingredents_popup_html .= '</div>';
     $change_value_amount = "";


   }

   // final_servingarray converted to json
  $final_servingarray_json = json_encode($final_servingarray);

  // piechart calculation addmore edit serving Start
  			$piechart_html = '';

			$recipe_carbs_pop = $already_exist_result->recipe_carbs;
			$recipe_protien_pop = $already_exist_result->recipe_protien;
			$recipe_fats_pop = $already_exist_result->recipe_fats;

			if($recipe_carbs_pop != 0 || $recipe_protien_pop != 0 || $recipe_fats_pop != 0){
			$recipe_carbs_pop = $recipe_carbs_pop * 4 ;
			$recipe_protien_pop = $recipe_protien_pop * 4 ;
			$recipe_fats_pop = $recipe_fats_pop * 9 ;
			$caloriesin_grams = $recipe_carbs_pop + $recipe_protien_pop + $recipe_fats_pop;

			$recipe_carbs_pop = $recipe_carbs_pop*100/$caloriesin_grams;
			$recipe_carbs_pop = round($recipe_carbs_pop);
			$recipe_protien_pop = $recipe_protien_pop*100/$caloriesin_grams;
			$recipe_protien_pop = round($recipe_protien_pop);
			$recipe_fats_pop = $recipe_fats_pop*100/$caloriesin_grams;
			$recipe_fats_pop = round($recipe_fats_pop);
			}

			$piechart_html .= '<div id="piechart'.$_POST['popservingrecipeid'].$_POST['popservingsmealtype'].'" class="pieclass"></div>';

			$piechart_html .= '<script type="text/javascript">
						                google.charts.load("current", {"packages":["corechart"]});
						              google.charts.setOnLoadCallback(drawChart);
						              function drawChart() {
						                var recipe_carbs=60;
						                var recipe_protien=15;
						                var recipe_fats=22;
						                var data = google.visualization.arrayToDataTable([
						                  ["Task", "Hours per Day"],
						                  ["Fats", '.$recipe_fats_pop.'],
						                  ["Carbs", '.$recipe_carbs_pop.'],
						                  ["Protein", '.$recipe_protien_pop.'],
						                  
						                ]);

						                var options = {
						                	width:400,
						                	height:200,
						                 chartArea:{left:0,top:2},
						                 colors: ["#47B650", "#28aae2", "#6538C9"]
						                };
						                var piechart = "piechart'.$_POST['popservingrecipeid'].$_POST['popservingsmealtype'].'";
						                var chart = new google.visualization.PieChart(document.getElementById(piechart));
						                chart.draw(data, options);
						              }

						            </script>';

  // piechart calculation addmore edit serving End

//echo 'HI<pre>'; print_r($final_servingarray);

      if($already_exist_result)
      {
          $sql_regerate_meal ="UPDATE ".$wpdb->prefix."addmore_recipe_info set recipe_calories='".$change_value_cal."',recipe_protien='".$change_value_protien."',recipe_fats='".$change_value_fats."',recipe_carbs='".$change_value_carbs."',recipe_serving='".$_POST['popservingsactualvalue']."',recipe_ingredients_value='".$final_servingarray_json."' WHERE meal_type ='".$_POST['popservingsmealtype']."' and recipe_id ='".$_POST['popservingrecipeid']."' and user_id ='".$userid."' and recipe_current_date='".$current_date."'";
      //$row_sql_regerate_meal=$wpdb->get_row($sql_regerate_meal);
      $row_sql_regerate_meal = $wpdb->query($sql_regerate_meal);
      //echo "update result = ".$row_sql_regerate_meal;
      
      if($row_sql_regerate_meal){

        echo json_encode(array("status"=>"success","change_value_cal"=>$change_value_cal,"change_value_protien"=>$change_value_protien,"change_value_fats"=>$change_value_fats,"change_value_carbs"=>$change_value_carbs,"ingredents_popup_html"=>$ingredents_popup_html,"poprecipe_carbs_pop"=>$recipe_carbs_pop,"poprecipe_protien_pop"=>$recipe_protien_pop,"poprecipe_fats_pop"=>$recipe_fats_pop,"piechart_html"=>$piechart_html));
      }
      else
          {

          echo json_encode(array("status"=>"failed","msg"=>'Servings not updated.'));

        }

            
      }
      else
      {

      echo json_encode(array("status"=>"failed","msg"=>'Servings not edited.'));

    }


		    
}	


// Add more pop up edit saervings end

// Create grocery list start

if (isset($_POST['create_grolist_startdate']) && isset($_POST['create_grolist_enddate']))
{
    
	$user_id = get_current_user_id();		
	global $wpdb;
	$create_grolist_startdate = $_POST['create_grolist_startdate'];
	$create_grolist_enddate = $_POST['create_grolist_enddate'];
	$sd = date_format(date_create($create_grolist_startdate),"Y/m/d");
	$ed = date_format(date_create($create_grolist_enddate),"Y/m/d");

	$date_present ="SELECT start_date,end_date FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$sd."' AND end_date = '".$ed."'";
	$date_result = $wpdb->query($date_present);
	$sd = '';
	$ed = '';
	if($date_result){

		echo json_encode(array("status"=>"present","msg"=>'Dates already exists.'));
		exit;
	}

	
    $month_startdate_name = date('F',strtotime($create_grolist_startdate));
    $month_startdate_day = date('d',strtotime($create_grolist_startdate)); 

    $last_modified_day = date('d',strtotime(date('Y/m/d')));
    $last_modified_month = date('F',strtotime(date('Y/m/d')));

    $month_enddate_name = date('F',strtotime($create_grolist_enddate));
    $month_enddate_day = date('d',strtotime($create_grolist_enddate));

    $grocerycreated = '';
    $pantrycreated = '';

	$deleted_date = str_replace("/", "", date_format(date_create($create_grolist_startdate),"Y/m/d"));
 	$cretedgro_list = '<div id="dletedlist'.str_replace("/", "", date_format(date_create($create_grolist_startdate),"Y/m/d")).str_replace("/", "", date_format(date_create($create_grolist_enddate),"Y/m/d")).'">
	<a href="javascript:void(0)" onclick="getgrocerydata(\''.date_format(date_create($create_grolist_startdate),"Y/m/d").'\',\''.date_format(date_create($create_grolist_enddate),"Y/m/d").'\');">
                            <div id="listgrodates" class="col-md-8 col-sm-8 activegrolist">
                            	<div class="sltuser">	
                              <p class="ltsite">
                                <span id="grocey_startdate'.str_replace("/", "",date_format(date_create($create_grolist_startdate),"Y/m/d")).'">'.$month_startdate_name.' '.$month_startdate_day.'</span>
                                <span> - </span>
                                <span id="grocey_enddate'.str_replace("/", "",date_format(date_create($create_grolist_enddate),"Y/m/d")).'">'.$month_enddate_name.' '.$month_enddate_day.'</span>
                              </p>
                              <span class="label"><span class="lat">Last Modified -</span> <span id="lastmodified">'.$last_modified_day.' '.$last_modified_month.'</span></span>
                              </div>
                            </div>
                          </a>  
                            <div class="col-md-4 col-sm-4 delet-btn">
                              <button onclick="deletegroceylist(\''.date_format(date_create($create_grolist_startdate),"Y/m/d").'\',\''.date_format(date_create($create_grolist_enddate),"Y/m/d").'\');" class="btn btn-primary" type="button">Delete</button> 
                            </div>
                        </div>
                        <script type="text/javascript">
  $(document).ready(function(){
  $("#grocerydatelist a #listgrodates").click(function(){
  $("#grocerydatelist a #listgrodates").removeClass("activegrolist");
  $(this).addClass("activegrolist");
   }); 
});
</script>';

    // create grocery date wise start

	$sql_get_data ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."meal_plan WHERE  user_id ='".$user_id."' AND recipe_current_date >= '".date_format(date_create($create_grolist_startdate),"Y/m/d")."' AND recipe_current_date <= '".date_format(date_create($create_grolist_enddate),"Y/m/d")."'";
	$recipe_result = $wpdb->get_results($sql_get_data, ARRAY_A);

	$grocery_data = $recipe_result;
	$liste_aisle = array();
	$liste_value = array();

	
		for ($i=0 ; $i < count($grocery_data) ; $i++ ) { 
	
	$jsongrocey_data = json_decode($grocery_data[$i]['recipe_ingredients_value'],true);
	//echo '<br>'.count($jsongrocey_data);
	//print_r($jsongrocey_data);

	// Getting aisle Start

	for ($j=0; $j < count($jsongrocey_data); $j++) { 
		
		//echo '<br>aisle - '.$jsongrocey_data[$j]['aisle'];

		if(!in_array($jsongrocey_data[$j]['aisle'], $liste_aisle, true)){
			array_push($liste_aisle, $jsongrocey_data[$j]['aisle']);
		}

		
	}

	// Getting aisle End

	// Geeting ingredents name start
	for ($m=0; $m < count($jsongrocey_data); $m++) { 
		
		if(!in_array($jsongrocey_data[$m]['name'], $liste_value, true)){
			array_push($liste_value, $jsongrocey_data[$m]['name']);

    	}

	}
	// Getting ingredents name end
}

	$add_ingredents_inaisle = array();
	$ingredents_value = array();
	$final_unit = array();
        
     

	foreach ($liste_aisle as $liste_aisle_value) { 
		
		for ($k=0 ; $k < count($grocery_data) ; $k++ ) { 
		$ingredents_get = json_decode($grocery_data[$k]['recipe_ingredients_value'],true);

			for ($l=0; $l < count($ingredents_get); $l++) {

				$ingredents_aisle = $ingredents_get[$l]['aisle'];
				if($liste_aisle_value == $ingredents_aisle){

					if($ingredents_get[$l]['id'] != "" || $ingredents_get[$l]['id'] != null){

					$ingredents_value['id'] = $ingredents_get[$l]['id'];
					$ingredents_value['aisle'] = $ingredents_get[$l]['aisle'];
					$ingredents_value['name'] = $ingredents_get[$l]['name'];
					$ingredents_value['amount'] = $ingredents_get[$l]['amount'];
					$ingredents_value['unit'] = $ingredents_get[$l]['unit'];
					$ingredents_value['image'] = $ingredents_get[$l]['image'];
					$add_ingredents_inaisle[$ingredents_value['name']][] = $ingredents_value;
					unset($ingredents_value);

					}

				}
			}
		}
	}
	
	$finalarray = array();
	$list_array = array();

	for ($l=0; $l<count($liste_value); $l++) {
		
		$ingre_value = $liste_value[$l];
		 $ingre_actual_value = $add_ingredents_inaisle[$ingre_value];
                 
       
		if($ingre_actual_value[0]['id'] != "" || $ingre_actual_value[0]['id'] != null){
			$a = 0;
			for ($n=0; $n < count($ingre_actual_value); $n++) { 
				
				 $a = $a + $ingre_actual_value[$n]['amount'];

				$list_array['id'] = $ingre_actual_value[0]['id'];
				$list_array['name'] = str_replace("'", "", $ingre_actual_value[0]['name']);
				$list_array['unit'] = $ingre_actual_value[0]['unit'];
				$list_array['image'] = $ingre_actual_value[0]['image'];
				$list_array['aisle'] = $ingre_actual_value[0]['aisle'];

			}
			$list_array['amount'] = $a;
			// $finalarray[$list_array['aisle']][$ingre_actual_value[0]['name']] = $list_array;
			$finalarray[$list_array['aisle']][] = $list_array;
			unset($list_array);
		}
	}        
        
	 
	// Convert measurement START
	$convert_arraykeys = array_keys($finalarray);
	$convertarray = array();
	 $final_convertarray = array();
       for ($ll=0; $ll<count($convert_arraykeys); $ll++) {
	 	$valuesfinal = $finalarray[$convert_arraykeys[$ll]];
	 	for ($nn=0; $nn < count($finalarray[$convert_arraykeys[$ll]]); $nn++) { 

		$convert_value1 = convert_measurement($valuesfinal[$nn]['name'],$valuesfinal[$nn]['amount'],$valuesfinal[$nn]['unit']);
		
                
                $convert_value = get_typicalunit_measurement($valuesfinal[$nn]['id']);
                 
                    foreach($convert_value as $key1 => $items) { 
                                
                            if($key1 == 'shoppingListUnits'){
                                echo "hasunit";
                                $typicalunit =  $items[0]; 
                               
                            }
//                        
                        }
                            
                    if($convert_value1['targetamount'] == 0 && $convert_value1['targetamount'] == ''){

				$convertvalue_unit = $valuesfinal[$nn]['unit'];
				$convertvalue_amount = $valuesfinal[$nn]['amount'];
			}
	 		else{
	 			$convertvalue_unit = $convert_value1['targetunit'];
				$convertvalue_amount = $convert_value1['targetamount'];
			}
//                         $convertvalue_unit;
                        
//                         $convertvalue_amount;
		 	$convertarray['id'] = $valuesfinal[$nn]['id'];
			$convertarray['name'] = $valuesfinal[$nn]['name'];
			$convertarray['unit'] = $typicalunit;
	 		$convertarray['image'] = $valuesfinal[$nn]['image'];
	 		$convertarray['aisle'] = $valuesfinal[$nn]['aisle'];
			$convertarray['amount'] = $convertvalue_amount;
			$final_convertarray[$convert_arraykeys[$ll]][] = $convertarray;
	 	}
		
	 	unset($convertarray);
	 }
         
        
//         echo "<pre>";
//         print_r($final_convertarray);
//         echo "</pre>";
//         exit;
//	 for ($ll=0; $ll<count($convert_arraykeys); $ll++) {
//	 	$valuesfinal = $finalarray[$convert_arraykeys[$ll]];
//	 	for ($nn=0; $nn < count($finalarray[$convert_arraykeys[$ll]]); $nn++) { 
////
////			$convert_value = convert_measurement($valuesfinal[$nn]['name'],$valuesfinal[$nn]['amount'],$valuesfinal[$nn]['unit']);
////			if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){
////
////				$convertvalue_unit = $valuesfinal[$nn]['unit'];
////				$convertvalue_amount = $valuesfinal[$nn]['amount'];
////			}
////	 		else{
////	 			$convertvalue_unit = $convert_value['targetunit'];
////				$convertvalue_amount = $convert_value['targetamount'];
////			}
////                         $convertvalue_unit;
////                        
////                         $convertvalue_amount;
//		
//                    
//                 $ingredient_id =  $valuesfinal[$nn]['id']; echo "<br/>";
//                 $ingredient_name =  $valuesfinal[$nn]['name']; echo "<br/>";
////                curl --get --include 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/9266/information?amount=100&unit=gram' \
////  -H 'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh' \
////  -H 'Accept: application/json'
//                
//                
//                
//                $url =  "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/".$ingredient_id."/information";
//                $ch = curl_init();
//                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_HTTPGET, true);
//                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh',
//                'Accept: application/json'
//                ));
//
//                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//                curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
//                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0'); 
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//                $body = curl_exec($ch);
//                $info = curl_getinfo($ch);
//                $error = curl_errno($ch);
//                // print_r('Curl error: ' . curl_error($ch));
//
//                curl_close($ch); 
//              
//         $recipe_ingredients_data  =     $body;
//         
//         
//
////              $response_data = $body;
////                 
////                $new_ingredients_result_array = json_decode($body, TRUE);
////                      
////             echo "<pre>";
////             print_r($response_data);
////             echo "</pre>";
//     //echo "hello";
////                  for ($s=0; $s < count($new_ingredients_result_array); $s++) { 
//////	            echo $valuesfinal[$s]['shoppingListUnits']; 
//////                     die;
////                    $convertarray['id'] = $valuesfinal[$s]['id']; 
////                    $convertarray['name'] = $valuesfinal[$s]['name'];  
////                    $convertarray['unit'] = $valuesfinal[$s]['unit']; 
////                    $convertarray['image'] = $valuesfinal[$s]['image']; 
////                  //  $convertarray['aisle'] = $valuesfinal[$s]['aisle'];  
////                    $convertarray['amount'] =$valuesfinal[$s]['amount']; 
////                    $convertarray['shoppingListUnits'] = $valuesfinal[$s]['shoppingListUnits'][0];  
////			$final_convertarray[$convert_arraykeys[$ll]][] = $convertarray;
////	}
//        
////               echo "<pre>";
////             print_r($converted);
////             echo "</pre>";
//      }
//		
//	 	//unset($convertarray);
//	 }
//$recipe_ingredients_data = json_encode($final_convertarray);

// ";

	// Convert measurement END               	 
	//$recipe_ingredients_data = json_encode($finalarray);
        
        $recipe_ingredients_data = json_encode($final_convertarray);
//         //$recipe_ingredients_data = $new_ingredients_result_array;

//        <pre>{"Baking":[{"id":18371,"name":"baking powder","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-powder.jpg","aisle":"Baking","amount":null},{"id":20081,"name":"flour","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/flour.png","aisle":"Baking","amount":null},{"id":19335,"name":"sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sugar-in-bowl.png","aisle":"Baking","amount":null},{"id":18372,"name":"baking soda","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-powder.jpg","aisle":"Baking","amount":null},{"id":19334,"name":"brown sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-sugar-dark.jpg","aisle":"Baking","amount":null},{"id":2050,"name":"vanilla extract","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/vanilla-extract.jpg","aisle":"Baking","amount":null},{"id":19087,"name":"white chocolate","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-chocolate.jpg","aisle":"Baking","amount":null},{"id":19335,"name":"granulated sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sugar-in-bowl.png","aisle":"Baking","amount":null}],"Canned and Jarred":[{"id":10011693,"name":"canned tomatoes","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomatoes-canned.jpg","aisle":"Canned and Jarred","amount":null},{"id":43112,"name":"chili beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/kidney-beans.jpg","aisle":"Canned and Jarred","amount":null},{"id":16018,"name":"canned black beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/black-beans.jpg","aisle":"Canned and Jarred","amount":null},{"id":11549,"name":"tomato sauce","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomato-sauce-or-pasta-sauce.jpg","aisle":"Canned and Jarred","amount":null},{"id":6172,"name":"chicken stock","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/broth.jpg","aisle":"Canned and Jarred","amount":null}],"Produce":[{"id":11143,"name":"celery","unit":"stalks","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/celery.jpg","aisle":"Produce","amount":null},{"id":11260,"name":"mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/mushrooms.jpg","aisle":"Produce","amount":null},{"id":11282,"name":"onion","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-onion.jpg","aisle":"Produce","amount":null},{"id":11485,"name":"butternut squash","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butternut-squash.jpg","aisle":"Produce","amount":null},{"id":10011457,"name":"spinach","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/spinach.jpg","aisle":"Produce","amount":null},{"id":9152,"name":"lemon juice","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/lemon-juice.jpg","aisle":"Produce","amount":null},{"id":11109,"name":"cabbage","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cabbage.jpg","aisle":"Produce","amount":null},{"id":31015,"name":"green chiles","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-peppers-green.jpg","aisle":"Produce","amount":null},{"id":11215,"name":"whole garlic cloves","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic.jpg","aisle":"Produce","amount":null},{"id":11011,"name":"asparagus tips","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/asparagus.png","aisle":"Produce","amount":null},{"id":2064,"name":"mint","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/lemon-balm.jpg","aisle":"Produce","amount":null},{"id":11215,"name":"garlic","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic.jpg","aisle":"Produce","amount":null},{"id":10011238,"name":"mixed mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/mixed-mushrooms.png","aisle":"Produce","amount":null},{"id":2049,"name":"thyme","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/thyme.jpg","aisle":"Produce","amount":null},{"id":10511282,"name":"yellow onion","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-onion.jpg","aisle":"Produce","amount":null},{"id":9040,"name":"bananas","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/bananas.jpg","aisle":"Produce","amount":null}],"Spices and Seasonings":[{"id":1022028,"name":"chili seasoning mix","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/paprika.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1062047,"name":"garlic salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic-salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2047,"name":"salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1002030,"name":"black pepper","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/pepper.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2031,"name":"cayenne pepper","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2009,"name":"chili powder","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1002014,"name":"cumin","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/ground-cumin.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1022020,"name":"garlic powder","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2010,"name":"cinnamon","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cinnamon.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1082047,"name":"kosher salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2010,"name":"ground cinnamon","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cinnamon.jpg","aisle":"Spices and Seasonings","amount":null}],"Meat":[{"id":10023572,"name":"ground beef","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fresh-ground-beef.jpg","aisle":"Meat","amount":null}],"Milk, Eggs, Other Dairy":[{"id":1077,"name":"milk","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/milk.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1123,"name":"eggs","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/egg.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":4073,"name":"margarine","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1145,"name":"unsalted butter","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001053,"name":"whipping cream","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fluid-cream.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001056,"name":"creme fraiche","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sour-cream.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001,"name":"butter","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null}],"Oil, Vinegar, Salad Dressing":[{"id":4582,"name":"oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/vegetable-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null},{"id":4053,"name":"olive oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/olive-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null},{"id":4053,"name":"light olive oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/olive-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null}],"Ethnic Foods;Baking":[{"id":35137,"name":"yellow cornmeal","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cornmeal.jpg","aisle":"Ethnic Foods;Baking","amount":null}],"Pasta and Rice;Ethnic Foods":[{"id":6599,"name":"enchilada sauce","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salsa-verde.jpg","aisle":"Pasta and Rice;Ethnic Foods","amount":null}],"Cheese":[{"id":1011026,"name":"shredded cheese","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cheddar-cheese.jpg","aisle":"Cheese","amount":null},{"id":1033,"name":"parmesan","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/parmesan.jpg","aisle":"Cheese","amount":null},{"id":1036,"name":"ricotta","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/ricotta-cheese.jpg","aisle":"Cheese","amount":null},{"id":1017,"name":"cream cheese","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cream-cheese.jpg","aisle":"Cheese","amount":null}],"Bakery\/Bread;Pasta and Rice;Ethnic Foods":[{"id":93675,"name":"whole wheat tortillas","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tortillas-whole-wheat.jpg","aisle":"Bakery\/Bread;Pasta and Rice;Ethnic Foods","amount":null}],"Tea and Coffee":[{"id":14214,"name":"instant coffee","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/instant-coffee-or-instant-espresso.png","aisle":"Tea and Coffee","amount":null}],"Pasta and Rice;Canned and Jarred":[{"id":16015,"name":"black beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/black-beans.jpg","aisle":"Pasta and Rice;Canned and Jarred","amount":null}],"Savory Snacks":[{"id":19003,"name":"corn chips","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fritos-or-corn-chips.jpg","aisle":"Savory Snacks","amount":null}],"Frozen":[{"id":11913,"name":"frozen corn","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/corn.png","aisle":"Frozen","amount":null}],"Produce;Spices and Seasonings":[{"id":2027,"name":"oregano","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/oregano.jpg","aisle":"Produce;Spices and Seasonings","amount":null}],"Pasta and Rice":[{"id":11887,"name":"tomato paste","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomato-paste.jpg","aisle":"Pasta and Rice","amount":null},{"id":10120420,"name":"farfalle","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/farfalle.png","aisle":"Pasta and Rice","amount":null},{"id":10020052,"name":"arborio rice","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/rice-white-uncooked.jpg","aisle":"Pasta and Rice","amount":null}],"Produce;Ethnic Foods":[{"id":10011268,"name":"dried porcini mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/porcini-mushrooms-dried.jpg","aisle":"Produce;Ethnic Foods","amount":null}],"Alcoholic Beverages":[{"id":14106,"name":"dry white wine","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-wine.jpg","aisle":"Alcoholic Beverages","amount":null}],"Nuts;Baking":[{"id":12142,"name":"pecans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/pecans.jpg","aisle":"Nuts;Baking","amount":null}]}</pre>
//        
//           echo "<pre>";
//             print_r($recipe_ingredients_data);
//             echo "</pre>";  
                
//        <pre>{"Baking":[{"id":18371,"name":"baking powder","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-powder.jpg","aisle":"Baking","amount":null},{"id":20081,"name":"flour","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/flour.png","aisle":"Baking","amount":null},{"id":19335,"name":"sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sugar-in-bowl.png","aisle":"Baking","amount":null},{"id":18372,"name":"baking soda","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-powder.jpg","aisle":"Baking","amount":null},{"id":19334,"name":"brown sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-sugar-dark.jpg","aisle":"Baking","amount":null},{"id":2050,"name":"vanilla extract","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/vanilla-extract.jpg","aisle":"Baking","amount":null},{"id":19087,"name":"white chocolate","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-chocolate.jpg","aisle":"Baking","amount":null},{"id":19335,"name":"granulated sugar","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sugar-in-bowl.png","aisle":"Baking","amount":null}],"Canned and Jarred":[{"id":10011693,"name":"canned tomatoes","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomatoes-canned.jpg","aisle":"Canned and Jarred","amount":null},{"id":43112,"name":"chili beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/kidney-beans.jpg","aisle":"Canned and Jarred","amount":null},{"id":16018,"name":"canned black beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/black-beans.jpg","aisle":"Canned and Jarred","amount":null},{"id":11549,"name":"tomato sauce","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomato-sauce-or-pasta-sauce.jpg","aisle":"Canned and Jarred","amount":null},{"id":6172,"name":"chicken stock","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/broth.jpg","aisle":"Canned and Jarred","amount":null}],"Produce":[{"id":11143,"name":"celery","unit":"stalks","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/celery.jpg","aisle":"Produce","amount":null},{"id":11260,"name":"mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/mushrooms.jpg","aisle":"Produce","amount":null},{"id":11282,"name":"onion","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-onion.jpg","aisle":"Produce","amount":null},{"id":11485,"name":"butternut squash","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butternut-squash.jpg","aisle":"Produce","amount":null},{"id":10011457,"name":"spinach","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/spinach.jpg","aisle":"Produce","amount":null},{"id":9152,"name":"lemon juice","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/lemon-juice.jpg","aisle":"Produce","amount":null},{"id":11109,"name":"cabbage","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cabbage.jpg","aisle":"Produce","amount":null},{"id":31015,"name":"green chiles","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-peppers-green.jpg","aisle":"Produce","amount":null},{"id":11215,"name":"whole garlic cloves","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic.jpg","aisle":"Produce","amount":null},{"id":11011,"name":"asparagus tips","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/asparagus.png","aisle":"Produce","amount":null},{"id":2064,"name":"mint","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/lemon-balm.jpg","aisle":"Produce","amount":null},{"id":11215,"name":"garlic","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic.jpg","aisle":"Produce","amount":null},{"id":10011238,"name":"mixed mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/mixed-mushrooms.png","aisle":"Produce","amount":null},{"id":2049,"name":"thyme","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/thyme.jpg","aisle":"Produce","amount":null},{"id":10511282,"name":"yellow onion","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/brown-onion.jpg","aisle":"Produce","amount":null},{"id":9040,"name":"bananas","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/bananas.jpg","aisle":"Produce","amount":null}],"Spices and Seasonings":[{"id":1022028,"name":"chili seasoning mix","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/paprika.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1062047,"name":"garlic salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic-salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2047,"name":"salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1002030,"name":"black pepper","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/pepper.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2031,"name":"cayenne pepper","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2009,"name":"chili powder","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/chili-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1002014,"name":"cumin","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/ground-cumin.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1022020,"name":"garlic powder","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/garlic-powder.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2010,"name":"cinnamon","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cinnamon.jpg","aisle":"Spices and Seasonings","amount":null},{"id":1082047,"name":"kosher salt","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salt.jpg","aisle":"Spices and Seasonings","amount":null},{"id":2010,"name":"ground cinnamon","unit":"pieces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cinnamon.jpg","aisle":"Spices and Seasonings","amount":null}],"Meat":[{"id":10023572,"name":"ground beef","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fresh-ground-beef.jpg","aisle":"Meat","amount":null}],"Milk, Eggs, Other Dairy":[{"id":1077,"name":"milk","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/milk.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1123,"name":"eggs","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/egg.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":4073,"name":"margarine","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1145,"name":"unsalted butter","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001053,"name":"whipping cream","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fluid-cream.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001056,"name":"creme fraiche","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/sour-cream.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null},{"id":1001,"name":"butter","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/butter-sliced.jpg","aisle":"Milk, Eggs, Other Dairy","amount":null}],"Oil, Vinegar, Salad Dressing":[{"id":4582,"name":"oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/vegetable-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null},{"id":4053,"name":"olive oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/olive-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null},{"id":4053,"name":"light olive oil","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/olive-oil.jpg","aisle":"Oil, Vinegar, Salad Dressing","amount":null}],"Ethnic Foods;Baking":[{"id":35137,"name":"yellow cornmeal","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cornmeal.jpg","aisle":"Ethnic Foods;Baking","amount":null}],"Pasta and Rice;Ethnic Foods":[{"id":6599,"name":"enchilada sauce","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/salsa-verde.jpg","aisle":"Pasta and Rice;Ethnic Foods","amount":null}],"Cheese":[{"id":1011026,"name":"shredded cheese","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cheddar-cheese.jpg","aisle":"Cheese","amount":null},{"id":1033,"name":"parmesan","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/parmesan.jpg","aisle":"Cheese","amount":null},{"id":1036,"name":"ricotta","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/ricotta-cheese.jpg","aisle":"Cheese","amount":null},{"id":1017,"name":"cream cheese","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/cream-cheese.jpg","aisle":"Cheese","amount":null}],"Bakery\/Bread;Pasta and Rice;Ethnic Foods":[{"id":93675,"name":"whole wheat tortillas","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tortillas-whole-wheat.jpg","aisle":"Bakery\/Bread;Pasta and Rice;Ethnic Foods","amount":null}],"Tea and Coffee":[{"id":14214,"name":"instant coffee","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/instant-coffee-or-instant-espresso.png","aisle":"Tea and Coffee","amount":null}],"Pasta and Rice;Canned and Jarred":[{"id":16015,"name":"black beans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/black-beans.jpg","aisle":"Pasta and Rice;Canned and Jarred","amount":null}],"Savory Snacks":[{"id":19003,"name":"corn chips","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/fritos-or-corn-chips.jpg","aisle":"Savory Snacks","amount":null}],"Frozen":[{"id":11913,"name":"frozen corn","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/corn.png","aisle":"Frozen","amount":null}],"Produce;Spices and Seasonings":[{"id":2027,"name":"oregano","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/oregano.jpg","aisle":"Produce;Spices and Seasonings","amount":null}],"Pasta and Rice":[{"id":11887,"name":"tomato paste","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/tomato-paste.jpg","aisle":"Pasta and Rice","amount":null},{"id":10120420,"name":"farfalle","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/farfalle.png","aisle":"Pasta and Rice","amount":null},{"id":10020052,"name":"arborio rice","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/rice-white-uncooked.jpg","aisle":"Pasta and Rice","amount":null}],"Produce;Ethnic Foods":[{"id":10011268,"name":"dried porcini mushrooms","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/porcini-mushrooms-dried.jpg","aisle":"Produce;Ethnic Foods","amount":null}],"Alcoholic Beverages":[{"id":14106,"name":"dry white wine","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/white-wine.jpg","aisle":"Alcoholic Beverages","amount":null}],"Nuts;Baking":[{"id":12142,"name":"pecans","unit":"ounces","image":"https:\/\/spoonacular.com\/cdn\/ingredients_100x100\/pecans.jpg","aisle":"Nuts;Baking","amount":null}]}</pre>
//       echo "<pre>";
//       print_r($recipe_ingredients_data);
//       echo "</pre>";
        
        

	 $last_modified = date('Y/m/d');

       
	$sql_grocery_list ="insert into ".$wpdb->prefix."grocerylist(user_id,start_date,end_date,recipe_ingredients_value,last_modified) VALUES ('".$user_id."','".date_format(date_create($create_grolist_startdate),"Y/m/d")."','".date_format(date_create($create_grolist_enddate),"Y/m/d")."','".$recipe_ingredients_data."','".$last_modified."')";
	$sql_grocerylist_result = $wpdb->query($sql_grocery_list);	

	if($sql_grocerylist_result){
            
 
         
		$get_grocerydata = get_grocery_ondates(date_format(date_create($create_grolist_startdate),"Y/m/d"),date_format(date_create($create_grolist_enddate),"Y/m/d"));
		$get_pantrydata = get_pantry_ondates(date_format(date_create($create_grolist_startdate),"Y/m/d"),date_format(date_create($create_grolist_enddate),"Y/m/d"));
	    $get_keys = array_keys($get_grocerydata);
	    if($get_pantrydata){
	    $get_pantrykeys = array_keys($get_pantrydata);
		}

	// get grocery data start	    
	for ($ii=0; $ii < count($get_keys); $ii++) {

        $get_values = $get_grocerydata[$get_keys[$ii]];

        // $grocerycreated .='<div id="getgroceryingredents">';
        $grocerycreated .='<li class="title-text">'.$get_keys[$ii].'</li>';
        
        for ($j=0; $j < count($get_grocerydata[$get_keys[$ii]]); $j++) {
        
            if($get_keys[$ii] == $get_values[$j]['aisle']){

            	$grocerycreated .= '<li id="moveitem'.$get_values[$j]['id'].'">
                                      
                                          <input type="checkbox" id="'.$get_keys[$ii].'*$#'.$get_values[$j]['name'].'" class="grocheckbox" onclick="getData(\''.$get_values[$j]['id'].'\');" name="check[]">
                                          
                                          <span class="pic"> <img class="img-block" id="imageid-'.$get_values[$j]['id'].'" src="'.$get_values[$j]['image'].'">
                                          
                                          </span> 
                                          <span class="ingriname" id="aisleid-'.$get_values[$j]['id'].'" style="display:none;">'.$get_keys[$ii].'</span>
                                          <span class="ingriname" id="nameid-'.$get_values[$j]['id'].'">'.$get_values[$j]['name'].'</span>
                                     
                                      <p class="infoinstrc">
                                        <span id="amountid-'.$get_values[$j]['id'].'" class="ingreamount">'.$get_values[$j]['amount'].'</span>
                                        <span id="unitid-'.$get_values[$j]['id'].'" class="ingreunit">'.$get_values[$j]['unit'].'</span>
                                      </p>
                                    </li>';

			 }
      }
      // $grocerycreated .='<div id="getgroceryingredents">';
    }
    // get grocery data end
  
    //get pantry data start
    if($get_pantrydata){
  
     for ($k=0; $k < count($get_pantrykeys); $k++) {

        $get_pantryvalues = $get_pantrydata[$get_pantrykeys[$k]];

        $pantrycreated .='<li class="title-text">'.$get_pantrykeys[$k].'</li>';

        for ($l=0; $l < count($get_pantrydata[$get_pantrykeys[$k]]); $l++) {
        
            if($get_pantrykeys[$k] == $get_pantryvalues[$l]['aisle']){

            	$pantrycreated .= '<li id="moveitem'.$get_pantryvalues[$l]['id'].'">
                                      
                                          <input type="checkbox" id="'.$get_pantrykeys[$k].'*$#'.$get_pantryvalues[$l]['name'].'" class="pancheckbox1" onclick="getpantryData(\''.$get_pantryvalues[$l]['id'].'\');" name="check[]">
                                          
                                          <span class="pic"> <img class="img-block" id="pantryimageid-'.$get_pantryvalues[$l]['id'].'" src="'.$get_pantryvalues[$l]['image'].'">
                                          
                                          </span> 
                                          <span class="ingriname" id="pantryaisleid-'.$get_pantryvalues[$l]['id'].'" style="display:none;">'.$get_pantrykeys[$k].'</span>
                                          <span class="ingriname" id="pantrynameid-'.$get_pantryvalues[$l]['id'].'">'.$get_pantryvalues[$l]['name'].'</span>
                                      
                                      <p class="infoinstrc">
                                        <span id="pantryamountid-'.$get_pantryvalues[$l]['id'].'" class="ingreamount">'.round($get_pantryvalues[$l]['amount'], 2).'</span>
                                        <span id="pantryunitid-'.$get_pantryvalues[$l]['id'].'" class="ingreunit">'.$get_pantryvalues[$l]['unit'].'</span>
                                      </p>
                                    </li>';

			 }
      }
    }
    }
   
    // get pantry data end
		echo json_encode(array("status"=>"success","msg"=>'Grocey list created.',"create_grolist_startdate"=>$create_grolist_startdate,"create_grolist_enddate"=>$create_grolist_enddate,"cretedgro_list"=>$cretedgro_list,"grocerycreated"=>$grocerycreated,"pantrycreated"=>$pantrycreated,"startdate"=>date_format(date_create($create_grolist_startdate),"Y/m/d"),"enddate"=>date_format(date_create($create_grolist_enddate),"Y/m/d")));
		exit;
              
  
            }                        

        	//echo json_encode(array("status"=>"success","msg"=>'Grocey list created.',"create_grolist_startdate"=>$create_grolist_startdate,"create_grolist_enddate"=>$create_grolist_enddate,"cretedgro_list"=>$cretedgro_list,"grocerycreated"=>$grocerycreated,"pantrycreated"=>$pantrycreated,"startdate"=>date_format(date_create($create_grolist_startdate),"Y/m/d"),"enddate"=>date_format(date_create($create_grolist_enddate),"Y/m/d")));

	

}	

// Create grocery list end


// Delete grocery list start
		
if (isset($_POST['gro_del_startdate']) && isset($_POST['gro_del_enddate']))
{
	$user_id = get_current_user_id();
	$gro_del_startdate = $_POST['gro_del_startdate'];
	$gro_del_enddate = $_POST['gro_del_enddate'];
	$deleted_date = str_replace("/", "", $gro_del_startdate.$gro_del_enddate);

	global $wpdb;
	$sql_grocery_par_list_delete ="DELETE FROM ".$wpdb->prefix."grocerylist WHERE user_id = '".$user_id."' AND  start_date = '".$gro_del_startdate."' AND end_date = '".$gro_del_enddate."'";
	$sql_grocery_par_listdelete_result = $wpdb->query($sql_grocery_par_list_delete);

	$sql_pantry_par_list_delete ="DELETE FROM ".$wpdb->prefix."pantrylist WHERE user_id = '".$user_id."' AND  start_date = '".$gro_del_startdate."' AND end_date = '".$gro_del_enddate."'";
	$sql_pantry_par_listdelete_result = $wpdb->query($sql_pantry_par_list_delete);

	if($sql_grocery_par_listdelete_result){

		echo json_encode(array("status"=>"success","msg"=>'Grocey list deleted.',"gro_del_startdate"=>$gro_del_startdate,"gro_del_enddate"=>$gro_del_enddate,"deleted_date"=>$deleted_date));

	}
	else{

		echo json_encode(array("status"=>"faild","msg"=>'Grocey list not deleted.'));
	}
	

}

// Delete grocery list end

// Get grocery data date wise start

if (isset($_POST['get_gro_startdate']) && isset($_POST['get_gro_enddate']))
{

	$user_id = get_current_user_id();
	$get_gro_startdate = $_POST['get_gro_startdate'];
	$get_gro_enddate = $_POST['get_gro_enddate'];

	$ingredents_data = '';
	$ingredents_pantrydata = '';

	// global $wpdb;
	// $sql_grocery_dates_list ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$get_gro_startdate."' AND end_date = '".$get_gro_enddate."' ";
	// $sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates_list);

	$get_grocerydata = get_grocery_ondates($get_gro_startdate,$get_gro_enddate);
	$get_pantrydata = get_pantry_ondates($get_gro_startdate,$get_gro_enddate);
    $get_keys = array_keys($get_grocerydata);
    if($get_pantrydata){
    	$get_pantrykeys = array_keys($get_pantrydata);
    }
    

	for ($ii=0; $ii < count($get_keys); $ii++) {

        $get_values = $get_grocerydata[$get_keys[$ii]];
        if(count($get_values)){
        	$ingredents_data .='<li class="title-text">'.$get_keys[$ii].'</li>';
        }
        for ($j=0; $j < count($get_grocerydata[$get_keys[$ii]]); $j++) {
        
            if($get_keys[$ii] == $get_values[$j]['aisle']){

            	$ingredents_data .= '<li id="moveitem'.$get_values[$j]['id'].'">
                                      
                                          <input type="checkbox" id="'.$get_keys[$ii].'*$#'.$get_values[$j]['name'].'" class="grocheckbox" onclick="getData(\''.$get_values[$j]['id'].'\');" name="check[]">
                                          
                                          <span class="pic"> <img class="img-block" id="imageid-'.$get_values[$j]['id'].'" src="'.$get_values[$j]['image'].'">
                                          
                                          </span> 
                                          <span id="aisleid-'.$get_values[$j]['id'].'" style="display:none;">'.$get_keys[$ii].'</span>
                                          <span id="nameid-'.$get_values[$j]['id'].'">'.$get_values[$j]['name'].'</span>
                                      
                                      <p class="infoinstrc">
                                        <span id="amountid-'.$get_values[$j]['id'].'" class="ingreamount">'.$get_values[$j]['amount'].'</span>
                                        <span id="unitid-'.$get_values[$j]['id'].'" class="ingreunit">'.$get_values[$j]['unit'].'</span>
                                      </p>
                                    </li>';

			 }
      }
    }

    for ($k=0; $k < count($get_pantrykeys); $k++) {

        $get_pantryvalues = $get_pantrydata[$get_pantrykeys[$k]];
        
        if(count($get_pantryvalues)){
        	$ingredents_pantrydata .='<li class="title-text">'.$get_pantrykeys[$k].'</li>';
    	}

        for ($l=0; $l < count($get_pantrydata[$get_pantrykeys[$k]]); $l++) {
        
            if($get_pantrykeys[$k] == $get_pantryvalues[$l]['aisle']){

            	$ingredents_pantrydata .= '<li id="moveitem'.$get_pantryvalues[$l]['id'].'">
                                      
                                          <input type="checkbox" id="'.$get_pantrykeys[$k].'*$#'.$get_pantryvalues[$l]['name'].'" class="pancheckbox1" onclick="getpantryData(\''.$get_pantryvalues[$l]['id'].'\');" name="check1[]">
                                          
                                          <span class="pic"> <img class="img-block" id="pantryimageid-'.$get_pantryvalues[$l]['id'].'" src="'.$get_pantryvalues[$l]['image'].'">
                                          
                                          </span> 
                                          <span id="pantryaisleid-'.$get_pantryvalues[$l]['id'].'" style="display:none;">'.$get_pantrykeys[$k].'</span>
                                          <span id="pantrynameid-'.$get_pantryvalues[$l]['id'].'">'.$get_pantryvalues[$l]['name'].'</span>
                                      
                                      <p class="infoinstrc">
                                        <span id="pantryamountid-'.$get_pantryvalues[$l]['id'].'" class="ingreamount">'.$get_pantryvalues[$l]['amount'].'</span>
                                        <span id="pantryunitid-'.$get_pantryvalues[$l]['id'].'" class="ingreunit">'.$get_pantryvalues[$l]['unit'].'</span>
                                      </p>
                                    </li>';

			 }
      }
    }

	if($get_grocerydata){

		echo json_encode(array("status"=>"success","msg"=>'Grocey list.',"get_gro_startdate"=>$get_gro_startdate,"get_gro_enddate"=>$get_gro_enddate,"ingredents_data"=>$ingredents_data,"ingredents_pantrydata"=>$ingredents_pantrydata));

	}
	else{

		echo json_encode(array("status"=>"faild","msg"=>'Grocey list not deleted.'));
	}

}
// Get grocery data date wise end

// Move grocery to pantry start

if (isset($_POST['move_asile']) && isset($_POST['move_name']) )
{	
	$user_id = get_current_user_id();
	$movedasile = $_POST['move_asile'];
	$movedname = $_POST['move_name'];
	$move_startdate = $_POST['move_startdate'];
	$move_enddate = $_POST['move_enddate'];
	$getmovedasile = array();

	//echo '<pre>'; //print_r($movedasile);

	for ($i=0; $i < count($movedasile); $i++) { 
		
		if(!in_array($movedasile[$i], $getmovedasile)){

			$getmovedasile[$i] = $movedasile[$i];
		}

	}

	//print_r($getmovedasile);
	//echo implode("*$#",$getmovedasile);
	//echo implode("*$#",$movedname);

	$ingredent_search_aisle = implode("*$#",$getmovedasile);
	$ingredent_search_name = $movedname;
	$plan_startdate = $move_startdate;
	$plan_enddate = $move_enddate;

	// print_r($ingredent_search_aisle);
	// print_r($ingredent_search_name);
	// die();

	global $wpdb;

	$sql_grocery_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates, ARRAY_A);

	$get_ingredents_value = json_decode($sql_grocery_datesresult[0]['recipe_ingredients_value'],TRUE);

	

	// Loop for searching according to aisle start
	$new_get_ingredents_value = array();
	$moved_ingredent = array();
	$final_moved_ingredent = array();

	$search_aisle = explode("*$#",$ingredent_search_aisle);
	$search_name = $ingredent_search_name;
	// print_r($search_aisle);
	// print_r($search_name);
	// die();
	if($get_ingredents_value){

		$aisle_keys = array_keys($get_ingredents_value);
	}
	

	for ($bb=0; $bb < count($aisle_keys); $bb++) {

		//echo '<br>'.$aisle_keys[$bb];
		$get_aislevalue = $get_ingredents_value[$aisle_keys[$bb]];
		//print_r($get_aislevalue);

		for ($cc=0; $cc < count($get_aislevalue) ; $cc++) { 

			$get_search_aisle = $search_aisle;
			$get_search_name = $search_name;
			for ($dd=0; $dd < count($get_search_aisle); $dd++) { 
				
				for ($ee=0; $ee < count($get_search_name); $ee++) { 
					
					if($get_search_aisle[$dd] == $get_aislevalue[$cc]['aisle'] && $search_name[$ee] == $get_aislevalue[$cc]['name']){

							$moved_ingredent['id'] = $get_aislevalue[$cc]['id'];
							$moved_ingredent['name'] = $get_aislevalue[$cc]['name'];
							$moved_ingredent['aisle'] = $get_aislevalue[$cc]['aisle'];
							$moved_ingredent['unit'] = $get_aislevalue[$cc]['unit'];
							$moved_ingredent['image'] = $get_aislevalue[$cc]['image'];
							$moved_ingredent['amount'] = $get_aislevalue[$cc]['amount'];

							$final_moved_ingredent[$get_search_aisle[$dd]][] = $moved_ingredent;
							// deleting particular ingredent from array
							array_splice($get_aislevalue, $cc, 1);
							}

				}
			}
		}
		//print_r($get_aislevalue); // print deleting particular ingredent from array
		$new_get_ingredents_value[$aisle_keys[$bb]] = $get_aislevalue; 

	}
	//print_r($new_get_ingredents_value);
	// Check pantry data already exist start

	$sql_pantry_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_pantry_datesresult = $wpdb->get_results($sql_pantry_dates, ARRAY_A);

	if($sql_pantry_datesresult){

		$pantry_ingredents_value = json_decode($sql_pantry_datesresult[0]['recipe_ingredients_value'],TRUE);

		$final_new_ing_array = array();
		$pantry_aisle = array_keys($pantry_ingredents_value);

		for ($ii=0; $ii < count($search_aisle); $ii++) { 
			
			//echo '<br>'.$search_aisle[$ii];

			if(array_key_exists($search_aisle[$ii], $pantry_ingredents_value)){

				//echo 'Yes';
				for ($kk=0; $kk < count($final_moved_ingredent[$search_aisle[$ii]]); $kk++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle']){

					//create new array of final with aisle name
						$ifyes_data_get =array();
						$ifyes_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['id'];
						$ifyes_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle'];
						$ifyes_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['name'];
						$ifyes_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['unit'];
						$ifyes_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['image'];
						$ifyes_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['amount'];
						//$final_new_ing_array[] = $ifyes_data_get;
						array_push($pantry_ingredents_value[$search_aisle[$ii]],$ifyes_data_get);
	 	 			
					
				}	
				unset($ifyes_data_get);	
			}
		}
			else{

				//echo '<br>No';
				for ($jj=0; $jj < count($final_moved_ingredent[$search_aisle[$ii]]); $jj++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle']){

						//create new array of final with aisle name
						$array_data_get =array();
						$array_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['id'];
						$array_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle'];
						$array_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['name'];
						$array_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['unit'];
						$array_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['image'];
						$array_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['amount'];
						$final_new_ing_array[] = $array_data_get;
					}
					
					
				}
				$pantry_ingredents_value[$search_aisle[$ii]] = $final_new_ing_array;
				unset($array_data_get);
				unset($final_new_ing_array);
			}
		}

	}

	//print_r($pantry_ingredents_value); die();

	// Check pantry data already exist end
// Loop for searching according to aisle end

	$last_modified = date('Y/m/d');

	$update_grocery_list="UPDATE ".$wpdb->prefix."grocerylist set recipe_ingredients_value='".json_encode($new_get_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$update_grocery_list_result = $wpdb->query($update_grocery_list);

	if($sql_pantry_datesresult){

		$update_pantry_list="UPDATE ".$wpdb->prefix."pantrylist set recipe_ingredients_value='".json_encode($pantry_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
		$sql_pantry_result = $wpdb->query($update_pantry_list);
	}
	else{

		$sql_pantry_insert ="insert into ".$wpdb->prefix."pantrylist(user_id,start_date,end_date,recipe_ingredients_value,last_modified) VALUES ('".$user_id."','".$plan_startdate."','".$plan_enddate."','".json_encode($final_moved_ingredent)."','".$last_modified."')";
		$sql_pantry_result = $wpdb->query($sql_pantry_insert);
	}
	
	if($update_grocery_list_result && $sql_pantry_result){

		$gro_pandata = new_pantry_ondates($plan_startdate,$plan_enddate);

		$grodata = $gro_pandata['grocey'];
		$pandata = $gro_pandata['pantry'];

	echo json_encode(array("status"=>"success","msg"=>'Moved to grocey.',"movedasile"=>$movedasile,"movedname"=>$movedname,"grodata"=>$grodata,"pandata"=>$pandata));
	}

}	

// Move grocery to pantry end


// Move pantry to grocery start

if (isset($_POST['pantrymove_asile']) && isset($_POST['pantrymove_name']) )
{

	$user_id = get_current_user_id();
	$pantrymove_asile = $_POST['pantrymove_asile'];
	$pantrymove_name = $_POST['pantrymove_name'];
	$pantrymove_startdate = $_POST['pantrymove_startdate'];
	$pantrymove_enddate = $_POST['pantrymove_enddate'];

	$getmovedasile = array();

	//echo '<pre>'; //print_r($movedasile);

	for ($i=0; $i < count($pantrymove_asile); $i++) { 
		
		if(!in_array($pantrymove_asile[$i], $getmovedasile)){

			$getmovedasile[$i] = $pantrymove_asile[$i];
		}

	}

	$ingredent_search_aisle = implode("*$#",$getmovedasile);
	$ingredent_search_name = $pantrymove_name;

	$plan_startdate = $pantrymove_startdate;
	$plan_enddate = $pantrymove_enddate;

	// echo '<pre>';
	// print_r($pantrymove_asile);
	// print_r($pantrymove_name);
	// echo $ingredent_search_aisle;
	// echo $ingredent_search_name;

	global $wpdb;

	$sql_grocery_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."pantrylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_grocery_datesresult = $wpdb->get_results($sql_grocery_dates, ARRAY_A);

	$get_ingredents_value = json_decode($sql_grocery_datesresult[0]['recipe_ingredients_value'],TRUE);

	//print_r($get_ingredents_value);

	// Loop for searching according to aisle start
	$new_get_ingredents_value = array();
	$moved_ingredent = array();
	$final_moved_ingredent = array();

	$search_aisle = explode("*$#",$ingredent_search_aisle);
	$search_name = $ingredent_search_name;
	//print_r($search_aisle);
	//print_r($search_name);
	$aisle_keys = array_keys($get_ingredents_value);

	for ($bb=0; $bb < count($aisle_keys); $bb++) {

		//echo '<br>'.$aisle_keys[$bb];
		$get_aislevalue = $get_ingredents_value[$aisle_keys[$bb]];
		//print_r($get_aislevalue);

		for ($cc=0; $cc < count($get_aislevalue) ; $cc++) { 

			$get_search_aisle = $search_aisle;
			$get_search_name = $search_name;
			for ($dd=0; $dd < count($get_search_aisle); $dd++) { 
				
				for ($ee=0; $ee < count($get_search_name); $ee++) { 
					
					if($get_search_aisle[$dd] == $get_aislevalue[$cc]['aisle'] && $search_name[$ee] == $get_aislevalue[$cc]['name']){

							$moved_ingredent['id'] = $get_aislevalue[$cc]['id'];
							$moved_ingredent['name'] = $get_aislevalue[$cc]['name'];
							$moved_ingredent['aisle'] = $get_aislevalue[$cc]['aisle'];
							$moved_ingredent['unit'] = $get_aislevalue[$cc]['unit'];
							$moved_ingredent['image'] = $get_aislevalue[$cc]['image'];
							$moved_ingredent['amount'] = $get_aislevalue[$cc]['amount'];

							$final_moved_ingredent[$get_search_aisle[$dd]][] = $moved_ingredent;
							// deleting particular ingredent from array
							array_splice($get_aislevalue, $cc, 1);
							}

				}
			}
		}
		//print_r($get_aislevalue); // print deleting particular ingredent from array
		$new_get_ingredents_value[$aisle_keys[$bb]] = $get_aislevalue; 

	}
	//print_r($new_get_ingredents_value);
	// Check pantry data already exist start

	$sql_pantry_dates ="SELECT recipe_ingredients_value FROM ".$wpdb->prefix."grocerylist WHERE  user_id ='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."' ";
	$sql_pantry_datesresult = $wpdb->get_results($sql_pantry_dates, ARRAY_A);

	if($sql_pantry_datesresult){

		$pantry_ingredents_value = json_decode($sql_pantry_datesresult[0]['recipe_ingredients_value'],TRUE);

		$final_new_ing_array = array();
		$pantry_aisle = array_keys($pantry_ingredents_value);

		for ($ii=0; $ii < count($search_aisle); $ii++) { 
			
			//echo '<br>'.$search_aisle[$ii];

			if(array_key_exists($search_aisle[$ii], $pantry_ingredents_value)){

				//echo 'Yes';
				for ($kk=0; $kk < count($final_moved_ingredent[$search_aisle[$ii]]); $kk++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle']){

					//create new array of final with aisle name
						$ifyes_data_get =array();
						$ifyes_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['id'];
						$ifyes_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['aisle'];
						$ifyes_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['name'];
						$ifyes_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['unit'];
						$ifyes_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['image'];
						$ifyes_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$kk]['amount'];
						//$final_new_ing_array[] = $ifyes_data_get;
						array_push($pantry_ingredents_value[$search_aisle[$ii]],$ifyes_data_get);
	 	 			
					
				}	
				unset($ifyes_data_get);	
			}
		}
			else{

				//echo '<br>No';
				for ($jj=0; $jj < count($final_moved_ingredent[$search_aisle[$ii]]); $jj++) { 
					
					if( $search_aisle[$ii] == $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle']){

						//create new array of final with aisle name
						$array_data_get =array();
						$array_data_get['id'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['id'];
						$array_data_get['aisle'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['aisle'];
						$array_data_get['name'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['name'];
						$array_data_get['unit'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['unit'];
						$array_data_get['image'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['image'];
						$array_data_get['amount'] = $final_moved_ingredent[$search_aisle[$ii]][$jj]['amount'];
						$final_new_ing_array[] = $array_data_get;
					}
					
					
				}
				$pantry_ingredents_value[$search_aisle[$ii]] = $final_new_ing_array;
				unset($array_data_get);
				unset($final_new_ing_array);
			}
		}

	}

	//print_r($pantry_ingredents_value); die();

	// Check pantry data already exist end
// Loop for searching according to aisle end

	$last_modified = date('Y/m/d');

	$update_grocery_list="UPDATE ".$wpdb->prefix."pantrylist set recipe_ingredients_value='".json_encode($new_get_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
	$update_grocery_list_result = $wpdb->query($update_grocery_list);

	if($sql_pantry_datesresult){

		$update_pantry_list="UPDATE ".$wpdb->prefix."grocerylist set recipe_ingredients_value='".json_encode($pantry_ingredents_value)."', last_modified = '".$last_modified."' where user_id='".$user_id."' AND start_date = '".$plan_startdate."' AND end_date = '".$plan_enddate."'";
		$sql_pantry_result = $wpdb->query($update_pantry_list);
	}
	else{

		$sql_pantry_insert ="insert into ".$wpdb->prefix."grocerylist(user_id,start_date,end_date,recipe_ingredients_value,last_modified) VALUES ('".$user_id."','".$plan_startdate."','".$plan_enddate."','".json_encode($final_moved_ingredent)."','".$last_modified."')";
		$sql_pantry_result = $wpdb->query($sql_pantry_insert);
	}

	if($update_grocery_list_result && $sql_pantry_result){

		$gro_pandata = new_pantry_ondates($plan_startdate,$plan_enddate);

		$pantrygrodata = $gro_pandata['grocey'];
		$pantrypandata = $gro_pandata['pantry'];
echo json_encode(array("status"=>"success","msg"=>'Moved to grocey.',"pantrymove_asile"=>$pantrymove_asile,"pantrymove_name"=>$pantrymove_name,"pantrygrodata"=>$pantrygrodata,"pantrypandata"=>$pantrypandata));

	}

}	

// Move pantry to grocery end

// Delete all grocery list start

if (isset($_POST['deluseridgrolist']))
{

	global $wpdb;
	$sql_grocerylist_delete ="DELETE FROM ".$wpdb->prefix."grocerylist WHERE user_id='".$_POST['deluseridgrolist']."'";
	$sql_grocerylistdelete_result = $wpdb->query($sql_grocerylist_delete);

	$sql_pantrylist_delete ="DELETE FROM ".$wpdb->prefix."pantrylist WHERE user_id='".$_POST['deluseridgrolist']."'";
	$sql_pantrylistdelete_result = $wpdb->query($sql_pantrylist_delete);

	if($sql_grocerylistdelete_result){

		echo json_encode(array("status"=>"success","msg"=>'All Grocery List Deleted.'));

	}		

}	

// Delete all grocery list end

// Invite uses start
if (isset($_POST['nutritionists_id']) && isset($_POST['user_id']))
{
	global $wpdb;
	$invitation_date = date('Y/m/d');
	$sql_invite ="INSERT INTO " . $wpdb->prefix . "invite_user(nutritionist_id,user_id,invite_status,invitation_date,block_status) VALUES ('".$_POST['nutritionists_id']."','".$_POST['user_id']."','0','$invitation_date','Unblock')";
	$row=$wpdb->get_row($sql_invite);

    echo json_encode(array("status"=>"success","nvitation_status"=>"sent","user_id"=>$_POST['user_id'],"msg"=>"Invitation sent successfully."));
}
//  Invite uses start End

// Block user and nutritionist start

if (isset($_POST['block_currentuserid']) && isset($_POST['block_nutritionistid']))
{
	global $wpdb;
	$block_query = "select block_status from " . $wpdb->prefix . "invite_user where user_id='".$_POST['block_currentuserid']."' and nutritionist_id='".$_POST['block_nutritionistid']."' and invite_status='1'";
	$block_queryresult = $wpdb->get_results($block_query,ARRAY_A);

	//echo $block_queryresult[0]['block_status'];

	if('Block' == $block_queryresult[0]['block_status'])
	{

	 $sql_block_user ="UPDATE `".$wpdb->prefix."invite_user` set `block_status`='Unblock' WHERE `nutritionist_id` LIKE  '".$_POST['block_nutritionistid']."' and `user_id` LIKE  '".$_POST['block_currentuserid']."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
			
		
	echo json_encode(array("status"=>"success","msg"=>"Unblock"));	
	}
	else{

		$sql_block_user ="UPDATE `".$wpdb->prefix."invite_user` set `block_status`='Block' WHERE `nutritionist_id` LIKE  '".$_POST['block_nutritionistid']."' and `user_id` LIKE  '".$_POST['block_currentuserid']."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
		echo json_encode(array("status"=>"success","msg"=>"Block"));		
	}

    
}

// Block user and nutritionist end

// Nutritionist side block user start
if (isset($_POST['block_nutri_currentuserid']) && isset($_POST['block_nutri_usertid']))
{
	
	
	global $wpdb;
	$block_query = "select block_status from " . $wpdb->prefix . "invite_user where user_id='".$_POST['block_nutri_usertid']."' and nutritionist_id='".$_POST['block_nutri_currentuserid']."' and invite_status='1'";
	$block_queryresult = $wpdb->get_results($block_query,ARRAY_A);

	//echo $block_queryresult[0]['block_status'];

	if('Block' == $block_queryresult[0]['block_status'])
	{

	 $sql_block_user ="UPDATE `".$wpdb->prefix."invite_user` set `block_status`='Unblock' WHERE `nutritionist_id` LIKE  '".$_POST['block_nutri_currentuserid']."' and `user_id` LIKE  '".$_POST['block_nutri_usertid']."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
			
		
	echo json_encode(array("status"=>"success","msg"=>"Block"));	
	}
	else{

		$sql_block_user ="UPDATE `".$wpdb->prefix."invite_user` set `block_status`='Block' WHERE `nutritionist_id` LIKE  '".$_POST['block_nutri_currentuserid']."' and `user_id` LIKE  '".$_POST['block_nutri_usertid']."'";
			$row_block_user=$wpdb->get_row($sql_block_user);
		echo json_encode(array("status"=>"success","msg"=>"Unblock"));		
	}

    
}
// Nutritionist side block user end

?>
