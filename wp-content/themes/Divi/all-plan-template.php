<?php
/*
Template Name: All Plans Template 
*/
get_header(); 
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
  <div class="page-title-head"><h1 class="entry-title main_title"><?php the_title(); ?></h1></div>
  </div>
  <section class="pickplan">
  <div class="container">
    <div class="row">

<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$custom_args = array(
'post_type' => 'plan', 
'order' =>'ASC',
'paged' => $paged
);
$custom_query = new WP_Query( $custom_args ); ?>
<?php if ( $custom_query->have_posts() ) : ?> 

<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

      <div class="col-lg-6 comm-item hovereffect"> 
      <?php if( has_post_thumbnail( $post_id ) ): ?>
      <div class="post-image">
      <img class="wp-post-image" 
      src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="width:1000px; height:400px;">
      
       <? endif; ?>
      <div class="plan-title-head"><h1><?php the_title();?></h1></div>
     </div>
      <div class="disc_bottom overlay">
      
      <p><?php 
      $content = get_the_content();
      echo substr($content, 0, 300);
      ?></p>
      
      <a href="<?php echo get_permalink($post->ID)?>" title="Learn More" class="et_pb_promo_button et_pb_button hvr-shutter-in-horizontal">
      <span class="">VIEW PLAN</span></a>
       </div>
        </div>

  
<?php endwhile; ?>
<?php endif; ?>
  </div>
  </div>
</section>
<div class="et_pb_section et_pb_section_3 et_section_regular" id="testi_moniel">
<div align="center"><h1>What other people are saying<br> about Fit Like This</h1></div>
      <?php echo do_shortcode('[show-testimonials pagination="on" orderby="menu_order" order="ASC" post_status="publish" layout="slider" options="transition:horizontal,adaptive:false,controls:controls,pause:4000,auto:on,columns:1,theme:speech,info-position:info-below,text-alignment:center,review_title:on,quote-content:short,charlimitextra: (...),display-image:on,image-size:thumbnail,image-shape:circle,image-effect:transparency,image-link:on"]
      ')
      ?>
      </div>
      </div>
<?php get_footer(); ?>

