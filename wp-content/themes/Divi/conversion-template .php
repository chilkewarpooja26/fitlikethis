<?php
/*
Template Name: Conversion 
*/
get_header(); 
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
  <div class="page-title-head"><h1 class="entry-title main_title"><?php the_title(); ?></h1></div>
  </div>
  <section class="pickplan">
  <div class="container">
    <div class="row">

  <?php
  global $wpdb;
  $user_id = get_current_user_id();
  // $sql_get_data ="SELECT recipe_current_date,recipe_ingredients_value FROM ".$wpdb->prefix."meal_plan WHERE  user_id ='".$user_id."' AND recipe_current_date >='2017/09/22' AND recipe_current_date <= '2017/09/22' ";

  $sql_get_data ="SELECT recipe_current_date,recipe_ingredients_value FROM ".$wpdb->prefix."meal_plan WHERE  user_id ='".$user_id."' ";

  $recipe_result = $wpdb->get_results($sql_get_data, ARRAY_A);

  echo "<pre>"; 
  //print_r($recipe_result); exit();
  $afterconvert = array();
  $converted = array();
  $final_converted = array();
  for ($i=0; $i < count($recipe_result) ; $i++) { 
    
    //print_r(json_decode($recipe_result[$i]['recipe_ingredients_value'],TRUE));

    $ingredent_value = json_decode($recipe_result[$i]['recipe_ingredients_value'],TRUE);
    if($ingredent_value != null){
    $ingredent_date = $recipe_result[$i]['recipe_current_date'];
    for ($j=0; $j < count($ingredent_value); $j++) { 
        $convertvalue_unit = "";
        $convertvalue_amount = "";
       $convert_value = convert_measurement($ingredent_value[$j]['name'],$ingredent_value[$j]['amount'],$ingredent_value[$j]['unit']);
       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){

        $convertvalue_unit = $ingredent_value[$j]['unit'];
        $convertvalue_amount = $ingredent_value[$j]['amount'];
      }
      else{
        $convertvalue_unit = $convert_value['targetunit'];
        $convertvalue_amount = $convert_value['targetamount'];
      }
      unset($convert_value);
        $afterconvert['id'] = $ingredent_value[$j]['id'];
        $afterconvert['aisle'] = $ingredent_value[$j]['aisle'];
        $afterconvert['image'] = $ingredent_value[$j]['image'];
        $afterconvert['consistency'] = $ingredent_value[$j]['consistency'];
        $afterconvert['name'] = $ingredent_value[$j]['name'];
        // $afterconvert['amount'] = $ingredent_value[$j]['amount'];
        // $afterconvert['unit'] = $ingredent_value[$j]['unit'];
        $afterconvert['amount'] = $convertvalue_amount;
        $afterconvert['unit'] = $convertvalue_unit;
        $afterconvert['unitShort'] = $ingredent_value[$j]['unitShort'];
        $afterconvert['unitLong'] = $ingredent_value[$j]['unitLong'];
        // $afterconvert['originalString'] = $ingredent_value[$j]['originalString'];
        $converted[$j] = $afterconvert;
        unset($afterconvert);

    }
    print_r($converted);
    $final_converted = $converted;
    $sql_grocery_list ="insert into ".$wpdb->prefix."grocery_conversion(user_id,recipe_current_date,recipe_ingredients_value) VALUES ('".$user_id."','".$ingredent_date."','".json_encode($final_converted)."')";
  $sql_grocerylist_result = $wpdb->query($sql_grocery_list);
    unset($converted);
    unset($final_converted);
  } 
    

  }
  print_r($final_converted);

  ?>

  

  </div>
  </div>
</section>
<div class="et_pb_section et_pb_section_3 et_section_regular" id="testi_moniel">

      </div>
<?php get_footer(); ?>

