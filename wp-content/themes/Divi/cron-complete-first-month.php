<?php
/*
Template Name: Cron Complete First Month
*/

require_once('../../../wp-config.php');
global $wpdb;

$loginDate = array();
$date = date('Y/m/d');

$date_diff = array();

echo $name_query = "select nu_users.ID,nu_users.user_email, nu_payments.payment_date from nu_users join nu_payments where nu_users.ID = nu_payments.user_id and nu_payments.payment_status = 'completed'";

$name_result = $wpdb->get_results($name_query);

//print_r($name_result);

if($name_result)
{

	for($i = 0 ;$i<count($name_result);$i++)
	{
		$loginDate[$i] = $name_result[$i]->payment_date;
		$date1=date_create($loginDate[$i]);
		$date2=date_create($date);
		$diff=date_diff($date1,$date2);
		$date_diff[$i] = $diff->format("%a");

		if($date_diff[$i] == 30 )
		{

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// Additional headers
				$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

				$htmlContent = "Can you believe you have completed your first month?! Way to go! You are one step closer to reaching your goals!";

				$subject = 'Completing first month';
				

				//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
				//$status = true;


				wp_mail($name_result[$i]->user_email,$subject, $htmlContent, $headers);
		}

	}

	//print_r($loginDate);
}

