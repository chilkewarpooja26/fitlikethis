<?php
/*
Template Name: Cron Login Email
*/

require_once('../../../wp-config.php');

global $wpdb;
$loginDate = array();
$date = date('m/d/Y');

$date_diff = array();

$name_query = "select nu_users.ID,nu_users.user_email,nu_usermeta.meta_value from nu_users join nu_usermeta  where nu_users.ID = nu_usermeta.user_id and meta_key = '_um_last_login' and ( nu_users.weekly_email = '0' || nu_users.monthly_email='0')";


$name_result = $wpdb->get_results($name_query);

if($name_result)
{

	for($i = 0 ;$i<count($name_result);$i++)
	{
		$loginDate[$i] = date('m/d/Y',$name_result[$i]->meta_value);
		$date1=date_create($loginDate[$i]);
		$date2=date_create($date);
		$diff=date_diff($date1,$date2);
		$date_diff[$i] = $diff->format("%a");

		if($date_diff[$i] == 0 )
		{

			$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

						// Additional headers
						$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

						$htmlContent = "We understand life gets busy. Remember the vision you had for yourself when beginning our program. We know you can do it! Let's get you logged in and moving towards your goal! For helpful tools to keep you going strong, take part in our “Master Your Mindset” webinar series.";

						$subject = 'Have not logged into APP';
						

						//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
						//$status = true;


						wp_mail($name_result[$i]->user_email,$subject, $htmlContent, $headers);

						/*$ins_qry ="INSERT INTO nu_usermeta(user_id,meta_key,meta_value) VALUES(".$name_result[$i]->ID.",'weekly_email','1')";
    					$row=$wpdb->get_row($ins_qry);*/

    					$upd_qry ="UPDATE nu_users SET weekly_email='1' WHERE ID = '".$name_result[$i]->ID."'";
    					$row=$wpdb->get_row($upd_qry);


		}
		if($date_diff[$i] == 30 )
		{
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

						// Additional headers
						$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

						$htmlContent = "We noticed you have not logged into the app for 1 month. Setbacks are a normal process of a major lifestyle change. Do not get discouraged! We know you have the drive to reach your weight loss goals!";

						$subject = 'Have not logged into APP for a month';
						

						//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
						//$status = true;
						wp_mail($name_result[$i]->user_email,$subject, $htmlContent, $headers);

						$upd_qry ="UPDATE nu_users SET monthly_email='1' WHERE ID = '".$name_result[$i]->ID."'";
    					$row=$wpdb->get_row($upd_qry);
		}
	}
}




?>