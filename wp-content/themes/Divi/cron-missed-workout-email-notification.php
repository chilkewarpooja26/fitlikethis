<?php
/*
Template Name: Cron Missed Workout
*/
require_once('../../../wp-config.php');
global $wpdb;
$workoutDate = array();
$date = date('Y/m/d');

$date_diff = array();

$name_query = "SELECT nu_users.ID,nu_users.user_email,nu_add_workout.workout_date,nu_add_workout.missed_workout FROM nu_add_workout JOIN nu_users WHERE nu_add_workout.user_id = nu_users.ID ORDER BY nu_add_workout.workout_date DESC";


$name_result = $wpdb->get_results($name_query);

if($name_result)
{

	for($i = 0 ;$i<count($name_result);$i++)
	{
		$workoutDate[$i] = $name_result[$i]->workout_date;
		$date1=date_create($workoutDate[$i]);
		$date2=date_create($date);
		$diff=date_diff($date1,$date2);
		$date_diff[$i] = $diff->format("%a");

		if($date_diff[$i] == 7 || $date_diff[$i] == 14 || $date_diff[$i] == 21)
		{
			if($name_result[$i]->missed_workout == 0)
			{
				$htmlContent = "We noticed you forgot to complete any workouts this week. Focus on one workout at a time. You got this!";

				$upd_qry ="UPDATE nu_add_workout SET missed_workout='1' WHERE user_id = '".$name_result[$i]->ID."'";
    					
			}
			if($name_result[$i]->missed_workout == 1)
			{
				$htmlContent = "It has been a couple weeks since you have completed any workouts! No worries, we know life happens. Let's get you back on track to your goals next week!";

				$upd_qry ="UPDATE nu_add_workout SET missed_workout='2' WHERE user_id = '".$name_result[$i]->ID."'";
			}
			if($name_result[$i]->missed_workout == 2)
			{
				$htmlContent = "We haven't seen you in a while! Your exercise plan is an important part of your weight loss. In our “Master Your Mindset” series, there are tools to help keep you motivated and moving forward.";

				$upd_qry ="UPDATE nu_add_workout SET missed_workout='3' WHERE user_id = '".$name_result[$i]->ID."'";
			}

			if($name_result[$i]->missed_workout < 3)
			{

			

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// Additional headers
				$headers .= 'From: sanket@exceptionaire.co' . "\r\n";
				$subject = 'Missed Workouts';

				wp_mail($name_result[$i]->user_email,$subject, $htmlContent, $headers);

				$row=$wpdb->get_row($upd_qry);
			}



		}
	}
}

