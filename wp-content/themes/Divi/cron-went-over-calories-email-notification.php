<?php
/*
Template Name: Cron Went Over Calories
*/

require_once('../../../wp-config.php');
global $wpdb;

$lastDates = array();

for($i=1;$i<=3;$i++)
{

	$lastDates[$i] = date('Y/m/d',strtotime("-".$i." days"));
}

$dateData = implode(",",$lastDates);

$name_query = "SELECT user_id,last_three_days,email_count, sum( recipe_calories ) as sumCal FROM nu_meal_plan WHERE recipe_current_date
IN ('".$lastDates[1]."','".$lastDates[2]."','".$lastDates[3]."') GROUP BY user_id";

$name_result = $wpdb->get_results($name_query);

if($name_result)
{
	for($j=0;$j<count($name_result);$j++)
	{
		$lastThreeDays = explode(',',$name_result[$j]->last_three_days);

		$query = "select user_email from nu_users where ID = ".$name_result[$j]->user_id;
		$res = $wpdb->get_results($query);

		$queryCalories = "select old_calories from nu_plan_history where user_id = ".$name_result[$j]->user_id ." limit 0,1";

		$queryCaloriesRes = $wpdb->get_results($queryCalories);

		
		if($name_result[$j]->sumCal > $queryCaloriesRes[0]->old_calories)
		{
			
			if(!array_diff($lastThreeDays,$lastDates))
			{

				if($name_result[$j]->email_count == 0) 	
				{
					$htmlContent = "We noticed you are going over your recommended calorie level. No worries! Plan successfully tomorrow to get back on track!";



					$upd_qry ="UPDATE nu_meal_plan SET last_three_days='".$dateData."' and email_cnt = 1 WHERE ID = '".$name_result[$j]->user_id."'";
					$row=$wpdb->get_row($upd_qry);
				}

				if($name_result[$j]->email_count == 1) 	
				{
					$htmlContent = "Make sure you are not going above your personalized daily calorie budget. We want you to reach your goals as quickly as possible and reach your weight loss goals!";

					

					$upd_qry ="UPDATE nu_meal_plan SET last_three_days='".$dateData."' and email_cnt = 2 WHERE ID = '".$name_result[$j]->user_id."'";
					$row=$wpdb->get_row($upd_qry);
				}

				if($name_result[$j]->email_count == 2) 	
				{
					$htmlContent = "Unwanted weight gain may result from consuming too many calories on a regular basis. Reach your goals quicker by properly planning your meals!";

					

					$upd_qry ="UPDATE nu_meal_plan SET last_three_days='".$dateData."' and email_cnt = 3 WHERE ID = '".$name_result[$j]->user_id."'";
					$row=$wpdb->get_row($upd_qry);
				}
				if($name_result[$j]->email_count < 3)
				{
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					// Additional headers
					$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

					
					$subject = 'Went over calories';
					//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
					//$status = true;
					wp_mail($res[0]->user_email,$subject, $htmlContent, $headers);
				}

				

				
		 	}

		}

		
			
	}
}

//print_r($name_result);