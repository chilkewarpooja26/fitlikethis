<?php
/*
Template Name: Cron Within Your FitFood Budget
*/

require_once('../../../wp-config.php');
global $wpdb;

$lastDates = array();

for($i=1;$i<=3;$i++)
{

	$lastDates[$i] = date('Y/m/d',strtotime("-".$i." days"));
}

$dateData = implode(",",$lastDates);



$name_query = "SELECT user_id,last_three_days,email_count, sum( recipe_calories ) as sumCal FROM nu_meal_plan WHERE recipe_current_date
IN ('".$lastDates[1]."','".$lastDates[2]."','".$lastDates[3]."') GROUP BY user_id";

$name_result = $wpdb->get_results($name_query);

if($name_result)
{
	for($j=0;$j<count($name_result);$j++)
	{


		$lastThreeDays = explode(',',$name_result[$j]->last_three_days);

		$query = "select user_email from nu_users where ID = ".$name_result[$j]->user_id;
		$res = $wpdb->get_results($query);

		$queryCalories = "select old_calories from nu_plan_history where user_id = ".$name_result[$j]->user_id ." limit 0,1";

		$queryCaloriesRes = $wpdb->get_results($queryCalories);

		
		if($name_result[$j]->sumCal > $queryCaloriesRes[0]->old_calories)
		{
			
			if(!array_diff($lastThreeDays,$lastDates))
			{

				$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					$subject = 'Hit calories 3 days in a row';

					// Additional headers
					$headers .= 'From: sanket@exceptionaire.co' . "\r\n";

					$htmlContent = 'You successfully stayed within your FitFood budget for 3 days in a row! Keep this up and you will reach your goals in no time!';
					

					//$headers .= 'Cc: sanket@exceptionaire.co' . "\r\n";
					//$status = true;
					wp_mail($res[0]->user_email,$subject, $htmlContent, $headers);

					$upd_qry ="UPDATE nu_meal_plan SET last_three_days='".$dateData."' WHERE ID = '".$name_result[$j]->user_id."'";
					$row=$wpdb->get_row($upd_qry);
				

				
		 	}

		}

		
			
	}
}