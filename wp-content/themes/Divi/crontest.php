<?php
	require_once('../../../wp-config.php');
	
	global $wpdb;
	//die();
	//mail("abhijeet@exceptionaire.co","My subject","Hi");
	// die();
	
	$getuserid= "SELECT `user_id`,`meta_value` FROM `nu_usermeta` WHERE `meta_key`='remain_plan'";
	$userdata = $wpdb->get_results($getuserid,ARRAY_A);

	
	$dataresult = $userdata;
	$update_remianplan_userid = "";
	for ($i=0; $i < count($userdata); $i++) { 
			
			if($dataresult[$i]['meta_value']){
			$re_userid = $dataresult[$i]['user_id'];
			$update_remianplan_userid = $re_userid;
			$re_dates = json_decode($dataresult[$i]['meta_value'],TRUE);

			$start_plandate = $re_dates['re_plan_startdate'];
			$end_planddate = $re_dates['re_plan_enddate'];

			$begin = new DateTime( $start_plandate );
			$end = new DateTime( $end_planddate );
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end->modify('+1 day'));

			foreach ( $period as $dt ){
 				
 				$plans = array("Breakfast","Lunch","Dinner","Snacks");

		 				foreach ($plans as $plan) 
					{		
							$allergiesone = get_user_meta($re_userid,"allergiesone", true);
					        $exclude_string = "";
					        for ($ea=0; $ea < count($allergiesone); $ea++) { 
					          $exclude_string .= str_replace(" ","+",$allergiesone[$ea]).",";
					        }
					        $exclude_allergies = "exclude=".$exclude_string;
					        $exclude_allergies = rtrim($exclude_allergies,",");

							$mealtype = "";
					 		if("Breakfast" == $plan){ $mealtype = "Breakfast";}
					 		if("Lunch" == $plan){ $mealtype = "main+course";}
					 		if("Dinner" == $plan){ $mealtype = "main+course";}
					 		if("Snacks" == $plan){ $mealtype = "appetizer";}

							$url = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/mealplans/generate?'.$exclude_allergies.'&timeFrame=day&type='.$mealtype.'';

						    $ch = curl_init();
						    curl_setopt($ch, CURLOPT_URL, $url);
						   // curl_setopt($ch, CURLOPT_HTTPGET, true);
						    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						    'X-Mashape-Key: VLnwtLFLEcmsh1uAeQuPgf14IDFvp1DxB1ejsnZgth5ONUAKYh',
						    'Accept: application/json'
						    ));

						    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
						    curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
						    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0'); 
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
						    $body = curl_exec($ch);
						    $info = curl_getinfo($ch);
						    $error = curl_errno($ch);
						    // print_r('Curl error: ' . curl_error($ch));

						    curl_close($ch); 
						    
						    $spoon_result_array = json_decode($body, TRUE);

							

							$array_spoonacular = array_slice($spoon_result_array['meals'], 0, 2);

							foreach ($array_spoonacular as $value) {
							  
							  $recipe_id = $value['id'];
							  $recipe_title = $value['title'];
							  $recipe_image = "https://spoonacular.com/recipeImages/".$value['image'];

							  
							  $spoonacula_getsechit = spoonacula_getsechit($recipe_id);

							  $spoonacula_getsechit = json_decode($spoonacula_getsechit, TRUE);

							  $nutrients = $spoonacula_getsechit['nutrition']['nutrients'];

							  $countattr = count($nutrients);
							  $stringnutrition = array();
							  for($i=0;$i<$countattr;$i++)
							  {

							    if("Calories" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
							    {
							        $stringnutrition['calories'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
							    }
							    if("Fat" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
							    {
							        $stringnutrition['fat'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
							    }        
							    if("Carbohydrates" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
							    {
							        $stringnutrition['carbohydrates'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
							    }
							      if("Protein" == $spoonacula_getsechit['nutrition']['nutrients'][$i]['title'] )
							    {
							        $stringnutrition['protein'] = $spoonacula_getsechit['nutrition']['nutrients'][$i]['amount'];
							    }
							  }


							  $meal_type = $plan;
							  $recipe_image = $recipe_image;
							  $recipe_name = str_replace("+"," ",$recipe_title);
							  $recipe_protine = $stringnutrition['protein'];
							  $recipe_carbs = $stringnutrition['carbohydrates'];
							  $recipe_fats = $stringnutrition['fat'];
							  $recipe_calories = $stringnutrition['calories'];
							  $recipe_ingredients_lines = json_encode($spoonacula_getsechit['extendedIngredients']);

							  // Conversion grocery START
							  				  
							  $afterconvert = array();
							  $converted = array();
							  
							  $final_ingredientline = json_decode($recipe_ingredients_lines,TRUE);
							 
							  for ($gg=0; $gg< count($final_ingredientline) ; $gg++) {


							  	$convertvalue_unit = "";
							    $convertvalue_amount = "";
							    $convert_value = convert_measurement($final_ingredientline[$gg]['name'],$final_ingredientline[$gg]['amount'],$final_ingredientline[$gg]['unit']);
							       if($convert_value['targetamount'] == 0 && $convert_value['targetamount'] == ''){

							        $convertvalue_unit = $final_ingredientline[$gg]['unit'];
							        $convertvalue_amount = $final_ingredientline[$gg]['amount'];
							      }
							      else{
							        $convertvalue_unit = $convert_value['targetunit'];
							        $convertvalue_amount = $convert_value['targetamount'];
							      }
							      unset($convert_value);

							    $afterconvert['id'] = $final_ingredientline[$gg]['id'];
							  	$afterconvert['name'] = $final_ingredientline[$gg]['name'];
							  	$afterconvert['aisle'] = $final_ingredientline[$gg]['aisle'];
							  	$afterconvert['image'] = $final_ingredientline[$gg]['image'];
							  	$afterconvert['consistency'] = $final_ingredientline[$gg]['consistency'];
							  	$afterconvert['amount'] = $convertvalue_amount;
							  	$afterconvert['unit'] = $convertvalue_unit;
							  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];
							  	$afterconvert['unitLong'] = $final_ingredientline[$gg]['unitLong'];
							  	$afterconvert['unitShort'] = $final_ingredientline[$gg]['unitShort'];

							  	$converted[$gg] = $afterconvert;
							    unset($afterconvert);
							  }
							  
							  $recipe_ingredients_lines = json_encode($converted);
							  unset($converted);
							  

							  // Conversion grocery END

							  $original_recipe_numberofservings = $spoonacula_getsechit['servings'];
							  $recipe_numberofservings = 1;
							  $recipe_source_recipe_url = $spoonacula_getsechit['spoonacularSourceUrl'];
							  $direction = json_encode($spoonacula_getsechit['analyzedInstructions']);
							  $recipe_current_date = $dt->format("Y/m/d");

							  if($recipe_calories){$recipe_calories = $recipe_calories;}else{ $recipe_calories = 0; }
							  if($recipe_protine){$recipe_protine = $recipe_protine;}else{ $recipe_protine = 0; }
							  if($recipe_carbs){$recipe_carbs = $recipe_carbs;}else{ $recipe_carbs = 0; }
							  if($recipe_fats){$recipe_fats = $recipe_fats;}else{ $recipe_fats = 0; }
							  if($direction){$direction = $direction;}else{ $direction = 0; }

							  if($recipe_calories != 0 || $recipe_protine != 0 || $recipe_carbs != 0 || $recipe_fats !=0){
							  $sql_check1 ="INSERT INTO ".$wpdb->prefix."meal_plan(user_id,meal_type,recipe_name,recipe_image,recipe_calories,recipe_protien,recipe_fats,recipe_carbs,recipe_ingredients_value,recipe_serving,original_serving,recipe_id,source_recipe_url,direction,favorite_status,blocked_status,recipe_current_date) VALUES ('$re_userid','$meal_type','$recipe_name','$recipe_image','$recipe_calories','$recipe_protine','$recipe_fats','$recipe_carbs','$recipe_ingredients_lines','$recipe_numberofservings','$original_recipe_numberofservings','$recipe_id','$recipe_source_recipe_url','$direction','0','0','$recipe_current_date')";
							  $row=$wpdb->get_row($sql_check1);
							}
							}
					}	
					$plan_payment  = "SELECT calories,carb_gram,protein_gram,fat_gram FROM ".$wpdb->prefix."payments where user_id = '".$re_userid."' order by payment_id DESC";
				            $row_plan_payment = $wpdb->get_row($plan_payment);

				            $plan_history ="insert into ".$wpdb->prefix."plan_history(user_id,old_calories,old_protien, old_fats,old_carbs,old_date) VALUES ('".$re_userid."','".$row_plan_payment->calories."','".$row_plan_payment->protein_gram."','".$row_plan_payment->fat_gram."','".$row_plan_payment->carb_gram."','".$recipe_current_date."')";
					        $row_plan_history=$wpdb->query($plan_history);

			}//end foreach

					update_user_meta($update_remianplan_userid, 'remain_plan',0);

			}else{

				echo "There is not anything for generate.";
			}
		}	

?>