<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 *  Template Name: forgate password 
 */
 get_header();
 ?> 
 
 <div class="cd-user-modal">
 <div class="modal-content" id="forgot_show" >
    <div class="modal-header">
        <button id="btn-login" class="close mfp-login-close" type="button"><i class="fa fa-times"></i></button>
        <h4 class="modal-title">Forgot Password</h4>
    </div>
    
    <div class="modal-body">
         <div class="cd-user-modal">
        <div id="cd-reset-password">
            <form name="frmlogin" id="frmlogin" role="form" class="cd-form">
                <p class="fieldset">
                    <span class="common-success-message hide"></span>
                </p>
                <div class="col-md-12 login_box">
                    <div id="login-reg-msg"></div>
                    <div id="login_success_msg_for_activation_link"></div>
                    <div class="form-group">
                        <label for="reset-email" class="image-replace cd-email"><i class="fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
                        <input class="full-width has-padding has-border form-control" id="reset-email" type="email" placeholder="<?php _e('E-mail', 'nutrafit'); ?>">
                        <span class="cd-error-message">Error message here!</span>
                    </div>
                    <div class="login_btn"><span><i class="fa fa-sign-in"></i></span>
                        <input class="full-width has-padding" id="reset-submit" type="submit" value="<?php _e('Reset password', 'nutrafit'); ?>">
                        <img src="<?php echo NU_LOADER_CYCLE; ?>" id="loader" class="H-loader" style="display: none;" />
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>
<!-- Modal content loginsecreen_show-->

</div>

 <?php
         get_footer();
         ?>

