<?php 
/*
Template Name: Grocery Template 
*/
get_header(); 
  
  $user_id = get_current_user_id();
  global $wpdb;
  $sql_grocery_dates ="SELECT start_date,end_date,last_modified FROM ".$wpdb->prefix."grocerylist WHERE user_id ='".$user_id."' ORDER BY start_date DESC";
  $grocery_datesresult = $wpdb->get_results($sql_grocery_dates, ARRAY_A);

  $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$user_id."'";
  $getdate = $wpdb->get_row($getplans_date);
  $startplan_date = date_create($getdate->payment_date);
  $endplan_date = date_create($getdate->plan_expire_date);
?>

          <div id="main-content" class="inner-pages-section">
            <div class="sub-banner dash-sect">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
              <div class="page-title-head">
                <h1 class="entry-title main_title">Groceries</h1>
              </div>
            </div>

            <section class="groceries">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 dash-sect-title"><h1 id="glist">Grocery Lists</h1></div>

                  <?php  //$get_grocerydata = get_grocery_ondates($start_date,$end_date);
                  //$get_pantrydata = get_pantry_ondates($start_date,$end_date);
                    //echo 'get_grocerydata<pre> - '; print_r($get_grocerydata);
                  //echo 'get_pantrydata<pre> - '; print_r($get_pantrydata);

                    //echo 'count - '.count(array_keys($get_grocerydata));

                    // $get_keys = array_keys($get_grocerydata);

                    // for ($ii=0; $ii < count($get_keys); $ii++) { 
                      
                    //   //echo '<br><b>'.$get_keys[$ii].'</b>';

                    //   //echo '<pre>'; print_r($get_grocerydata[$get_keys[$ii]]);
                    //   $get_values = $get_grocerydata[$get_keys[$ii]];
                    //   for ($j=0; $j < count($get_grocerydata[$get_keys[$ii]]); $j++) { 
                        
                    //    //echo '<br>'. $get_values[$j]['aisle'];

                    //     if($get_keys[$ii] == $get_values[$j]['aisle']){

                    //       // echo '<br> name- '.$get_values[$j]['name'];
                    //       // echo '<br> unit- '.$get_values[$j]['unit'];
                    //       // echo '<br> image- '.$get_values[$j]['image'];
                    //       // echo '<br> aisle- '.$get_values[$j]['aisle'];
                    //       // echo '<br> amount- '.$get_values[$j]['amount'];
                    //       // echo "<br>string////////////////////////////////////";

                    //     }

                    //   }

                    // }

                    ?>

                  <div class="col-md-12 col-sm-12 list-create">
                  <div class="cnteralign">
                    <div class="col-sm-12 col-md-12 list-create-inner">
                      <div class="top-btn">
                      <div class="date-picker">
                            <div class="well configurator hidden">
                               
                              <form>
                              <div class="row">

                                <div class="col-md-4">

                                  <div class="form-group">
                                    <label for="parentEl">parentEl</label>
                                    <input type="text" class="form-control" id="parentEl" value="" placeholder="body">
                                  </div>

                                  <div class="form-group">
                                    <label for="startDate">startDate</label>
                                    <input type="text" class="form-control" id="startDate" value="">
                                  </div>

                                  <div class="form-group">
                                    <label for="endDate">endDate</label>
                                    <input type="text" class="form-control" id="endDate" value="">
                                  </div>

                                  <div class="form-group">
                                    <label for="minDate">minDate</label>
                                    <input type="text" class="form-control" id="minDate" value="<?php echo date_format($startplan_date,"m/d/Y");?>" placeholder="MM/DD/YYYY">
                                  </div>

                                  <div class="form-group">
                                    <label for="maxDate">maxDate</label>
                                    <input type="text" class="form-control" id="maxDate" value="<?php echo date_format($endplan_date,"m/d/Y");?>" placeholder="MM/DD/YYYY">
                                  </div>

                                </div>
                                <div class="col-md-4">

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="autoApply"> autoApply
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="singleDatePicker"> singleDatePicker
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showDropdowns"> showDropdowns
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showWeekNumbers"> showWeekNumbers
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showISOWeekNumbers"> showISOWeekNumbers
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePicker"> timePicker
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePicker24Hour"> timePicker24Hour
                                    </label>
                                  </div>

                                  <div class="form-group">
                                    <label for="timePickerIncrement">timePickerIncrement (in minutes)</label>
                                    <input type="text" class="form-control" id="timePickerIncrement" value="1">
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="timePickerSeconds"> timePickerSeconds
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="dateLimit"> dateLimit (with example date range span)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="ranges"> ranges (with example predefined ranges)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="locale"> locale (with example settings)
                                    </label>
                                    <label id="rtl-wrap">
                                      <input type="checkbox" id="rtl"> RTL (right-to-left)
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="alwaysShowCalendars"> alwaysShowCalendars
                                    </label>
                                  </div>

                                </div>
                                <div class="col-md-4">

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="linkedCalendars" checked="checked"> linkedCalendars
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="autoUpdateInput" checked="checked"> autoUpdateInput
                                    </label>
                                  </div>

                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" id="showCustomRangeLabel" checked="checked"> showCustomRangeLabel
                                    </label>
                                  </div>

                                  <div class="form-group">
                                    <label for="opens">opens</label>
                                    <select id="opens" class="form-control">
                                      <option value="right" selected>right</option>
                                      <option value="left">left</option>
                                      <option value="center">center</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="drops">drops</label>
                                    <select id="drops" class="form-control">
                                      <option value="down" selected>down</option>
                                      <option value="up">up</option>
                                    </select>
                                  </div>

                                  <div class="form-group">
                                    <label for="buttonClasses">buttonClasses</label>
                                    <input type="text" class="form-control" id="buttonClasses" value="btn btn-sm">
                                  </div>

                                  <div class="form-group">
                                    <label for="applyClass">applyClass</label>
                                    <input type="text" class="form-control" id="applyClass" value="btn-success">
                                  </div>

                                  <div class="form-group">
                                    <label for="cancelClass">cancelClass</label>
                                    <input type="text" class="form-control" id="cancelClass" value="btn-default">
                                  </div>

                                </div>

                              </div>
                              </form>

                            </div>

                            <div class="row">

                              <div class="col-md-12 demo">
                                 <label class="cal-text">Create New List</label>
                                 <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/regeneatelod.gif" class="groceryloading" id="loadinggrocery">
                                
                                <div class="inptfild"><input type="text" id="config-demo" class="form-control" placeholder="Create New List" value="">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                </div>
                              </div>

                              <div class="col-md-6 hidden">
                                <h4>Configuration</h4>

                                <div class="well">
                                  <textarea id="config-text" style="height: 300px; width: 100%; padding: 10px"></textarea>
                                </div>
                              </div>

                            </div>

                          </div>
                      </div>
                     <!--  <div class="left-btn">
                        <button type="button" class="btn btn-primary">Create New List</button>
                      </div> -->
                      <div class="right-btn">   
                        <button type="button" class="btn btn-primary" onclick="deleteallgro();">Delete All Lists</button>
                      </div>

                      <div class="col-md-12 col-sm-12 date-list" id="grocerydatelist">
                      <?PHP 
                       //echo "<pre>"; echo 'Count - '.count($grocery_datesresult); print_r($grocery_datesresult);
                      $startingdate = '';
                      $endingdate = '';
                      $get_grocerydata = '';
                      if($grocery_datesresult){

                        for ($i=0; $i < count($grocery_datesresult); $i++) { 
                            
                            // echo '<br>'.$grocery_datesresult[$i]['start_date'];
                            // echo '<br>'.$grocery_datesresult[$i]['end_date'];
                            // echo '<br>'.$grocery_datesresult[$i]['last_modified'];

                            $gro_startdate = $grocery_datesresult[$i]['start_date'];
                            $gro_enddate = $grocery_datesresult[$i]['end_date'];

                            if(0 == $i){

                              
                              $get_grocerydata = get_grocery_ondates($gro_startdate,$gro_enddate);
                              $get_pantrydata = get_pantry_ondates($gro_startdate,$gro_enddate);
                              $startingdate = $grocery_datesresult[$i]['start_date'];
                              $endingdate = $grocery_datesresult[$i]['end_date'];
                              $classactive = $i;
                            }

                            $month_startdate_name = date('F',strtotime($grocery_datesresult[$i]['start_date']));

                            $month_startdate_day = date('d',strtotime($grocery_datesresult[$i]['start_date'])); 

                            $last_modified_day = date('d',strtotime($grocery_datesresult[$i]['last_modified']));   
                            $last_modified_month = date('F',strtotime($grocery_datesresult[$i]['last_modified']));
                            // echo '<br>month name - '.$month_name; 
                            // echo '<br>month day '.$month_day;
                            // echo '<br>Last modified '.$last_modified;

                            $month_enddate_name = date('F',strtotime($grocery_datesresult[$i]['end_date']));
                            $month_enddate_day = date('d',strtotime($grocery_datesresult[$i]['end_date'])); 
                            
                         ?>
                        
                          <div id="dletedlist<?php echo str_replace("/", "", $gro_startdate.$gro_enddate);?>">
                            <a href="javascript:void(0)" onclick="getgrocerydata('<?php echo $grocery_datesresult[$i]['start_date'];?>','<?php echo $grocery_datesresult[$i]['end_date'];?>');"> 
                              
                              <div class="col-md-8 col-sm-8 <?php if(0==$classactive){echo 'activegrolist'; $classactive++;} ?>" id="listgrodates">
                              <input type="text" style="display: none;" value="<?php echo $grocery_datesresult[$i]['start_date']; ?>" name="startgrodate"></input>
                              <input type="text" style="display: none;" value="<?php echo $grocery_datesresult[$i]['end_date']; ?>" name="endgrodate"></input>
                                <div class="sltuser">
                                  <p class="ltsite"><span id="grocey_startdate<?php echo str_replace("/", "",$grocery_datesresult[$i]['start_date']); ?>"><?php echo $month_startdate_name." ".$month_startdate_day; ?></span>
                                  <span> - </span>
                                  <span id="grocey_enddate<?php echo str_replace("/", "",$grocery_datesresult[$i]['end_date']);?>"><?php echo $month_enddate_name." ".$month_enddate_day; ?></span>
                                  </p>
                                  <span class="label"><span class="lat">Last Modified -</span> <span id="lastmodified"><?php echo $last_modified_day." ".$last_modified_month; ?></span></span>
                                </div>
                              </div>
                            </a>   
                              <div class="col-md-4 col-sm-4 delet-btn">
                                <button type="button" class="btn btn-primary" onclick="deletegroceylist('<?php echo $grocery_datesresult[$i]['start_date'];?>','<?php echo $grocery_datesresult[$i]['end_date'];?>');">Delete</button> 
                              </div>
                             
                          </div>
                       
                        <?php }
                            }
                         ?>
                      </div>
                      </div>
                       </div>
                     <!--create-inner--> 

                     <?php

                     ?>
                     
                      <div class="col-md-12 col-sm-12 grocery-moving-content">
                        <div class="col-md-6 col-sm-6 moving-left">
                          <div class="grocery-inner">
                            <h1>Grocery List</h1>

                            <div class="grocery-content">
                              <div class="col-sm-12 col-md-12 gro-top">
                                <a href="javascript:void(0)" id="movtopantry" onclick="movetopantry();"><p class="move-title" id="mtop">Move to Pantry</p></a>
                              </div>

                              <div class="col-sm-12 col-md-12 gro-bottom" id="boxscroll5">
                              <div id="createdgrocery">
                                <ul class="listsect">
                                    <li class="firstsltall">
                                      <!-- <label for="select_all"> -->
                                          <input type="checkbox" name="grocerycheck[]" class="checkbox" id="select_all"/>
                                         <!--  <i></i> --> <span> Select All</span> 
                                     <!--  </label> -->
                                    </li>
                                    <div id="getgroceryingredents">
                                    <?php 
                                      if($get_grocerydata){
                                      $get_keys = array_keys($get_grocerydata);
                                      }
                                      for ($ii=0; $ii < count($get_keys); $ii++) {

                                        $get_values = $get_grocerydata[$get_keys[$ii]];

                                        if(count($get_values)){

                                          echo '<li class="title-text">'.$get_keys[$ii].'</li>';

                                        }
                                        
                                        for ($j=0; $j < count($get_grocerydata[$get_keys[$ii]]); $j++) {
                                        
                                            if($get_keys[$ii] == $get_values[$j]['aisle']){
                                    ?>
                                    
                                    <li id="moveitem<?php echo $get_values[$j]['id'];?>">
                                     <!--  <label for="<?php echo $get_values[$j]['id'];?>">
                                          <input type="checkbox" name="check[]" onclick="getData('<?php echo $get_values[$j]['id'];?>');" class="grocheckbox" id="<?php echo $get_values[$j]['id'];?>">
                                          <i></i> 
                                          <span> <img src="<?php echo $get_values[$j]['image'];?>" id="imageid-<?php echo $get_values[$j]['id'];?>" class="img-block">
                                          
                                          </span> 
                                          <span style="display:none;" id="aisleid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_keys[$ii];?></span>
                                          <span id="nameid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_values[$j]['name'];?></span>
                                      </label>
 -->

                                      
                                          <input type="checkbox" name="check[]" onclick="getData('<?php echo $get_values[$j]['id'];?>');" class="grocheckbox" id="<?php echo $get_keys[$ii].'*$#'.$get_values[$j]['name'];?>">
                                      
                                          <span class="pic"> <img src="<?php echo $get_values[$j]['image'];?>" id="imageid-<?php echo $get_values[$j]['id'];?>" class="img-block">
                                          
                                          </span> 
                                          <span class="ingriname" style="display:none;" id="aisleid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_keys[$ii];?></span>
                                          <span class="ingriname" id="nameid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_values[$j]['name'];?></span>
                                     


                                       <p class="infoinstrc">
                                        <span class="ingreamount" id="amountid-<?php echo $get_values[$j]['id'];?>"><?php echo round($get_values[$j]['amount'], 2);?></span>
                                        <span class="ingreunit" id="unitid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_values[$j]['unit'];?></span>
                                      </p>
                                    </li>
                                    <?php 
                                        }
                                      }
                                    } ?>
                                    </div>
                                  </ul>
                                </div>
                                <script>
                                  //select all checkboxes
                                  $("#select_all").change(function(){  //"select all" change
                                      $(".grocheckbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
                                  });

                                  //".checkbox" change
                                  $('.grocheckbox').change(function(){
                                      //uncheck "select all", if one of the listed checkbox item is unchecked
                                      if(false == $(this).prop("checked")){ //if this item is unchecked
                                          $("#select_all").prop('checked', false); //change "select all" checked status to false
                                      }
                                      //check "select all" if all checkbox items are checked
                                      if ($('.grocheckbox:checked').length == $('.grocheckbox').length ){
                                          $("#select_all").prop('checked', true);
                                      }
                                  }); 
                                </script>

                              </div>

                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 moving-right">
                          <div class="grocery-inner">
                            <h1>Pantry</h1>

                            <div class="grocery-content">
                              <div class="col-sm-12 col-md-12 gro-top">
                               <a href="javascript:void(0)" onclick="movetogrocery();"> <p class="move-title" id="mtog">Move to Grocery</p></a>
                              </div>

                              <div class="col-sm-12 col-md-12 gro-bottom" id="boxscroll6">
                              <div id="createdpantry">
                                  <ul class="listsect">
                                    <li class="firstsltall">
                                      <!-- <label for="select_all1"> -->
                                          <input type="checkbox" name="pantrycheck[]" class="checkbox1" id="select_all1"/>
                                          <!-- <i></i> --> <span> Select All</span> 
                                      <!-- </label> -->
                                    </li>
                                    <div id="getpantryingredents">
                                    <?php 
                                      if($get_pantrydata){
                                      $get_keys = array_keys($get_pantrydata);
                                      }
                                      for ($ii=0; $ii < count($get_keys); $ii++) {

                                        $get_values = $get_pantrydata[$get_keys[$ii]];
                                        
                                        if(count($get_values)){
                                            echo '<li class="title-text">'.$get_keys[$ii].'</li>';
                                        }
                                        
                                        for ($j=0; $j < count($get_pantrydata[$get_keys[$ii]]); $j++) {
                                        
                                            if($get_keys[$ii] == $get_values[$j]['aisle']){
                                    ?>
                                    
                                    <!-- <li>
                                      <label for="<?php echo $get_values[$j]['id'];?>">
                                          <input type="checkbox" name="check1[]" onclick="getpantryData('<?php echo $get_values[$j]['id'];?>');" class="pancheckbox1" id="<?php echo $get_values[$j]['id'];?>">
                                          <i></i> 
                                          <span> <img src="<?php echo $get_values[$j]['image'];?>" id="pantryimageid-<?php echo $get_values[$j]['id'];?>" class="img-block"></span> 
                                          <span style="display:none;" id="pantryaisleid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_keys[$ii];?></span>
                                          <span id="pantrynameid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_values[$j]['name'];?></span>
                                      </label>
                                      <p>
                                        <span class="ingreamount" id="pantryamountid-<?php echo $get_values[$j]['id'];?>"><?php echo round($get_values[$j]['amount'], 2);?></span>
                                        <span class="ingreunit" id="pantryunitid-<?php echo $get_values[$j]['id'];?>" ><?php echo $get_values[$j]['unit'];?></span>
                                      </p>
                                    </li> -->

                                    <li>
                                      
                                          <input type="checkbox" name="check1[]" onclick="getpantryData('<?php echo $get_values[$j]['id'];?>');" class="pancheckbox1" id="<?php echo $get_keys[$ii].'*$#'.$get_values[$j]['name'];?>">
                                          
                                         <span class="pic"> <img src="<?php echo $get_values[$j]['image'];?>" id="pantryimageid-<?php echo $get_values[$j]['id'];?>" class="img-block"></span> 
                                          <span class="ingriname" style="display:none;" id="pantryaisleid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_keys[$ii];?></span>
                                          <span class="ingriname" id="pantrynameid-<?php echo $get_values[$j]['id'];?>"><?php echo $get_values[$j]['name'];?></span>
                                      
                                     <p class="infoinstrc">
                                        <span class="ingreamount" id="pantryamountid-<?php echo $get_values[$j]['id'];?>"><?php echo round($get_values[$j]['amount'], 2);?></span>
                                        <span class="ingreunit" id="pantryunitid-<?php echo $get_values[$j]['id'];?>" ><?php echo $get_values[$j]['unit'];?></span>
                                      </p>
                                    </li>

                                    <?php 
                                        }
                                      }
                                    } ?>
                                    </div>
                                  </ul>
                                </div>  
                              </div>

                              <script>
                                  //select all checkboxes
                                  $("#select_all1").change(function(){  //"select all" change
                                      $(".pancheckbox1").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
                                  });

                                  //".checkbox" change
                                  $('.pancheckbox1').change(function(){
                                      //uncheck "select all", if one of the listed checkbox item is unchecked
                                      if(false == $(this).prop("checked")){ //if this item is unchecked
                                          $("#select_all1").prop('checked', false); //change "select all" checked status to false
                                      }
                                      //check "select all" if all checkbox items are checked
                                      if ($('.pancheckbox1:checked').length == $('.pancheckbox1').length ){
                                          $("#select_all1").prop('checked', true);
                                      }
                                  }); 
                                </script>

                            </div>
                          </div>      
                        </div><!--moving-right-->
                      </div><!--grocery-moving-content-->

                    
                  </div>
                </div> 
        </div>              
            </section>



  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/css/daterangepicker.css" />
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.nicescroll.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {

         $("#config-demo").focus(function() {
            $('.daterangepicker').addClass("notranslate"); 
         }); 
         

        $('#config-text').keyup(function() {
          eval($(this).val());
        });
        
        $('.configurator input, .configurator select').change(function() {
          updateConfig();
        });

        $('.demo i').click(function() {
          $(this).parent().find('input').click();
        });

        $('#startDate').daterangepicker({
          singleDatePicker: true,
         // startDate: moment().subtract(6, 'days')
        });

        $('#endDate').daterangepicker({
          singleDatePicker: true,
         // startDate: moment()
        });

        updateConfig();

        function updateConfig() {
          var options = {};

          if ($('#singleDatePicker').is(':checked'))
            options.singleDatePicker = true;
          
          if ($('#showDropdowns').is(':checked'))
            options.showDropdowns = true;

          if ($('#showWeekNumbers').is(':checked'))
            options.showWeekNumbers = true;

          if ($('#showISOWeekNumbers').is(':checked'))
            options.showISOWeekNumbers = true;

          if ($('#timePicker').is(':checked'))
            options.timePicker = true;
          
          if ($('#timePicker24Hour').is(':checked'))
            options.timePicker24Hour = true;

          if ($('#timePickerIncrement').val().length && $('#timePickerIncrement').val() != 1)
            options.timePickerIncrement = parseInt($('#timePickerIncrement').val(), 10);

          if ($('#timePickerSeconds').is(':checked'))
            options.timePickerSeconds = true;
          
          if ($('#autoApply').is(':checked'))
            options.autoApply = true;

          if ($('#dateLimit').is(':checked'))
            options.dateLimit = { days: 7 };

          if ($('#ranges').is(':checked')) {
            options.ranges = {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            };
          }

          if ($('#locale').is(':checked')) {
            $('#rtl-wrap').show();
            options.locale = {
              direction: $('#rtl').is(':checked') ? 'rtl' : 'ltr',
              format: 'MM/DD/YYYY HH:mm',
              separator: ' - ',
              applyLabel: 'Apply',
              cancelLabel: 'Cancel',
              fromLabel: 'From',
              toLabel: 'To',
              customRangeLabel: 'Custom',
              daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
              monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
              firstDay: 1
            };
          } else {
            $('#rtl-wrap').hide();
          }

          if (!$('#linkedCalendars').is(':checked'))
            options.linkedCalendars = false;

          if (!$('#autoUpdateInput').is(':checked'))
            options.autoUpdateInput = false;

          if (!$('#showCustomRangeLabel').is(':checked'))
            options.showCustomRangeLabel = false;

          if ($('#alwaysShowCalendars').is(':checked'))
            options.alwaysShowCalendars = true;

          if ($('#parentEl').val().length)
            options.parentEl = $('#parentEl').val();

          if ($('#startDate').val().length) 
            options.startDate = $('#startDate').val();

          if ($('#endDate').val().length)
            options.endDate = $('#endDate').val();
          
          if ($('#minDate').val().length)
            options.minDate = $('#minDate').val();

          if ($('#maxDate').val().length)
            options.maxDate = $('#maxDate').val();

          if ($('#opens').val().length && $('#opens').val() != 'right')
            options.opens = $('#opens').val();

          if ($('#drops').val().length && $('#drops').val() != 'down')
            options.drops = $('#drops').val();

          if ($('#buttonClasses').val().length && $('#buttonClasses').val() != 'btn btn-sm')
            options.buttonClasses = $('#buttonClasses').val();

          if ($('#applyClass').val().length && $('#applyClass').val() != 'btn-success')
            options.applyClass = $('#applyClass').val();

          if ($('#cancelClass').val().length && $('#cancelClass').val() != 'btn-default')
            options.cancelClass = $('#cancelClass').val();

          $('#config-text').val("$('#demo').daterangepicker(" + JSON.stringify(options, null, '    ') + ", function(start, end, label) {\n  console.log(\"New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')\");\n});");

          $('#config-demo').daterangepicker(options, function(start, end, label) { console.log('New date range selected: ' + start.format('YYYY/MM/DD') + ' to ' + end.format('YYYY/MM/DD') + ' (predefined range: ' + label + ')'); });
          
        }
        
        
        });
      </script>   


       
      <script type="text/javascript">
      var fruitvegbasket = [];
      var image = [];
      var unit = [];
      var asile = [];
      var name = [];
      var amount = [];
      var dategrostartdata = '<?php echo $startingdate; ?>';
      var dategroenddata = '<?php echo $endingdate; ?>';
      var pantryid = [];
      var pantryimage = [];
      var pantryunit = [];
      var pantryasile = [];
      var pantryname = [];
      var pantryamount = [];

         $(document).ready(function() {
     
            $("#boxscroll5").niceScroll({touchbehavior:false,cursorcolor:"#00F",cursoropacitymax:0.7,cursorwidth:6,background:"#ccc",autohidemode:false});
            $("#boxscroll6").niceScroll({touchbehavior:false,cursorcolor:"#00F",cursoropacitymax:0.7,cursorwidth:6,background:"#ccc",autohidemode:false});

           

            $(".range_inputs .applyBtn").click(function(create_grolist_startdate,create_grolist_enddate){  

              console.log('create grocery list');
              var create_grolist_startdate = '';
              var create_grolist_enddate = '';
              setTimeout(function(){ 

                  var d = $('#config-demo').val();
                  console.log($('#config-demo').val());
                  console.log(d.split("-", 2));
                  var dd = d.split("-", 2);
                  create_grolist_startdate = dd[0];
                  create_grolist_enddate = dd[1];
                  console.log('Start date - '+create_grolist_startdate);
                  console.log('End date - '+create_grolist_enddate);

                  var dataObj = {
                    create_grolist_startdate:create_grolist_startdate,
                    create_grolist_enddate:create_grolist_enddate,
                    
                  };
                  $('#loadinggrocery').show();
              $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){
                                 
                                 $('#loadinggrocery').css("display", "none");
                                 // alert(html.create_grolist_startdate);
                                 // alert(html.create_grolist_enddate);
                                 
                                 if("listas de la compra" == $('#glist').text()){
                                alert('Se creó la lista de comestibles.');  
                              }else{alert(html.msg);}
                                 
                                 $('.activegrolist').removeClass('activegrolist');
                                 $('#grocerydatelist').append(html.cretedgro_list);
                                 $('#getgroceryingredents').html(html.grocerycreated);
                                 $('#getpantryingredents').html(html.pantrycreated);

                                 dategrostartdata = '';
                                 dategroenddata = '';
                                 dategrostartdata = html.startdate;
                                 dategroenddata = html.enddate;
                                 console.log('Created sd'+dategrostartdata);
                                 console.log('Created sd'+dategroenddata);

                            }

                            if(html.status == 'present'){
                              $('#loadinggrocery').css("display", "none");
                              alert(html.msg);
                            }
                        }
                    });

               }, 1000);
              
              

            });


          });

         function deletegroceylist(gro_del_startdate,gro_del_enddate){

          //alert('Do you want to delete grocery list');
          console.log(gro_del_startdate);
          console.log(gro_del_enddate);

          var dataObj = {
                    gro_del_startdate:gro_del_startdate,
                    gro_del_enddate:gro_del_enddate,
                    
                  };

                  var conmsg = '';
                  if("listas de la compra" == $('#glist').text()){
                      conmsg = '¿Estás seguro de que quieres eliminar esta lista de compras?';  
                    }else{conmsg = 'Are you sure you want to delete this grocery list ?';}

                  if(confirm(conmsg)){

                    $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 //alert(html.msg);
                                 $('#dletedlist'+html.deleted_date).fadeOut('slow').remove();
                                 location.reload();
                            }
                            if(html.status == 'faild'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 alert(html.msg);
                            }
                        }
                    });

                  }

              

         }
         
         function getgrocerydata(get_gro_startdate,get_gro_enddate){

            //alert('Get data date wise');
            dategrostartdata = '';
            dategroenddata = '';
            dategrostartdata = get_gro_startdate;
            dategroenddata = get_gro_enddate;
            
            
            var dataObj = {
                    get_gro_startdate:get_gro_startdate,
                    get_gro_enddate:get_gro_enddate,
                    
                  };

              $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){
                                 
                                 groasile_selected = [];
                                 groname_selected = [];
                                 // alert(html.get_gro_startdate);
                                 // alert(html.get_gro_enddate);
                                 $('#getgroceryingredents').html(html.ingredents_data);
                                 $('#getpantryingredents').html(html.ingredents_pantrydata);
                                 // console.log(html.ingredents_data);
                                 // console.log(html.ingredents_pantrydata);
                                 //alert(html.msg);
                                 //$('#dletedlist'+html.deleted_date).fadeOut('slow').remove();
                            }
                            if(html.status == 'faild'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 //alert(html.msg);
                            }
                        }
                    });

         }


         var pantryasile_selected = [];
         var pantryname_selected = [];
         function movetogrocery(){

            //alert('Move to grocery');

            // alert('Startdate - '+dategrostartdata);
            // alert('enddate - '+dategroenddata);
            

            $(':checkbox:checked.pancheckbox1').each(function() {
            //alert('check');
              var id = $(this).attr('id');
              var dataasile = id.split("*$#");
              //selected.push($(this).attr('id'));
              pantryasile_selected.push(dataasile[0]);
              pantryname_selected.push(dataasile[1]);
          });
          console.log('pantryasile_selected - '+pantryasile_selected);
          console.log('pantryname_selected - '+pantryname_selected);

          if ((pantryname_selected.length <= 0)  && (pantryasile_selected.length <= 0)) {
          
            if("Mover a la tienda de comestibles" == $('#mtog').text()){
            alert('Seleccione al menos un elemento.');  
          }else{alert('Select at least one item.');}

          return 0; 
        }

            var dataObj = {
                    pantrymove_asile:pantryasile_selected,
                    pantrymove_name:pantryname_selected,
                    pantrymove_startdate:dategrostartdata,
                    pantrymove_enddate:dategroenddata
                                                            
                  };

              $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){

                                  pantryasile_selected = [];
                                  pantryname_selected = [];   
                                  pantryid = [];
                                  pantryimage = [];
                                  pantryunit = [];
                                  pantryasile = [];
                                  pantryname = [];
                                  pantryamount = [];
                                 console.log('id - '+pantryid); 
                                 console.log('name - '+html.pantrymove_name);
                                 console.log('asile - '+html.pantrymove_asile);
                                 $('#getgroceryingredents').html(html.pantrygrodata);
                                 $('#getpantryingredents').html(html.pantrypandata);
                                 // alert(html.get_gro_startdate);
                                 // alert(html.get_gro_enddate);
                                 //alert(html.msg);
                                 //$('#dletedlist'+html.deleted_date).fadeOut('slow').remove();
                            }
                            if(html.status == 'faild'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 //alert(html.msg);
                            }
                        }
                    });

         }
         //var selected = [];
         var groasile_selected = [];
         var groname_selected = [];
         function movetopantry(){

            //alert('Move to pantry');

          $(':checkbox:checked.grocheckbox').each(function() {
            //alert('check');
              var id = $(this).attr('id');
              var asiledata = id.split("*$#");
              //selected.push($(this).attr('id'));
              groasile_selected.push(asiledata[0]);
              groname_selected.push(asiledata[1]);
          });
          console.log('gro selected - '+groasile_selected);
          console.log('groname_selected - '+groname_selected);
          
          if ((groname_selected.length <= 0)  && (groasile_selected.length <= 0)) {
          
          
          if("Moverse en Pantry" == $('#mtop').text()){
            alert('Seleccione al menos un elemento.');  
          }else{alert('Select at least one item.');}
          
          return 0; 
        }

          
          // alert('Startdate - '+dategrostartdata);
          // alert('enddate - '+dategroenddata);
            
            var dataObj = {
                    move_asile:groasile_selected,
                    move_name:groname_selected,
                    move_startdate:dategrostartdata,
                    move_enddate:dategroenddata
                                        
                  };
                  // groasile_selected = [];
                  // groname_selected = [];
              $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){
                                 
                                  groasile_selected = [];
                                  groname_selected = [];
                                  fruitvegbasket = [];
                                  image = [];
                                  unit = [];
                                  asile = [];
                                  name = [];
                                  amount = [];
                                  console.log('id - '+fruitvegbasket);
                                 console.log(html.movedasile);
                                 console.log(html.movedname);
                                 $('#getgroceryingredents').html(html.grodata);
                                 $('#getpantryingredents').html(html.pandata);
                                 //$('#dletedlist'+html.deleted_date).fadeOut('slow').remove();
                            }
                            if(html.status == 'faild'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 //alert(html.msg);
                            }
                        }
                    });
          
         }

         function deleteallgro(){

          var deluseridgrolist = '<?php echo get_current_user_id(); ?>';

          var dataObj = {
                    deluseridgrolist:deluseridgrolist                                                          
                  };
                  console.log(deluseridgrolist);

                  var conmsg = '';
                  if("listas de la compra" == $('#glist').text()){
                      conmsg = '¿Estás seguro de que quieres eliminar todas las listas de compras?';  
                    }else{conmsg = 'Are you sure you want to delete all the grocery lists ?';}

                  if(confirm(conmsg)){

                    $.ajax({
                        type:"POST",
                        data:dataObj,
                        url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                        cache:false,
                        dataType:'json',
                        success:function(html)
                        {
                            if(html.status == 'success'){

                              
                            if("listas de la compra" == $('#glist').text()){
                                alert('Toda la lista de la tienda de comestibles suprimida.');  
                              }else{alert(html.msg);}
                                  
                                 //console.log(html);
                                 $('#grocerydatelist').html('');
                                 $('#getgroceryingredents').html('');
                                 $('#getpantryingredents').html('');
                              }
                            if(html.status == 'faild'){
                                 
                                 // alert(html.gro_del_startdate);
                                 // alert(html.gro_del_enddate);
                                 //alert(html.msg);
                            }
                        }
                    });

                  }

          

         }

         function getData(id)
         {
          if($.inArray(id,fruitvegbasket) != -1)
          {
            var index = fruitvegbasket.indexOf(id);
            fruitvegbasket.splice(index, 1);
            image.splice(index, 1);
            unit.splice(index, 1);
            asile.splice(index, 1);
            name.splice(index, 1);
            amount.splice(index, 1);
          }
          else
          {
              fruitvegbasket.push(id);
              image.push($('#imageid-'+id).attr('src'));
             if($('#unitid-'+id).text()!='')
             {
                unit.push($('#unitid-'+id).text());
             }
             else
             {
                unit.push('');
             }
             asile.push($('#aisleid-'+id).text());
             name.push($('#nameid-'+id).text());
             amount.push($('#amountid-'+id).text());

          }
            
            //alert(id);
            

            // console.log(fruitvegbasket);
            // console.log(image);
            // console.log(unit);
            console.log(asile);
            console.log(name);
            // console.log(amount);
         } 

         function getpantryData(id)
         {
          if($.inArray(id,pantryid) != -1)
          {
            var index = pantryid.indexOf(id);
            pantryid.splice(index, 1);
            pantryimage.splice(index, 1);
            pantryunit.splice(index, 1);
            pantryasile.splice(index, 1);
            pantryname.splice(index, 1);
            pantryamount.splice(index, 1);
          }
          else
          {
              pantryid.push(id);
              pantryimage.push($('#pantryimageid-'+id).attr('src'));
             if($('#unitid-'+id).text()!='')
             {
                pantryunit.push($('#pantryunitid-'+id).text());
             }
             else
             {
                pantryunit.push('');
             }
             pantryasile.push($('#pantryaisleid-'+id).text());
             pantryname.push($('#pantrynameid-'+id).text());
             pantryamount.push($('#pantryamountid-'+id).text());

          }
            
            //alert(id);
            

            // console.log(pantryid);
            // console.log(pantryimage);
            // console.log(pantryunit);
            //console.log(pantryasile);
            //console.log(pantryname);
            // console.log(pantryamount);
         }

      </script>
<script type="text/javascript">
  $(document).ready(function(){
  $('#grocerydatelist a #listgrodates').click(function(){
  $('#grocerydatelist a #listgrodates').removeClass('activegrolist');
  $(this).addClass('activegrolist');
   }); 
});
</script>
<?php get_footer(); ?>

