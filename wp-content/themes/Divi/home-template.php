<?php
/**
 * Template name: Home Template 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 
 */
get_header();
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>   
<div id="main-content hello">
	<?php if ( ! $is_page_builder_used ) : ?>
	<!-- <div class="container">-->
		<div id="content-area hello" class="clearfix">
			<?php endif; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! $is_page_builder_used ) : ?>
				<?php
				$thumb = '';
				$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
				$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
				$classtext = 'et_featured_image';
				$titletext = get_the_title();
				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
				$thumb = $thumbnail["thumb"];
				if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
				print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>
				<?php endif; ?>
				<div class="entry-content" id="custom_message">
				<h1 class="hide">&nbsp;</h1>
				<?php
				// ********Home page slider and pie chart *******//
				the_content();
				if ( ! $is_page_builder_used )
				wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
				?>
				</div> <!-- .entry-content -->
				<?php
				if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>
				</article> <!-- .et_pb_post -->
			<?php endwhile; ?>

<section class="pickplan">
<div class="container">

			<!-- ********Home page Plan ******* -->
			<div class="top-head-sect text-center"><h1>Pick Your Plan</h1><h3>Choose the right plan for your fitness goals.</h3>
				<img class="img-center" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/heading-separtor.png">
			</div>
			<?php
			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
			$custom_args = array(
			'post_type' => 'plan', 
			'posts_per_page' => 10,
			'order' =>'ASC',
			'paged' => $paged
			);
			$custom_query = new WP_Query( $custom_args ); ?>
			<div class="row">
				<div class="col-lg-12">
				<?php if ( $custom_query->have_posts() ) : ?> 
				<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
					
					<div class="col-lg-6 comm-item hovereffect">
							<? if( has_post_thumbnail( $post_id ) ): ?>

                            <div class="post-image">
                            <img class="wp-post-image" 
                             src="<?=wp_get_attachment_url( get_post_thumbnail_id() ); ?>" style="width:1000px; height:400px;" alt="">
                           
                             <? endif; ?>
                             <div class="plan-title-head"><h1><?php the_title();?></h1></div>
                               </div>
							<div class="disc_bottom overlay">
							
							<p><?php 
							$content = get_the_content();
							echo substr($content, 0, 300);
							?></p>
							<div class="view-pln-btn"><a href="<?php echo get_permalink($post->ID)?>" title="Learn More" class="et_pb_promo_button et_pb_button hvr-shutter-in-horizontal">
							VIEW PLAN</a></div> 
							</div>   
							</div> 
							     
				<?php endwhile; ?>
				<?php endif; ?>
				</div>  
			</div>
		
    	 <div class="find-fitness">

			<div class="text-center">
				<h1>Find the Right Fitness Plan for You!</h1> 
<!--				<div class="view-all-pln-btn"><a href="<?php //echo home_url()?>/all-plans/" class="et_pb_promo_button et_pb_button hvr-rectangle-out ">SEE ALL PLANS <div class="arrow_bck"><span class="glyphicon glyphicon-menu-right"></span></div></a></div>-->
			</div>
</div>

			</div> 
    	 </section>


			<!-- ********Home page Features ******* -->

			<section class="futur-section">
				<div class="container">
				<div class="row">
<!--					<div class="col-lg-12">
					<div class="col-lg-4 left-col">
						<div class="et_pb_column et_pb_column_1_3  et_pb_column_7">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_0 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_1.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 1</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div>  .et_pb_blurb_content 
						</div>  .et_pb_blurb 
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_1 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_2.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 2</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div>  .et_pb_blurb_content 
						</div>  .et_pb_blurb 
					</div>


					</div>



					<div class="col-lg-4 mid-cont"><img class="img-responsive" src="<?php// echo get_stylesheet_directory_uri(); ?>/images/mob-img.png" alt=""></div>
					<div class="col-lg-4 right-cont">

					<div class="et_pb_column et_pb_column_1_3  et_pb_column_9">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_2 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/featured_icon_4.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 3</h4>				
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
					            </div>
							</div>  .et_pb_blurb_content 
						</div>  .et_pb_blurb 
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_3 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
								<img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/featured_icon_3.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 4</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div>  .et_pb_blurb_content 
						</div>  .et_pb_blurb 
					</div>  .et_pb_column 

					</div>

					<div class="down-app-sect">

						<h1 style="text-align: center;" class="heading-app">Download the App</h1>
		<div class="down-btn"><a href="#"><img alt="" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/app-store-btn.png"></a>
		<a href="#"><img alt="" src="<?php //echo get_stylesheet_directory_uri(); ?>/images/google-play-btn.png"></a>
					</div>
					</div>
						</div>
                                    -->
                                    
                                    
                                    
                                    <div class="col-lg-12">
					<div class="col-lg-4 left-col">
						<div class="et_pb_column et_pb_column_1_3  et_pb_column_7">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_0 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_1.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>In-App Messaging</h4>			
					            <div class="features features_1">
					            <p>Chat live with your Certified Nutritionist from anywhere.</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_1 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_2.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Workouts</h4>			
					            <div class="features features_1">
					            <p>The most effective fat loss workouts that can be done anywhere, anytime!</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
					</div>


					</div>



					<div class="col-lg-4 mid-cont"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mob-img.png" alt=""></div>
					<div class="col-lg-4 right-cont">

					<div class="et_pb_column et_pb_column_1_3  et_pb_column_9">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_2 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_4.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Motivation</h4>				
					            <div class="features features_1">
					            <p>Practical tools from a mental health expert to keep you on track.</p>
					            </div>
					            </div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_3 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
								<img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_3.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Meal Planning</h4>			
					            <div class="features features_1">
					            <p>Personalized macronutrient and calorie recommendations from your Certified Nutritionists.</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
					</div> <!-- .et_pb_column -->

					</div>

					<div class="down-app-sect">

						<h1 style="text-align: center;" class="heading-app">Download the App</h1>
		<div class="down-btn"><a href="#"><img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/app-store-btn.png"></a>
		<a href="#"><img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/google-play-btn.png"></a>
					</div>
					</div>
						</div>
				</div>
				</div>
			</section>




			<div class="" id="fetured-product">
				<div class="features-row et_pb_row et_pb_row_5">
					<div class="et_pb_column et_pb_column_1_3  et_pb_column_7">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_0 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_1.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 1</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_1 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_2.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 2</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
					</div> <!-- .et_pb_column -->
					<div class="et_pb_column et_pb_column_1_3  et_pb_column_8">
					<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_12">
		            <div class="spacing">
		            </div>
					</div> <!-- .et_pb_text -->
					</div> <!-- .et_pb_column -->
					<div class="et_pb_column et_pb_column_1_3  et_pb_column_9">
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_2 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image"><img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_4.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 3</h4>				
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
					            </div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
						<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_3 et_pb_blurb_position_top">
							<div class="et_pb_blurb_content">
								<div class="et_pb_main_blurb_image">
								<img class="et-waypoint et_pb_animation_top et-animated" alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_icon_3.png">
								</div>
								<div class="et_pb_blurb_container">
								<h4>Features 4</h4>			
					            <div class="features features_1">
					            <p>Feature Description.This area could explain a certain app area or feature.</p>
					            </div>
								</div>
							</div> <!-- .et_pb_blurb_content -->
						</div> <!-- .et_pb_blurb -->
					</div> <!-- .et_pb_column -->
				</div>
				<h1 style="text-align: center;" class="download_heading">Download the App</h1>
				<div class=" et_pb_row et_pb_row_3">
					<div class="et_pb_column et_pb_column_1_2  et_pb_column_5">
					<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_0 et_always_center_on_mobile et-animated">
					<a href="#"><img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/android-btn.png">
					</a>
					</div>
					</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_6">
					<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_1 et_always_center_on_mobile et-animated">
					<a href="#"><img alt="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-btn.png">
					</a>
					</div>
					</div> <!-- .et_pb_column -->
				</div> <!-- .et_pb_row -->
			</div>

    </div><!-- #content-area -->
    
    	 
	      <!-- .container -->

			<!-- ********Home page Testimonials ******* -->
			<div class="et_pb_section et_pb_section_3 et_section_regular" id="testi_moniel">
<!--<div class="text-center"><h1>What other people are saying<br> about Fit Like This</h1></div>-->
<div class="text-center"><h1>What are our members saying about FitLikeThis?</h1></div>
 
			<?php echo do_shortcode('[show-testimonials pagination="on" orderby="menu_order" order="ASC" post_status="publish" layout="slider" options="transition:horizontal,adaptive:false,controls:controls,pause:4000,auto:on,columns:1,theme:speech,info-position:info-below,text-alignment:center,review_title:on,quote-content:short,charlimitextra: (...),display-image:on,image-size:thumbnail,image-shape:circle,image-effect:transparency,image-link:on"]
			')
			?>
			</div>

	
</div> <!-- #main-content -->
<?php get_footer(); ?>
