

<?php
  /*
  Template Name: How it works Template 
  */
  get_header(); 
  ?>

<div id="main-content" class="inner-pages-section workout-temp">
    	<?php if ( ! $is_page_builder_used ) : ?>
	<!-- <div class="container">-->
		<div id="content-area" class="clearfix">
			<?php endif; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! $is_page_builder_used ) : ?>
				<?php
				$thumb = '';
				$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
				$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
				$classtext = 'et_featured_image';
				$titletext = get_the_title();
				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
				$thumb = $thumbnail["thumb"];
				if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
				print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>
				<?php endif; ?>
				<div class="entry-content" id="custom_message">
				<h1 class="hide">&nbsp;</h1>
				<?php
				// ********Home page slider and pie chart *******//
				the_content();
				if ( ! $is_page_builder_used )
				wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
				?>
				</div> <!-- .entry-content -->
				<?php
				if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>
				</article> <!-- .et_pb_post -->
			<?php endwhile; ?>
<!--  <div class="sub-banner dash-sect workout-banner">
    
    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/background_inner.jpg">
    <div class="page-title-head">
      <h1 class="entry-title main_title"><?php// the_title(); ?></h1>
    </div>
  </div>-->

</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<?php get_footer(); ?>

