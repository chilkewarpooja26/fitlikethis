var $=jQuery.noConflict();
function submitFormData () {
	var age        = $("#age").val();   
	var weight     = $("#weight").val();
	var height_ft  = $("#height_ft").val();
	var height_in  = $("#height_in").val();
	var goal       = $("#goal").val();
	var gender     = $("#gender").val();
    var activity_factor=$("#activity_factor").val();
   
  
if(weight=='')
{
$('.weight-error').fadeOut(200).show(); 
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.gender-error').fadeOut(200).hide();
$('.goal-error').fadeOut(200).hide();   
$('.activity-factor-error').fadeOut(200).hide();
$("#piechartwithdata").hide();
}else if(age=='')
{
$('.age-error').fadeOut(200).show();    
$('.success').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.goal-error').fadeOut(200).hide(); 
$('.activity-factor-error').fadeOut(200).hide();  
}else if(height_ft=='')
{
$('.height_ft-error').fadeOut(200).show();  
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.gender-error').fadeOut(200).hide();
$('.goal-error').fadeOut(200).hide();  
$('.activity-factor-error').fadeOut(200).hide(); 
}else if(activity_factor=='')
{
$('.activity-factor-error').fadeOut(200).show();    
$('.goal-error').fadeOut(200).hide();   
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.gender-error').fadeOut(200).hide();
}
else if(gender=='')
{
$('.gender-error').fadeOut(200).show(); 
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.goal-error').fadeOut(200).hide();  
$('.activity-factor-error').fadeOut(200).hide(); 
}else if(goal=='')
{
$('.goal-error').fadeOut(200).show();   
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.gender-error').fadeOut(200).hide();
$('.activity-factor-error').fadeOut(200).hide();
}

else
{
$('.goal-error').fadeOut(200).hide();   
$('.success').fadeOut(200).hide();
$('.age-error').fadeOut(200).hide();
$('.weight-error').fadeOut(200).hide();
$('.height_ft-error').fadeOut(200).hide();
$('.gender-error').fadeOut(200).hide();
$('.activity-factor-error').fadeOut(200).hide();

$("#piechartwithdata").show();
	 $.post(ajaxurl,{action: 'calories_func',age: $("#age").val(),weight: $("#weight").val(),height_ft:$("#height_ft").val(),height_in:$("#height_in").val(),goal:$("#goal").val(),gender:$("#gender").val(),activity_factor:$("#activity_factor").val()},function(data, status){
	 	//console.log(data);
	 	var result = JSON.parse(data);
	 	//console.log(result.Calories);
	 	

function drawChart() {
var dataArray = [
['Protein', result.Protein],
['Fat', result.Fat],
['Carb', result.Carb],
];
    
var total = getTotal(dataArray);
// Adding tooltip column  
for (var i = 0; i < dataArray.length; i++) {
dataArray[i].push(customTooltip(dataArray[i][0], dataArray[i][1], total));
}

// Changing legend  
for (var i = 0; i < dataArray.length; i++) {
dataArray[i][0] = dataArray[i][0] + "  " + ((dataArray[i][1] / total) * 100).toFixed(1) + "%"; 
}
    
// Column names
dataArray.unshift(['Goal Name', 'No. of times Requested', 'Tooltip']);
   
var data = google.visualization.arrayToDataTable(dataArray);
 // Setting role tooltip
data.setColumnProperty(2, 'role', 'tooltip');
data.setColumnProperty(2, 'html', true);

var options = {
        title: '',
        width: 900,
        height: 400,
        colors: ['#9bbb59', '#4f81bd', '#c0504d'],
        tooltip: { isHtml: true }
};
document.getElementById("dailycalories").innerHTML = 'Daily Caloric intake Goal' +result.Calories;    
var chart = new google.visualization.PieChart(document.getElementById('jqChart'));
chart.draw(data, options);
}

function customTooltip(name, value, total) {
    return name + '<br/><b>' + value + ' (' + ((value/total) * 100).toFixed(1) + '%)</b>';
}

function getTotal(dataArray) {
var total = 0;
for (var i = 0; i < dataArray.length; i++) {
total += dataArray[i][1];
}
return total;
}

google.load('visualization', '1', {packages:['corechart'], callback: drawChart});

});
    
}
}

$(document).ready(function () {
            $("#piechartwithdata").hide();
        });



function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
