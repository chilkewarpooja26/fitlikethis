<?php

/* ===============================   GENERAL  ====================================== */
/* --------- DEFINE ---------- */

include( get_stylesheet_directory() . '/functions/scripts.php');


/* --------- CLASS ---------- */




/* ------- AJAX ------------ */
include( get_stylesheet_directory() . '/ajax/ajax-account.php');



/* ===============================    ADMIN   ======================================= */

/* -------- FIRST RUN ---------- */




/* -------------ADMIN DAMAGE REPORTS--------------- */


/* --------- OPTIONS ---------- */


/* --------- TOOLS ---------- */


/* ------ CUSTOM POST ----- */







/* ---- CUSTOM TAXONOMY --- */



/* ------- FUNCTIONS ------ */


/* --------- SETUP ---------- */


/* =================================  USER  ========================================= */

/* ------- FUNCTIONS -------- */

function checkForgotPasswordToken($cred = array()) {
    extract($cred);
    $error = false;
    $message = '';

    $users = get_users(array('meta_key' => 'forgot_password_token', 'meta_value' => $token));


    if ($users[0]->ID > 0) {

        $token_time = get_user_meta($users[0]->ID, 'forgot_password_token_time', true);
        $token_time = ($token_time + 4) * 3600;

        if ($token_time < time()) {
            $error = true;
            $message = 'Token time is expired';
        } else {

            $message = $users[0]->ID;
        }
    } else {

        $error = true;
        $message = 'Invalid token';
    }

    $results['type'] = $error ? 'error' : 'success';
    $results['message'] = $message;
    return $results;
}

//nutrafit( get_stylesheet_directory() . '/functions/add_image_size.php');
//nutrafit( get_stylesheet_directory() . '/functions/ajax/ajax-account.php');
//DEFINE LOADER 
define('NU_LOADER_CYCLE', get_stylesheet_directory_uri() . '/images/loading.gif');
?>
