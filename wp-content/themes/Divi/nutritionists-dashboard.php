<?php
/*
Template Name: Nutritionists Dashboard Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section">
  <div class="sub-banner dash-sect">
  <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
  <div class="page-title-head"><h1 class="entry-title main_title">Dashboard</h1></div>
  </div>
  <section id="mydashboard" class="dash-section">
      <div class="container">
        <div class="row">

          <div class="col-lg-12 dash-sect-title"><h1>Nutritionists Dashboard</h1></div>
          <div class="part-sect-1">
            <div class="col-lg-6 right-sect workout-sect">
              <div class="img-top"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/invite-img.png" class="img-responsive"></div>
              <h6 class="com-head" id="up-body">Invite Users</h6>
              <div class="fitwork"><a href="<?php echo home_url()?>/invite-users/"" class="comm-all-btn">View Users List</a></div>
            </div>
            <div class="col-lg-6 right-sect workout-sect">
              <div class="img-top"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy_user.png" class="img-responsive"></div>
              <h6 class="com-head" id="up-body">My Users</h6>
              <div class="fitwork"><a href="<?php echo home_url()?>/my-users/"" class="comm-all-btn">View Users</a></div>
            </div>
            <div class="col-lg-6 right-sect message min-show">
              <div class="img-top" ><span class="glyphicon glyphicon-envelope"></span></div>
              <h3 class="com-head">Messages</h3>
              <p class="com-phara msg"><?php echo do_shortcode( '[kiran]' );?> </p>
              <div class="button-sect"><a href="<?php echo get_home_url();?>/messaging" class="btn-dash" id="msg-btn">See Messages <span class="glyphicon glyphicon-chevron-right"></span></a></div>
            </div>
          </div>
          <div class="col-lg-12 dash-sect-title"><h1>Top 3 Plan</h1> </div>
           <?php
           $con = mysql_connect("localhost", "etpl2012_nutrafi", "nutrafit123");  
           $selectdb = mysql_select_db("etpl2012_nutrafitdivi",$con);  

            $month_user_sql= mysql_query("SELECT nu_users.ID,nu_users.display_name,nu_users.user_registered,nu_usermeta.meta_value 
                                          AS user_type
                                          FROM nu_users, nu_usermeta 
                                          WHERE nu_users.ID = nu_usermeta.user_id
                                          AND nu_usermeta.meta_key = 'user_type' 
                                          AND nu_usermeta.meta_value = 'User'
                                          AND MONTH( CAST( `user_registered` AS date ) ) = MONTH( NOW( ) )
                                          AND YEAR( CAST( `user_registered` AS date ) ) = YEAR( NOW( ) )
                                          order by ID");
            $month_user_cnt = mysql_num_rows($month_user_sql);
            $today = date("F-Y");      
           // echo "<p>";echo $today;"</p>";
           // echo "<p>";echo "Singh up to site";"</p>";
            //echo "<p>";echo $month_user_cnt;"</p>";
            //echo "<p>";echo "Top 3 sub plans users subscribed to:";"</p>";

           

            while($month_row = mysql_fetch_array($month_user_sql,MYSQL_ASSOC))
            {
               $rawDate = $month_row["user_registered"];        
               global $wpdb;
               $plan_query = "SELECT DISTINCT(sub_plan_title) as title,(plan_title) as plan_title, 
                              count(sub_plan_title) AS count 
                              FROM nu_payments
                              WHERE MONTH( CAST( `payment_date` AS date ) ) = MONTH( NOW( ) ) 
                              AND YEAR( CAST( `payment_date` AS date ) ) = YEAR( NOW( ) ) 
                              GROUP BY sub_plan_title 
                              order by count DESC limit 3";
               $plan_result = $wpdb->get_results($plan_query);
               $row_pk=$wpdb->get_row( $plan_query);      
               if($row_pk==0)
               { 
                 //echo  'No plan subscribed for this month.';
               }
               else
               {
                 foreach($plan_result as $plan_row)
                 {

                   //echo "<p>";echo $plan_row->title;"</p>";
                   //echo "<p>";echo $plan_row->count;"</p>";
                            
                 }
               }
            }
           ?>
         

        </div>
      </div>
  </section>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
