<?php
ob_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Admin save design custom menu backend penal functionality and html. 
 * 
 */
// Register custom admin menu save design 
add_action('admin_menu', 'fitlikethis_reports_custom_menu_page');

function fitlikethis_reports_custom_menu_page() {
    add_menu_page('Payment Reports', 'Payment Reports', 'manage_options', 'fitlikethis-reports', 'fitlikethis_reports_custom_menu_function', get_stylesheet_directory_uri() . '/Customadmin/images/check.png', 98);
}

function fitlikethis_reports_custom_menu_function() {
    error_reporting(0);
    global $wpdb;
    $MonthList = array(__('Month', 'fitlikethis'), __('January', 'fitlikethis'), __('February', 'fitlikethis'), __('March', 'fitlikethis'), __('April', 'fitlikethis'), __('May', 'fitlikethis'), __('June', 'fitlikethis'), __('July', 'fitlikethis'), __('August', 'fitlikethis'), __('September', 'fitlikethis'), __('October', 'fitlikethis'), __('November', 'fitlikethis'), __('December', 'fitlikethis'));
    $customPagHTML = "";
    $query = "select * from " . $wpdb->prefix . "payments"; 
     $total_query = $wpdb->get_results($query);

    // PAGENIATION 
    $total = count($total_query);
    $items_per_page = 10;
    $page = isset($_GET['cpage']) ? abs((int) $_GET['cpage']) : 1;
    $offset = ( $page * $items_per_page ) - $items_per_page;
    $result = $wpdb->get_results($query . " order by payment_id desc limit ${offset}, ${items_per_page}");
    $totalPage = ceil($total / $items_per_page);
    if ($totalPage > 1) {
        $customPagHTML = '<div><span>Page ' . $page . ' of ' . $totalPage . '</span> &nbsp;&nbsp;' . paginate_links(array(
                    'base' => add_query_arg('cpage', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => $totalPage,
                    'current' => $page
                )) . '</div>';
    }
    ?>
    <div id="wpbody">
        <div id="wpbody-content" aria-label="Main content" tabindex="0">
            <div class="wrap">
                <h2>
                    <?php _e('Transaction Reports'); ?>
                </h2>

                <form action="<?php echo admin_url() . 'admin.php?page=fitlikethis-reports'; ?>" method="get">
                    <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>">
                    <p class="search-box">
                        <label class="screen-reader-text" for="user-search-input">Month</label>
                        <select name="month" id="month">
                            <?php for ($i = 0; $i < COUNT($MonthList); $i++): ?>
                                <option <?php echo $_GET['month'] == $i ? 'selected="selected"' : '' ?> value="<?php echo $i ?>"><?php echo $MonthList[$i] ?></option>
                            <?php endfor; ?>
                        </select>
                        <select id="year"  name="year">
                            <option value=""><?php _e('Year', 'fitlikethis') ?></option>
                            <?php for ($i = (date("Y")); $i >= (date("Y") - 10); $i--): ?>
                                <option <?php if ($_GET['year'] == $i) echo 'selected'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <input type="submit" name="" id="search-submit" class="button" value="Filters">
                        <a id="" href="<?php echo get_stylesheet_directory_uri().'/includes/common/export-all-damage-reports.php?type=transaction&year='.$_GET['year'].'&month='.$_GET['month']; ?>" class="button">Export to xls</a>
                    </p>
                    <br>
                    <table class="wp-list-table widefat fixed users">
            
                <?php

                  


echo "<table  class='wp-list-table widefat fixed users' border='1' >"; 
echo "<tr><th scope='col' id='cb' class='manage-column column-cb check-column' style=''> Name</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Email Id</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Transaction Id</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Plan</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Sub Plan</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Plan Duration</th>

<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Payment (USD)</th>
<th scope='col' id='cb' class='manage-column column-cb check-column' style=''>Payment Date</th></tr>" ;
foreach($result as $row){ 
/*//echo $query1 = "select * from " . $wpdb->prefix . "users where user_id=".$row['user_id'];  
 $user_info = get_userdata($row->user_id);
      'Username: ' . $user_info->display_name . "\n";
       'User roles: ' . implode(', ', $user_info->roles) . "\n";
       'User ID: ' . $user_info->ID . "\n";

     //print_r($query1);
 //Creates a loop to loop through results*/
 $user_info = get_userdata($row->user_id);  
echo "<tr><td>" .$user_info->display_name. "</td><td>" .$user_info->user_email. "</td><td>" . $row->txn_id ."</td><td>" . $row->plan_title ."</td><td>" . $row->sub_plan_title . "</td><td>" .$row->sub_plan_duration . "</td><td>" .$row->payment_gross . "</td><td>".$row->payment_date . "</td></tr>";  //$row['index'] the index here is a field name


}

echo "</table>";?>
                    
                </form>

                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right;"><?php echo $customPagHTML; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                
                <br class="clear">
            </div>
            <div class="clear"></div></div><!-- wpbody-content -->
            <?php
        }
        ?>

