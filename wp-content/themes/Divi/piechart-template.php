<?php
/*
Template Name: Piechart Template 
*/
session_start();
get_header(); 
global $wpdb;
$user_id = get_current_user_id();

$arravd = $_SESSION["sub_plans"][$_GET["sel"]][$_GET["sub_plan"]];
$post_id=$arravd["post_id"];
$plan_title=$arravd["plan_title"];
$sub_plan_title=$arravd["sub_plan_title"];
$sub_plan_duration=$arravd["sub_plan_duration"];
$sub_plan_speciality=$arravd["sub_plan_speciality"];
$sub_plan_price=$arravd["sub_plan_price"];
$formula_for_calories=$arravd["formula_for_calories"];
$formula_for_proteins= $arravd["formula_for_proteins"];
$formula_for_carbs= $arravd["formula_for_carbs"];
$women_formula_for_calories=$arravd["women_formula_for_calories"];
$women_formula_for_proteins= $arravd["women_formula_for_proteins"];
$women_formula_for_carbs= $arravd["women_formula_for_carbs"];
$payment_type= $arravd["payment_type"];
$content= $arravd["content"];


$_SESSION["post_id"]=$post_id;
$_SESSION["plan_title"]=$plan_title;
$_SESSION["sub_plan_title"]=$sub_plan_title;
$_SESSION["sub_plan_duration"]=$sub_plan_duration;
$_SESSION["sub_plan_speciality"]=$sub_plan_speciality;
$_SESSION["sub_plan_price"]=$sub_plan_price;
$_SESSION["formula_for_calories"]=$formula_for_calories;
$_SESSION["formula_for_proteins"]=$formula_for_proteins;
$_SESSION["formula_for_carbs"]=$formula_for_carbs;
$_SESSION["women_formula_for_calories"]=$women_formula_for_calories;
$_SESSION["women_formula_for_proteins"]=$women_formula_for_proteins;
$_SESSION["women_formula_for_carbs"]=$women_formula_for_carbs;
$_SESSION['payment_type'] =$payment_type;



if(!isset($arravd["sub_plan_price"]) && empty($arravd["sub_plan_price"]) && 
   !isset($arravd["formula_for_calories"]) && empty($arravd["formula_for_calories"]) && 
   !isset($arravd["formula_for_proteins"]) && empty($arravd["formula_for_proteins"]) && 
   !isset($arravd["formula_for_carbs"]) && empty($arravd["formula_for_carbs"]) && 
   !isset($arravd["women_formula_for_calories"]) && empty($arravd["women_formula_for_calories"]) && 
   !isset($arravd["women_formula_for_proteins"]) && empty($arravd["women_formula_for_proteins"])&&
   !isset($arravd["women_formula_for_carbs"]) && empty($arravd["women_formula_for_carbs"]) )
{
    wp_redirect( home_url() ); exit;
}



global $wpdb;
        
        $query = "select * from " . $wpdb->prefix . "users where user_id=" . $user_id;
        $results = $wpdb->get_row($query);
        $age = '';
        $gender = ( get_user_meta($user_id,"gender", true) );
        $user_weight = ( get_user_meta($user_id,"weight", true) );
        $user_age = ( get_user_meta($user_id,"age", true) );
        $user_type = ( get_user_meta($user_id,"user_type", true) );
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
  <div class="page-title-head"><h1 class="entry-title main_title"><?php the_title(); ?></h1></div>
  </div>
<section class="lose-wieght-pane-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="col-lg-6 littleus">
          <h1>Tell us a little bit about yourself.</h1>
          <form id="frmPaypal"  name="form" method="post" autocomplete="on">
              <div class="form-group gender-group">
              <div class="checkbox">
              <div class="col-lg-6"><input type="radio" name="gender" <?php  checked( $gender, 'Male' ); ?> value="Male" autocomplete="on"> <span>Male</span></div>
              <div class="col-lg-6"><input type="radio" name="gender" <?php  checked( $gender, 'Female' ); ?> value="Female" autocomplete="on"> <span>Female</span></div>
              <span class="gender-error commen-error" style="display: none;">Please select gender</span>
              </div>
              </div>
              <div class="form-group wieght-group">
              <input id="weight" name="weight" type="text" placeholder="Weight" autocomplete="on" onKeyPress="return IsWeight(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $user_weight;?>" />
              <span class="">lbs</span>
              <span class="weight-error" style="display: none;">Please enter weight</span> 
              <span id="weightdigit" style="display: none">Input only digits</span>
              </div>
              <div class="form-group age-group">
              <input id="age" name="age" type="text" placeholder="Age" autocomplete="on" onKeyPress="return IsAge(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $user_age;?>" />
              <span class="age-block">years</span>
			  <span class="age-error commen-error" style="display: none;">Please enter age</span>
              <span id="agedigit" class="commen-error" style="display: none">Input only digits</span>
              </div>
              <div class="form-group">
              <select id="activity_factor" name="activity_factor" class="form-control select_css" autocomplete="on">
              <option value="">Daily Activity Level</option>
              <option value="Sedentary">Sedentary</option>
              <option value="Mild activity level">Mild activity level</option>
              <option value="Moderate activity level">Moderate activity level</option>
              <option value="Heavy activity level">Heavy activity level</option>
              <option value="Extreme level">Extreme level</option>
              </select>
              <span class="activity-factor-error commen-error" style="display: none;">Please select daily activity level</span>
              </div>
              <div class="form-group">
              <input id="pieform" class="submit hvr-shutter-in-horizontal" type="button" onclick="pie()" value="CONTINUE" />
              </div>
              <div id="piechartwithdata">    
               <?php 
                if(is_user_logged_in())
                 {
                     if($user_type=='User')
                    {
                        if($payment_type!='')
                        {
                            //echo "hello";
                           $_SESSION['payment_type']='Yes';
                            $aa=$sub_plan_price.".00";?>
                          <?php
                            echo do_shortcode( '[gravityform id="1" field_values="sub_plan_price='.$aa.';&amp;sub_plan_title='.$sub_plan_title.'&amp;plan_title='.$plan_title.' &amp;sub_plan_duration='.$sub_plan_duration.'&amp;user_id='.$user_id.'&amp;sub_plan_speciality='.$sub_plan_speciality.'&amp;calories='.$calories.'&amp;calories='.$calories.'&amp;carb='.$carb.'" title="false" description="false"]' );
                          //echo "Recuring";
                        } 
                        else
                        {
                           
                          $_SESSION['payment_type']='No';
                          $aa=$sub_plan_price.".00";
                            echo do_shortcode( '[gravityform id="2" field_values="sub_plan_price='.$aa.';&amp;sub_plan_title='.$sub_plan_title.'" title="false" description="false"]' );
                        }
                    }//user_type else
                   }//login user
              else
               {
                       //echo"hello";[ajax_login], [ajax_register]
                  echo do_shortcode('[wp-ajax-login text="Login / Sign Up"]');
               }
                    ?>
              </div>
          </form>
        </div>

        <div class="col-lg-6 wieghtplan">
          <p>You are a just couple of steps away from starting your own fitness plan!</p>
          <span><?php echo $sub_plan_title;?> in <?php echo $sub_plan_duration;?></span>
          <span><h1>Total: <?php echo $sub_plan_price;?></h1></span>  
        </div>
        <img class="load-icon" id="gifpiechart" src="<?php echo get_stylesheet_directory_uri(); ?>/images/addrecipegif.gif" />
        <div id="jqChart">
          <script>
          $(document).ready(function()
          {
          $(document).ajaxComplete(function(){

            $('#pieform').click(function(){

              //alert("hi");
              $("#gifpiechart").css("display", "inline-block");

            });

          $("#wait").css("display", "none");
          $("#gifpiechart").css("display", "none");
          }); 
          });
          </script>
          <div id="wait" style="display:none;width:69px;height:auto;border:1px solid black;position:absolute;top:auto;left:auto;padding:2px; bottom: -11px;right: 38%;"><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/loading.gif' width="50" height="50" alt="Loading Image"/><span>Loading..</span></div>
        </div>
        
      </div>
    </div>
    <div class="hidesection">
      <div id="calories"></div>
      <div id="protein"></div>
      <div id="fat"></div>
      <div id="carb"></div>
      <div id="Proteins_gram"></div>
      <div id="Carbs_gram"></div>
      <div id="Fats_gram"></div>
    </div>
  </div>
  </div>
</section>
 </div>

<section class="program-include">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
         <div class="prog_includ_discription"><p><?php echo $content;?></p></div>
      </div>
    </div>
  </div>
</section>
<div class="et_pb_section et_pb_section_3 et_section_regular" id="testi_moniel" style="display:none;">
  <div align="center"><h1>What other people are saying<br> about Fit Like This</h1></div>
  <?php echo do_shortcode('[show-testimonials pagination="on" orderby="menu_order" order="ASC" post_status="publish" layout="slider" options="transition:horizontal,adaptive:false,controls:controls,pause:4000,auto:on,columns:1,theme:speech,info-position:info-below,text-alignment:center,review_title:on,quote-content:short,charlimitextra: (...),display-image:on,image-size:thumbnail,image-shape:circle,image-effect:transparency,image-link:on"]
  ')
  ?>
</div>
<script type="text/javascript"> 
$('#frmPaypal input[type=text]').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#pieform').click();//Trigger search button click event
        }
    });

$('#frmPaypal select').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#pieform').click();//Trigger search button click event
        }
    });
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php get_footer(); ?>
