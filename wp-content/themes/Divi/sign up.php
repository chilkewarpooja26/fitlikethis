<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 *  Template Name: Sign up
 */
 get_header();
 ?>
 </br></br> </br></br> </br></br>
 
<!--Signup popup Start here-->
 <div class="cd-user-modal">
        <!-- Modal content-->
      
            <div id="cd-signup">
                <form id="frmUserRegister" name="frmUserRegister" class="cd-form" >         
                    <p class="fieldset">
                        <span class="common-success-message hide"></span>
                    </p>
                    <div class="modal-body">
                        <?php //do_action('facebook_login_button'); ?>
                        <!--<div class="or-separator"><span><?php// _e('or', 'nutrafit'); ?></span><hr /></div>-->
                        <div id="reg-msg"></div>
                        <div class="col-md-12 register_box">

                            <div class="form-group full_wrapper">
                                <label class="" for="user"><?php _e('User', 'nutrafit'); ?></label>
                                <input class="full-width has-padding has-border signup-user-type" name="user-type" id="user" type="radio" value="user" autocomplete="off">
                                <label class="" for="nutrafit"><?php _e('Nutritionist', 'nutrafit'); ?></label>
                                <input class="full-width has-padding has-border signup-user-type" id="nutrafit" name="user-type" type="radio" value="nutrafit" autocomplete="off">
                                <span class="cd-error-message tag-error">Error message here!</span>
                            </div>

                            <div class="form-group margin_right10">
                                <label class="image-replace cd-email" for="signup-first-name"><i class="fa fa-user"></i><?php _e('Name', 'nutrafit'); ?><span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signup-first-name" type="first-name" placeholder="<?php _e('Name', 'nutrafit'); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>

                            <!--<div class="form-group">
                                <label class="image-replace cd-email" for="signup-last-name"><i class="fa fa fa-user"></i><?php _e('Last Name', 'nutrafit'); ?><span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signup-last-name" type="last-name" placeholder="<?php _e('Last Name', 'nutrafit'); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>-->

                            <div class="form-group">
                                <label class="image-replace cd-email" for="signup-email"><i class="fa fa fa-envelope"></i><?php _e('E-mail', 'nutrafit'); ?><span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signup-email" type="email" placeholder="<?php _e('E-mail', 'nutrafit'); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>

                            <div class="form-group margin_right10">
                                <label class="image-replace cd-password" for="signup-password"><i class="fa fa-unlock-alt"></i><?php _e('Password', 'nutrafit'); ?><span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signup-password" type="password"  placeholder="<?php _e('Password', 'nutrafit'); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>

                            <div class="form-group">
                                <label class="image-replace cd-email" for="signup-address"><?php _e('Address', 'nutrafit'); ?><span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signup-address" type="address" placeholder="<?php _e('Address', 'nutrafit'); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>

                            <div class="form-group margin_right10">
                                <label class="image-replace cd-email" for="signup-gender"><?php _e('Gender', 'nutrafit'); ?><span class="aster">*</span></label>
                                <!--<input class="form-control full-width has-padding has-border" id="signup-gender" type="text" placeholder="<?php _e('Gender', 'nutrafit'); ?>">-->
                                <div class="select-style">
                                    <select id="signup-gender" >
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>
                                    <span class="cd-error-message">Error message here!</span>
                                </div>
                            </div>

                            <div class="form-group signup-wt ">
                                <label class="image-replace cd-email" for="signup-weight"><?php _e('Weight', 'nutrafit'); ?></label>
                                <!--<input class="form-control full-width has-padding has-border" id="signup-weight" type="text" placeholder="<?php _e('Weight', 'nutrafit'); ?>">-->
                                <div class="select-style">
                                    <select id="signup-weight" >
                                        <option value="">Select Weight</option>
                                        <?php for ($i = 1; $i <= 150; $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></option>
                                        <?php endfor; ?>
                                        <option value="150+">150+</option>
                                    </select>
                                    <span class="cd-error-message">Error message here!</span>
                                </div>
                            </div>
                            
                            <div class="form-group margin_right10">
                                <label class="image-replace cd-email" for="signup-age"><?php _e('Age', 'nutrafit'); ?>
                                <span class="aster">*</span></label>
                                <!--<input class="form-control full-width has-padding has-border" id="signup-age" type="text" placeholder="<?php _e('Age', 'nutrafit'); ?>">-->
                                <div class="select-style">
                                    <select id="signup-age">
                                        <option value="">Select Age</option>
                                        <?php for ($i = 1; $i <= 150; $i++): ?>
                                            <option value="<?php echo $i ?>"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></option>
                                        <?php endfor; ?>
                                        <option value="150+">150+</option>
                                    </select>
                                    <span class="cd-error-message">Error message here!</span>
                                </div>
                            </div>

                            <div class="form-group signup-gl ">
                                <label class="image-replace cd-email" for="signup-goal"><?php _e('Goal', 'nutrafit'); ?></label>
                                <div class="select-style">
                                    <select id="signup-goal" >
                                        <option value="">Select Goal</option>
                                        <option value="Maintain Weight">Maintain Weight</option>
                                        <option value="Gain Weight">Gain Weight</option>
                                        <option value="Loose Weight">Loose Weight</option>
                                    </select>
                                    <span class="cd-error-message">Error message here!</span>
                                </div>
                            </div>

                            <div class="register_btn">
                                <span><i class="fa fa-sign-in"></i></span>
                                <input class="full-width has-padding" id="signup-submit" type="submit" value="<?php _e('Create account', 'nutrafit'); ?>">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/loading.gif" id="loader" class="H-loader" style="display:none;" />
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                  <!--  <input type="hidden" name="redirect_to" id="redirect_to" value="<?php echo esc_attr($_SERVER["REQUEST_URI"]); ?>" />-->
                </form>
            </div>
        </div>


        <?php
         get_footer();
         ?>
