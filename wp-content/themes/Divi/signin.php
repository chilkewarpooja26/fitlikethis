<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 *  Template Name: Sign in 
 */
 get_header();
 ?> 
 </br></br> </br></br>
 <div role="dialog" class="modal fade login_popup in" id="loginmodel"  aria-hidden="false">
 <div class="cd-user-modal">
<div id="cd-login">

                    <?php do_action('facebook_login_button'); ?>
                    
                    <form name="frmlogin" id="frmlogin" role="form" class="cd-form">
                        <p class="fieldset" id="common">
                            <span class="common-success-message hide"></span>
                            <span class="common-error-message hide"></span>
                        </p>
                        <div class="col-md-12 login_box">
                            <div id="login-reg-msg"></div>
                            <div id="login_success_msg_for_activation_link"></div>
                            <div class="form-group">
                                <label for="signin-email"><i class="fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signin-email" type="email" placeholder="<?php _e('E-mail', 'nutrafit'); ?>" value="<?php echo $_COOKIE['nutrafit_email']; ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>
                            <div class="form-group">
                                <label for="signin-password"><i class="fa fa-unlock-alt"></i>Password<span class="aster">*</span></label>
                                <input class="form-control full-width has-padding has-border" id="signin-password" type="password"  placeholder="<?php _e('Password', 'nutrafit'); ?>" value="<?php echo base64_decode($_COOKIE['nutrafit_password']); ?>">
                                <span class="cd-error-message">Error message here!</span>
                            </div>
                            <div class="form-group">
                                <div class="icheckbox_minimal-grey">
                                    <input type="checkbox" id="remember-me" <?php echo $_COOKIE['nutrafit_remember'] ? 'checked="checked"' : '' ?>>
                                    <label for="remember-me"><?php _e('Remember me', 'nutrafit'); ?></label>
                                </div>
                            </div>

                            <div class="form-group" id="forgot_section">
                                <label for="forgot"><a href="<?php echo get_home_url();?>/forgate-password/" id="forgot_btn">Forgot Password</a></label>
                            </div>

                            <div class="login_btn">
                                <input type="hidden" name="redirect_to" id="redirect_to" value="<?php echo 'nutrafit/dashboard'; //esc_attr($_SERVER["REQUEST_URI"]);        ?>" />
                                <span><i class="fa fa-sign-in"></i></span>
                                <input class="full-width" id="signin-submit" type="submit" value="<?php _e('Login', 'nutrafit'); ?>">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/loading.gif" id="loader" class="H-loader" style="display: none;" />
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                </div>
        <!-- Modal content loginsecreen_show-->
        <?php
         get_footer();
         ?>
