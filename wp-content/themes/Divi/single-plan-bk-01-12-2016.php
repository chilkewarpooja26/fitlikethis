<?php
session_start();
//unset($_SESSION["sub_plans"]);
get_header();
$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
  <div class="page-title-head"><h1 class="entry-title main_title"><?php the_title(); ?></h1></div>
  </div>
<form id="form" method = "post" action = "<?php echo home_url()?>/piechart/" class="viewpalnform">
 
  <?php 
  while ( have_posts() ) : the_post(); 
  ?>
      <?php 
      //the_title();
      //$content = get_the_content();
      $post_id=get_the_ID();
      ?> 
    <?php if( get_field('plan_banner_image') ): ?>
      <img class="hide-img" src="<?php the_field('plan_banner_image'); ?>" />
      <?php endif; ?>
      <h2 class="hide-title"><?php $plan_banner_title = get_field( "plan_banner_title" );echo $plan_banner_title;?></h2>
       <div class="allplan-show">
       <div class="container">
    <div class="row">
      <div class="view-get-plan-section">
      <?php
      if( have_rows('sub_plan') ):
      $a = 0;
      while ( have_rows('sub_plan') ) : the_row();  
      ?>
          <ul class="viewplan_listing col-lg-3">
            <li class="spciality">
          <?php 
          $sub_plan_speciality = get_sub_field('sub_plan_speciality'); 
          if($sub_plan_speciality!='')
          {
          ?>
          <span>
          <?php the_sub_field('sub_plan_speciality');
          } 
          ?>
          </span>
          </li>
          <li class="tit-sup-tit">
          <h1><?php 
          the_sub_field('sub_plan_title');
          $sub_plan_title = get_sub_field('sub_plan_title');
          ?>
        </h1>
          </li>
          <li class="duration">
          <?php  
          the_sub_field('sub_plan_duration'); echo "$nbsp Plan";
          $sub_plan_duration = get_sub_field('sub_plan_duration');
          ?>
          </li>
          <li class="priceplan">
          <?php the_sub_field('sub_plan_price');
          $sub_plan_price = get_sub_field('sub_plan_price'); 
          ?>
          <div class="plan-pay-type">
          <?php
           $payment_type_msg = get_sub_field('payment_type'); 
          if($payment_type_msg!='' && !empty($payment_type_msg))
          {
            echo " a month";
          } else
          {
          echo " one time purchase";
          }
          ?>
          </div>
          </li>

          
        <?php 
          $user_id = get_current_user_id();
          if($user_id!=0)
          {
              $plan_id=get_the_ID();      
              $sub_plan_title = get_sub_field('sub_plan_title');
              $sub_plan_duration = get_sub_field('sub_plan_duration');
              $query = "select * from " . $wpdb->prefix . "payments where user_id='".$user_id."' and post_id='".$plan_id."' and sub_plan_title='".$sub_plan_title."' and sub_plan_duration='".$sub_plan_duration."' and status='Active'";  
              $total_query = $wpdb->get_results($query);

              $row_total = $wpdb->get_row($query);
              $pur_sub_plan_title = $row_total->sub_plan_title;
              $current_date = date("Y/m/d");
              $sql_check = "select count(0) as row_count from " . $wpdb->prefix . "payments where user_id='".$user_id."' and sub_plan_title='".$pur_sub_plan_title."'";
              $row=$wpdb->get_row($sql_check);
              $count=$row->row_count;
              if($count!=0)
              {
                  $query = "select * from " . $wpdb->prefix . "payments where user_id='".$user_id."' and post_id='".$plan_id."' and sub_plan_title='".$sub_plan_title."' and sub_plan_duration='".$sub_plan_duration."' and status='Active'";  
                  $total_query = $wpdb->get_results($query);

                  $row_total = $wpdb->get_row($query);
                  $pur_sub_plan_title = $row_total->sub_plan_title;
                  $spd = $row_total->sub_plan_duration;
                  $spdvar = $spd; 
                  if($spdvar!='')
                  {
                  $spdChunks = explode(" ", $spdvar);
                  $spdvar= $spdChunks[0]; 
                  $delidate = $row_total->payment_date; 
                  $monthlyDate = strtotime("+$spdvar week".$delidate);
                  $monthly = date("Y/m/d", $monthlyDate);
                  $ExpireDate = $monthly;
                  }
                  $type = $row_total->recuring;
                          
                  if($ExpireDate<$current_date && $type!='Yes')
                  {
                  ?>
                  <li class="get-started">
                  <a class="et_pb_promo_button et_pb_button hvr-bounce-out" href="<?php echo home_url()?>/piechart/?sel=<?php echo $post_id; ?>&sub_plan=<?php echo $a; ?>">Get Started!</a>
                  </li>  
                  <?php
                  }
                  elseif($type=='No' && $ExpireDate>=$current_date && $sub_plan_title==$pur_sub_plan_title )
                  {
                  ?> 
                  <li class="get-started">
                  <a class="et_pb_promo_button et_pb_button purchace hvr-bounce-out" href="#.">Purchased</a>
                  </li>
                  <?php 
                  }elseif($type=='Yes' && $sub_plan_title==$pur_sub_plan_title )
                  {
                  ?> 
                  <li class="get-started">
                  <a class="et_pb_promo_button et_pb_button purchace" href="#.">Purchased</a>
                  </li>
                  <?php 
                  }
              }
              else //else of count "login but not purches plan"
              {   
                  $query = "select * from " . $wpdb->prefix . "payments where user_id='".$user_id."' and status='Active'";  
                  $total_query = $wpdb->get_results($query);
                  $row_total = $wpdb->get_row($query);
                  $spd = $row_total->sub_plan_duration;
                  $spdvar = $spd;
                  $spdChunks = explode(" ", $spdvar);
                  $spdvar= $spdChunks[0]; 
                  $delidate = $row_total->payment_date; 
                  $monthlyDate = strtotime("+$spdvar week".$delidate);
                  $monthly = date("Y/m/d", $monthlyDate);
                  $ExpireDate = $monthly;
                  if($ExpireDate<$current_date && $type!='Yes')  
                  {
                    ?>
                    <li class="get-started">
                     <a class="et_pb_promo_button et_pb_button" href="<?php echo home_url()?>/piechart/?sel=<?php echo $post_id; ?>&sub_plan=<?php echo $a; ?>">Get Started!</a>
                    </li> 
                    <?php
                  }
                  else
                  {
                      $sql_check = "select count(0) as row_count from " . $wpdb->prefix . "users where ID='".$user_id."' and ID NOT IN(select user_id from " . $wpdb->prefix . "payments) ";
                      $row=$wpdb->get_row($sql_check);
                      $count_user=$row->row_count;
                      if($count_user<=0)
                      {
                        /*query to show sub_plan_title on mouse over*/
                        $sql_pay = "select * from " . $wpdb->prefix . "payments where user_id='".$user_id."'";
                        $pay_row=$wpdb->get_row($sql_pay);
                        $pay_sub_plan_title=$pay_row->sub_plan_title;
                        ?>
                        <li class="get-started">
                        <a class="et_pb_promo_button et_pb_button show-tool" href="#.">Get Started!
                        <ul class="subscribe-already">
                        <li class="not-allow-start"> <span class="tooltip">You have alredy subscribed<br> <?php echo $pay_sub_plan_title;?> plan</span></li>
                        </ul>
                        </a>
                        </li> 
                        <?php
                      }
                      else
                      {
                        ?>
                        <li class="get-started">
                        <a class="et_pb_promo_button et_pb_button" href="<?php echo home_url()?>/piechart/?sel=<?php echo $post_id; ?>&sub_plan=<?php echo $a; ?>">Get Started!</a>
                        </li> 
                        <?php
                      }
                  } 
              }
              }
          else//else of user_id "if user not login"
          {
            ?>
            <li class="get-started">
            <a class="et_pb_promo_button et_pb_button" href="<?php echo home_url()?>/piechart/?sel=<?php echo $post_id; ?>&sub_plan=<?php echo $a; ?>">Get Started!</a>
            </li>  
            <?php 
          }
          ?>



          
          <?php $payment_type = get_sub_field('payment_type');?>
          </ul>
          <?php
          if( have_rows('men') ):
          while ( have_rows('men') ) : the_row();       
              $men_formula_for_calories = get_sub_field('formula_for_calories'); 
              $men_formula_for_proteins = get_sub_field('formula_for_proteins'); 
              $men_formula_for_carbs = get_sub_field('formula_for_carbs');
          endwhile;//men
          else :
          endif;//men
          ?>
          <?php
          if( have_rows('women') ):
          while ( have_rows('women') ) : the_row();       
              $women_formula_for_calories = get_sub_field('women_formula_for_calories');
              $women_formula_for_proteins = get_sub_field('women_formula_for_proteins');
              $women_formula_for_carbs = get_sub_field('women_formula_for_carbs');
          endwhile;//women
          else :
          endif;//women
          ?>
          
          <?php 
          $plan_title = get_the_title();
          $content = get_the_content();
          $_SESSION["sub_plans"][$post_id][$a] = array(
          "post_id"=>$post_id,
          "plan_title"=>$plan_title,
          "sub_plan_title"=>$sub_plan_title,
          "sub_plan_duration"=>$sub_plan_duration,
          "sub_plan_price"=>$sub_plan_price,
          "sub_plan_speciality"=>$sub_plan_speciality,
          "formula_for_calories"=>$men_formula_for_calories,
          "formula_for_proteins"=>$men_formula_for_proteins,
          "formula_for_carbs"=>$men_formula_for_carbs,
          "women_formula_for_calories"=>$women_formula_for_calories,
          "women_formula_for_proteins"=>$women_formula_for_proteins,
          "women_formula_for_carbs"=>$women_formula_for_carbs,
          "payment_type"=>$payment_type,
          "content"=>$content,
          );
          $a++;
          $post_id123=$plan_title = $sub_plan_title = $sub_plan_duration = $sub_plan_price = $sub_plan_speciality = $men_formula_for_calories = $men_formula_for_proteins = $men_formula_for_carbs = $women_formula_for_calories = $women_formula_for_proteins = $women_formula_for_carbs =$payment_type=$content= "";
          ?>
      <?php
      endwhile;//sub plan
      else :
      // no rows found
      endif;//sub plan
      ?>
  <?php 
  endwhile;//have post top 
  ?>
  </div>
    </div>
      </div>
       </div>
</form>

<div class="plan-loss-section">
<div class="container">
    <div class="row">
<div class="our-plan-loss col-lg-6">
<h2>Key Features:</h2>
<?php the_content();?>  
</div>
<div class="our-plan-loss_video col-lg-6">
<?php 
if( get_field('youtube_video_link') ):
    $youtube_video_link = get_field( "youtube_video_link" );
    $ytarray=explode("/", $youtube_video_link);
    $ytendstring=end($ytarray);
    $ytendarray=explode("?v=", $ytendstring);
    $ytendstring=end($ytendarray);
    $ytendarray=explode("&", $ytendstring);
    $ytcode=$ytendarray[0];
    echo "<div class='col-lg-12'><iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/$ytcode\" frameborder=\"0\" allowfullscreen></iframe></div>";
endif;
?>
<?php 
if( get_field('vimeo_video_link') ):
    $vimeo_video_link = get_field("vimeo_video_link");
    $ytarray=explode("/", $vimeo_video_link);
    $ytendstring=end($ytarray);
    $ytendarray=explode("?v=", $ytendstring);
    $ytendstring=end($ytendarray);
    $ytendarray=explode("&", $ytendstring);
    $vmcode=$ytendarray[0];
    echo "<div class='col-lg-12'><iframe width=\"420\" height=\"315\" src=\"https://player.vimeo.com/video/$vmcode\" frameborder=\"0\" allowfullscreen></iframe></div>";
endif;
?>
</div>
</div>
</div>
</div>
<div class="et_pb_section et_pb_section_3 et_section_regular" id="testi_moniel" style="display:none;">
<div align="center"><h1>What other people are saying<br> about Fit Like This</h1></div>
<?php echo do_shortcode('[show-testimonials pagination="on" orderby="menu_order" order="ASC" post_status="publish" layout="slider" options="transition:horizontal,adaptive:false,controls:controls,pause:4000,auto:on,columns:1,theme:speech,info-position:info-below,text-alignment:center,review_title:on,quote-content:short,charlimitextra: (...),display-image:on,image-size:thumbnail,image-shape:circle,image-effect:transparency,image-link:on"]
')
?>
</div>
</div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>

