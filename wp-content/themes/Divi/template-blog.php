
<?php
/*
Template Name: Blog Template 
*/
get_header();
?> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>

/* ============================================================
  CUSTOM PAGINATION
============================================================ */
.custom-pagination span,
.custom-pagination a {
  display: inline-block;
  padding: 2px 10px;
}
.custom-pagination a {
  background-color: #ebebeb;
  color: #2e9ed7;
}
.custom-pagination a:hover {
  background-color: #2e9ed7;
  color: #fff;
}
.custom-pagination span.page-num {
  margin-right: 10px;
  padding: 0;
}
.custom-pagination span.dots {
  padding: 0;
  color: gainsboro;
}
.custom-pagination span.current {
  background-color: #2e9ed7;
  color: #fff;
}
</style>



<?php
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
<div class="page-title-head"><h1 class="entry-title main_title"><?php the_title(); ?></h1></div>
</div>
<div class="container" id="cont-100">
<div class="row">

<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$custom_args = array(
'post_type' => 'post',	
'posts_per_page' => 2,
'status'=>'publish',
'paged' => $paged
);
$custom_query = new WP_Query( $custom_args ); ?>
<?php if ( $custom_query->have_posts() ) : ?>	
<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
	<div class="blog-page-section col-lg-12">
	
<?php if ( has_post_thumbnail()) 
{
$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
echo '<div class="blog-pic col-lg-4"><img class="img-responsive" src="' . $large_image_url[0] . '" title="' . the_title_attribute('echo=0') . '""></div>';
}
?>

<div class="blog-deiscription col-lg-8">
<h2><?php the_title();?></h2>
  <?php $content = get_the_content();echo substr($content, 0, 300);?>
	
<div class="blog-readmore"><a href="<?php echo get_permalink($post->ID)?>" title="Read More" class="btn btn-primary">Read More</a></div>	
</div>
</div>
<?php endwhile; ?>


<!-- pagination here -->
<?php
if (function_exists(custom_pagination)) {
custom_pagination($custom_query->max_num_pages,"",$paged);
}
?>
<?php wp_reset_postdata(); ?>
<?php else:  ?>
<p style="padding:50px;margin-left:440px;"><?php _e( 'Sorry, No Blog found......' ); ?></p>
<?php endif; ?>
</div>
</div>

<?php get_footer(); ?>