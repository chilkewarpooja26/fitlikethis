<?php
/**
 * Template Name: Change Password
 *
 */
get_header(); 
?>

<?php 
global $current_user, $wp_roles;
$error = array();    
$user_id = get_current_user_id();
if($user_id=='')
{
    wp_redirect( home_url() ); exit;
}    
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) 
{
	    /* Update user password. */
	if (strlen($_POST['pass1']) <= 7)
	{
	  $error[] = __('Your Password must contain at least 8 characters.');
	} 
	else 
	{
		if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) 
        {
    		if ( $_POST['pass1'] == $_POST['pass2'] )
              {  
    		  wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
              //wp_redirect( site_url('profile/') );exit;
              $error[] = '<p class="success_msg">You have successfully changed your password.</p>';
            }
    		else
              {  
    		$error[] = __('The passwords and confirm password not match.', 'profile');
             }
		}
	}
	 if ( count($error) == 0 ) 
	{
	//action hook for plugins and extra fields saving
	do_action('edit_user_profile_update', $current_user->ID);
	wp_redirect( get_permalink() );
	exit;
	}
}
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
<div class="page-title-head">
<h1 class="entry-title main_title">Change Password</h1>
</div>
</div>
            <div class="reset-pass-section">
                <div class="container">
                    <div class="row">
                
                <form method="post" id="adduser" action="<?php the_permalink(); ?>" class="col-xs-offset-3 col-lg-6">
                   <?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
                    <div class="form-group">
                        <label for="pass1"><?php _e('New Password <span>*</span>', 'profile'); ?> </label>
                        <input class="text-input" name="pass1" type="password" id="pass1" required="" />
                    </div><!-- .form-password -->
                    <div class="form-group">
                        <label for="pass2"><?php _e('Confirm Password <span>*</span>', 'profile'); ?></label>
                        <input class="text-input" name="pass2" type="password" id="pass2" required=""/>
                    </div><!-- .form-password -->
                    
                    <?php 
                        //action hook for plugin and extra fields
                        do_action('edit_user_profile',$current_user); 
                    ?>
                    <div class="form-submit form-group">
                        <?php echo $referer; ?>
                        <div class="col-lg-6 chng-pawd-btn"><input name="updateuser" type="submit" id="updateuser" class="submit button comm-all-btn hvr-rectangle-out" value="<?php _e('Submit', 'profile'); ?>" />
                            </div>
                        <div class="col-lg-6 can-pawd-btn"><a href="/nutrafitDivi/profile/" class="cancel button comm-all-btn hvr-rectangle-out">Cancel</a> </div>
                        <?php wp_nonce_field( 'update-user' ) ?>
                        <input name="action" type="hidden" id="action" value="update-user" />
                    </div><!-- .form-submit -->
                </form><!-- #adduser -->
          </div>
        </div>
        </div>
  
   </div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>






