<?php
/*
Template Name:  Edit Profile Template 
*/
get_header(); 
?>
<!-- <script src="<?php echo bloginfo('template_url')?>/js/jquery.fs.dropper.min.js" type="text/javascript"></script> -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/jquery.fs.dropper.min.css" media="all" type="text/css" />

<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
<div class="page-title-head">
<h1 class="entry-title main_title">
<?php
$user_id = get_current_user_id();
global $wpdb;
$profile_query = "select * from " . $wpdb->prefix . "users as u where u.ID='".$user_id."'"; 
$profile_row=$wpdb->get_row($profile_query);
$user_type= get_user_meta($user_id,"user_type", true);
$gender= get_user_meta($user_id,"gender", true);
if($user_type=='User')
{
echo "User Profile";
}else
{
echo "Nutritionist Profile";
}
?>
</h1>
</div>
</div>
<section class="payment-page user-profile-edit-page">
<div class="container">
<div class="row">
<?php
if($user_id!='')
{
global $current_user, $wp_roles;
$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    /* Update user information. */
    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name']));
       global $wpdb;
       $sql_check2 ="UPDATE nu_users set display_name='".esc_attr( $_POST['first-name'])."' WHERE ID LIKE  '".$user_id."' ";
       $row=$wpdb->get_row($sql_check2);
    if ( !empty( $_POST['gender'] ) )
         update_user_meta($current_user->ID, 'gender', esc_attr( $_POST['gender'] ) );   
    if ( !empty( $_POST['age'] ) )
        update_user_meta($current_user->ID, 'age', esc_attr( $_POST['age'] ) );
    if ( !empty( $_POST['weight'] ) )
        update_user_meta( $current_user->ID, 'weight', esc_attr( $_POST['weight'] ) );
    //if ( !empty( $_POST['allergies'] ) )
        //update_user_meta( $current_user->ID, 'allergies', esc_attr( $_POST['allergies'] ) );
    if ( !empty( $_POST['goal'] ) )
        update_user_meta( $current_user->ID, 'goal', esc_attr( $_POST['goal'] ) );
    if ( !empty( $_POST['description-about-me'] ) )
        update_user_meta( $current_user->ID, 'description-about-me', esc_attr( $_POST['description-about-me'] ) );
    if ( !empty( $_POST['allergiesone'] ) )
        update_user_meta( $current_user->ID, 'allergiesone',  $_POST['allergiesone'] );

    /* Redirect so the page will show updated info.*/
    if ( count($error) == 0 ) {
        do_action('edit_user_profile_update', $current_user->ID);
        //wp_redirect( get_permalink() );
       // exit;
        wp_redirect( site_url('profile/') );exit;

    }
}
?>
 <form method="post" id="adduser" action="<?php the_permalink(); ?>" class="user-profile-edit-user col-xs-offset-2">
 <?php
 if($user_type=='User')
{
?>                    
<div class="user-profile-edit-section">
             <div class="form-group">
                <p class="form-name">
                        <label for="first-name"><?php _e('Name', 'profile'); ?></label>
                        <input class="text-input form-control" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" required="required"/>
<div class="error_text_weight"><p id="name_error_text"></p></div>
                    </p>
                    </div>
<div class="form-group">
                    <p class="form-email">
                        <label for="email"><?php _e('Email', 'profile'); ?></label>
                        <input class="text-input form-control" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" readonly="" />
                    </p>
                    </div>
<div class="form-group gend-select">
                    <p class="form-gender">
                     <label for="email"><?php _e('Gender', 'profile'); ?></label>
                   <span><input type="radio" name="gender" <?php  checked( $gender, 'Male' ); ?> value="Male" required="required">Male</span>
                <span><input type="radio" name="gender" <?php  checked( $gender, 'Female' ); ?> value="Female" required="required">Female</span>
            </p>
            </div>

 <!-- <div class="form-group">
                    <p class="form-allergies">
                        <label for="allergies"><?php _e('Allergies', 'profile'); ?></label>
                        <input class="text-input form-control" name="allergies" type="text" id="allergies" value="<?php the_author_meta( 'allergies', $current_user->ID ); ?>" />
                    </p>
                    </div> -->
<div class="form-group">
                    <p class="form-allergies alg-sec">
                        <label for="allergies"><?php _e('Allergies', 'profile'); ?></label>
                     <?php 

                        $allergiesone = get_user_meta($current_user->ID,"allergiesone", true);
                        if($allergiesone!='')
                        {
                        $resultstr = array();
                        foreach ($allergiesone as $result) { $resultstr[] = $result;}
                        $allergie = implode(",",$resultstr);
                        }
                          global $wpdb;
                            $allergies_list_query = "SELECT `meta_value` FROM `nu_postmeta` WHERE `meta_key`= '_um_custom_fields'";
                            $allergies_list_query_result = $wpdb->get_results($allergies_list_query, 'ARRAY_A');
                            $allergies_list_query_result;
                            $a = unserialize($allergies_list_query_result[0]['meta_value']);
                            
                             $mArray = $a['allergiesone']['options'];
                        ?>
                        
                       <select multiple="multiple" id="allergiesone" name="allergiesone[]">
                        <?php 
                        foreach($mArray as $alle){ ?>
                            <option value="<?php echo $alle; ?>"><?php echo $alle; ?></option>
                            <?php } ?>
                            
                        </select>
                        <script type="text/javascript">
                            var s = <?php echo json_encode($allergie); ?>;
                            var ab=s.split(",");
                            $('#allergiesone').val(ab);
                        </script>
                    </p>
                    </div>                      
<div class="form-group">
                     <p class="form-age">
                        <label for="age"><?php _e('Age', 'profile'); ?></label>
                        <input class="text-input form-control" name="age" type="text" id="age" value="<?php the_author_meta( 'age', $current_user->ID ); ?>" required="required"/>
						<span class="age-sec">years</span>
                        <div class="error_text_weight"><p id="age_error_text"></p></div>
					</p>
                    </div>
<div class="form-group">
                    <p class="form-weight">
                        <label for="weight"><?php _e('Weight', 'profile'); ?></label>
                        <input class="text-input form-control" name="weight" type="text" id="weight" value="<?php the_author_meta( 'weight', $current_user->ID ); ?>" required="required"/>
						<span class="lbs-sec">lbs</span>
                         <div class="error_text_weight"><p id="weight_error_text"></p></div>
					</p>
                    </div>

<div class="form-group">
                    <p class="form-goal">
 <label for="weight"><?php _e('Your Fitness Goal', 'profile'); ?></label>
<?php
                        $goal = get_user_meta($current_user->ID, "goal", true);
                        ?>
                       <select id="goal" name="goal" class="text-input form-control">
    <option value="Loose Weight"<?php if ($goal == 'Loose Weight') echo ' selected="selected"'; ?>>Loose Weight</option>
    <option value="Gain Weight"<?php if ($goal == 'Gain Weight') echo ' selected="selected"'; ?>>Gain Weight</option>
    <option value="Maintain Weight"<?php if ($goal == 'Maintain Weight') echo ' selected="selected"'; ?>>Maintain Weight</option>
</select>
                    </p>
                    </div>

                </div>
<?php
}
else
{
?>
<div class="nutrnist-profile">
  
                    <div class="form-group">
                <p class="form-name">
                        <label for="first-name"><?php _e('Name', 'profile'); ?></label>
                        <input class="text-input form-control" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" required="required"/>
<div class="error_text_weight"><p id="name_error_text"></p></div>
                    </p>
                      </div>
                    <div class="form-group">
                    <p class="form-email">
                        <label for="email"><?php _e('Email', 'profile'); ?></label>
                        <input class="text-input form-control" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" readonly="" />
                    </p>
                      </div>
                    <div class="form-group gend-select">
                    <p class="form-gender">
                     <label for="email"><?php _e('Gender', 'profile'); ?></label>
                   <span><input type="radio" name="gender" <?php  checked( $gender, 'Male' ); ?> value="Male" required="required">Male </span>
                    <span><input type="radio" name="gender" <?php  checked( $gender, 'Female' ); ?> value="Female" required="required"> Female </span>
                  </p>
                    </div>

                    
 
 <div class="form-group">
                     <p class="form-age">
                        <label for="age"><?php _e('Age', 'profile'); ?></label>
                        <input class="text-input form-control" name="age" type="text" id="age" value="<?php the_author_meta( 'age', $current_user->ID ); ?>" required="required"/>
						<span class="age-sec">years</span>
                         <div class="error_text_weight"><p id="age_error_text"></p></div>
					</p>
                      </div>
                    <div class="form-group text-field">
                    <p class="form-description">
                        <label for="description"><?php _e('Description', 'profile'); ?></label>
                        <textarea class="text-input form-control" name="description-about-me"><?php the_author_meta( 'description-about-me', $current_user->ID ); ?></textarea>

                    </p>
                      </div>
                </div>

 <?php 
 }
 ?>                   <?php 
                        //action hook for plugin and extra fields
                        do_action('edit_user_profile',$current_user); 
                    ?>
                   

									<div class="form-group upload-form">
                                        <label class="col-sm-3 pd-L-0 control-label"><?php _e('Upload Profile Picture', 'viralytics'); ?> : 
                                        </label>
                                        <div class="col-sm-9 pd-0">
                                            <ul class="profile_image_gallery">
                                                <?php
                                                $fbpicture = get_user_meta($user_id,"fb_profile_picture", true);
                                                $image_attributeskk = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
                                                if (get_user_meta($user_id,"profile_image", true) != '' || get_user_meta($user_id,"fb_profile_picture", true) != ''){
                                                    if($image_attributeskk){
                                                        $imgfb = $image_attributeskk;  
                                                        $profileimagefb = "profile_image";
                                                    }else{
                                                        $imgfb = $fbpicture;
                                                        $profileimagefb = "fb_profile_picture";
                                                    }                                                    
                                                    
                                                    ?>
                                                    <li class="featured">
                                                        <span class="file">
                                                            <div class="image-single" id="<?php echo get_user_meta($user_id,"profile_image", true); ?>">
                                                                <span>
                                                                    <img src="<?php echo $imgfb; ?>" class="uploaded_image" />
                                                                
                                                                </span>
                                                            </div>
                                                             <div id="<?php echo get_user_meta($user_id, $profileimagefb, true); ?>" class="delete-tags">
                                                                <a href="javascript:void(0)" class="delete-attachment"><i class="fa fa-trash-o"></i></a>
                                                            </div>
                                                        </span>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                            <div class="profile_image"></div>
                                            <div class="profile_image dropper msg-dropper-block">
                        <div class="dropper-dropzone">Drag and drop files or click to select</div>
                                    <input class="dropper-input" type="file">
                                    </div>
                                            <div class="filelists">
                                                <ul class="filelist queue"></ul>
                                            </div>
                                            <input type="hidden" id="profile_image_gallery" value="<?php echo 1; ?>" />
                                            <input type="hidden" id="max_profile_image_gallery" value="<?php echo 1; ?>" />
                                        </div>
                                    </div>
    <div class="form-group update-profile"><p class="form-submit sub-text">
	                    <?php echo $referer; ?>
                        <input name="updateuser" type="button" id="updateuser" class="submit button comm-all-btn can-sub-btn hvr-shutter-in-horizontal" value="<?php _e('Update Profile', 'profile'); ?>" onclick="beforeSubmit();"/>
                        <input type="hidden" name="nutname" id="nutname" value="<?php echo $user_type;?>">
                        <?php wp_nonce_field( 'update-user' ) ?>
                        <input name="action" type="hidden" id="action" value="update-user" />
					<a class="um-button um-alt pro-can-btn hvr-rectangle-out" href="/nutrafitDivi/profile/">Cancel</a>	
						
                    </p>
                    </div> <!-- .form-submit -->
            
</div>  
</div>
</form>
<?php    
}//if user not login
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>
<script type="text/javascript">
beforeSubmit = function()
{
    var x,y,types,name;
    var types=document.getElementById("nutname").value;
    y = document.getElementById("age").value;
    name=document.getElementById("first-name").value;
    if(types=='User')
    {
        x = document.getElementById("weight").value;

        if((name.trim())==='')
        {
            text = "Name is required.";
            document.getElementById("name_error_text").innerHTML = text;
            return false;
        }
       if(/^[a-zA-Z ]*$/.test(name) == false) 
       {
            text = "Please enter valid name.";
            document.getElementById("name_error_text").innerHTML = text;
            return false;
       } 
        if (isNaN(x) || x < 89 || x > 664) 
        {
            text = "Enter valid weight between 89 to 664 pounds.";
            document.getElementById("weight_error_text").innerHTML = text;


            if (isNaN(y))
            {
                text = "Enter age in digit only";
                document.getElementById("age_error_text").innerHTML = text;
                return false;
            }
            return false;
        }
        else if (isNaN(y) || y < 13 || y > 80)
        {
            text = "Enter valid age between 13 to <span class='notranslate'>80</span>.";
            document.getElementById("age_error_text").innerHTML = text;
            return false;
        }else 
        {
          $("#adduser").submit();
        }
    }else
    {
        if((name.trim())==='')
        {
            text = "Name is required.";
            document.getElementById("name_error_text").innerHTML = text;
            return false;
        }
       if(/^[a-zA-Z ]*$/.test(name) == false) 
       {
            text = "Please enter valid name.";
            document.getElementById("name_error_text").innerHTML = text;
            return false;
       } 
        if (isNaN(y) || y < 13 || y > 80)
        {
        text = "Enter valid age between 13 to 80";
        document.getElementById("age_error_text").innerHTML = text;
        return false;
        }else 
        {
        $("#adduser").submit();
        }
    }    
}
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
