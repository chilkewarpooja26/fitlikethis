<?php
/*
Template Name: Invitations Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section nutri_invitations">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
<div class="page-title-head">
<h1 class="entry-title main_title">Invitations</h1>
</div>
</div>
<section class="payment-page user-page-profile">
<div class="container">
<div class="row">
  <form class="dashboard-inner-form">
<?php
$user_id = get_current_user_id();
if($user_id!='')
{
  global $wpdb;
  $name_query = "select * from " . $wpdb->prefix . "invite_user where user_id='".$user_id."' and invite_status='0'";
  $name_result = $wpdb->get_results($name_query);
  if($name_result)
  {
    foreach($name_result as $name_row)
    {
      $nutritionist_id=$name_row->nutritionist_id;
      $name = (get_user_meta($nutritionist_id,"first_name", true));
      $image_url = wp_get_attachment_url( get_user_meta($nutritionist_id,"profile_image", true) );
      $description = (get_user_meta($nutritionist_id,"description-about-me", true));
      $invitation_date=$name_row->invitation_date;
      ?>
      
      <div class="form-group award-win">
      <p class="nutri-descrip">
      
      <?php
      if($image_url)
      {?>
      <img src="<?php echo $image_url; ?>" height="50" width="50"> 
      <?php 
      }else
      {?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="50" height="50">
      <?php 
      }
      ?>
      <b><?php echo $name;?> </b> 
       <div class="short-descrip"><?php echo $description;?> </div>
      
      </p>
      <p class="invit_date_info"><?php echo $invitation_date;?></p>
      <p class="form-group"><a class="comm-all-btn" href="javascript:void(0)"  onclick="my_invitation('<?php echo $nutritionist_id;?>','1')">Accept</a>
        <a  class="comm-all-btn" href="javascript:void(0)" onclick="my_invitation('<?php echo $nutritionist_id;?>','2')">Reject</a>
      </p>
      </div>
    <?php                     
    }
  }else
  {
  echo  '<div class="norecordlist">No invitations found.</div>';
  }
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>
<script type="text/javascript">


 function my_invitation(nutraid,status)
 {
        var dataObj = {nutri_id:nutraid,status:status};
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/user_type.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
              if(html.status == 'success'){
                alert(html.msg);
                window.location.reload();
              }else{
                alert(html.msg);
                 window.location.reload();
              }
          }
        }
        );
      return false;
}

</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
