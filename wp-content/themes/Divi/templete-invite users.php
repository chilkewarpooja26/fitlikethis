<?php
/*
Template Name: Invite Users Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section nutri_invitations">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
<div class="page-title-head">
<h1 class="entry-title main_title">User List</h1>
</div>
</div>
<section class="payment-page user-page-profile">
<div class="container">
<div class="row">
  <form class="dashboard-inner-form">
<?php
$nutritionists_id = get_current_user_id();
if($nutritionists_id!='')
{
  global $wpdb;
  $name_query = "SELECT nu_users.ID,nu_users.display_name,nu_users.user_email,nu_usermeta.meta_value 
                        AS user_type
                        FROM nu_users, nu_usermeta
                        WHERE nu_users.ID = nu_usermeta.user_id
                        AND nu_usermeta.meta_key = 'payment_status'
                        AND nu_usermeta.meta_value = '1' 
                        order by ID";
  $name_result = $wpdb->get_results($name_query);
  if($name_result)
  {
    foreach($name_result as $name_row)
    {
      $paid_user_id=$name_row->ID;
      $name = (get_user_meta($paid_user_id,"first_name", true));
      $image_url = wp_get_attachment_url( get_user_meta($paid_user_id,"profile_image", true) );
      ?>
      
      <div class="form-group award-win">
      <p class="nutri-descrip">
      
      <?php
      if($image_url)
      {?>
      <img src="<?php echo $image_url; ?>" height="50" width="50"> 
      <?php 
      }else
      {?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="50" height="50">
      <?php 
      }
      ?>
      <b><?php echo $name;?> </b> 
      </p>
      <p class="form-group">
      <?php
      $plan_query="SELECT * from " . $wpdb->prefix . "invite_user where user_id= '".$paid_user_id."' and nutritionist_id= '".$nutritionists_id."'";
      $plan_result = $wpdb->get_row($plan_query);
      if($plan_result->user_id)
      {
      ?> 
      <span id="id<?php echo $paid_user_id; ?>"><a class="comm-all-btn" href="javascript:void(0)">Invited</a></span>
      <?php } else {?>
      <span id="id<?php echo $paid_user_id; ?>"><a class="comm-all-btn" href="javascript:void(0)"  onclick="invite_user('<?php echo $nutritionists_id;?>','<?php echo $paid_user_id;?>')">Invite</a></span>
       <?php }?>

      </p>
      </div>
    <?php                     
    }
  }else
  {
  echo  '<div class="norecordlist">User list is empty.</div>';
  }
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>
<script type="text/javascript">


 function invite_user(nid,uid)
 {

        var dataObj = {nutritionists_id:nid,user_id:uid};
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/ajax_meal_plan.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
              if(html.status == 'success')
                {
                  var invited_user_id = html.user_id;
                  $("#id"+invited_user_id).html('<a class="comm-all-btn" href="javascript:void(0)")">Invited</a>');
                }
          }
        }
        );
      return false;
}

</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
