<?php
/*
Template Name: My Block Items Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
<div class="page-title-head">
<h1 class="entry-title main_title"><?php the_title(); ?></h1>
</div>
</div>
<section class="payment-page user-page-profile myblock-items">
<div class="container">
<div class="row">
  <form class="dashboard-inner-form">
<?php
$user_id = get_current_user_id();
if($user_id!='')
{
  global $wpdb;
  $block_items_query = "select * from " . $wpdb->prefix . "block_items where user_id='".$user_id."'";
  $block_items_result = $wpdb->get_results($block_items_query. " order by id DESC");
  if($block_items_result)
  {
    foreach($block_items_result as $block_items_row)
    {
      $recipe_id=$block_items_row->recipe_id;
      $recipe_name =$block_items_row->recipe_name;
      $image_url =$block_items_row->recipe_image;
      $recipe_calories =$block_items_row->recipe_calories;
      $recipe_protien =$block_items_row->recipe_protien;
      $recipe_fats =$block_items_row->recipe_fats;
      $recipe_carbs =$block_items_row->recipe_carbs; 
      ?>

      <div class="form-group award-win">
      <p class="nutri-descrip">
      
      <?php
      if($image_url)
      {?>
      <img src="<?php echo $image_url; ?>" height="150" width="150"> 
      <?php 
      }else
      {?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/nutrinix.jpg" width="50" height="50">
      <?php 
      }
      ?>
     <div class="block-item-content">
        <b><?php echo $recipe_name;?> </b>
        <div class="details">
          <ul>
            <li>
              <span>Cal</span>
              <p><?php echo  $recipe_calories;?></p>
            </li>
            <li class="purple">
              <span>P</span>
              <p><?php echo $recipe_protien."g";?></p>
            </li>
            <li class="grenn">
              <span>F</span>
              <p><?php echo $recipe_fats."g";?></p>
            </li>
            <li class="orange">
              <span>C</span>
              <p><?php echo $recipe_carbs."g";?></p>
            </li>
          </ul>
        </div>  
      </div>
      <a class="comm-all-btn del_btn" href="javascript:void(0)"  onclick="delete_block_items('<?php echo $recipe_id;?>',<?php echo $user_id;?>)">Delete</i>
</a>
      </p>
      </p>
      </div>
    <?php                     
    }
  }else
  {
  echo  '<div class="norecordlist">Block item list is empty.</div>';
  }
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>

 <script type="text/javascript">
 function delete_block_items(rid,uid)
 {
        var dataObj = {delblock_recipe_id:rid,delblock_user_id:uid};
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/ajax_meal_plan.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
              if(html.status == 'success'){
                alert(html.msg);
                window.location.reload();
              }else{
                alert(html.msg);
                 window.location.reload();
              }
          }
        }
        );
      return false;
}
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
