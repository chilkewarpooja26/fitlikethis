<?php
/*
Template Name: My Nutritionists Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section mynutritionist">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
<div class="page-title-head">
<h1 class="entry-title main_title">My Nutritionists</h1>
</div>
</div>
<section class="payment-page user-page-profile">
<div class="container">
<div class="row">
  <form class="dashboard-inner-form">
<?php
$user_id = get_current_user_id();
if($user_id!='')
{
  global $wpdb;
  $name_query = "select * from " . $wpdb->prefix . "invite_user where user_id='".$user_id."' and invite_status='1'";
  $name_result = $wpdb->get_results($name_query);
  if($name_result)
  {
    foreach($name_result as $name_row)
    {
      $nutritionist_id=$name_row->nutritionist_id;
      $name = (get_user_meta($nutritionist_id,"first_name", true));
      $image_url = wp_get_attachment_url( get_user_meta($nutritionist_id,"profile_image", true) );
      $age = (get_user_meta($nutritionist_id,"age", true));
      $description = (get_user_meta($nutritionist_id,"description-about-me", true));
      ?>
      <div class="form-group award-win">
      <p class="nutri-descrip">
      
      <?php
      if($image_url)
      {?>
      <img src="<?php echo $image_url; ?>" height="50" width="50"> 
      <?php 
      }else
      {?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="50" height="50">
      <?php 
      }
      ?>
      <b>Name: <?php echo $name;?> </b> 
      <div class="short-descrip">
       <div class="col-md-3 text-centre"><a href="javascript:void(0);" class="comm-all-btn" id="blockunblock<?php echo $name_row->nutritionist_id;?>" onclick="blockuser('<?php echo get_current_user_id(); ?>','<?php echo $nutritionist_id;?>');"><?php if('Block' == $name_row->block_status){ echo "Unblock"; } elseif('Unblock' == $name_row->block_status){ echo "Block"; } ?></a></div>
      <p>Age: <?php echo $age;?></p>
      <p>Description: <?php echo $description;?></p> 
      <div id="divblock<?php echo $name_row->nutritionist_id;?>" class="col-md-3 text-centre <?php if('Block' == $name_row->block_status){echo 'block-elem';}  ?>"><a href="<?php echo get_home_url();?>/messaging" class="comm-all-btn <?php if('Block' == $name_row->block_status){echo 'blockavoidclicks';}  ?>" id="msg-btn<?php echo $name_row->nutritionist_id;?>">Sent Messages</a></div>
      </div>
      </p>
      </div>
    <?php                     
    }
  }else
  {
  echo  '<div class="norecordlist">Nutritionist list is empty.</div>';
  }
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
  
  function blockuser(currentuserid,nutritionistid){

        var block_currentuserid = currentuserid;
        var block_nutritionistid = nutritionistid;
        var dataObj = {block_currentuserid:block_currentuserid,block_nutritionistid:block_nutritionistid}; 
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/ajax_meal_plan.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
            
            
            if('Block' == html.msg){
              $('#blockunblock'+nutritionistid).html('Unblock');
              $('#msg-btn'+nutritionistid).addClass('blockavoidclicks');
              $('#divblock'+nutritionistid).addClass('block-elem');
            }
            else{
              $('#blockunblock'+nutritionistid).html('Block');
              $('#msg-btn'+nutritionistid).removeClass('blockavoidclicks');
              $('#divblock'+nutritionistid).removeClass('block-elem');
            }
          }
        }
        );

  }
</script>
<?php get_footer(); ?>
