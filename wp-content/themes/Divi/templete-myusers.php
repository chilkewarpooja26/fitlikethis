<?php
/*
Template Name: My Users Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section mynutritionist">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
<div class="page-title-head">
<h1 class="entry-title main_title">My Users</h1>
</div>
</div>
<section class="payment-page user-page-profile">
<div class="container">
<div class="row">
  <form class="dashboard-inner-form">
<?php
$nutritionist_id = get_current_user_id();
if($nutritionist_id!='')
{
  global $wpdb;
  $name_query = "select * from " . $wpdb->prefix . "invite_user where nutritionist_id='".$nutritionist_id."' and invite_status='1'";
  $name_result = $wpdb->get_results($name_query);
  if($name_result)
  {
    foreach($name_result as $name_row)
    {
      $user_id=$name_row->user_id;
      $name = (get_user_meta($user_id,"first_name", true));
      $image_url = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
      $age = (get_user_meta($user_id,"age", true));
      $weight = (get_user_meta($user_id,"weight", true));
      ?>
      <div class="form-group award-win">
      <p class="nutri-descrip">
      
      <?php
      if($image_url)
      {?>
      <img src="<?php echo $image_url; ?>" height="50" width="50"> 
      <?php 
      }else
      {?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="50" height="50">
      <?php 
      }
      ?>
      <b>Name: <?php echo $name;?> </b> 
      <div class="short-descrip">
      <div class="col-md-3 text-centre"><a href="javascript:void(0);" class="comm-all-btn" id="blockunblock<?php echo $user_id;?>" onclick="blockuser('<?php echo get_current_user_id(); ?>','<?php echo $user_id;?>');"><?php if('Block' == $name_row->block_status){echo 'Unblock'; }else{echo 'Block';} ?></a></div>
      <p>Age: <?php echo $age;?></p>
      <p>Weight: <?php echo $weight;?></p> 
      </div>
      </p>
      </div>
    <?php                     
    }
  }else
  {
  echo  '<div class="norecordlist">Users list is empty.</div>';
  }
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</div>
</div>
</section>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
  
  function blockuser(currentuserid,userid){

        var block_currentuserid = currentuserid;
        var block_nutritionistid = userid;
        var dataObj = {block_nutri_currentuserid:block_currentuserid,block_nutri_usertid:block_nutritionistid}; 
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/ajax_meal_plan.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
            
            $('#blockunblock'+userid).html(html.msg);
            
          }
        }
        );

  }
</script>

<?php get_footer(); ?>
