<?php
/*
Template Name:  Profile Template 
*/
get_header(); 
?>
<div id="main-content" class="inner-pages-section">
<div class="sub-banner plan-page">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg" alt="inner-banner-image">
<div class="page-title-head">
<h1 class="entry-title main_title">
<?php
$user_id = get_current_user_id();
global $wpdb;
$profile_query = "select * from nu_users as u where u.ID='".$user_id."'"; 
$profile_row=$wpdb->get_row($profile_query);
$user_type= get_user_meta($user_id,"user_type", true);
if($user_type=='User')
{
echo "User Profile";
}else
{
echo "Nutritionist Profile";
}
?>
</h1>
</div>
</div>
<section class="payment-page user-page-profile">
<div class="container">
<div class="row">
  <form class="user-profile col-xs-offset-4">
<?php
if($user_id!='')
{
global $wpdb;
$profile_query = "select * from nu_users as u where u.ID='".$user_id."'"; 
$profile_row=$wpdb->get_row($profile_query);
$user_type= get_user_meta($user_id,"user_type", true);
if($user_type=='User')
{
?>

<div class="form-group user_name"><p><?php echo $name= get_user_meta($user_id,"first_name", true);?></p></div>
<div class="form-group email"><p><a><?php echo $eamil=$profile_row->user_email;?></a></p></div>
<div class="form-group gend"><p><b>Gender : </b>
<?php 
$gender= get_user_meta($user_id,"gender", true);
if($gender=='') {echo "NA";} else  {echo $gender;}
?>
</p></div>
<div class="form-group age"><p><b>Age : </b>
<?php 
$age= get_user_meta($user_id,"age", true);
if($age=='') {echo "NA";} else  {echo $age;}
?>
</p></div>
<div class="form-group wieght"><p><b>Weight : </b>
<?php 
$weight= get_user_meta($user_id,"weight", true);
if($weight=='') {echo "NA";} else  {echo $weight;}
?>
</p></div>
<?php 
$allergiesone = get_user_meta($user_id,"allergiesone", true);
if($allergiesone!='')
{
?> 
<div class="form-group goal"><p><b>Allergies : </b><?php 
$resultstr = array();
foreach ($allergiesone as $result) { $resultstr[] = $result;}
$allergie = implode(", ",$resultstr);
echo $allergie;
?></p></div>
<?php 
}
?>
<div class="form-group goal"><p><b>Your Fitness Goal : </b>
<?php 
$goal= get_user_meta($user_id,"goal", true);
if($goal=='') {echo "NA";} else  {echo $goal;}
?>
</p></div>
<?php 
$fbpicture = get_user_meta($user_id,"fb_profile_picture", true); 
$image_attributeskk = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
if($image_attributeskk)
{
$image_attributes = $image_attributeskk;
?>
<div class="form-group profile-pic"><img src="<?php echo $image_attributes;?>" width="100" height="100"></div>
<?php
}elseif ($fbpicture) {
  ?>
<div class="form-group profile-pic"><img src="<?php echo $fbpicture;?>" width="100" height="100"></div>
<?php
}


else
{
?>
<div class="form-group profile-pic"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="100" height="100"></div>
<?php 
}


$plan_query = "select post_id,sub_plan_duration,payment_date from nu_payments where user_id='".$user_id."' order by payment_id ASC"; 
               $paln_result = $wpdb->get_row($plan_query);
               $plan_id= $paln_result->post_id;

               /* get paln expire date*/
               $spd = $paln_result->sub_plan_duration;
               $spdvar = $spd; 
               if($spdvar!='')
               {
               $spdChunks = explode(" ", $spdvar);
               $spdvar= $spdChunks[0]; 
               $delidate = $paln_result->payment_date; 
               $monthlyDate = strtotime("+$spdvar week".$delidate);
               $monthly = date("Y/m/d", $monthlyDate);
               $ExpireDate = $monthly;
               }
              $current_date = date("Y/m/d");
               
              $custom_args = array('post_type' => 'plan');
              $custom_query = new WP_Query( $custom_args ); 
              if ($custom_query->have_posts())
              {
              while ($custom_query->have_posts()) 
             {
             $custom_query->the_post();
             if(have_rows('sub_plan') ):
             while ( have_rows('sub_plan') ) : the_row();            
             $award = get_sub_field('award');            
             $myplan_id=get_the_ID();
             if($myplan_id ==$plan_id && $award!='') 
            {
            if($ExpireDate<$current_date)
            {
            //echo $award;      
            ?>
            <!--<div class="form-group award-win"><p><b>Awards :</b> <img src="<?php //echo $award;?>" width="100" height="100"></p></div>-->
            <?php                        
            }else
            {
            echo "";
            } 
            }
            else 
           { 
           echo "";
           }
          endwhile;//sub plan
            endif;//sub plan
          }//while custom_query
         
            }//if custom_query

}else
  {
  ?>
  <div class="form-group user_name"><p>  <?php echo $name= get_user_meta($user_id,"first_name", true);?></p></div>
  <div class="form-group email"><p><a><?php echo $eamil=$profile_row->user_email;?></a></p></div>
  <div class="form-group gend"><p><b>Gender : </b>
  <?php 
  $gender= get_user_meta($user_id,"gender", true);
  if($gender=='') {echo "NA";} else  {echo $gender;}
  ?>
  </p></div>
 <div class="form-group age"> <p> <b>Age : </b>
 <?php
 $age= get_user_meta($user_id,"age", true);
 if($age=='') {echo "NA";} else  {echo $age;}
 ?>
 </p></div>
  <div class="form-group discr"><p> <b>Description :</b> 
  <?php 
  $description= get_user_meta($user_id,"description-about-me", true);
 if($description=='') {echo "NA";} else  {echo $description;}
  ?>
  </p></div>
 
  <?php  
  $fbpicture = get_user_meta($user_id,"fb_profile_picture", true);
  $image_attributeskk = wp_get_attachment_url( get_user_meta($user_id,"profile_image", true) );
  if($image_attributeskk)
  {
  $image_attributes = $image_attributeskk;
  ?>
 <div class="form-group profile-pic"> <img src="<?php echo $image_attributes;?>" width="100" height="100"></div>
  <?php
  }elseif($fbpicture){ ?>

    <div class="form-group profile-pic"> <img src="<?php echo $fbpicture;?>" width="100" height="100"></div>  

 <?php }
  else
  {
  ?>
  <div class="form-group profile-pic"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/default-profile.jpg" width="100" height="100"></div>
  <?php 
  }
  }
?>

<div class="form-group edit-profile"><a href="<?php echo get_home_url();?>/edit-profile" class="btn-dash comm-all-btn hvr-rectangle-out">Edit Profile</a></div>
<?php    
}
else
{
  wp_redirect( home_url() ); exit;
}
?>
</form>
</div>
</div>
</section>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
