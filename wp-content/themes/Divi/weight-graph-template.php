<?php
/*
Template Name: Weight Graph Template 
*/
get_header();
?> 
<?php 
global $wpdb;
$user_id = get_current_user_id();
if ($user_id == 0) 
{
wp_redirect( home_url() ); exit;
} 
else 
{
$sql_check_login ="SELECT user_id FROM `nu_weight_tracker` WHERE `user_id` LIKE  '".$user_id."' ";
$row_see=$wpdb->get_row($sql_check_login);
?>

<div id="main-content" class="inner-pages-section workout-temp">
<!--		<div class="sub-banner dash-sect">
<img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/inner-banner.jpg">
  <div class="page-title-head"><h1 class="entry-title main_title"><?php //the_title(); ?></h1></div>
  </div>-->
	<?php if ( ! $is_page_builder_used ) : ?>
	<!-- <div class="container">-->
		<div id="content-area hello" class="clearfix">
			<?php endif; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! $is_page_builder_used ) : ?>
				<?php
				$thumb = '';
				$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
				$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
				$classtext = 'et_featured_image';
				$titletext = get_the_title();
				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
				$thumb = $thumbnail["thumb"];
				if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
				print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>
				<?php endif; ?>
				<div class="entry-content" id="custom_message">
				<h1 class="hide">&nbsp;</h1>
				<?php
				// ********Home page slider and pie chart *******//
				the_content();
				if ( ! $is_page_builder_used )
				wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
				?>
				</div> <!-- .entry-content -->
				<?php
				if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>
				</article> <!-- .et_pb_post -->
			<?php endwhile; ?>
<div class="container">
<div class="row">
<h1 class="track-title">Weight Tracker Graph</h1>



<!-- My code  start-->
<?php
$query = "select * from " . $wpdb->prefix . "payments where user_id=".$user_id; 
$row=$wpdb->get_row($query . " order by payment_id DESC");

$query_rr = "select current_weight as ww_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."' order by id DESC";
$row_weight=$wpdb->get_row($query_rr);

$sql_check_weight = "select count(0) as row_count_weight from " . $wpdb->prefix . "weight_tracker where user_id='".$user_id."'";
$row_weight_kk=$wpdb->get_row($sql_check_weight);


if($row_weight_kk->row_count_weight>0)
{
  $before_weight=$row_weight->ww_weight;
  $before_lbs_weight=$before_weight;
}else
{
   $before_weight=$row->weight;
   $before_lbs_weight=$before_weight;
}
$reg_user_weight=$row->weight;
$current_weight = $_POST['current_weight'];
$current_lbs_weight=$current_weight;
$weight_difference=$current_lbs_weight-$reg_user_weight;
$current_date = date("Y/m/d");


$sql_check ="SELECT * FROM  `nu_weight_tracker` WHERE  `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
$row=$wpdb->get_row($sql_check);

$sql_check ="SELECT user_id,current_weight FROM  `nu_weight_tracker` WHERE  `user_id` LIKE  '".$user_id."' ORDER BY id DESC";
$row11=$wpdb->get_row($sql_check);

if($row==0 && $current_weight!='')
{

$sql_check1 ="INSERT INTO `nu_weight_tracker`(`user_id`,`before_weight`,`current_weight`, `current_lbs_weight`, `weight_difference`,`current_date`,`date`) VALUES ('$user_id','$before_weight','$current_weight','$current_lbs_weight','$weight_difference','$current_date','$current_date')";
$row=$wpdb->get_row($sql_check1);
$aa=$current_weight; 

}else if($current_weight!='')
{
$sql_check2 ="UPDATE `nu_weight_tracker` set `current_weight`=('$current_weight'),`current_lbs_weight`=('$current_lbs_weight'),`weight_difference`=('$weight_difference') WHERE `current_date` LIKE  '".$current_date."' and `user_id` LIKE  '".$user_id."'";
$row=$wpdb->get_row($sql_check2);
}	
?>

<div class="button-sect"><a href="#" class="btn-dash" id="weight-btn" data-toggle="modal" data-target="#myModal">Weigh-in <span class="glyphicon glyphicon-chevron-right"></span></a>
</div>

<!-- Modal Weight-in start-->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<form method="post" id="form_weight_tracker">
<div class="form-group">
<label for="cal">Current Weight: (lbs)</label>
<div class="form-group">                       
<input type="text"  name="current_weight" id="numb"/>
<div class="error_text_weight"><p id="weight_error_text"></p></div>
</div>
</div>
<button type="button" class="comm-all-btn" onclick="beforeSubmit();" >Update Weight</button>

</form>
</div>
</div>
</div>
</div>
<!-- Modal Weight-in end-->

<!-- js for weight validation popup-->
<script type="text/javascript">
beforeSubmit = function()
{
  if (1 == 1)
  {
    var x, text;
    x = document.getElementById("numb").value;
    var uid = <?php echo $user_id;?>;
    if (isNaN(x) || x < 10 || x > 664) 
    {
      text = "Enter weight between 10 to 664 in lbs";
      document.getElementById("weight_error_text").innerHTML = text;
      return false;
    }
    else 
    {
      update_weight_on_formule(uid,x);
      $("#form_weight_tracker").submit();
    }
  }        
}

$("#numb").keypress(function(e) {
    if(e.which == 13) {
      e.preventDefault();
        if (1 == 1)
  {
    var x, text;
    x = document.getElementById("numb").value;
    var uid = <?php echo $user_id;?>;
    if (isNaN(x) || x < 10 || x > 664) 
    {
      text = "Enter weight between 10 to 664 in lbs";
      document.getElementById("weight_error_text").innerHTML = text;
      return false;
    }
    else 
    {
      update_weight_on_formule(uid,x);
      $("#form_weight_tracker").submit();
      
      
    }
  }
    }
});

 function update_weight_on_formule(uid,cwt)
 {
        $('#weight_error_text').hide();
        var dataObj = {formule_user_id:uid,formule_current_weight:cwt};
        $.ajax({
          type:"POST",
          data:dataObj,
          url:"../wp-content/themes/Divi/ajax_meal_plan.php",
          cache:false,
          dataType:'json',
          success:function(html)
          {
            //$("#form_weight_tracker").submit();
            //window.location = 'http://exceptionaire.co/nutrafitDivi/weight-graph/';
          }
        }
        );
      return false;
}
</script>
<!-- My code  end-->



</div>
<link href="<?php echo bloginfo('template_url')?>/graph/css/graph_xcharts.min.css" rel="stylesheet">
<link href="<?php echo bloginfo('template_url')?>/graph/css/graph_style.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/d3/2.10.0/d3.v2.js"></script>
<script src="<?php echo bloginfo('template_url')?>/graph/js/graph_xcharts.min.js"></script>
<script src="<?php echo bloginfo('template_url')?>/graph/js/graph_sugar.min.js"></script>
<script src="<?php echo bloginfo('template_url')?>/graph/js/graph_daterangepicker.js"></script>
<script src="<?php echo bloginfo('template_url')?>/graph/js/graph_script.js"></script>

<div id="placeholder" class="wight-grap-sect">Weight (in Pounds)<figure id="chart"></figure></div>

</div>
</div>
</div>
<?php 
} 
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
<?php get_footer(); ?>

