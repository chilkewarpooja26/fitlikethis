

<?php
  /*
  Template Name: Workout Template 
  */
  get_header(); 
  ?>
<?php 
  global $wpdb;
  $user_id = get_current_user_id();  
  $sql_check = "select count(0) as row_count from " . $wpdb->prefix . "payments where user_id='".$user_id."'";
  $row=$wpdb->get_row($sql_check);
  $count=$row->row_count;
  if ($user_id == 0 || $count==0) 
  {
  wp_redirect( home_url() ); exit;
  } 
  else 
  { 
  $query = "select * from " . $wpdb->prefix . "payments where user_id=".$user_id; 
  $row=$wpdb->get_row($query . " order by payment_id DESC");
  //foreach($result as $row)
  //{
  $post_id=$row->post_id;   
  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
  $custom_args = array(
  'post_type' => 'plan', 
  'order' =>'ASC',
  'paged' => $paged
  );
  $custom_query = new WP_Query( $custom_args );
  //if ( $custom_query->have_posts() ) : 
  //while ( $custom_query->have_posts() ) : $custom_query->the_post(); 
  ?>
<div id="main-content" class="inner-pages-section workout-temp">
    	<?php if ( ! $is_page_builder_used ) : ?>
	<!-- <div class="container">-->
		<div id="content-area hello" class="clearfix">
			<?php endif; ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! $is_page_builder_used ) : ?>
				<?php
				$thumb = '';
				$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
				$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
				$classtext = 'et_featured_image';
				$titletext = get_the_title();
				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
				$thumb = $thumbnail["thumb"];
				if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
				print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>
				<?php endif; ?>
				<div class="entry-content" id="custom_message">
				<h1 class="hide">&nbsp;</h1>
				<?php
				// ********Home page slider and pie chart *******//
				the_content();
				if ( ! $is_page_builder_used )
				wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
				?>
				</div> <!-- .entry-content -->
				<?php
				if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>
				</article> <!-- .et_pb_post -->
			<?php endwhile; ?>
<!--  <div class="sub-banner dash-sect workout-banner">
    
    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/background_inner.jpg">
    <div class="page-title-head">
      <h1 class="entry-title main_title"><?php// the_title(); ?></h1>
    </div>
  </div>-->
  <div class="container" id="cont-100">
    <div class="row">
      <!-- Planning calendar start -->
      <?php 
        $getplans_date= "select payment_date,plan_expire_date from ".$wpdb->prefix."payments where user_id='".$user_id."'";
        $getdate = $wpdb->get_row($getplans_date);
        
        //echo "plan start date - ".$getdate->payment_date;
        //echo "plan end date - ".$getdate->plan_expire_date;
        //echo "<br><br>";
        
        // get video list from table start
        
        $a = getworkoutvideo($getdate->payment_date,$getdate->plan_expire_date);
        //echo '<pre>'; print_r($a);
        $videolistarray = array();
        $finelvideolistarray = array();
        for ($i=0; $i < count($a); $i++) { 
            //echo $a[$i]['workout_date'];
            //echo $a[$i]['workout_count'];
            //echo $a[$i]['workout_name'].'<br>';
            $videolistarray[] = $a[$i]['workout_date'];
            $videolistarray[] = $a[$i]['workout_count'];
            $videolistarray[] = $a[$i]['workout_name'];
            $videolistarray[] = $a[$i]['youtube_video_id'];
            $finelvideolistarray[$a[$i]['workout_date']][] = $videolistarray;
            unset($videolistarray);
          }
        //echo 'Hihi - '.$finelvideolistarray['2017/03/07'][2]; 
          //echo '<pre>new'; print_r($finelvideolistarray); die();
        // get video list from table end
        
        
          $plan_start_date = $getdate->payment_date;
          $plan_end_date = date('Y-m-d', strtotime($getdate->plan_expire_date. ' + 1 days'));
          //$plan_end_date = '2017/02/28';
          //echo "<br><br>";
          
          $begin = new DateTime($plan_start_date);
          $end = new DateTime($plan_end_date);
          $interval = new DateInterval('P7D');
          $daterange = new DatePeriod($begin, $interval, $end);
        
          // $week = week_startday($getdate->payment_date,$getdate->plan_expire_date);
          // $firstday = $week['startday'];
          // $lastday = $week['endday'];
          $firstday = date("Y-m-d", strtotime('monday this week', strtotime($plan_start_date)));   
          $lastday = date("Y-m-d", strtotime('sunday this week', strtotime($plan_end_date)));
          $getweekarr = getweekarray($firstday,$lastday); 
        
         //echo '<pre>'; print_r($getweekarr);
        
         for ($i=0; $i < count($getweekarr); $i++) { 
            
            //print_r($getweekarr[$i]);
            $get_weekweek = $getweekarr[$i];
            // echo  $first_weekday = reset($getweekarr[$i]);
            //  echo $last_weekday = end($getweekarr[$i]);
            // echo "</br></br>";
          }
        
          // for ($i=0; $i < count($getweekarr); $i++) { 
            
          //   //print_r($getweekarr[$i]);
          //   $get_weekweek = $getweekarr[$i];
          //   for ($j=0; $j < count($get_weekweek); $j++) { 
          //     //echo $get_weekweek[$j]."<br>";
          //   }
          // }
        
        /** For tabbing content START **/
        
          $tab_begin = new DateTime($plan_start_date);
          $tab_end = new DateTime($plan_end_date);
          $tab_interval = new DateInterval('P1D');
          $tab_daterange = new DatePeriod($tab_begin, $tab_interval, $tab_end);
        
             
        
        /** For tabbing content END **/  
        
        
        
          // foreach($daterange as $dateone){
          //  //   echo  "<br>".$full_date = str_replace('-','',$dateone->format("Y-m-d"));
        
          //  // $dstring = date_parse_from_format("Y-m-d", $dateone->format("Y-m-d"));
          //  //  echo "<br>";
          //  // echo $dstring["day"]."/";
          //  //  echo $dstring["month"]."<br>";
          // }
        
        
        
        
        
         ?>
      <div class="top-content">
        <h1 class="plan-head">PLANNING</h1>
      </div>
      <h1 class="date-head"><?php echo date('F',strtotime($getdate->payment_date)).' - '.date('F',strtotime($getdate->plan_expire_date)).' '.date('Y',strtotime($getdate->plan_expire_date)); ?></h1>
      <div class="sliding-buttons">
        <button class="pp2"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button class="nn2"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
      </div>
      <section class="slider three">
        <?php 
          $activeclass = 0;
          $initial = "";
          for ($i=0; $i < count($getweekarr); $i++) { 
                // logic changed 13-04-2017
                //print_r($getweekarr[$i]);
                $get_weekweek = $getweekarr[$i];
                $first_weekday = reset($getweekarr[$i]);
                $last_weekday = end($getweekarr[$i]);
                
                $tab_next_endweekday = date_parse_from_format("Y-m-d", $last_weekday);
                $tab_next_endweekday = $tab_next_endweekday["day"]."/".$tab_next_endweekday["month"];
          
                $tab_next_startweekday = date_parse_from_format("Y-m-d", $first_weekday);
                $tab_next_startweekday = $tab_next_startweekday["day"]."/".$tab_next_startweekday["month"];

                $currentdate = date('Y/m/d');               
                $check_first_weekday = $first_weekday; 
                $check_last_weekday = $last_weekday;   
                $currentdate = DateTime::createFromFormat('Y/m/d', $currentdate);
                $check_first_weekday = DateTime::createFromFormat('Y/m/d', $check_first_weekday); 
                $check_last_weekday = DateTime::createFromFormat('Y/m/d', $check_last_weekday);
                  
               ?>
        <div class="<?php if($currentdate >= $check_first_weekday && $currentdate <= $check_last_weekday){echo 'active'; $initial = $i;} ?> demo">
          <a data-toggle="tab" href="#menu<?php echo str_replace('/','',$first_weekday); ?>"><?php echo $tab_next_startweekday.' - '.$tab_next_endweekday; ?></a>
        </div>
        <?php } ?>
      </section>
      <div id="month-slide" class="carousel slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
          <div class="item active">
            <div class="bottom-content">
              <!-- <ul class="nav nav-tabs"> -->
              <!-- <li class="active"><a data-toggle="tab" href="#menu9">27/2 - 05/03</a></li>
                <li><a data-toggle="tab" href="#menu10">06/03 - 12/03</a></li>
                <li><a data-toggle="tab" href="#menu11">13/2 - 19/03</a></li>
                <li><a data-toggle="tab" href="#menu12">20/2 - 06/03</a></li> -->
              <?php 
                // $activeclass = 0;
                // for ($i=0; $i < count($getweekarr); $i++) { 
                //       // logic changed 13-04-2017
                //       //print_r($getweekarr[$i]);
                //       $get_weekweek = $getweekarr[$i];
                //       $first_weekday = reset($getweekarr[$i]);
                //       $last_weekday = end($getweekarr[$i]);
                      
                //       $tab_next_endweekday = date_parse_from_format("Y-m-d", $last_weekday);
                //       $tab_next_endweekday = $tab_next_endweekday["day"]."/".$tab_next_endweekday["month"];
                
                //       $tab_next_startweekday = date_parse_from_format("Y-m-d", $first_weekday);
                //       $tab_next_startweekday = $tab_next_startweekday["day"]."/".$tab_next_startweekday["month"];
                        
                  ?>
              <!-- <li class="<?php if($activeclass == $i){echo 'active';} ?>"><a data-toggle="tab" href="#menu<?php echo str_replace('/','',$first_weekday); ?>"><?php echo $tab_next_startweekday.' - '.$tab_next_endweekday; ?></a></li> -->
              <?php 
                //} // inner for loop end
                //}// outer for loop end ?>
              <!-- </ul> -->
              <div class="tab-content">
                <?php 

                $tab_currentdate_check = new DateTime();
                $result = str_replace("/","",$tab_currentdate_check->format('Y/m/d'));

                //echo "<script>$(document).on('ready', function() { activaTab('menu20170911'); });</script>";

                  for ($i=0; $i < count($getweekarr); $i++) { 
                              $ac = 0;
                              $get_weekweek = $getweekarr[$i];
                              ?>
                <div id="menu<?php echo str_replace('/','',$getweekarr[$i][0]); ?>" class="tab-pane fade  <?php if(0 == $i){echo 'in active';} ?>">
                  <?php
                    for($j=0;$j<count($get_weekweek);$j++){
                    
                      if($get_weekweek[$j]<=$getdate->plan_expire_date){
                    
                    $tab_content_day = date_parse_from_format("Y-m-d", $get_weekweek[$j]);
                    $tab_content_day = date('F',strtotime($get_weekweek[$j]))." ".$tab_content_day["day"] ;
                    ?>
                  <div id="day<?php echo str_replace('/','',$get_weekweek[$j]); ?>" class="date-info <?php if (($get_weekweek[$j] >= $getdate->payment_date) && ($get_weekweek[$j] <= $getdate->plan_expire_date)) { }else{echo 'inactive';} ?>">
                    <div class="col-md-3 col-sm-3">
                      <div class="date">
                        <p><?php echo $tab_content_day;?></p>
                        <p><?php echo date('l',strtotime($get_weekweek[$j])); ?></p>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-9" id="viewvideolist">
                      <div class="video-names" id="nicescroll-hz">
                        <ul>
                          <div class="showrecord">
                            <?php 
                              if (array_key_exists($get_weekweek[$j], $finelvideolistarray)) {
                                      
                                   for ($ni=0; $ni < count($finelvideolistarray[$get_weekweek[$j]]); $ni++) { 
                                             
                                ?>   
                            <li id="video<?php echo str_replace('/','',$get_weekweek[$j]).$finelvideolistarray[$get_weekweek[$j]][$ni][3]; ?>" >
                              <?php echo $finelvideolistarray[$get_weekweek[$j]][$ni][2].'<span class="vcount">'.$finelvideolistarray[$get_weekweek[$j]][$ni][1].'</span>'; ?>
                            </li>
                            <?php
                              }
                              }
                              //else{echo '<li>No Videos</li>';}
                              ?>
                          </div>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <?php 
                    }
                      }?>
                </div>
                <?php } ?>  
              </div>
            </div>
          </div>
        </div>
        <!--          <a class="left carousel-control" href="#month-slide" data-slide="prev"><img src="http://exceptionaire.co/nutrafitDivi/wp-content/themes/Divi/images/montharrow-left.png" alt=""></a>
          <a class="right carousel-control" href="#month-slide" data-slide="next"><img src="http://exceptionaire.co/nutrafitDivi/wp-content/themes/Divi/images/montharrow-right.png" alt=""></a> -->
      </div>
      <!-- Planning calendar end -->
      <!-- <section class="regular slider">
        <?php 
          $activeclass = 0;
          for ($i=0; $i < count($getweekarr); $i++) { 
                // logic changed 13-04-2017
                //print_r($getweekarr[$i]);
                $get_weekweek = $getweekarr[$i];
                $first_weekday = reset($getweekarr[$i]);
                $last_weekday = end($getweekarr[$i]);
                
                $tab_next_endweekday = date_parse_from_format("Y-m-d", $last_weekday);
                $tab_next_endweekday = $tab_next_endweekday["day"]."/".$tab_next_endweekday["month"];
          
                $tab_next_startweekday = date_parse_from_format("Y-m-d", $first_weekday);
                $tab_next_startweekday = $tab_next_startweekday["day"]."/".$tab_next_startweekday["month"];
                  
               ?>
             
          <div class="<?php if($activeclass == $i){echo 'active';} ?>">
            <a data-toggle="tab" href="#menu<?php echo str_replace('/','',$first_weekday); ?>"><?php echo $tab_next_startweekday.' - '.$tab_next_endweekday; ?></a>
          </div>
         
          <?php } ?>
        </section> -->
      <div class="blog-section-main">
        <?php     
          $videoidarray = array();
          if( have_rows('workout_video',$post_id) ):
          while ( have_rows('workout_video',$post_id) ) : the_row();  
          $video_title = get_sub_field('video_title');
          ?>
        <div class="blog col-lg-6 col-md-6 col-sm-6">
          <h1> <?php echo $video_title;?></h1>
          <?php         
            if( get_sub_field('youtube_link') ):
            $youtube_video_link = get_sub_field("youtube_link");
            $ytarray=explode("/", $youtube_video_link);
            $ytendstring=end($ytarray);
            $ytendarray=explode("?v=", $ytendstring);
            $ytendstring=end($ytendarray);
            $ytendarray=explode("&", $ytendstring);
            $ytcode=$ytendarray[0];
            $videoidarray[] = $ytcode;
          //echo "<iframe id='player".$ytcode."' width='420' height='315' src='http://www.youtube.com/embed/".$ytcode."?enablejsapi=1&rel=1&modestbranding=1&autohide=1&showinfo=0&controls=1'  frameborder='0'></iframe>";
            ?>
          <!-- youtube code start -->
          <input type="text" id="vname<?php echo $ytcode;?>" value="<?php echo $video_title;?>" style="display: none;" />
          <div id="player<?php echo $ytcode;?>" showinfo='0'></div>
          
          <!-- youtube code end -->
          <?php 
            global $wpdb;
            $sql_workout_sum="SELECT sum(workout_count) as user_workout_count from " . $wpdb->prefix . "add_workout WHERE user_id = '".$user_id."' and workout_name = '".$video_title."' and youtube_video_id='".$ytcode."' and  workout_date BETWEEN '".$getdate->payment_date."' AND '".$getdate->plan_expire_date."'";
            $row_workout_sum=$wpdb->get_results($sql_workout_sum);
            if($row_workout_sum)
            {
                  foreach ($row_workout_sum as $workout_sum) 
                 {?>
          <div class="work_count" id="view_count<?php echo $ytcode;?>">
            <?php echo $workout_sum_count['user_workout_count'] = $workout_sum->user_workout_count;?>
          </div>
          <?php }
            }           
            ?>
          <?php 
            endif;
            ?>
        </div>
        <?php
          endwhile;//workout video while 
          endif;//workout video if 
          
          //endwhile; //posr query while 
          //endif; //posr query if 
          ?>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.nicescroll.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/slick/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/slick/slick-theme.css">
<?php
  //}//foreach of post_id 
  }//else of user is login
  ?>
<script type="text/javascript">

  var tag = document.createElement('script');
    tag.id = 'iframe-demo';
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var a = 1;
    var playerInfoList = <?php echo json_encode($videoidarray); ?>;

    function onYouTubeIframeAPIReady() {
        if(typeof playerInfoList === 'undefined')
           return; 

        for(var i = 0; i < playerInfoList.length;i++) {
          
          var curplayer = createPlayer(playerInfoList[i]);
        }   
      }

      function createPlayer(playerInfo) {
          return new YT.Player('player'+playerInfo, {
             height: 315,
             width: 420,
             videoId: playerInfo,
             playerVars: {                
                        showinfo: 0,
                        rel:0,
                        modestbranding:1
                      },
            events: {
             'onStateChange': onPlayerStateChange
           }                      
          });
      }

      function onPlayerStateChange(event) {

        //console.log(event.target);
        var target = event.target;
        //alert(target);
        var videoid = '';
        Object.keys(target).forEach(function(key) {
          var value = target[key];
          // use "key" and "value" here...
          var id = value.videoData;
         // console.log(id);
          for (var i in id) {
             //console.log(id[i],i);

             if(i == "video_id"){
                videoid = id[i];
                //alert(videoid);
             }
          }
          
        });

        if(event.data === 0) {

          
          var vname = $('#vname'+videoid).val();

          var wuid= '<?php echo get_current_user_id();?>';
          var wname= vname; 
          var wcount= '<?php echo '1';?>';
          var wvideoid= videoid;
          //alert('wuid '+wuid+'; '+'wname '+vname+'; '+'wcount '+wcount+'; '+'wvideoid '+videoid);

          var dataObj = {workout_userid:wuid, workout_name:wname, workout_count:wcount, workout_videoid:wvideoid};

          $.ajax({
                type:"POST",
                  data:dataObj,
                  url:"../wp-content/themes/Divi/ajax_meal_plan.php",
                  cache:false,
                  dataType:'json',
                  success:function(html)
                  {
                    //alert('Sucess');
                    var dayid = "#day"+html.current_date;
                    var videoid = "#video"+html.current_date+wvideoid;
                    //alert(dayid+'  '+videoid); 
                   if($(dayid+' '+videoid).length == 0) {
                        //it doesn't exist
                        //alert('it doesnt exist');
                        var aa = "<li id='video"+html.current_date+wvideoid+"'>"+wname+"<span class='vcount'>"+html.workout_count+"</span> </li>";
                        $(dayid).find('.showrecord').append(aa);
                      }
                      else{
            
                            //alert('it exist');
                            $(dayid+' '+videoid).find('.vcount').text(html.workout_count);
                          }
            
                    $('#view_count'+wvideoid). text(html.videosumcount);
                    $('#video'+html.current_date).text(html.workout_count);
                  }
                  
                }
                );

        }
        
      }

  $(document).on('ready', function() {
  
  
  $(".slider.three").slick({
  slidesToShow: 3,
  slidesToScroll: 4,
  infinite: true,
  initialSlide: '<?php echo $initial; ?>',
  mobileFirst: true,
  prevArrow: $(".pp2"),
  nextArrow: $(".nn2"),
  responsive: [{
      breakpoint: 500,
      settings: {
          slidesToShow: 4
      }
  }]
  });
  
  $('.demo').click(function(){
  $('.demo').removeClass('active');
  $(this).addClass('active');
  });

  $("#nicescroll-hz").niceScroll({touchbehavior:false,cursorcolor:"#0000FF",cursoropacitymax:0.6,cursorwidth:8});
  
  
  var a = $(".active.demo a").attr("href"); var s = a.replace(/^#+/, ""); 
  $(".tab-pane").removeClass("in");
  $(".tab-pane").removeClass("active");
  $("#"+s).addClass("in");
  $("#"+s).addClass("active");


    });
  
  
</script>
<?php get_footer(); ?>

