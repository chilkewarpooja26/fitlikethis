<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Nap_App
 */

get_header(); ?>

	<div id="main" class="clearfix padding">
		<div class="container">
			<div class="row">
				<div id="content" class="site-content col-md-9">
					<section class="error-404 not-found">
						<header class="page-header">
							<h1 class="page-title"><?php echo esc_html__( '404', 'napapp' ); ?></h1>
							<h3 class="page-title"><?php echo esc_html__( 'Oops! That page can&rsquo;t be found.', 'napapp' ); ?></h3>
						</header><!-- .page-header -->

						<div class="page-content">
							<p><?php echo esc_html__( 'It looks like you are lost somewhere. Maybe try going back to homepage?', 'napapp' ); ?></p>
						</div><!-- .page-content -->
					</section><!-- .error-404 -->
				</div>
				<div id="sidebar" class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div><!-- #main -->

<?php
get_footer();
