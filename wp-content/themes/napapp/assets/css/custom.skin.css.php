<?php
header("Content-type: text/css; charset: UTF-8");
global $napapp_theme_options;
?>
<?php if( !empty( $napapp_theme_options['na_custom_skin'] ) ) : ?>
a, .navbar-nav li.active a,
a:focus,
a:hover,
.btn-primary:hover,
.navbar-nav li a.btn-primary:hover,
.compatibility a:hover,
.price .btn-primary:hover,
.price.featured .btn-primary:hover,
.twitter-feed a,
#main-contact-form .btn-primary:hover,
.entry-meta i,
.pagination>li>a:focus,
.pagination>li>a:hover,
.pagination>li>span:focus,
.pagination>li>span:hover,
.comments .btn-reply:hover, .widget-content li a:hover {
    color: <?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

ul.sub-menu {
    border-top: 2px solid <?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

.btn-primary,
.owl-theme .owl-controls .owl-page span,
.widget_search button[type=submit], .widget-content li:before,
button, input[type="button"], input[type="reset"], input[type="submit"]{
	background-color:<?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

.form-control:focus,
.comment-wrapper .btn.btn-primary,
.entry-footer .edit-link {
	border:1px solid <?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

.btn-primary.btn-animated:hover,
.btn-primary.btn-animated:focus,
.price .btn-primary:hover,
.price.featured .btn-primary:hover,
#main-contact-form .btn-primary,
#main-contact-form .btn-primary:hover,
.price .btn-primary {
	border:3px solid <?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

.overlay-bg {
    background:<?php echo esc_html( $napapp_theme_options['na_skin_color_rgba']['rgba'] ); ?> !important;
}

.apps ul li a:hover {
	color:<?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}

.flat .overlay-bg {
	background:<?php echo esc_html( $napapp_theme_options['na_custom_skin'] ); ?>;
}
<?php endif; ?>
