<?php
header("Content-type: text/css; charset: UTF-8");
global $napapp_theme_options;

echo esc_html( $napapp_theme_options['na_css_editor'] );
?>
body{
    <?php if( !empty( $napapp_theme_options['na_body_typo']['font-family'] ) ) : ?>
	font-family: '<?php echo esc_html( $napapp_theme_options['na_body_typo']['font-family'] ); ?>';
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_body_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_body_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_body_typo']['font-weight'] ) ) : ?>
	font-weight: <?php echo esc_html( $napapp_theme_options['na_body_typo']['font-weight'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_body_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_body_typo']['color'] ); ?>;
    <?php endif; ?>
}
h1, h2, h3, h4, h5, h6 {
    <?php if( !empty( $napapp_theme_options['na_head_typo']['font-family'] ) ) : ?>
	font-family: '<?php echo esc_html( $napapp_theme_options['na_head_typo']['font-family'] ); ?>';
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_head_typo']['font-weight'] ) ) : ?>
	font-weight: <?php echo esc_html( $napapp_theme_options['na_head_typo']['font-weight'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_head_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_head_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_head_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_head_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h1 {
    <?php if( !empty( $napapp_theme_options['na_h1_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h1_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h1_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h1_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h1_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h1_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h2 {
    <?php if( !empty( $napapp_theme_options['na_h2_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h2_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h2_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h2_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h2_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h2_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h3 {
    <?php if( !empty( $napapp_theme_options['na_h3_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h3_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h3_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h3_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h3_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h3_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h4 {
    <?php if( !empty( $napapp_theme_options['na_h4_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h4_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h4_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h4_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h4_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h4_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h5 {
    <?php if( !empty( $napapp_theme_options['na_h5_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h5_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h5_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h5_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h5_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h5_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
h6 {
    <?php if( !empty( $napapp_theme_options['na_h6_typo']['font-size'] ) ) : ?>
	font-size: <?php echo esc_html( $napapp_theme_options['na_h6_typo']['font-size'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h6_typo']['color'] ) ) : ?>
	color: <?php echo esc_html( $napapp_theme_options['na_h6_typo']['color'] ); ?>;
    <?php endif; ?>
    <?php if( !empty( $napapp_theme_options['na_h6_typo']['text-transform'] ) ) : ?>
    text-transform: <?php echo esc_html( $napapp_theme_options['na_h6_typo']['text-transform'] ); ?>;
    <?php endif; ?>
}
.na-preloader {
    background: url("<?php echo esc_html( $napapp_theme_options['na_page_preloader']['url'] ); ?>") center no-repeat #fff;
}
