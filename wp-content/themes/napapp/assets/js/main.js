jQuery(function ($) {

    'use strict';

	/*==============================================================*/
    // Table of index
    /*==============================================================*/

    /*==============================================================
    # Menu add class
    # Magnific Popup
    # WoW Animation
    ==============================================================*/

	/*==============================================================*/
    // # Preloader
    /*==============================================================*/

    (function () {
        $(window).load(function(){
            $(".na-preloader").fadeOut("slow");
        });

    }());


	/*==============================================================*/
	//Mobile Toggle Control
	/*==============================================================*/

	 $(function(){
		 var navMain = $("#mainmenu");
		 navMain.on("click", "a", null, function () {
			 navMain.collapse('hide');
		 });
	 });


	/*==============================================================*/
    // Menu add class
    /*==============================================================*/
	(function () {
		function menuToggle(){
			var windowWidth = $(window).width();
			if(windowWidth > 767 ){
				$(window).on('scroll', function(){
					if( $(window).scrollTop()>650 ){
						$('.navbar').addClass('navbar-fixed-top');
					} else {
						$('.navbar').removeClass('navbar-fixed-top');
					};
					if( $(window)){
						$('#home-three .navbar').addClass('navbar-fixed-top');
					}
				});
			}else{

				$('.navbar').addClass('navbar-fixed-top');

			};
		}

		menuToggle();
	}());

    $('#mainmenu').onePageNav({
        currentClass: 'active',
    });


	/*==============================================================*/
    // Owl Carousel
    /*==============================================================*/

    $("#team-slider").owlCarousel({
        items: 5,
		dots: false,
        nav: true,
        margin: 25,
		navText: [
            "<span class='team-slider-left'><i class=' fa fa-angle-left '></i></span>",
		    "<span class='team-slider-right'><i class=' fa fa-angle-right'></i></span>"
        ],
        responsive: {
            0: {
                items: 1,
                slideBy:1,
                dots: true,
                navText: false
            },
            480: {
                items: 3,
                slideBy:1,
                dots: true,
                navText: false
            },
            768: {
                items: 4,
                slideBy:1,
                dots: true,
                navText: false
            },
            991: {
                items: 5,
                slideBy:1,
                dots: true,
                navText: false
            },
            1199: {
                items: 5,
                slideBy: 1,
            }
        }
	});

	$("#screenshot-slider").owlCarousel({
		items: 4,
        dots: true,
        responsive: {
            0: {
                items: 1,
                slideBy:1
            },
            480: {
                items: 2,
                slideBy:1
            },
            768: {
                items: 2,
                slideBy:1
            },
            991: {
                items: 4,
                slideBy:1
            },
        }
	});

	$(".twitter-slider").owlCarousel({
		items:1,
        responsive: {
            0: {
                items: 1,
                slideBy:1
            },
            480: {
                items: 1,
                slideBy:1
            },
            768: {
                items: 1,
                slideBy:1
            },
            991: {
                items: 1,
                slideBy:1
            },
        }
	});


	/*==============================================================*/
    // Magnific Popup
    /*==============================================================*/

	(function () {
		$('.image-link').magnificPopup({
			gallery: {
			  enabled: true
			},
			type: 'image'
		});
		$('.feature-image .image-link').magnificPopup({
			gallery: {
			  enabled: false
			},
			type: 'image'
		});
		$('.image-popup').magnificPopup({
			type: 'image'
		});
		$('.video-link').magnificPopup({type:'iframe'});
	}());


	/*==============================================================*/
    // AOS animation
    /*==============================================================*/
    (function () {
	    AOS.init({
	      disable: 'mobile'
	    });
	}());
});
