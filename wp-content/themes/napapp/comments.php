<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Nap_App
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comment-wrapper">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<h3 class="comments-title"><?php echo esc_html__( 'Comments', 'napapp' ); ?> </h3>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php echo esc_html__( 'Comment navigation', 'napapp' ); ?></h2>

			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'napapp' ) ); ?></div>

				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'napapp' ) ); ?></div>


			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-above -->
		<?php endif; // Check for comment navigation. ?>

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ul',
					'short_ping' => true,
					'callback'	 => 'napapp_comment'
				) );
			?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php echo esc_html__( 'Comment navigation', 'napapp' ); ?></h2>

			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'napapp' ) ); ?></div>

				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'napapp' ) ); ?></div>


			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php
		endif; // Check for comment navigation.

	endif; // Check for have_comments().


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php echo esc_html__( 'Comments are closed.', 'napapp' ); ?></p>

	<?php
	endif;
	?>
	<?php
	    $req = get_option( 'require_name_email' );
	    $aria_req = ( $req ? " aria-required='true'" : '' );

		$comments_args = array(
	        // change the title of send button
	        'label_submit'=> esc_html__( 'Send Comment', 'napapp' ),
			// Change the class of send button
			'class_submit'=> 'btn btn-primary pull-right',
	        // change the title of the reply section
	        'title_reply'=> esc_html__( 'Leave a Comment', 'napapp' ),
	        // remove "Text or HTML to be displayed after the set of comment fields"
	        'comment_notes_after' => '',
	        // redefine your own textarea (the comment body)
	        'comment_field' => ' <div class="form-group"><textarea class="form-control" rows="10" id="comment" name="comment" aria-required="true" placeholder="' . esc_html__( 'Your Comment', 'napapp' ) . '"></textarea></div>',

	        'fields' => apply_filters( 'comment_form_default_fields', array(

			    'author' =>
			      	'<div class="form-group">' .
			      	'<input class="form-control" id="author" name="author" type="text" placeholder="' . esc_html__( 'Name', 'napapp' ) . ( $req ? '*' : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
			      	'" size="30"' . $aria_req . ' /></div>',

			    'email' =>
					'<div class="form-group">' .
			      	'<input class="form-control" id="email" name="email" type="text" placeholder="' . esc_html__( 'Email', 'napapp' ) . ( $req ? '*' : '' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) .
			      	'" size="30"' . $aria_req . ' /></div>',

			    'url' =>
					'<div class="form-group">' .
			      	'<input class="form-control" id="url" name="url" type="text" placeholder="' . esc_html__( 'Website', 'napapp' ) . ( $req ? '*' : '' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) .
			      	'" size="30" /></div>'
	    	)
	  	),
	);
	comment_form( $comments_args );
	?>

</div><!-- #comments -->
