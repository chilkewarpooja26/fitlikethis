<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nap_App
 */
global $napapp_theme_options;
?>

	</div><!-- #content -->
<?php if ( !empty( $napapp_theme_options['na_copyright'] ) || $napapp_theme_options['na_enable_footer_nav'] == 1 ) : ?>
	<footer id="footer">
		<div class="overlay-bg"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
				<?php if ( !empty( $napapp_theme_options['na_copyright'] ) ) : ?>
					<p class="copyright"><?php echo wp_kses( $napapp_theme_options['na_copyright'],'','' );  ?></a></p>
				<?php endif; ?>
				</div>
				<div class="col-sm-5">
					<?php
					if( has_nav_menu( 'footer-nav' ) ) :
						if( $napapp_theme_options['na_enable_footer_nav'] == 1 ) :
							wp_nav_menu( array(
								'theme_location' 	=> 'footer-nav',
								'menu_id' 			=> 'footer-navigation',
								'menu_class'      	=> 'list-inline footer-menu text-right',
							) );
						endif;
					endif;
					?>
				</div>
			</div>
		</div>
	</footer><!--# footer -->
<?php endif; ?>
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
