<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nap_App
 */
global $napapp_theme_options;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
		<?php if ( !empty( $napapp_theme_options['na_favicon']['url'] ) ) : ?>
		<link rel="shortcut icon" href="<?php echo esc_url( $napapp_theme_options['na_favicon']['url'] ); ?>">
		<?php endif; ?>
		<?php if ( !empty( $napapp_theme_options['na_apple_fav_114']['url'] ) ) : ?>
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo esc_url( $napapp_theme_options['na_apple_fav_114']['url'] ); ?>">
		<?php endif; ?>
		<?php if ( !empty( $napapp_theme_options['na_apple_fav_72']['url'] ) ) : ?>
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo esc_url( $napapp_theme_options['na_apple_fav_72']['url'] ); ?>">
		<?php endif; ?>
		<?php if ( !empty( $napapp_theme_options['na_apple_fav_57']['url'] ) ) : ?>
		<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url( $napapp_theme_options['na_apple_fav_57']['url'] ); ?>">
		<?php endif; ?>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="na-preloader"></div>

	<?php
	if( is_page() ){
		global $wp_query;
	   	$template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
	   	$move_banner = get_post_meta( $wp_query->post->ID, '_na_mv_banner', true );
	   	if( $template_name == 'tpl-onepage.php' ) {
			if( isset( $move_banner ) && $move_banner != 'on' )
				do_action( 'napapp_banner' );
		}
	}
	?>

	<header id="navigation">
		<div class="navbar" role="banner">
			<div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only"><?php echo esc_html__( 'Toggle navigation', 'napapp' ); ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

					<?php if ( !empty( $napapp_theme_options['na_logo_header']['url'] ) ) : ?>
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img class="main-logo img-responsive" src="<?php echo esc_url( $napapp_theme_options['na_logo_header']['url'] ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                    </a>
					<?php else : ?>
						<div class="navbar-brand">
						<?php
						if ( is_front_page() && is_home() ) : ?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php else : ?>
							<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
						endif;

						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
						<?php
						endif; ?>
						</div>
					<?php endif; ?>

                </div>
				<?php if( !empty( $napapp_theme_options['na_head_btn_title'] ) ) : ?>
				<a href="<?php echo esc_url( $napapp_theme_options['na_head_btn_link'] ); ?>" class="btn btn-primary"><?php echo esc_html( $napapp_theme_options['na_head_btn_title'] ); ?></a>
				<?php endif; ?>
				<?php if ( has_nav_menu( 'primary-nav' ) ) : ?>
				<nav id="mainmenu" class="collapse navbar-collapse navbar-right">
					<?php
						wp_nav_menu( array(
							'theme_location' 	=> 'primary-nav',
							'menu_id' 			=> 'primary-navigation',
							'menu_class'      	=> 'nav navbar-nav',
							'walker'          	=> new wp_bootstrap_navwalker()
						) );
					?>
				</nav><!-- #site-navigation -->
				<?php endif; ?>
            </div>
        </div>
    </header><!--/#navigation-->

	<?php
	if( is_page() ){
		if( $template_name == 'tpl-onepage.php' ) {
			if( isset( $move_banner ) && $move_banner == 'on' )
				do_action( 'napapp_banner' );
		}
	}
	?>

	<div id="page" class="site">

		<div id="content" class="site-content">
