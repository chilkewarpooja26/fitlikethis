<?php
/**
 * The template for requried actions hooks.
 *
 * @package napapp
 * @since 1.0
 */

add_action( 'wp_enqueue_scripts', 'napapp_enqueue_scripts' );
add_action( 'widgets_init', 'napapp_widgets_init' );

/*
 * Register sidebar.
 */
 if ( ! function_exists( 'napapp_widgets_init' ) ) {
     function napapp_widgets_init() {
     	register_sidebar( array(
     		'name'          => esc_html__( 'Blog Sidebar', 'napapp' ),
     		'id'            => 'blog-sidebar',
     		'description'   => esc_html__( 'Add widgets here.', 'napapp' ),
     		'before_widget' => '<div id="%1$s" class="widget %2$s">',
     		'after_widget'  => '</div>',
     		'before_title'  => '<h3 class="widget_title">',
     		'after_title'   => '</h3>',
     	) );
     }
 }

/**
 * Enqueue scripts and styles.
 */
if ( ! function_exists( 'napapp_enqueue_scripts' ) ) {
	function napapp_enqueue_scripts() {

		// napapp options
		$napapp = wp_get_theme();
        global $napapp_theme_options;

		// general settings
		if ( ( is_admin() ) ) {
			return;
		}

		wp_enqueue_style( 'na-style', get_stylesheet_uri() );
		wp_enqueue_style( 'bootstrap', NA_URI . '/assets/css/bootstrap.min.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'fontawesome', NA_URI . '/assets/css/font-awesome.min.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'magnific-popup', NA_URI . '/assets/css/magnific-popup.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
        wp_enqueue_style( 'carousel', NA_URI . '/assets/css/owl.carousel.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'aos', NA_URI . '/assets/css/aos.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'na-underscore', NA_URI . '/assets/css/underscore.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'na-base', NA_URI . '/assets/css/main.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'na-responsive', NA_URI . '/assets/css/responsive.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );

        if ( isset( $napapp_theme_options['na_skin'] ) && $napapp_theme_options['na_skin'] == 'blue' )
            wp_enqueue_style( 'na-blue', NA_URI . '/assets/css/presets/blue.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
        elseif ( isset( $napapp_theme_options['na_skin'] ) && $napapp_theme_options['na_skin'] == 'blue-flat' )
            wp_enqueue_style( 'na-blue-flat', NA_URI . '/assets/css/presets/blue-flat.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
        elseif ( isset( $napapp_theme_options['na_skin'] ) && $napapp_theme_options['na_skin'] == 'red' )
            wp_enqueue_style( 'na-red', NA_URI . '/assets/css/presets/red.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
        elseif ( isset( $napapp_theme_options['na_skin'] ) && $napapp_theme_options['na_skin'] == 'dark' )
            wp_enqueue_style( 'na-dark', NA_URI . '/assets/css/presets/dark.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
        elseif ( isset( $napapp_theme_options['na_skin'] ) && $napapp_theme_options['na_skin'] == 'custom' )
            wp_enqueue_style( 'napapp-custom-skin-style', admin_url( 'admin-ajax.php' ) . '?action=napapp_custom_skin_css', '', true, 'all' );
        else
            wp_enqueue_style( 'na-default', NA_URI . '/assets/css/presets/default.css', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );

        // Napapp Default Google Fonts
		wp_enqueue_style( 'na-gfonts-ubuntu', 'https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );
		wp_enqueue_style( 'na-gfonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700', '', apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ) );

        // Custom CSS
        wp_enqueue_style( 'napapp-dynamic-style', admin_url( 'admin-ajax.php' ) . '?action=napapp_dynamic_css', '', true, 'all' );

        // add TinyMCE style
		add_editor_style();

		// including jQuery plugins
		wp_localize_script( 'jquery', 'get',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'siteurl' => get_template_directory_uri(),
			)
		);

        wp_enqueue_script( 'html5shiv', NA_URI . '/assets/js/html5shiv.js', array( '' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', NA_URI . '/assets/js/respond.min.js', array( '' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

        $jsFinder = '(function(html){html.className = html.className.replace(/\bno-js\b/,"js")})(document.documentElement);';
        wp_add_inline_script( 'jquery', $jsFinder );

        wp_enqueue_script( 'bootstrap', NA_URI . '/assets/js/bootstrap.min.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
        wp_enqueue_script( 'jquery-magnific-popup', NA_URI . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
        wp_enqueue_script( 'jquery-nav', NA_URI . '/assets/js/jquery.nav.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
		wp_enqueue_script( 'carousel', NA_URI . '/assets/js/owl.carousel.min.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
		wp_enqueue_script( 'aos', NA_URI . '/assets/js/aos.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
		wp_enqueue_script( 'na-main', NA_URI . '/assets/js/main.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
		wp_enqueue_script( 'na-navigation', NA_URI . '/assets/js/navigation.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );
		wp_enqueue_script( 'na-skip-link-focus-fix', NA_URI . '/assets/js/skip-link-focus-fix.js', array( 'jquery' ), apply_filters( 'na_version_filter', $napapp->get( 'Version' ) ), true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

        /* Add Custom JS */
		if ( !empty( $napapp_theme_options['na_js_editor_header'] ) ) {
			wp_add_inline_script( 'jquery-migrate', $napapp_theme_options['na_js_editor_header'] );
		}
        if ( !empty( $napapp_theme_options['na_js_editor_footer'] ) ) {
			wp_add_inline_script( 'na-main', $napapp_theme_options['na_js_editor_footer'] );
		}
	}
}

// Custom row styles for onepage site type+-.
if ( ! function_exists('napapp_dynamic_css' ) ) {
    function napapp_dynamic_css() {
        require_once get_template_directory() . '/assets/css/dynamic.css.php';
        wp_die();
    }
}
add_action( 'wp_ajax_nopriv_napapp_dynamic_css', 'napapp_dynamic_css' );
add_action( 'wp_ajax_napapp_dynamic_css', 'napapp_dynamic_css' );

// Custom skin style
if ( ! function_exists('napapp_custom_skin_css' ) ) {
    function napapp_custom_skin_css() {
        require_once get_template_directory() . '/assets/css/custom.skin.css.php';
        wp_die();
    }
}
add_action( 'wp_ajax_nopriv_napapp_custom_skin_css', 'napapp_custom_skin_css' );
add_action( 'wp_ajax_napapp_custom_skin_css', 'napapp_custom_skin_css' );

/**
 * Filter the page title.
 */
if ( ! function_exists( 'napapp_wp_title' ) ) {
	function napapp_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'napapp' ), max( $paged, $page ) );
		}

		return $title;
	}
}
add_filter( 'wp_title', 'napapp_wp_title', 10, 2 );

/**
 * Push down the comment text area below
 */
if ( !function_exists( 'napapp_move_comment_below' ) ) {
	function napapp_move_comment_below( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}
	add_filter( 'comment_form_fields', 'napapp_move_comment_below' );
}

if ( !function_exists( 'napapp_replace_reply_link_class' ) ) {
    add_filter('comment_reply_link', 'napapp_replace_reply_link_class');
    function napapp_replace_reply_link_class($class){
        $class = str_replace("class='comment-reply-link", "class='btn-reply pull-right", $class);
        return $class;
    }
}


if ( !function_exists( 'napapp_core_activated' ) ) {
    function napapp_core_activated() {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        $plugin='napapp-core/napapp-core.php';
        if( is_plugin_active( $plugin ) ){
            return true;
        } else {
            return false;
        }
    }
}

if ( !function_exists( 'napapp_onepage_banner' ) ){
    function napapp_onepage_banner(){
        $metaPrefix    = '_na_';
        $edBanner      = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'ed_banner', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'ed_banner', true ) : '';
        $sectionID     = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'section_id', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'section_id', true ) : '';
        $bannerBG      = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'banner_bg', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'banner_bg', true ) : '';
        $bannerCpt     = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'banner_callout', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'banner_callout', true ) : '';
        $bannerContent = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'banner_content', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'banner_content', true ) : '';
        $ctaBtn1Txt    = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_1_text', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_1_text', true ) : '';
        $ctaBtn1Url    = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_1_link', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_1_link', true ) : '';
        $ctaBtn2Txt    = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_2_text', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_2_text', true ) : '';
        $ctaBtn2Url    = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_2_link', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'cta_btn_2_link', true ) : '';
        $socialList    = ( !empty( get_post_meta( get_the_ID(), $metaPrefix.'social_icons_list', true ) ) ) ? get_post_meta( get_the_ID(), $metaPrefix.'social_icons_list', true ) : '';

        $output = '';

        if ( $edBanner != '' ) :
            $output .= '<div id="'. esc_attr( $sectionID ) .'" class="image-bg" style="background-image: url('. esc_url( $bannerBG ) .');">';
                $output .= '<div class="container">';
                    $output .= '<div class="home-content padding">';
                        $output .= '<img class="img-responsive" src="'. esc_url( $bannerCpt ) .'" />';
                        $output .= $bannerContent;
                        $output .= '<div class="button">';
                            $output .= '<a href="'. esc_html( $ctaBtn1Url ) .'" class="btn btn-primary btn-animated">'. esc_html( $ctaBtn1Txt ) .'</a>';
                            $output .= '<a href="'. esc_html( $ctaBtn2Url ) .'" class="btn btn-primary btn-animated">'. esc_html( $ctaBtn2Txt ) .'</a>';
                        $output .= '</div>';
                        $output .= '<div class="app-icons text-center">';
                            $output .= '<ul class="list-inline">';
                            foreach( $socialList as $list ) :
                                $output .= '<li><a href="'. esc_url( $list['_na_icon_url'] ) .'"><i class="fa '. esc_html( $list['_na_icon'] ) .'"></i></a></li>';
                            endforeach;
                            $output .= '</ul>';
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';

            printf ( '%s', $output );
        else :
            echo '';
        endif;
    }

    add_action( 'napapp_banner', 'napapp_onepage_banner' );
}

if ( !function_exists( 'napapp_register_required_plugins' ) ){

    add_action( 'tgmpa_register', 'napapp_register_required_plugins' );

    function napapp_register_required_plugins() {

        $plugins = array(

            array(
                'name'               => esc_html__('WPBakery Visual Composer','napapp'),
                'slug'               => 'js_composer',
                'source'             => esc_url( 'https://plugins.themeregion.com/global/js_composer.zip' ),
                'required'           => true,
            ),
            array(
                'name'               => esc_html__('Napapp Core','napapp'),
                'slug'               => 'napapp-core',
                'source'             => esc_url( 'https://plugins.themeregion.com/napapp/napapp-core.zip' ),
                'required'           => true,
            ),
            array(
                'name'      => esc_html__('Contact Form 7','napapp'),
                'slug'      => 'contact-form-7',
                'required'  => true,
            )
        );

        $config = array(
            'default_path' => '',                    // Default absolute path to pre-packaged plugins.
            'menu'         => 'tgmpa-install-plugins', // Menu slug.
            'has_notices'  => true,                    // Show admin notices or not.
            'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
            'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false,                   // Automatically activate plugins after installation or not.
            'message'      => '',                      // Message to output right before the plugins table.
            'strings'      => array(
                'page_title'                      => esc_html__( 'Install Required Plugins', 'napapp' ),
                'menu_title'                      => esc_html__( 'Install Plugins', 'napapp' ),
                'installing'                      => esc_html__( 'Installing Plugin: %s', 'napapp' ), // %s = plugin name.
                'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'napapp' ),
                'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'napapp' ), // %1$s = plugin name(s).
                'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'napapp' ), // %1$s = plugin name(s).
                'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'napapp' ), // %1$s = plugin name(s).
                'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'napapp' ), // %1$s = plugin name(s).
                'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'napapp' ), // %1$s = plugin name(s).
                'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'napapp' ), // %1$s = plugin name(s).
                'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'napapp' ), // %1$s = plugin name(s).
                'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'napapp' ), // %1$s = plugin name(s).
                'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'napapp' ),
                'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'napapp' ),
                'return'                          => esc_html__( 'Return to Required Plugins Installer', 'napapp' ),
                'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'napapp' ),
                'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'napapp' ), // %s = dashboard link.
                'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
            )
        );
        tgmpa( $plugins, $config );
    }
}
