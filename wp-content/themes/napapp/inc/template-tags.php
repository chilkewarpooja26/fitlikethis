<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Nap_App
 */
if ( !function_exists( 'napapp_entry_meta' ) ) :

function napapp_entry_meta() {

	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$comments_count = wp_count_comments(get_the_ID());
	$comments = sprintf( esc_html__('%s Comments', 'napapp'), $comments_count->approved );

	$napapp_meta = '';
	$napapp_meta .= '<div class="entry-meta">';
		$napapp_meta .= '<ul class="list-inline">';
			$napapp_meta .= '<li class="author"><i class="fa fa-user-o"></i><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></li>';
			$napapp_meta .= '<li class="publish-date"><i class="fa fa-calendar-check-o"></i><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a></li>';
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() && is_single() ) {
		$categories_list = get_the_category_list( esc_html__( ', ', 'napapp' ) );
		if ( $categories_list && napapp_categorized_blog() ) :
			$napapp_meta .= '<li class="categories"><i class="fa fa-file-archive-o"></i>' . $categories_list . '</li>';
		endif;
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'napapp' ) );
		if ( $tags_list ) :
			$napapp_meta .= '<li class="tag"><i class="fa fa-tags"></i>' . $tags_list . '</li>';
		endif;
	}
		if ( is_sticky() && !is_single() ){
			$napapp_meta .= '<li class="stickypost"><i class="fa fa-sticky-note-o"></i>' . __( 'Sticky', 'napapp' ) . '</li>';
		}
			$napapp_meta .= '<li class="comments"><a href="#comments"><i class="fa fa-comment-o"></i>' . $comments . '</a></li>';
		$napapp_meta .= '</ul>';
	$napapp_meta .= '</div>';

	if ( !is_single() ) {
		$napapp_meta .= '<div class="read-more">';
			$napapp_meta .= '<a href="'. get_the_permalink() .'" class="readmore">Read More <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>';
		$napapp_meta .= '</div>';
	}

	printf( '%s', $napapp_meta );
}

endif;

if ( ! function_exists( 'napapp_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function napapp_entry_footer() {

	if ( is_single() ) :
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit This Post %s', 'napapp' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	endif;
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function napapp_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'napapp_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'napapp_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so napapp_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so napapp_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in napapp_categorized_blog.
 */
function napapp_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'napapp_categories' );
}
add_action( 'edit_category', 'napapp_category_transient_flusher' );
add_action( 'save_post',     'napapp_category_transient_flusher' );
