<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Nap_App
 */

get_header(); ?>

	<div id="main" class="clearfix padding">
		<div class="container">
			<div class="row">
				<div id="content" class="site-content col-md-9">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'template-parts/content', get_post_format() ); ?>

						<div class="prev-next">
							<div class="pull-left">
								<?php previous_post_link( '%link', '<i class="fa fa-long-arrow-left"></i> ' . esc_html__( 'Previous Post', 'napapp' ) ); ?>
							</div>
							<div class="pull-right">
								<?php next_post_link( '%link', esc_html__( 'Next Post', 'napapp' ) . ' <i class="fa fa-long-arrow-right"></i>' ); ?>
							</div>
						</div>

						<?php if ( comments_open() || get_comments_number() ) : ?>
							<?php comments_template(); ?>
						<?php endif; ?>

					<?php endwhile; ?>
				</div>
				<div id="sidebar" class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div><!-- #main -->

<?php
get_footer();
