<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Nap_App
 */

?>
<div id="post-<?php the_ID(); ?>" <?php post_class('post post-details'); ?>>
	<div class="entry-header">
		<div class="entry-thumbnail">
			<?php if( has_post_thumbnail() ) : ?>
        	<?php $url_thumbpost = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
            	<img width="<?php echo $url_thumbpost[1]; ?>" height="<?php echo $url_thumbpost[2]; ?>" class="img-responsive" src="<?php echo esc_url($url_thumbpost[0]); ?>" alt="<?php the_title(); ?>">
        	<?php endif; ?>
		</div>
	</div><!-- .entry-header -->

	<div class="entry-content">

		<div class="post-content">
			<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			?>
			<div class="entry-summary">
				<?php
				if ( is_single() ) :
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'napapp' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'napapp' ),
						'after'  => '</div>',
					) );
				else :
					the_excerpt();
				endif;
				?>
			</div>
		</div>
		<div class="post-meta">
			<?php napapp_entry_meta(); ?>
		</div>
		<div class="entry-footer">
			<?php napapp_entry_footer(); ?>
		</div><!-- .entry-footer -->
	</div><!-- .entry-content -->
</div><!-- #post-## -->
