# WordPress MySQL database migration
#
# Generated: Saturday 29. July 2017 11:37 UTC
# Hostname: localhost
# Database: `etpl2012_nutrafitdivi`
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `nu_AnythingPopup`
#

DROP TABLE IF EXISTS `nu_AnythingPopup`;


#
# Table structure of table `nu_AnythingPopup`
#

CREATE TABLE `nu_AnythingPopup` (
  `pop_id` int(11) NOT NULL AUTO_INCREMENT,
  `pop_width` int(11) NOT NULL DEFAULT '380',
  `pop_height` int(11) NOT NULL DEFAULT '260',
  `pop_headercolor` varchar(10) NOT NULL DEFAULT '#4D4D4D',
  `pop_bordercolor` varchar(10) NOT NULL DEFAULT '#4D4D4D',
  `pop_header_fontcolor` varchar(10) NOT NULL DEFAULT '#FFFFFF',
  `pop_title` varchar(1024) NOT NULL DEFAULT 'Anything Popup',
  `pop_content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pop_caption` varchar(2024) NOT NULL DEFAULT 'Click to open popup',
  PRIMARY KEY (`pop_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


#
# Data contents of table `nu_AnythingPopup`
#
INSERT INTO `nu_AnythingPopup` ( `pop_id`, `pop_width`, `pop_height`, `pop_headercolor`, `pop_bordercolor`, `pop_header_fontcolor`, `pop_title`, `pop_content`, `pop_caption`) VALUES
(1, 380, 260, '#4D4D4D', '#4D4D4D', '#FFFFFF', 'Registration', '[ajax_register]', 'Registration'),
(2, 400, 400, '#4D4D4D', '#4D4D4D', '#FFFFFF', 'Login', '[ajax_login]', 'Login'),
(3, 300, 250, '#4D4D4D', '#4D4D4D', '#FFFFFF', 'Choose User Type', 'HelloHelloHelloHelloHelloHelloHelloHelloHelloHello', 'popup') ;

#
# End of data contents of table `nu_AnythingPopup`
# --------------------------------------------------------

